<h1>Bonus Referral</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Bonus Details';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType'=> 'nested',
    'mergeColumns' => array(),
    'extraRowColumns' => array('referral_nama'),
    'extraRowPos' => 'below',
    'extraRowTotals' => function ($data, $row, &$totals) {
        if (!isset($totals['sum'])) {
            $totals['sum'] = 0;
        }
        $totals['sum'] += $data['bonus'];
    },
    'extraRowExpression' => '"<span class=\"subtotal\">Total ".$data["referral_nama"]." Nominal : ".format_number_report($totals["sum"])."</span>"',
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tdate'
        ),
        array(
            'header' => 'Nama Referral',
            'name' => 'referral_nama'
        ),
        array(
            'header' => 'Nama Customer',
            'name' => 'nama_customer'
        ),
        array(
            'header' => 'No. Receipt',
            'name' => 'doc_ref',
			'footer' => "Total All Bonus"
        ),
        array(
            'header' => 'Bonus',
            'name' => 'bonus',
            'value' => function ($data) {
                return format_number_report($data['bonus']);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
			'footer' => format_number_report($total_bonus)
        )
    ),
));
?>