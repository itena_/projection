<html>
<head>


	<script type="text/javascript"
	        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript"
	        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.min.js"></script>
	<script type='text/javascript' src="<?php echo Yii::app()->request->baseUrl; ?>/js/natasha/sticky/jquery.stickytableheaders.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/natasha/tabel.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/natasha/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.min.css" />

</head>
<body>

<h1>History Transaksi</h1>
<h2>Nama : <?=$nama?></h2>
<h2>Periode <?=$from?> sampai <?=$to?></h2>

<div class="application">
<table class="full">
	<thead>
		<tr>
			<th colspan="4">KMR</th>
		</tr>
		<tr>
			<th>Nama</th>
			<th>No. Receipt</th>
			<th>Net Amount</th>
			<th>Final Amount</th>
		</tr>
	</thead>

	<tbody>
	<?
        $tot_net = 0;
        $tot_bon = 0;
        foreach ($detail as $row)
        {
            echo '<tr>';
            echo '<td>'.$row['nama_customer'].'</td>';
            echo '<td>'.$row['doc_ref'].'</td>';
            echo '<td>'.number_format($row['net'], 2, ',', '.').'</td>';
            echo '<td>'.number_format($row['amount_bonus'], 2, ',', '.').'</td>';
            echo '</tr>';
            $tot_bon += $row['amount_bonus'];
	        $tot_net += $row['net'];
        }
	?>
        <tr>
            <td colspan="2">TOTAL</td>
            <td><?=number_format($tot_net, 2, ',', '.')?></td>
            <td><?=number_format($tot_bon, 2, ',', '.')?></td>
        </tr>
	</tbody>
</table>

<br />
<br />
<br />


<table  class="full">
	<thead>
	<tr>
		<th colspan="5">KMT</th>
	</tr>
	<tr>
		<th>Nama</th>
		<th>No. Receipt</th>
		<th>Tindakan</th>
		<th>Net Amount</th>
		<th>Final Amount</th>
	</tr>
	</thead>

	<tbody>
	<?
	$tot_net = 0;
	$tot_bon = 0;
	foreach ($detail_kmt as $row)
	{
		echo '<tr>';
		echo '<td>'.$row['nama_customer'].'</td>';
		echo '<td>'.$row['doc_ref'].'</td>';
		echo '<td>'.$row['kode_barang'].'</td>';
		echo '<td>'.number_format($row['net'], 2, ',', '.').'</td>';
		echo '<td>'.number_format($row['amount_bonus'], 2, ',', '.').'</td>';
		echo '</tr>';
		$tot_bon += $row['amount_bonus'];
		$tot += $row['net'];
	}
	?>
    <tr>
        <td colspan="3">TOTAL</td>
        <td><?=number_format($tot, 2, ',', '.')?></td>
        <td><?=number_format($tot_bon, 2, ',', '.')?></td>
    </tr>
	</tbody>
</table>

</div>
</body>