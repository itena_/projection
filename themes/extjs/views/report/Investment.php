<h1>Investment Plans</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>ACCOUNT CODE : <?= $accountcode ?></h3>
<h3>ACCOUNT NAME : <?= $accountname ?></h3><?
$this->pageTitle = 'Investment Plans';

$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'summaryText' => '',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array('category_name','group_name','account_name'),
    'extraRowPos' => 'above',
    //'extraRowColumns' => array('firstLetter'),
    //'extraRowExpression' => '"<b style=\"font-size: 3em; color: green\">".substr($data[\'account_name\'], 0, 1)."</b>"',

    'extraRowColumns' => array('category_name','group_name','alltotal'),
    //'extraRowExpression' => 'category_name',
    'extraRowExpression' => '"<p><span style=\'color: black;\' class=\"subtotal\">".$data["group_name"]."</span><span style=\'float:right;color: black;\' class=\"subtotal\">  ".format_number_report($data["alltotal"],0)."</span></p>"',
    //'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    /*'extraRowTotals' => function($data, $row, &$totals) {
         if(!isset($totals['amount'])) $totals['amount'] = 0;
         $totals['amount'] += $data['amount'];
    },*/

    'columns' => array(
        array(
            'header' => 'Account',
            'name' => 'account_name'
        ),
        array(
            'header' => 'Description',
            'name' => 'name'
        ),
        array(
            'header' => 'Q',
            'name' => 'qty',
            'htmlOptions' => array('style' => 'text-align: center;'),
        ),
        array(
            'header' => 'Price / stn',
            'name' => 'amount',
            'value' => function ($data) {
                return format_number_report($data['amount'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Total'
        ),
        array(
            'header' => 'Sub Total',
            'name' => 'total',
            'value' => function ($data) {
                return format_number_report($data['total'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 0)
        ),
        array(
            'header' => 'Total',
            'value' => '',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 0),
        ),
    )
));

?>