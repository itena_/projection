<h1>Tenders</h1>
<h3>Date : <?= $start; ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Tenders';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Payment Method',
            'name' => 'nama_bank',
//            'footerHtmlOptions' => array ('style' => 'text-align: center;font-weight:bold;' ),
//            'footer' => $tender
        ),
        array(
            'header' => 'System',
            'name' => 'system',
            'value' => function ($data) {
                return format_number_report($data['system'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Tendered',
            'name' => 'tenders',
            'value' => function ($data) {
                return format_number_report($data['tenders'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
//            'footerHtmlOptions' => array ('style' => 'text-align: right;font-weight:bold;' ),
//            'footer' => number_format($selisih,2)
        ),
        array(
            'header' => 'Status',
            'name' => 'Status',
//            'footerHtmlOptions' => array ('style' => 'text-align: center;font-weight:bold;' ),
//            'footer' => $tender
        ),
        array(
            'header' => 'Diff',
            'name' => 'diff',
            'value' => function ($data) {
                return format_number_report($data['diff'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
//            'footerHtmlOptions' => array ('style' => 'text-align: right;font-weight:bold;' ),
//            'footer' => number_format($selisih,2)
        ),
    ),
));
?>