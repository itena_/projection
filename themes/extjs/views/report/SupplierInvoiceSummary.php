<h1>Invoice Supplier Summary</h1>
<h3>FROM : <?=$start?></h3>
<h3>TO : <?=$to?></h3>
<h3>Supplier : <?= $supplier ?></h3>
<?php
$this->pageTitle='Invoice Supplier Summary';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Tipe',
            'name' => 'type_'
        ),
        array(
            'header' => 'Tanggal',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Doc. Ref.',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Supplier',
            'name' => 'supplier_name'
        ),
        array(
            'header' => 'Jatuh Tempo',
            'name' => 'tgl_jatuh_tempo',
            'footer' => "Total All"
        ),
        array(
            'header' => 'Total',
            'name' => 'total',
            'value' => function ($data) {
                    return format_number_report($data['total'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' ),
            'footerHtmlOptions' => array ('style' => 'text-align: right;' ),
            'footer' => format_number_report($total,2)
        ),
        array(
            'header' => 'Status',
            'name' => 'status',
            'value' => function ($data) {
                switch ($data['status']) {
                    case 0: return 'OPEN'; break;
                    case 1: return 'LUNAS'; break;
                    default: return '-'; break;
                }
            },
            'htmlOptions' => array ('style' => 'text-align: center;' )
        )
    ),
));
?>