<h1>Investment Plan PerMonth</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>

<?
$this->pageTitle = 'Investment Plan PerMonth';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'summaryText' => '',
    'mergeColumns' => is_report_excel() ? array() : array('group_name','account_name'),
    'extraRowPos' => 'below',
    'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
        array(
            'header' => 'Group Name',
            'name' => 'group_name',
        ),
        array(
            'header' => 'Account Name',
            'name' => 'account_name',
        ),
        array(
            'header' => 'January',
            'name' => 'january',
            'value' => function ($data) {
                return format_number_report($data['january'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'February',
            'name' => 'february',
            'value' => function ($data) {
                return format_number_report($data['february'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'March',
            'name' => 'march',
            'value' => function ($data) {
                return format_number_report($data['march'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'April',
            'name' => 'april',
            'value' => function ($data) {
                return format_number_report($data['april'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'May',
            'name' => 'may',
            'value' => function ($data) {
                return format_number_report($data['may'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'June',
            'name' => 'june',
            'value' => function ($data) {
                return format_number_report($data['june'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'July',
            'name' => 'july',
            'value' => function ($data) {
                return format_number_report($data['july'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'August',
            'name' => 'august',
            'value' => function ($data) {
                return format_number_report($data['august'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'September',
            'name' => 'september',
            'value' => function ($data) {
                return format_number_report($data['september'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Oktober',
            'name' => 'oktober',
            'value' => function ($data) {
                return format_number_report($data['oktober'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'November',
            'name' => 'november',
            'value' => function ($data) {
                return format_number_report($data['november'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'December',
            'name' => 'december',
            'value' => function ($data) {
                return format_number_report($data['december'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Total'
        ),
        array(
            'header' => 'Total',
            'name' => 'total',
            'value' => function ($data) {
                return format_number_report($data['total'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 0),
        )
    )
));


