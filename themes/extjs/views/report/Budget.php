<h1>Budget Plan</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>ACCOUNT CODE : <?= $accountcode ?></h3>
<h3>ACCOUNT NAME : <?= $accountname ?></h3><?
$this->pageTitle = 'Budget Plan';
$this->widget('ext.groupgridview.GroupGridView', array(
    'summaryText' => '',
    'id' => 'the-table',
    'dataProvider' => $dp,
    'extraRowPos' => is_report_excel() ? 'below' : 'above',
    //'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
        array(
            'header' => 'Account Code',
            'name' => 'account_code'
        ),
        array(
            'header' => 'Account Name',
            'name' => 'account_name'
        ),
        array(
            'header' => 'Description',
            'name' => 'description'
        ),
        array(
            'header' => 'Date',
            'name' => 'tdate',
            'footer' => 'Total'
        ),
        array(
            'header' => 'Amount',
            'name' => 'amount',
            'value' => function ($data) {
                return format_number_report($data['amount'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 0)
        ),
    )
));
?>