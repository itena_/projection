<h1>Bonus Summary</h1>
<h3>FROM : <?=$start?></h3>
<h3>TO : <?=$to?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle='Bonus Summary';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array('nama_employee'),
    'columns' => array(
        array(
            'header' => 'Employee Name',
            'name' => 'nama_employee',
            'footer' => "Total All Bonus"
        ),
        array(
            'header' => 'Total Bonus',
            'name' => 'amount_bonus',
            'value' => function ($data) {
                    return format_number_report($data['amount_bonus'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' ),
            'footerHtmlOptions' => array ('style' => 'text-align: right;' ),
            'footer' => format_number_report($total_bonus,2)
        )
    ),
));
?>