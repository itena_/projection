<h1>Beauty Details</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Beauty Details';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType'=> 'nested',
//    'mergeColumns' => array('doc_ref', 'nama_beauty', 'nama_customer'),
    'mergeColumns' => array('nama_beauty'),
    'extraRowColumns' => array('nama_beauty'),
    'extraRowPos' => 'below',
    'extraRowTotals' => function ($data, $row, &$totals) {
        if (!isset($totals['sum'])) {
            $totals['sum'] = 0;
        }
        $totals['sum'] += $data['beauty_tip'];
        if (!isset($totals['qty'])) $totals['qty'] = 0;
        $totals['qty'] += $data['qty'];
    },
    'extraRowExpression' => '"<span class=\"subtotal\">Total ".$data["nama_beauty"]." Qty : ".$totals["qty"]." Nominal : ".format_number_report($totals["sum"])."</span>"',
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Beautician Name',
            'name' => 'nama_beauty'
        ),
        array(
            'header' => 'Customer',
            'name' => 'nama_customer'
        ),
        array(
            'header' => "No. ". $pt_negara ." Receipt",
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Item Code',
            'name' => 'kode_barang',
            'footer' => "Total All Services"
        ),
        array(
            'header' => 'Quantity',
            'name' => 'qty',
            'value' => function ($data) {
                return format_number_report($data['qty']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Total Services Tips',
            'name' => 'beauty_tip',
            'value' => function ($data) {
                return format_number_report($data['beauty_tip']);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total_beauty_tip)
        )
    ),
));
?>