<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/rsvp-3.1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/sha-256.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-main.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-tray.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/0.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/locstor.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsul_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsul_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsul_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/aishaAntrian_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/antrian_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/antrian_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/antrian_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/barang_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/diagnosa_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/diagnosa_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/customers_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/customers_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/customers_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/negara_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/kota_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/provinsi_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/kecamatan_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/store_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/info_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/statusCust_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestransDetails_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestransDetails_store.js"></script>   
<script>
    var LOGOUT = false;
    STORE = '<?=STOREID;?>';
    NO_PREVIEW = '<img src="<?=bu(); ?>/images/no-preview.jpg" alt=""/>';
    DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
    BASE_URL = '<?=bu() === "" ? "/" : bu();?>';
    SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
    SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
    SYSTEM_LOGO = '<img src="<?=bu() . app()->params['url_logo']; ?>" alt=""/>';
    SALES_OVERRIDE = false;
    PT_NEGARA = '<?=PT_NEGARA;?>';
    NEGARA = '<?=NEGARA;?>';
    ROUNDING = <?if (defined('ROUNDING')) {
        echo ROUNDING;
    } else {
        echo 50;
    }?>;
    function nwis_round_up(e) {
        return round(Math.round(round(e / ROUNDING, 2)) * ROUNDING, 2);
    }
    function goodbye(e) {
        if (!LOGOUT) {
            if (!e) e = window.event;
            e.cancelBubble = true;
            e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }
        }
    }
    window.onbeforeunload = goodbye;
    jun.ajaxCounter = 0;
    jun.is_nwjs = function is_enable_tools() {
        try {
            var gui = require('nw.gui');
            if (gui != null) {
                var win = gui.Window.get();
                win.maximize();
                // win.showDevTools();
                return true;
            }
        } catch (err) {
            console.log(err.message);
            return false;
        }
    };
    jun.Counter = '1';
    jun.runner = new Ext.util.TaskRunner();
    jun.myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Processing... please wait"});
    Ext.onReady(function () {
        Ext.EventManager.addListener(document, "keypress", function (e) {
            if (jun.ajaxCounter > 0) {
                return false;
            }
        });
        Ext.Ajax.timeout = 1800000;
        Ext.QuickTips.init();
        Ext.Ajax.on("requestexception", function (conn, response, opts) {
            switch (response.status) {
                case 403:
                    window.location.href = 'site/logout';
                    break;
                case 500:
                    Ext.Msg.alert('Internal Server Error', response.responseText);
                    break;
                default :
                    Ext.Msg.alert(response.status + " " + response.statusText, response.responseText);
                    break;
            }
        });
        if (jun.is_nwjs()) {
            var fs = require('fs');
            var obj = JSON.parse(fs.readFileSync('config.json', 'utf8'));
            jun.Counter = obj.COUNTER;
        }
        jun.reloadAllStoreDokter = function () {
            Ext.Ajax.request({
                url: 'SysPrefs/get',
                method: 'POST',
                params: {
                    val: 'tdate_antrian'
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    var msg = window.btoa(response.msg);
                    var tdate_antrian = Locstor.get('tdate_antrian');
                    if (tdate_antrian != null) {
                        if (tdate_antrian != msg) {
                            jun.rztAntrianDokter.reload();
                            jun.rztAntrianDokterPending.reload();
                        }
                    }
                    Locstor.set('tdate_antrian', msg);
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        };
        jun.runner.start({
            run: jun.reloadAllStoreDokter,
            interval: 5 * 1000
        });
        var viewport = new Ext.Viewport({
            layout: 'border',
            items: [
                {
                    region: 'center',
//                    split: true,
                    id: 'viewport-dokter',
                    width: '60%',
                    weight: -2,
                    collapsible: false,
//                    margins: '5 0 5 5',
//                    cmargins: '35 5 5 5',
                    layout: 'accordion',
                    layoutConfig: {
                        animate: true
                    },
//                    defaults: {
//                        // margins: '0 5 0 0',
//                        frame: false
//                    },
                    items: [
                        new jun.AntrianDokterGridWin(),
                        {
                            id: 'form-dokter-gridHistory',
                            title: 'History Terapi',
                            autoScroll: true,
                            border: true
                        },
                        new jun.CustomerDokterGridWin(),
                        {
                            id: 'form-dokter-gridHistoryForm',
                            title: 'History Pasien',
                            autoScroll: true,
                            border: true
                        },
                        {
                            id: 'form-dokter-gridHistoryTransaksiForm',
                            title: 'History Transaksi',
                            autoScroll: true,
                            border: true,

                                tbar : {
                                    xtype: 'toolbar',
                                    items: [
                                        {
                                            xtype: 'label',
                                            style: 'margin:5px',
                                            text: 'Tanggal Awal :'
                                        },
                                        {
                                            xtype: 'xdatefield',
                                            id: 'tglawal',
                                            width:120,
                                            format: 'Y-m-d',
                                            ref: '../tglawal'
                                        },
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        {
                                            xtype: 'label',
                                            style: 'margin:5px;padding-left:105px;',
                                            text: 'Tanggal Akhir :'
                                        },
                                        {
                                            xtype: 'xdatefield',
                                            id: 'tglakhir',
                                            ref: '../tglakhir',
                                            format: 'Y-m-d',
                                            width:120
                                        },
                                        {
                                            xtype: 'tbseparator'
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Show',
                                            style: 'margin:5px;padding-left:105px;',
                                            ref: '../btnShow',
                                            handler: function (t, e) {
                                                console.log('aaa');
                                                var form = document.createElement("form");
                                                var awal = document.createElement("input");
                                                var akhir = document.createElement("input");
                                                var format = document.createElement("input");

                                                form.method = "POST";
                                                form.action = "report/bonusdokter";
                                                form.target = "_blank";

                                                awal.value=this.refOwner.tglawal.getRawValue();
                                                awal.name="tglfrom";
                                                form.appendChild(awal);

                                                akhir.value=this.refOwner.tglakhir.getRawValue();
                                                akhir.name="tglto";
                                                form.appendChild(akhir);

                                                format.value='html';
                                                format.name="format";
                                                form.appendChild(format);

                                                document.body.appendChild(form);

                                                form.submit();
                                            }
                                        }
                                    ]
                                }
                        }
                    ]
                },
                {
                    region: 'west',
//                    margins: '5 5 5 0',
                    weight: -1,
                    width: 550,
                    layout: {
                        type: 'vbox',
                        padding: '2',
                        align: 'stretch'
                    },
                    defaults: {
                        margins: '0 0 2 0'
//                        padding: '5'
                    },
                    items: [
                        {
                            title: 'Informasi Pasien',
                            ref: '../../panel_info_pasien',
                            id: 'panel_info_pasien_id',
                            height: 100
                        },
                        new jun.DiagnosaGrid({
                            frameHeader: !1,
                            header: !1,
                            height: 260
                        }),
                        new jun.SalestransQtyDetailsGrid({
                            disabled: true,
                            title: "Terapi",
                            ref: '../../gridDetails',
                            frameHeader: !1,
                            header: !1,
                            flex: 1,
                            margins: '0'
                        })
                    ],
                    fbar: {
                        xtype: 'toolbar',
                        buttonAlign: 'left',
                        items: [
//                            {
//                                xtype: 'label',
//                                style: 'margin:5px',
//                                text: 'Total :'
//                            },
                            {
                                xtype: 'label',
                                id: 'dokterbrutoid',
                                readOnly: true,
                                style: {
                                    font: '35px sans-serif'
                                },
                                text: '0'
                            },
                            '->',
                            {
                                xtype: 'button',
                                scale: 'large',
                                width: 100,
                                text: 'Logout',
                                ref: '../btnlogout',
                                handler: function () {
                                    //win.hide();
                                }
                            },
                            {
                                xtype: 'button',
                                scale: 'large',
                                width: 100,
                                text: 'Save',
                                id: 'btnSaveCloseDokter',
                                ref: '../btnSaveClose',
                                handler: function (t, e) {
                                    t.setDisabled(true);
                                    var no_antrian = null;
                                    var medis = jun.Counter;
                                    var no_customer = null;
                                    var id_antrian = null;
                                    var customer_id = null;
                                    var nama_customer = null;
                                    var storedNames = Locstor.get('AntrianDokter');
                                    if (storedNames != null) {
                                        no_antrian = storedNames.nomor;
                                        no_customer = storedNames.no_base;
                                        customer_id = storedNames.customer_id;
                                        nama_customer = storedNames.nama_customer;
                                        id_antrian = storedNames.id_antrian;
                                        Ext.Ajax.request({
                                            url: 'Konsul/Create/',
                                            method: 'POST',
                                            scope: this,
                                            params: {
                                                no_antrian: no_antrian,
                                                medis: medis,
                                                customer_id: customer_id,
                                                no_customer: no_customer,
                                                nama_customer: nama_customer,
                                                id_antrian: id_antrian,
                                                tgl: DATE_NOW,
                                                diagnosa: Ext.encode(Ext.pluck(
                                                    jun.rztDiagnosa.data.items, "data")),
                                                detil: Ext.encode(Ext.pluck(
                                                    jun.rztSalestransQtyDetails.data.items, "data"))
                                            },
                                            success: function (f, a) {
                                                var response = Ext.decode(f.responseText);
                                                if (response.success) {
                                                    jun.rztSalestransQtyDetails.removeAll();
                                                    jun.rztSalestransQtyDetails.refreshData(jun.rztSalestransQtyDetails);
                                                    jun.rztDiagnosa.removeAll();
                                                    jun.rztDiagnosa.refreshData(jun.rztDiagnosa);
                                                    Ext.getCmp('docs-jun.AntrianDokterGridWin').expand();
                                                }
                                                t.setDisabled(false);
                                                Ext.MessageBox.show({
                                                    title: 'Info',
                                                    msg: response.msg,
                                                    buttons: Ext.MessageBox.OK,
                                                    icon: Ext.MessageBox.INFO
                                                });
                                            },
                                            failure: function (f, a) {
                                                t.setDisabled(false);
                                                switch (a.failureType) {
                                                    case Ext.form.Action.CLIENT_INVALID:
                                                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                                        break;
                                                    case Ext.form.Action.CONNECT_FAILURE:
                                                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                                                        break;
                                                    case Ext.form.Action.SERVER_INVALID:
                                                        Ext.Msg.alert('Failure', a.result.msg);
                                                }
                                            }
                                        });
                                    } else {
                                        Ext.Msg.show({
                                            title: 'Customers',
                                            msg: 'Pilih Customer, bisa dicari menggunakan no atau nama.',
                                            buttons: Ext.MessageBox.OKCANCEL,
                                            multiline: false,
                                            inputField: new Ext.form.ComboBox({
                                                ref: '../customer',
                                                // id: 'cmbcustomercounterfo',
                                                triggerAction: 'query',
                                                lazyRender: true,
                                                mode: 'remote',
                                                forceSelection: true,
                                                autoSelect: false,
                                                store: jun.rztCustomersDokterCmp,
                                                valueField: 'no_customer',
                                                displayField: 'nama_customer',
                                                hideTrigger: true,
                                                minChars: 3,
                                                matchFieldWidth: !1,
                                                pageSize: 20,
                                                itemSelector: 'div.search-item',
                                                tpl: new Ext.XTemplate(
                                                    '<tpl for="."><div class="search-item">',
                                                    '<h3><span>{tgl_lahir:date("j M Y")}<br />{telp}</span>{nama_customer} ({no_customer})</h3>',
                                                    '{alamat}',
                                                    '</div></tpl>'
                                                ),
                                                allowBlank: false,
                                                listWidth: 450,
                                                lastQuery: ""
                                            }),
                                            fn: function (btn, txt) {
                                                if (btn != 'ok') {
                                                    Ext.getCmp('btnSaveCloseDokter').setDisabled(false);
                                                    return;
                                                }
                                                txt = txt.toUpperCase();
                                                if (txt == '') {
                                                    Ext.MessageBox.show({
                                                        title: 'Info',
                                                        msg: 'Masukkan No Pasien atau No Base. Misal : 1JOG01',
                                                        buttons: Ext.MessageBox.OK,
                                                        icon: Ext.MessageBox.INFO
                                                    });
                                                    Ext.getCmp('btnSaveCloseDokter').setDisabled(false);
                                                    return;
                                                }
                                                var at = jun.rztCustomersDokterCmp.findExact('no_customer', txt);
                                                var record = jun.rztCustomersDokterCmp.getAt(at);
                                                no_customer = txt;
                                                customer_id = record.data.customer_id;
                                                nama_customer = record.data.nama_customer;
                                                Ext.Ajax.request({
                                                    url: 'Konsul/Create/',
                                                    method: 'POST',
                                                    scope: this,
                                                    params: {
                                                        no_antrian: no_antrian,
                                                        medis: medis,
                                                        customer_id: customer_id,
                                                        no_customer: no_customer,
                                                        nama_customer: nama_customer,
                                                        id_antrian: id_antrian,
                                                        diagnosa: Ext.encode(Ext.pluck(
                                                            jun.rztDiagnosa.data.items, "data")),
                                                        tgl: DATE_NOW,
                                                        detil: Ext.encode(Ext.pluck(
                                                            jun.rztSalestransQtyDetails.data.items, "data"))
                                                    },
                                                    success: function (f, a) {
                                                        var response = Ext.decode(f.responseText);
                                                        if (response.success) {
                                                            jun.rztSalestransQtyDetails.removeAll();
                                                            jun.rztSalestransQtyDetails.refreshData(jun.rztSalestransQtyDetails);
                                                            jun.rztDiagnosa.removeAll();
                                                            jun.rztDiagnosa.refreshData(jun.rztDiagnosa);
                                                        }
                                                        t.setDisabled(false);
                                                        Ext.MessageBox.show({
                                                            title: 'Info',
                                                            msg: response.msg,
                                                            buttons: Ext.MessageBox.OK,
                                                            icon: Ext.MessageBox.INFO
                                                        });
                                                    },
                                                    failure: function (f, a) {
                                                        t.setDisabled(false);
                                                        switch (a.failureType) {
                                                            case Ext.form.Action.CLIENT_INVALID:
                                                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                                                break;
                                                            case Ext.form.Action.CONNECT_FAILURE:
                                                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                                                break;
                                                            case Ext.form.Action.SERVER_INVALID:
                                                                Ext.Msg.alert('Failure', a.result.msg);
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            }
                        ]
                    }
                }
            ]
        });
    });
</script>
<div id="dokterView" style="height: 100%"></div>