<script>
    window.onerror = function (error, file, line) {
        var err, lasterr;
        lasterr = localStorage.getItem('error_nwis');
        err = 'line=' + line + '\nfile=' + encodeURIComponent(file) + '\nerror=' + encodeURIComponent(error);
        localStorage.setItem("error_nwis", lasterr + "\n" + err);
    };
</script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/defiant.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/rsvp-3.1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/sha-256.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-main.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-tray.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script>
    startConnection();
    var LOGOUT = false;
    DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
    DATE5YEAR = Date.parseDate('<?=date("Y-m-d H:i:s",strtotime("+5 years"))?>', 'Y-m-d H:i:s');

    NARSURL = '<?=NARSURL?>';
    BASE_URL = '<?=bu() === "" ? "/" : bu();?>';
    SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
    SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
    SYSTEM_LOGO = '<img src="<?=bu() . app()->params['url_logo']; ?>" alt=""/>';
    NO_PREVIEW = '<img src="<?=bu(); ?>/images/no-preview.jpg" alt=""/>';

    STYLEPREVIEW ='css/cardpreview.css';

    CARDPREVIEW ='<img src="<?=bu(); ?>/images/card.jpg" width="387px" height="244" alt="cardNatasha"/>';

    STYLECARD = 'css/card.css';

    SHOWLOG ='<div id="list"><p><iframe src="<?=bu(); ?>/logsync.log" width=381 height=199 frameborder=0 scrolling=yes></iframe></p></div>';

    NATASHA_CUSTOM = '<?=NATASHA_CUSTOM ? 'true' : 'false'?>';
    PT_CARD = '<?=PT_CARD;?>';

    PT_NEGARA = '<?=PT_NEGARA;?>';
    NEGARA = '<?=NEGARA;?>';
    SHUTTER_OGG_URL = 'js/jpeg_camera/shutter.ogg';
    SHUTTER_MP3_URL = 'js/jpeg_camera/shutter.mp3';
    SWF_URL = 'js/jpeg_camera/jpeg_camera.swf';
    SALES_TYPE = '<?=Users::is_audit();?>';
    EDIT_TGL =  <?
    $user = Users::model()->findByPk(Yii::app()->user->getId());
    echo $user->is_available_role(246) ? 'true' : 'false';
    ?>;
    RETUR_PRODUKSI =  <?
    $user = Users::model()->findByPk(Yii::app()->user->getId());
    echo $user->is_available_role(262) ? 'true' : 'false';
    ?>;
    HEADOFFICE = <?if (defined('HEADOFFICE')) {
        echo HEADOFFICE ? 'true' : 'false';
    } else {
        echo 'false';
    }?>;
    PRINT_STOK = <?if (defined('PRINT_STOK')) {
        echo PRINT_STOK ? 'true' : 'false';
    } else {
        echo 'true';
    }?>;

    PORT_CLOSED = true;
    //    Ext.chart.Chart.CHART_URL = '<?//=bu(); ?>///js/ext340/resources/charts.swf';
    METHODE_BONUS = <?=METHODE_BONUS;?>;
    VALID_CARD = <?=VALID_CARD;?>;
    INTERVALSYNC = <?=INTERVALSYNC;?>;
    ENABLESYNC = <?=ENABLESYNC ? 'true' : 'false';?>;
    STORE = '<?=STOREID;?>';
    STOREID_HO = '<?=STOREID_HO;?>';
    /*begin for restriction on input date*/
    RES_DATE = <?
    $a = U::getResDate();
    $user = Users::model()->findByPk(Yii::app()->user->getId());
    echo $user->is_available_role(501) ? 'false' : '"' . $a . '"';
    ?>;
    REST_START = <?
    $start = RestrictDate::getPersonalRestDate('1');
    echo $start != null ? '"' . $start . '"' : 'false';
    ?>;
    REST_END = <?
    $end = RestrictDate::getPersonalRestDate('2');
    echo $end != null ? '"' . $end . '"' : 'false';
    ?>;
    /*end for restriction on input date*/
    SALES_OVERRIDE = '<?
        $id = Yii::app()->user->getId();
        $user = Users::model()->findByPk($id);
        echo Users::get_override($user->user_id, $user->password) ? 1 : 0;
        ?>';
    ROUNDING = <?if (defined('ROUNDING')) {
        echo ROUNDING;
    } else {
        echo 50;
    }?>;
    function nwis_round_up(e) {
        return round(Math.round(round(e / ROUNDING, 2)) * ROUNDING, 2);
    }
    function goodbye(e) {
        if (!LOGOUT) {
            if (!e) e = window.event;
            e.cancelBubble = true;
            e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }
        }
        closeSerialPort(COM_POSIFLEX);
        endConnection();
    }
    window.onbeforeunload = goodbye;
    if (is_enable_tools()) {
        var gui = require('nw.gui');
        var win = gui.Window.get();
        win.maximize();
        win.on('new-win-policy', function (frame, url, policy) {
            policy.forceNewPopup();
        });
    }
</script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/CheckColumn.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridSorter.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridColumnResizer.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridNodeUI.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridLoader.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGridColumns.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/treegrid/TreeGrid.js"></script>

<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/0.js"></script>

<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/quickSalestrans_form.js"></script>

<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/account_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/account_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/account_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/analysis_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/analysis_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/budget_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/budget_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/budget_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/businessunit_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/businessunit_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/businessunit_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/category_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/category_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/category_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/group_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/group_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/group_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/groupproduk_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/groupproduk_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/groupproduk_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/investasi_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/investasi_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/investasi_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/securityRoles_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/securityRoles_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/securityRoles_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/store_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/store_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/store_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksi_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksi_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksi_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksiDetail_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksiDetail_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/unit_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/unit_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/unit_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/produk_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/produk_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/produk_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/outlet_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/outlet_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/outlet_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/realization_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/realization_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/realization_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/report_projection.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/importProjection.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/userEmployee_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/userEmployee_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/userEmployee_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/users_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/users_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/users_form.js"></script>

<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/employee_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/employee_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/employee_store.js"></script>

<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/investasiRealisasi_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/investasiRealisasi_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/investasiRealisasi_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/analysisInvestasi_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/analysisInvestasi_store.js"></script>

<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksiOverride_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksiOverride_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksiOverride_store.js"></script>

<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksiOverrideRealisasi_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksiOverrideRealisasi_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksiOverrideRealisasi_store.js"></script>

<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksiCategory_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksiCategory_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/transaksiCategory_grid.js"></script>






<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/mainpanel.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/sha512.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/FileUploadField.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/cc.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/xlsx.core.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jpeg_camera/canvas-to-blob.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jpeg_camera/jpeg_camera_no_flash.js"></script>
<iframe id="myFrame" name="myFrame" style="border:none"></iframe>

<script type="text/javascript">
    //info user logged in
    var UID = '<?=User()->getId()?>';
    //status
    var STATUS_OPEN = <?=STATUS_OPEN?>;
    var STATUS_CLOSE = <?=STATUS_CLOSE?>;
    //status PR
    var PR_NEED_SHIPMENT = <?=PR_NEED_SHIPMENT?>;
    var PR_DRAFT = <?=PR_DRAFT?>;
    var PR_OPEN = <?=PR_OPEN?>;
    var PR_PROCESS = <?=PR_PROCESS?>;
    var PR_CLOSED = <?=PR_CLOSED?>;
    //status PO
    var PO_OPEN = <?=PO_OPEN?>;
    var PO_PARTIALLY_RECEIVED = <?=PO_PARTIALLY_RECEIVED?>;
    var PO_RECEIVED = <?=PO_RECEIVED?>;
    var PO_CLOSED = <?=PO_CLOSED?>;
    //status Terima Barang & invoice
    var TB_DRAFT = <?=TB_DRAFT?>;
    var TB_OPEN = <?=TB_OPEN?>;
    var TB_INVOICED = <?=TB_INVOICED?>;
    var TB_CLOSED = <?=TB_CLOSED?>;

    //status send dropping
    var DR_SEND = <?=DR_SEND?>;
    var DR_PENDING = <?=DR_PENDING?>;
    var DR_APPROVE = <?=DR_APPROVE?>;
    var DR_PROCESS = <?=DR_PROCESS?>;
    var DR_RECEIVE = <?=DR_RECEIVE?>;
    var DR_CLOSE = <?=DR_CLOSE?>;


    //STATUS SYNC
    var SYNC_OK = <?=SYNC_OK?>;
    var SYNC_PR = <?=SYNC_PR?>;
    var SYNC_MK = <?=SYNC_MK?>;
    var SYNC_NO = <?=SYNC_NO?>;
    var SYNC_FL = <?=SYNC_FL?>;

    //STATUS SYNC
    var SCAN_PR = <?=SCAN_PR?>;
    var SCAN_OK = <?=SCAN_OK?>;
    var SCAN_MK = <?=SCAN_MK?>;
    var SCAN_NO = <?=SCAN_NO?>;
    var SCAN_FL = <?=SCAN_FL?>;


    jun.is_nwjs = function is_enable_tools() {
        try {
            var gui = require('nw.gui');
            if (gui != null) {
                var win = gui.Window.get();
                win.maximize();
                // win.showDevTools();
                return true;
            }
        } catch (err) {
            console.log(err.message);
            return false;
        }
    };
    if (jun.is_nwjs()) {
        var fs = require('fs');
        var obj = JSON.parse(fs.readFileSync('config.json', 'utf8'));
        jun.Counter = obj.COUNTER;
    }
    jun.Counter = 'A';
    var COA_ROUNDING = '<?=COA_ROUNDING?>';
    var REFERRAL = '<?= (defined('REFERRAL')?REFERRAL:FALSE)?>';





</script>


