<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/defiant.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/rsvp-3.1.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/dependencies/sha-256.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-main.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/qz-tray.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/tabs/Ext.ux.VrTabPanel.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/locstor.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/0.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsul_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsul_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsul_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsulDetil_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/konsulDetil_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/negara_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/kota_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/provinsi_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/kecamatan_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/store_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/info_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/statusCust_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/customers_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/customers_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/customers_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/aishaAntrian_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/antrian_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/antrian_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/antrian_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/grup_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/grupAttr_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/dokter_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/paket_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/beauty_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestrans_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestrans_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestrans_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/payment_form.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/payment_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/payment_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/bank_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/card_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/paketTrans_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/paketTrans_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestransDetails_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/salestransDetails_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/barang_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/beautyOndutyStatus_store.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/beautyServices_grid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jpeg_camera/canvas-to-blob.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jpeg_camera/jpeg_camera_no_flash.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/cc.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/view/memberCard_form.js"></script>

<script>
    startConnection();
    var LOGOUT = false;
    DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
    DATE5YEAR = Date.parseDate('<?=date("Y-m-d H:i:s",strtotime("+5 years"))?>', 'Y-m-d H:i:s');
    PT_CARD = '<?=PT_CARD;?>';
    STYLEPREVIEW ='css/cardpreview.css';

    CARDPREVIEW ='<img src="<?=bu(); ?>/images/card.jpg" width="387px" height="244" alt="cardNatasha"/>';

    STYLECARD = 'css/card.css';

    PRINT_STOK=<? echo PRINT_STOK?'true':'false'; ?>;
    NARSURL = '<?=NARSURL?>';
    //    BASE_URL = '<?//=bu() === "" ? "/" : bu();?>//';
    SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
    SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
    SYSTEM_LOGO = '<img src="<?=bu() . app()->params['url_logo']; ?>" alt=""/>';
    SALES_TYPE = '<?=Users::is_audit();?>';
    EDIT_TGL =  <?
    $user = Users::model()->findByPk(Yii::app()->user->getId());
    echo $user->is_available_role(246) ? 'true' : 'false';
    ?>;
    HEADOFFICE = <?if (defined('HEADOFFICE')) {
        echo HEADOFFICE ? 'true' : 'false';
    } else {
        echo 'false';
    }?>;
    NO_PREVIEW = '<img src="<?=bu(); ?>/images/no-preview.jpg" alt=""/>';
    SHUTTER_OGG_URL = 'js/jpeg_camera/shutter.ogg';
    SHUTTER_MP3_URL = 'js/jpeg_camera/shutter.mp3';
    SWF_URL = 'js/jpeg_camera/jpeg_camera.swf';
    PT_NEGARA = '<?=PT_NEGARA;?>';
    NEGARA = '<?=NEGARA;?>';
    SYSTEM_BANK_CASH = '<?=Bank::get_bank_cash_id();?>';
    SYSTEM_PETTY_CASH = '<?=Bank::get_petty_cash_id();?>';
    SYSTEM_BANK_CASH_WHILE = '<?=Bank::get_bank_cash_while_id();?>';
    PORT_CLOSED = true;
    //    Ext.chart.Chart.CHART_URL = '<?//=bu(); ?>///js/ext340/resources/charts.swf';
    VALID_CARD = <?=VALID_CARD;?>;
    INTERVALSYNC = <?=INTERVALSYNC;?>;
    ENABLESYNC = <?=ENABLESYNC ? 'true' : 'false';?>;
    STORE = '<?=STOREID;?>';
    SALES_OVERRIDE = '<?
        $id = Yii::app()->user->getId();
        $user = Users::model()->findByPk($id);
        echo Users::get_override($user->user_id, $user->password) ? 1 : 0;
        ?>';
    ROUNDING = <?if (defined('ROUNDING')) {
        echo ROUNDING;
    } else {
        echo 50;
    }?>;
    function nwis_round_up(e) {
        return round(Math.round(round(e / ROUNDING, 2)) * ROUNDING, 2);
    }
    function goodbye(e) {
        if (!LOGOUT) {
            if (!e) e = window.event;
            e.cancelBubble = true;
            e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }
        }
        closeSerialPort(COM_POSIFLEX);
        endConnection();
    }
    window.onbeforeunload = goodbye;
    jun.is_nwjs = function is_enable_tools() {
        try {
            var gui = require('nw.gui');
            if (gui != null) {
                var win = gui.Window.get();
                win.maximize();
                // win.showDevTools();
                return true;
            }
        } catch (err) {
            console.log(err.message);
            return false;
        }
    };
    jun.Counter = 'A';
    jun.ajaxCounter = 0;
    jun.runner = new Ext.util.TaskRunner();
    Ext.onReady(function () {
        Ext.EventManager.addListener(document, "keypress", function (e) {
            if (jun.ajaxCounter > 0) {
                return false;
            }
        });
        Ext.Ajax.timeout = 1800000;
        Ext.QuickTips.init();
        Ext.Ajax.on("requestexception", function (conn, response, opts) {
            switch (response.status) {
                case 403:
                    window.location.href = 'site/logout';
                    break;
                case 500:
                    Ext.Msg.alert('Internal Server Error', response.responseText);
                    break;
                default :
                    Ext.Msg.alert(response.status + " " + response.statusText, response.responseText);
                    break;
            }
        });
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBankLib.getTotalCount() === 0) {
            jun.rztBankLib.load();
        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztCardLib.getTotalCount() === 0) {
            jun.rztCardLib.load();
        }
        if (jun.rztCardCmp.getTotalCount() === 0) {
            jun.rztCardCmp.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztPaketCmp.getTotalCount() === 0) {
            jun.rztPaketCmp.load();
        }
        if (jun.rztPaketLib.getTotalCount() === 0) {
            jun.rztPaketLib.load();
        }
        if (jun.is_nwjs()) {
            var fs = require('fs');
            var obj = JSON.parse(fs.readFileSync('config.json', 'utf8'));
            jun.Counter = obj.COUNTER;
        }
        jun.rztAntrianMonitor = new jun.MonitorAntrianstore({
            baseParams: {counter: jun.Counter},
            method: 'POST'
        });
        jun.reloadAllStoreCounter = function () {
            Ext.Ajax.request({
                url: 'SysPrefs/get',
                method: 'POST',
                params: {
                    val: 'tdate_antrian'
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    var msg = window.btoa(response.msg);
                    var tdate_antrian = Locstor.get('tdate_antrian');
                    if (tdate_antrian != null) {
                        if (tdate_antrian != msg) {
                            jun.rztAntrianFO.load({
                                params: {
                                    bagian: "counter",
                                    counter: jun.Counter,
                                    mode: "grid"
                                }
                            });
                            jun.rztAntrianFOPending.reload();
                            jun.rztAntrianKasir.reload();
                            jun.rztAntrianKasirPending.reload();
                            jun.rztBeautyOndutyStatus.reload();
                            jun.rztAntrianMonitor.reload();
                            jun.rztKonsulCounter.reload();
                            jun.rztAntrianDokter.reload();
                            jun.rztAntrianDokterPending.reload();
                        }
                    }
                    Locstor.set('tdate_antrian', msg);
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        };
        jun.runner.start({
            run: jun.reloadAllStoreCounter,
            interval: 5 * 1000
        });
        var viewport = new Ext.Viewport({
            layout: 'fit',
            items: [
                {
                    xtype: 'tabpanel',
                    tabBarPosition: 'top',
                    activeTab: 0,
                    frame: true,
                    resizeTabs: true,
                    enableTabScroll: true,
                    tabWidth: 200,
                    tabBar: {
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        defaults: {flex: 1}
                    },
                    items: [
                        new jun.AntrianCounterWin({
                            title: "Antrian Counter"
                        }),
                        new jun.CustomersGrid({
                            title: 'P E N D A F T A R A N'
                        }),
                        new Ext.Panel({
                            title: 'K A S I R',
                            id: 'docs-jun.SalestransCounterPanelWin',
                            layout: 'border',
                            height: 260,
                            items: [
                                {
                                    region: 'west',
                                    id: 'west-panel',
                                    split: true,
                                    width: '30%',
                                    collapsible: false,
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    frameHeader: !1,
                                    header: !1,
                                    border: false,
                                    items: [
                                        new jun.KonsulCounterGrid({
                                            title: 'Suspend List',
                                            margins: '0 0 2px 0',
                                            flex: 1
                                        }),
                                        new jun.KonsulDetilCounterGrid({
                                            disabled: true,
                                            title: 'Suspend Details',
                                            flex: 1
                                        })
                                    ]
                                },
                                new jun.SalestransCounterWin({
                                    modez: 0,
                                    frameHeader: !1,
                                    header: !1,
                                    region: 'center'
                                })
                            ],
                            listeners: {
                                activate: function (t) {
                                    console.log('active panel');
                                    Ext.getCmp('docs-SalestransCounterWin').fireEvent('activate',
                                        Ext.getCmp('docs-SalestransCounterWin'));
                                }
                            }
                        }),
                        new jun.PasienHistoryWin({}),
                        new jun.BeautyMostReadyGrid({
                            title: 'Beauty OnDuty',
//                            margins: '0 2 0 0',
                            flex: 4
                        }),
                        new jun.AntrianDokterCounterGridWin({
                            title: 'Antrian Konsul'
                        })
                    ]
                }
            ]
        });
    });
</script>
<div id="counterView" style="height: 100%"></div>