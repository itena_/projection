<script type="text/javascript">
    window.onload = function () {
        var data = '<?php echo $data; ?>';
        var json = JSON.parse(data);
        var chart = new CanvasJS.Chart("chartContainer",
            {
                backgroundColor: null,
                title: {
                    text: "New Customer <?php echo $chart_title?>"
                },
                axisX:{
                    labelAngle: 90.1,
                    interval: 1
                },
                data: [
                    {
                        type: "line",
                        dataPoints: json
                    }
                ]
            });
        chart.render();
    }
</script>