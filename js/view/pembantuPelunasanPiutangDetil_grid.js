jun.PembantuPelunasanPiutangDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PelunasanPiutangDetil",
    id: 'docs-jun.pembantuPelunasanPiutangDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Customer Name',
            sortable: true,
            resizable: true,
            dataIndex: 'customer_id',
            width: 200,
            renderer: jun.renderSupplier
        },
        {
            header: 'Payment No.',
            sortable: true,
            resizable: true,
            dataIndex: 'no_faktur',
            width: 100
        },
        {
            header: 'Payment Value',
            sortable: true,
            resizable: true,
            dataIndex: 'nilai',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'COA.',
            sortable: true,
            resizable: true,
            dataIndex: 'account_code',
            width: 100
        },
        {
            header: 'Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'kas_dibayar',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Remains',
            sortable: true,
            resizable: true,
            dataIndex: 'sisa',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Type',
            sortable: true,
            resizable: true,
            dataIndex: 'type_',
            width: 50,
            align: "right",
            renderer: function (v, m, r) {
                if (r.get('type_') == 0) {
                    return 'PH';
                } else {
                    return 'UM';
                }
            }}
    ],
    initComponent: function () {
        this.store = jun.rztPembantuPelunasanPiutangDetil;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: "label",
                            text: "Customer:"
                        },
                        {
                            ref: '../../customer',
                            xtype: 'combo',
//                        //typeAhead: true,
                            triggerAction: 'query',
                            lazyRender: true,
                            mode: 'remote',
                            forceSelection: true,
                            autoSelect: false,
                            store: jun.rztCustomersPiutangCmp,
                            id: "customer_id",
                            hiddenName: 'customer_id',
                            valueField: 'customer_id',
                            displayField: 'nama_customer',
                            hideTrigger: true,
                            minChars: 3,
                            matchFieldWidth: !1,
                            pageSize: 20,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<h3><span>{telp} <br /> {tgl_lahir:date("M j, Y")}</span>{no_customer} | {nama_customer}</h3>',
                                '{alamat}',
                                "</div></tpl>"),
                            allowBlank: false,
                            listWidth: 350,
                            width: 175
                        },
                        {
                            xtype: 'label',
                            text: '\xA0Payment No:'
                        },
                        {
                            xtype: "combo",
                            typeAhead: !0,
                            triggerAction: "all",
                            lazyRender: !0,
                            mode: "local",
                            store: jun.rztPembantuFakturPiutang,
                            forceSelection: !0,
                            valueField: "payment_journal_id",
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                // '<span>{no_faktur} | Tgl: {tgl} | Nilai: {nilai:number("0,0.00")} | Sisa: {sisa:number("0,0.00")}</span>',
                                '<span>Doc.ref: {doc_ref}</span>', '<span> | Payment No: {no_faktur}</span>', '<span> | Date: {tgl:date("M j, Y")}</span><br>',
                                '<span>Value: {nilai:number("0,0.00")}</span>', '<span>| Remains: {sisa:number("0,0.00")}</span>',
                                "</div></tpl>"),
                            displayField: "no_faktur",
                            listWidth: 650,
                            editable: !0,
                            ref: "../../jual",
                            lastQuery: ""
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'COA :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
//                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            enableKeyEvents: true,
                            forceSelection: true,
                            fieldLabel: 'COA',
                            store: jun.rztChartMasterCmp,
                            ref: '../../account_code',
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                                "</div></tpl>"),
                            listWidth: 500,
//                            hiddenName: 'account_code',
                            valueField: 'account_code',
                            displayField: 'account_code'
                        },
                        {
                            xtype: 'label',
                            text: '\xA0Amount:'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../kas',
                            value: 0,
                            width: 175
                        },
                        {
                            xtype: 'label',
                            text: '\xA0Uang Muka :'
                        },
                        {
                            xtype: 'combo',
                            store: [[0, 'PH'],
                                [1, 'UM']
                            ],
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            value: 0,
                            name: "type_",
                            id: "type_id",
                            ref: '../../type_'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 2,
                    defaults: {
                        scale: 'large',
                        width: 40
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            ref: '../../btnDel'
                        }
                    ]
                }
            ]
        };
        jun.PembantuPelunasanUtangDetilGrid.superclass.initComponent.call(this);
//        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnDel.on('Click', this.deleteRec, this);
        this.jual.on('select', this.selectJual, this);
        this.customer.on('select', this.onCustomerChange, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);

    },
    selectJual: function (combo, record, index) {
        this.kas.setValue(record.data.sisa);
    },
    onStoreChange: function () {
        jun.rztPembantuPelunasanPiutangDetil.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    onCustomerChange: function () {
//        this.id_bank.reset();
        this.account_code.reset();
        this.jual.reset();
        this.kas.reset();
        this.type_.reset();
        var customer_id = this.customer.getValue();
//        jun.rztPembantuPelunasanUtangDetil.removeAll();

        jun.rztPembantuFakturPiutang.baseParams = {
            customer_id: customer_id
        };
        jun.rztPembantuFakturPiutang.load();
        jun.rztPembantuFakturPiutang.baseParams = {};
    },
    loadForm: function () {
        var jual = this.jual.getValue();
        if (jual == "") {
            Ext.MessageBox.alert("Error", "Payment must selected.");
            return
        }
        var kas = parseFloat(this.kas.getValue());
        if (kas < 0) {
            Ext.MessageBox.alert("Error", "Payment total must more than 0.");
            return;
        }
        var faktur_id = jun.rztPembantuFakturPiutang.findExact('payment_journal_id', jual);
        if (faktur_id == -1) {
            Ext.MessageBox.alert("Error", "Fatal Error.");
            return
        }

        var faktur = jun.rztPembantuFakturPiutang.getAt(faktur_id);
        var a = this.store.findExact("payment_journal_id", faktur.data.payment_journal_id);
        if (a > -1) {
            Ext.MessageBox.alert("Error", "Payment already added.");
            return;
        }
        if (kas > 0 && kas > faktur.data.sisa) {
            Ext.MessageBox.alert("Error", "total can't more than Payment total.");
            return;
        }
        var sisa = faktur.data.sisa - kas;
        var type_ = this.type_.getValue();
        var customer_id = this.customer.getValue();
        var account_code = this.account_code.getValue();

        var c = jun.rztPembantuPelunasanPiutangDetil.recordType,
            d = new c({
                payment_journal_id: faktur.data.payment_journal_id,
                kas_dibayar: kas,
                tgl: faktur.data.tgl,
                no_faktur: faktur.data.no_faktur,
                nilai: faktur.data.nilai,
                sisa: sisa,
                type_: type_,
                customer_id: customer_id,
                account_code: account_code
            });
        jun.rztPembantuPelunasanPiutangDetil.add(d);
        this.jual.reset();
        this.kas.reset();
        this.type_.reset();
        this.customer.reset();
        this.account_code.reset();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
//        console.log(record);
        Ext.MessageBox.confirm('Confirmation', 'Are you sure coba?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        this.store.remove(record);
    }
});