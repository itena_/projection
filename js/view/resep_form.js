jun.ResepWin = Ext.extend(Ext.Window, {
    title: 'Resep',
    modez: 1,
    width: 725,
    height: 300,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Resep',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaults: {
                    border: false,
                    bodyStyle: 'background-color: #E4E4E4;'
                },
                ref: 'formz',
                border: false,
                items: [
                    {
                        width: '100%',
                        height: 30,
                        layout: 'hbox',
                        defaults: {
                            // margins: '0 0 5 0',
                            border: false,
                            bodyStyle: 'background-color: #E4E4E4;'
                        },
                        layoutConfig: {
                            // padding: 5,
                            align: 'stretch'
                        },
                        items: [
                            {
                                flex: 1,
                                layout: 'form',
                                margins: '0 5px 0 0',
                                labelWidth: 100,
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Nama Resep',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'nama_resep',
                                        ref: '../../../nama_resep',
                                        maxLength: 100,
                                        //allowBlank: 1,
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                flex: 1,
                                layout: 'form',
                                labelWidth: 100,
                                items: [
                                    {
                                        xtype: 'combo',
                                        typeAhead: true,
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        fieldLabel: 'Item',
                                        store: jun.rztBarangCmp,
                                        hiddenName: 'barang_id',
                                        valueField: 'barang_id',
                                        displayField: 'kode_barang',
                                        ref: '../../../kode_barang',
                                        anchor: '100%'
                                    }
                                ]
                            }
                        ]
                    },
                    new jun.ResepDetailsGrid({
                        flex: 1,
                        frameHeader: !1,
                        header: !1
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ResepWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.kode_barang.on('beforeselect', this.onkodebarangSelect, this);
        this.on('close', this.onClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.kode_barang.setReadOnly(true);
        } else {
            this.btnSave.setVisible(true);
            this.kode_barang.setReadOnly(false);
        }
    },
    onClose: function () {
        jun.rztResepDetails.removeAll();
    },
    onkodebarangSelect: function (c, r, i) {
        if (jun.rztResep.findExact('barang_id', r.data.barang_id) > -1) {
            Ext.Msg.alert('Resep sudah ada!', 'Untuk edit resep mohon melalui menu Edit Resep!!!');
            // c.reset();
            return false;
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Resep/update/id/' + this.id;
        } else {
            urlz = 'Resep/create/';
        }
        Ext.getCmp('form-Resep').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this, params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztResepDetails.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztResep.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Resep').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    }
    ,
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    }
    ,
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    }
    ,
    onbtnCancelclick: function () {
        this.close();
    }
});