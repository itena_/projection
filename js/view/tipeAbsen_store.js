jun.TipeAbsenstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TipeAbsenstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TipeAbsenStoreId',
            url: 'TipeAbsen',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tipe_absen_id'},
                {name: 'tipe_absen_name'}
            ]
        }, cfg));
    }
});
jun.rztTipeAbsen = new jun.TipeAbsenstore();
jun.rztTipeAbsenCmp = new jun.TipeAbsenstore();
jun.rztTipeAbsenLib = new jun.TipeAbsenstore();
//jun.rztTipeAbsen.load();
