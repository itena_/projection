jun.CustomersWin = Ext.extend(Ext.Window, {
    title: 'Form Customer',
    modez: 1,
    width: 800,
    height: 575,
    layout: 'fit',
    modal: true,
    id: 'docs-jun.CustomersWin',
    padding: 5,
    resizable: !1,
    customer_id: null,
    closeForm: false,
    initComponent: function () {
        jun.rztCustomersMasterCmp.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tgl_lahirid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    //b.params.tgl = Ext.getCmp('tglbeutygridid').getValue();
                    b.params.mode = "master";
                }
            }
        });
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Customers',
                // labelWidth: 150,
                // labelAlign: 'left',
                 layout: 'form',
//                layout: {
//                    type: 'vbox',
//                    align: 'stretch'
//                },
                defaults: {
                    border: false,
                    bodyStyle: 'background-color: #E4E4E4;'
                },
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'hidden',
                        ref: '../thumb',
                        name: 'thumb'
                    },
                    {
                        xtype: 'hidden',
                        ref: '../thumb_kanan',
                        name: 'thumb_kanan'
                    },
                    {
                        xtype: 'hidden',
                        ref: '../thumb_kiri',
                        name: 'thumb_kiri'
                    },
                    {
                        xtype: 'hidden',
                        ref: '../photo_kanan',
                        name: 'photo_kanan'
                    },
                    {
                        xtype: 'hidden',
                        ref: '../photo_kiri',
                        name: 'photo_kiri'
                    },
                    {
                        xtype: 'hidden',
                        ref: '../photo',
                        name: 'photo'
                    },
                    {
                        xtype: 'hidden',
                        ref: '../store_',
                        name: 'store',
                        value: STORE
                    },
                    {
                        width: '100%',
                        height: 220,
                        layout: 'hbox',
                        layoutConfig: {
                            // padding: 5,
                            align: 'stretch'
                        },
                        items: [
                            {
                                border: false,
                                flex: 1,
                                labelWidth: 110,
                                labelAlign: 'left',
                                layout: 'form',
                                bodyStyle: 'background-color: #E4E4E4; padding-right: 5px;',
                                items: [
                                    {
                                        xtype: 'xdatefield',
                                        ref: '../../../tgl_lahir',
                                        fieldLabel: 'Tgl Lahir (*)',
                                        name: 'tgl_lahir',
                                        id: 'tgl_lahirid',
                                        format: 'd/m/Y',
                                        enableKeyEvents: true,
                                        listeners: {
                                            specialkey: function (nf, evt) {
                                                if (evt.getKey() == 9) {
                                                    this.fireEvent('select', this);
                                                }
                                            }
                                        },
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Complete Name (*)',
                                        ref: '../../../nama_customer_cmb',
                                        triggerAction: 'query',
                                        loadingText: 'Mencari...',
                                        mode: 'remote',
                                        store: jun.rztCustomersMasterCmp,
                                        displayField: 'Customers Name',
                                        hideTrigger: true,
                                        minChars: 3,
                                        matchFieldWidth: !1,
                                        pageSize: 20,
                                        itemSelector: 'div.search-item',
                                        tpl: new Ext.XTemplate(
                                            '<tpl for="."><div class="search-item">',
                                            '<h3><span>{tgl_lahir:date("j M Y")}<br />{telp}</span>{nama_customer} ({no_customer})</h3>',
                                            '{alamat}',
                                            '</div></tpl>'
                                        ),
                                        valueField: 'customer_id',
                                        allowBlank: true,
                                        anchor: '100%',
                                        listWidth: 450,
                                        lastQuery: "",
                                        disabled: true
                                    },
                                    {
                                        xtype: 'hidden',
                                        name: 'nama_customer',
                                        id: 'nama_customerid',
                                        ref: '../../../nama_customer'
                                    },
                                    {
                                        xtype: 'combo',
                                        editable: false,
                                        ref: '../../../info',
                                        triggerAction: 'all',
                                        lastQuery: '',
                                        mode: 'local',
                                        fieldLabel: 'Info (*)',
                                        store: jun.rztInfoCmp,
                                        hiddenName: 'info_id',
                                        valueField: 'info_id',
                                        displayField: 'info_name',
                                        anchor: '100%'
                                    },
                                    // {
                                    //     xtype: 'uctextfield',
                                    //     fieldLabel: 'Customers Name',
                                    //     hideLabel: false,
                                    //     //hidden:true,
                                    //     name: 'nama_customer',
                                    //     id: 'nama_customerid',
                                    //     ref: '../nama_customer',
                                    //     maxLength: 100,
                                    //     //allowBlank: ,
                                    //     anchor: '100%'
                                    // },
                                    //{
                                    //    xtype: 'uctextfield',
                                    //    fieldLabel: 'Place of Birth',
                                    //    hideLabel: false,
                                    //    //hidden:true,
                                    //    name: 'tempat_lahir',
                                    //    id: 'tempat_lahirid',
                                    //    ref: '../../../tempat_lahir',
                                    //    maxLength: 20,
                                    //    //allowBlank: ,
                                    //    anchor: '100%'
                                    //},
                                    {
                                        xtype: 'numericfield',
                                        ref: '../../../age',
                                        fieldLabel: 'Age',
                                        //allowBlank: ,
                                        value: 0,
                                        readOnly: true,
                                        anchor: '100%'
                                    },
                                    new jun.cmbSex({
                                        fieldLabel: 'Sex (*)',
                                        value: 'FEMALE',
                                        anchor: '100%'
                                    }),
                                    {
                                        xtype: 'uctextfield',
                                        fieldLabel: 'Email',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'email',
                                        id: 'emailid',
                                        ref: '../../../email',
                                        maxLength: 30,
                                        //allowBlank: 1,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'uctextfield',
                                        fieldLabel: 'Phone',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'telp',
                                        id: 'telpid',
                                        ref: '../../../telp',
                                        maxLength: 25,
                                        //allowBlank: 1,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'uctextfield',
                                        fieldLabel: 'Occupation',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'kerja',
                                        ref: '../../../kerja',
                                        maxLength: 50,
                                        //allowBlank: ,
                                        anchor: '100%'
                                    }//,
                                    // {
                                    //     xtype: 'combo',
                                    //     //typeAhead: true,
                                    //     fieldLabel: 'Origin Branch',
                                    //     ref: '../../../storeCode',
                                    //     triggerAction: 'all',
                                    //     lazyRender: true,
                                    //     mode: 'local',
                                    //     store: jun.rztStoreCmp,
                                    //     hiddenName: 'store',
                                    //     value: STORE,
                                    //     readOnly: true,
                                    //     name: 'store',
                                    //     valueField: 'store_kode',
                                    //     displayField: 'store_kode',
                                    //     anchor: '100%'
                                    // }
                                    //{
                                    //    xtype: 'combo',
                                    //    ref: '../customer',
                                    //    triggerAction: 'query',
                                    //    lazyRender: true,
                                    //    mode: 'remote',
                                    //    forceSelection: true,
                                    //    autoSelect: false,
                                    //    fieldLabel: 'MGM',
                                    //    store: jun.rztCustomersCmp,
                                    //    hiddenName: 'friend_id',
                                    //    valueField: 'friend_id',
                                    //    displayField: 'nama_customer',
                                    //    hideTrigger: true,
                                    //    minChars: 3,
                                    //    matchFieldWidth: !1,
                                    //    pageSize: 20,
                                    //    itemSelector: "div.search-item",
                                    //    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    //        '<h3><span>{telp} <br /> {tgl_lahir:date("M j, Y")}</span>{no_customer} | {nama_customer}</h3>',
                                    //        '{alamat}',
                                    //        "</div></tpl>"),
                                    //    //allowBlank: false,
                                    //    listWidth: 750,
                                    //    lastQuery: "",
                                    //    anchor: '100%'
                                    //},
                                ]
                            },
                            {
                                border: false,
                                flex: 1,
                                labelWidth: 110,
                                labelAlign: 'left',
                                layout: 'form',
                                bodyStyle: 'background-color: #E4E4E4; padding-left: 5px;',
                                items: [
                                    {
                                        xtype: 'uctextfield',
                                        fieldLabel: 'Customers No.',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'no_customer',
                                        id: 'no_customerid',
                                        ref: '../../../no_customer',
                                        maxLength: 15,
                                        allowBlank: true,
                                        anchor: '100%',
                                        readOnly: true
                                    },
                                    {
                                        xtype: 'combo',
                                        editable: false,
                                        ref: '../../../status',
                                        triggerAction: 'all',
                                        lastQuery: '',
                                        mode: 'local',
                                        fieldLabel: 'Status',
                                        store: jun.rztStatusCustCmp,
                                        hiddenName: 'status_cust_id',
                                        valueField: 'status_cust_id',
                                        displayField: 'nama_status',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        editable: false,
                                        ref: '../../../negara',
                                        triggerAction: 'all',
                                        lastQuery: '',
                                        mode: 'local',
                                        fieldLabel: 'Country',
                                        store: jun.rztNegaraCmp,
                                        hiddenName: 'negara_id',
                                        valueField: 'negara_id',
                                        displayField: 'nama_negara',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        editable: false,
                                        ref: '../../../provinsi',
                                        triggerAction: 'all',
                                        lastQuery: '',
                                        mode: 'local',
                                        fieldLabel: 'State',
                                        store: jun.rztProvinsiCmp,
                                        hiddenName: 'provinsi_id',
                                        valueField: 'provinsi_id',
                                        displayField: 'nama_provinsi',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        editable: false,
                                        ref: '../../../kota',
                                        triggerAction: 'all',
                                        lastQuery: '',
                                        mode: 'local',
                                        fieldLabel: 'City',
                                        store: jun.rztKotaCmp,
                                        hiddenName: 'kota_id',
                                        valueField: 'kota_id',
                                        displayField: 'nama_kota',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        ref: '../../../kecamatan',
                                        triggerAction: 'all',
                                        lastQuery: '',
                                        mode: 'local',
                                        fieldLabel: 'Sub District',
                                        store: jun.rztKecamatanCmp,
                                        hiddenName: 'kecamatan_id',
                                        valueField: 'kecamatan_id',
                                        displayField: 'nama_kecamatan',
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textarea',
                                        fieldLabel: 'Address',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'alamat',
                                        id: 'alamatid',
                                        ref: '../../../alamat',
                                        enableKeyEvents: true,
                                        style: {textTransform: "uppercase"},
                                        listeners: {
                                            change: function (field, newValue, oldValue) {
                                                field.setValue(newValue.toUpperCase());
                                            }
                                        },
                                        anchor: '100%'
                                        //allowBlank:
                                    }
                                    //{
                                    //    xtype: 'tabpanel',
                                    //    activeTab: 0,
                                    //    defaults: {autoHeight: true, bodyStyle: 'background-color: #E4E4E4; padding:10px'},
                                    //    items: [
                                    //        {
                                    //            title: 'Personal Details',
                                    //            layout: 'form',
                                    //            defaults: {width: 230},
                                    //            defaultType: 'textfield',
                                    //            items: [
                                    //                ]
                                    //        },
                                    //        {
                                    //            title: 'Patient Details',
                                    //            layout: 'form',
                                    //            defaults: {width: 230},
                                    //            defaultType: 'textfield',
                                    //            items: [
                                    //            ]
                                    //        },
                                    //        {
                                    //            title: 'Address',
                                    //            layout: 'form',
                                    //            defaults: {width: 230},
                                    //            defaultType: 'textfield',
                                    //            items: [
                                    //            ]
                                    //        }]
                                    //}
                                ]
                            }
                        ]
                    },
                    {
                        width: '100%',
                        height: 270,
                        layout: 'hbox',
                        layoutConfig: {
                            pack: 'center',
                            align: 'middle'
                        },
                        defaults:{
                            margins: '0 5px',
                            width: 172+10,
                            height: 212+55
                        },
                        items: [
                            {
                                title: 'Samping Kanan',
                                html: "<div id='docs-jun.CustomersWinPreviewKanan' class='customerForm_PhotoPreview'>" + NO_PREVIEW + "</div>",
                                ref: '../../photoPreviewKanan',
                                buttonAlign: 'center',
                                fbar: [
                                    {
                                        xtype: 'button',
                                        text: 'Take Photo',
                                        ref: '../../../../btnphotoKanan'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Browse',
                                        ref: '../../../../btnBrowseKanan'
                                    }
                                ]
                            },
                            {
                                title: 'Samping Kiri',
                                html: "<div id='docs-jun.CustomersWinPreviewKiri' class='customerForm_PhotoPreview'>" + NO_PREVIEW + "</div>",
                                ref: '../../photoPreviewKiri',
                                buttonAlign: 'center',
                                fbar: [
                                    {
                                        xtype: 'button',
                                        text: 'Take Photo',
                                        ref: '../../../../btnphotoKiri'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Browse',
                                        ref: '../../../../btnBrowseKiri'
                                    }
                                ]
                            },
                            {
                                title: 'Depan',
                                html: "<div id='docs-jun.CustomersWinPreview' class='customerForm_PhotoPreview'>" + NO_PREVIEW + "</div>",
                                ref: '../../photoPreview',
                                buttonAlign: 'center',
                                fbar: [
                                    {
                                        xtype: 'button',
                                        text: 'Take Photo',
                                        ref: '../../../../btnphoto'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Browse',
                                        ref: '../../../../btnBrowse'
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        xtype: 'hidden',
                        html: '<input type="file" id="docs-jun.CustomersWinBrowse" style="display: none" accept="image/*">'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Clear Form',
                    hidden: false,
                    ref: '../btnClear'
                },
//                {
//                    xtype: 'button',
//                    text: 'Take Photo',
//                    hidden: false,
//                    ref: '../btnPhoto'
//                },
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.CustomersWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.on('show', this.onShow, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnClear.on('click', this.onbtnClearclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.negara.on('select', this.onProvinsiclick, this);
        this.provinsi.on('select', this.onKotaclick, this);
        this.kota.on('select', this.onKecamatanclick, this);
        this.nama_customer_cmb.on('select', this.onSelectCmbCust, this);
        this.tgl_lahir.on('select', this.onTgllahir, this);
        //this.btnPhoto.on('click', this.onPhotoClick, this);
        this.btnphotoKanan.on('click', this.onPhotoKananClick, this);
        this.btnphotoKiri.on('click', this.onPhotoKiriClick, this);
        this.btnphoto.on('click', this.onPhotoClick, this);
        this.btnBrowse.on('click', this.onBrowseClick, this);
        this.btnBrowseKanan.on('click', this.onBrowseKananClick, this);
        this.btnBrowseKiri.on('click', this.onBrowseKiriClick, this);
        if (this.modez == 1) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
            //this.customer.setReadOnly(true);
            this.info.setReadOnly(true);
            this.nama_customer_cmb.minChars = 10000;
        } else if (this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            //this.customer.setReadOnly(true);
            this.info.setReadOnly(true);
            this.nama_customer_cmb.minChars = 10000;
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
            this.nama_customer_cmb.minChars = 3;
        }
    },
    onShow: function () {
        if (this.modez != 0) {
            Ext.Ajax.request({
                url: 'Customers/GetPhoto/',
                method: 'POST',
                scope: this,
                params: {
                    id: this.customer_id,
                    mode: 'thumb'
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    if (response.success && response.msg != '') {
                        var src = JSON.parse(response.msg);
                        
                        if(src.thumb){
                            var elem = document.createElement("img");
                            elem.setAttribute("src", src.thumb);
                            elem.setAttribute("height", "100%");
                            elem.setAttribute("width", "100%");
                            elem.setAttribute("alt", "Embedded Image");
                            var div = document.getElementById("docs-jun.CustomersWinPreview");
                            div.innerHTML = '';
                            div.appendChild(elem);
                        }
                        if(src.thumb_kanan){
                            elem = document.createElement("img");
                            elem.setAttribute("src", src.thumb_kanan);
                            elem.setAttribute("height", "100%");
                            elem.setAttribute("width", "100%");
                            elem.setAttribute("alt", "Embedded Image");
                            div = document.getElementById("docs-jun.CustomersWinPreviewKanan");
                            div.innerHTML = '';
                            div.appendChild(elem);
                        }
                        if(src.thumb_kiri){
                            elem = document.createElement("img");
                            elem.setAttribute("src", src.thumb_kiri);
                            elem.setAttribute("height", "100%");
                            elem.setAttribute("width", "100%");
                            elem.setAttribute("alt", "Embedded Image");
                            div = document.getElementById("docs-jun.CustomersWinPreviewKiri");
                            div.innerHTML = '';
                            div.appendChild(elem);
                        }
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
        
        document.getElementById("docs-jun.CustomersWinBrowse").addEventListener(
            "change",
            function(evt) {
                var files = evt.target.files;
//                for (var i = 0, f; f = files[i]; i++)
//                {
//                    
//                    var reader = new FileReader();
//                    reader.onload = (function(theFile)
//                        {
//                            return function(e)
//                            {
//                                document.getElementById('productPic').setAttribute('src', e.target.result);
//                            };
//                        })(f);
//                    reader.readAsDataURL(f);
//                }
                var file = files[0];
                if (!file.type.match('image.*')) return;
                var reader = new FileReader();
                reader.onloadend = function() {
                    var elem = document.createElement("img");
                    elem.setAttribute("src", reader.result);
                    elem.setAttribute("height", "100%");
                    elem.setAttribute("width", "100%");
                    elem.setAttribute("alt", "Embedded Image");
                    var div = document.getElementById(Ext.getCmp('docs-jun.CustomersWin').target_view);
                    div.innerHTML = '';
                    div.appendChild(elem);
                    
                    Ext.getCmp('docs-jun.CustomersWin').target_photo.setValue(reader.result);
                    Ext.getCmp('docs-jun.CustomersWin').target_thumb.setValue(reader.result);
                };
                reader.readAsDataURL(file);
            }
        );
    },
    onPhotoKananClick: function () {
        var photo = new jun.CustomersPhotoWin({
            title: 'Photo Samping Kanan',
            myParent: this.customer_id
        });
        photo.show();
        this.target_view = 'docs-jun.CustomersWinPreviewKanan';
        this.target_photo = this.photo_kanan;
        this.target_thumb = this.thumb_kanan;
    },
    onPhotoKiriClick: function () {
        var photo = new jun.CustomersPhotoWin({
            title: 'Photo Samping Kiri',
            myParent: this.customer_id
        });
        photo.show();
        this.target_view = 'docs-jun.CustomersWinPreviewKiri';
        this.target_photo = this.photo_kiri;
        this.target_thumb = this.thumb_kiri;
    },
    onPhotoClick: function () {
        var photo = new jun.CustomersPhotoWin({
            title: 'Photo Depan',
            myParent: this.customer_id
        });
        photo.show();
        this.target_view = 'docs-jun.CustomersWinPreview';
        this.target_photo = this.photo;
        this.target_thumb = this.thumb;
    },
    onBrowseKananClick: function (){
        this.target_view = 'docs-jun.CustomersWinPreviewKanan';
        this.target_photo = this.photo_kanan;
        this.target_thumb = this.thumb_kanan;
        document.getElementById("docs-jun.CustomersWinBrowse").click();
    },
    onBrowseKiriClick: function (){
        this.target_view = 'docs-jun.CustomersWinPreviewKiri';
        this.target_photo = this.photo_kiri;
        this.target_thumb = this.thumb_kiri;
        document.getElementById("docs-jun.CustomersWinBrowse").click();
    },
    onBrowseClick: function (){
        this.target_view = 'docs-jun.CustomersWinPreview';
        this.target_photo = this.photo;
        this.target_thumb = this.thumb;
        document.getElementById("docs-jun.CustomersWinBrowse").click();
    },
    onbtnClearclick: function () {
        Ext.getCmp('form-Customers').getForm().reset();
        this.nama_customer_cmb.setDisabled(true);
        this.btnDisabled(false);
    },
    onSelectCmbCust: function (c, r, i) {
        // this.nama_customer_cmb.setValue(r.data.customer_id);
        Ext.getCmp('form-Customers').getForm().loadRecord(r);
        this.nama_customer_cmb.setRawValue(r.data.nama_customer);
        this.btnDisabled(true);
    },
    onActivate: function () {
        this.onTgllahir();
    },
    onTgllahir: function () {
        var today = new Date();
        var lahir = this.tgl_lahir.getValue();
        if (lahir == undefined || lahir == "") {
            this.nama_customer_cmb.setDisabled(true);
            return;
        }
        this.age.setValue(today.getFullYear() - lahir.getFullYear());
        this.nama_customer_cmb.setDisabled(false);
        //return;
        //var dari = lahir.format("Y-m-d");
        //var sampai = today.format("Y-m-d");
        //Ext.Ajax.request({
        //    url: 'Site/DateDiff',
        //    method: 'POST',
        //    scope: this,
        //    params: {
        //        dari: lahir,
        //        sampai: sampai
        //    },
        //    success: function (f, a) {
        //        var response = Ext.decode(f.responseText);
        //        this.age.setValue(response.msg);
        //    },
        //    failure: function (f, a) {
        //        switch (a.failureType) {
        //            case Ext.form.Action.CLIENT_INVALID:
        //                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                break;
        //            case Ext.form.Action.CONNECT_FAILURE:
        //                Ext.Msg.alert('Failure', 'Ajax communication failed');
        //                break;
        //            case Ext.form.Action.SERVER_INVALID:
        //                Ext.Msg.alert('Failure', a.result.msg);
        //        }
        //    }
        //});
    },
    onProvinsiclick: function () {
        var negara_id = this.negara.getValue();
        this.provinsi.reset();
        this.provinsi.store.clearFilter();
        if (negara_id != '' || negara_id != undefined) {
            this.provinsi.store.filter('negara_id', negara_id, false, true);
        }
    },
    onKotaclick: function () {
        var provinsi_id = this.provinsi.getValue();
        this.kota.reset();
        this.kota.store.clearFilter();
        if (provinsi_id != '' || provinsi_id != undefined) {
            this.kota.store.filter('provinsi_id', provinsi_id, false, true);
        }
    },
    onKecamatanclick: function () {
        var kota_id = this.kota.getValue();
        this.kecamatan.reset();
        this.kecamatan.store.clearFilter();
        if (kota_id != '' || kota_id != undefined) {
            this.kecamatan.store.filter('kota_id', kota_id, false, true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Customers/update/id/' + this.customer_id;
        } else {
            urlz = 'Customers/create/';
        }
        this.nama_customer.setValue(this.nama_customer_cmb.getRawValue().toUpperCase());
        Ext.getCmp('form-Customers').getForm().submit({
            url: urlz,
            scope: this,
            success: function (f, a) {
                jun.rztCustomers.reload();
                jun.rztCustomersCmp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Customers').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
                var storedNames = Locstor.get('AntrianFO');
                if (storedNames != null) {
                    storedNames.no_base = response.Customers.no_customer;
                    var SalestransCounterWin = Ext.getCmp('docs-SalestransCounterWin');
                    if (SalestransCounterWin != null) {
                        jun.rztCustomersSalesCounterCmp.load({
                            params: {
                                customer_id: response.Customers.customer_id
                            },
                            callback: function () {
                                SalestransCounterWin.customer.setValue(response.Customers.customer_id);
                            },
                            scope: this
                        });
                    }
                    var tabpanel = Ext.getCmp('docs-jun.CustomersGrid').findParentByType('tabpanel');
                    var tab = tabpanel.getComponent('docs-jun.AntrianCounterWin');
                    if (tab) {
                        tabpanel.setActiveTab(tab);
                    }
                    var person = {
                        nomor: storedNames.nomor,
                        bagian: storedNames.bagian,
                        counter: storedNames.counter,
                        no_base: storedNames.no_base,
                        id_antrian: storedNames.id_antrian,
                        customer_id: response.Customers.customer_id,
                        nama_customer: response.Customers.nama_customer
                    };
                    Locstor.set('AntrianFO', person);
                    var AntrianFOGridWin = Ext.getCmp('docs-jun.AntrianFOGridWin');
                    Ext.Ajax.request({
                        url: 'AishaAntrian/Update/',
                        method: 'POST',
                        scope: this,
                        params: {
                            mode: 'Nobase',
                            id_antrian: storedNames.id_antrian,
                            no_base: person.no_base,
                            customer_id: person.customer_id,
                            nama_customer: person.nama_customer
                        },
                        success: function (f, a) {
                            var response = Ext.decode(f.responseText);
                            var storedNames = Locstor.get('AntrianFO');
                            if (storedNames != null) {
                                var person = {
                                    nomor: storedNames.nomor,
                                    bagian: storedNames.bagian,
                                    counter: storedNames.counter,
                                    no_base: storedNames.no_base,
                                    id_antrian: storedNames.id_antrian,
                                    customer_id: storedNames.customer_id,
                                    nama_customer: storedNames.nama_customer
                                };
                                Locstor.set('AntrianFO', person);
                                if (AntrianFOGridWin != null) {
                                    AntrianFOGridWin.updateLabel();
                                }
                            }
                        },
                        failure: function (f, a) {
                            if (AntrianFOGridWin != null) {
                                AntrianFOGridWin.updateLabel();
                            }
                            switch (a.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', a.result.msg);
                            }
                        }
                    });
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.CustomersHistoryWin = Ext.extend(Ext.Window, {
    title: 'Form Customer',
    modez: 1,
    width: 525,
    height: 525,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'panel',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-CustomersHistory',
                labelWidth: 100,
                labelAlign: 'left',
                autoLoad: {url: "http://history.naava", scripts: true},
                ref: 'formz',
                border: false
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.CustomersHistoryWin.superclass.initComponent.call(this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.PasienHistoryWin = Ext.extend(Ext.Panel, {
    title: 'History Pasien',
    frame: false,
    id: 'docs-jun.PasienHistoryWin',
    autoScroll: true,
    border: true,
    initComponent: function () {
        this.tbar = {
            xtype: 'toolbar',
            defaults: {
                margins: 5
            },
            items: [
                {
                    xtype: 'label',
                    text: 'No Customer'
                },
                ' ',
                {
                    xtype: 'uctextfield',
                    ref: '../no_customer',
                    id: 'ShowPasienHistoryWin-no_customer'
                },
                ' ',
                {
                    xtype: 'button',
                    text: 'Show History',
                    hidden: false,
                    ref: '../btnShow',
                    id: 'btnShowPasienHistoryWin'
                }
            ]
        };
        jun.PasienHistoryWin.superclass.initComponent.call(this);
        this.btnShow.on('click', this.onbtnShowclick, this);
    },
    onbtnShowclick: function () {
        var nobase = Ext.getCmp('ShowPasienHistoryWin-no_customer').getValue();
        Ext.getCmp('docs-jun.PasienHistoryWin').load({
            url: 'customers/SimpleHistory/nobase/' + nobase,
            scripts: true,
            params: {
                nobase: nobase,
                height: Ext.getCmp('docs-jun.PasienHistoryWin').getHeight() - 100
            }
        });
    }
});
jun.CustomersPhotoWin = Ext.extend(Ext.Window, {
    title: 'Photo',
    modez: 1,
    width: 430,
    height: 530,
    layout: 'fit',
    modal: true,
    id: 'docs-jun.CustomersPhotoWin',
    padding: 5,
    resizable: !1,
    closeForm: false,
    thumb_canvas: null,
    thumb_blob: null,
    photo_canvas: null,
    photo_blob: null,
    myParent: null,
    initComponent: function () {
        this.items = [
            {
                html: '<div id="docs-jun.CustomersPhotoWinPanel" style="display: inline-block;width: 100%;height: 100%" class="customerForm_TakePhotoPreview"></div>'
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Stream',
                    hidden: false,
                    ref: '../btnStream'
                },
                {
                    xtype: 'button',
                    text: 'Capture',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.CustomersPhotoWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnStream.on('click', this.onbtnStreamclick, this);
        this.on('close', this.onClose, this);
    },
    onClose: function () {
        if(jun.camera)
            jun.camera.stop_stream();
    },
    onbtnSaveclick: function () {
        var snapshot = jun.camera.capture();
        var snapshotThumb = jun.camera.capture({
            quality: 0.2
        });
        if (JpegCamera.canvas_supported()) {
            snapshot.get_canvas(function (element) {
                Ext.getCmp('docs-jun.CustomersPhotoWin').photo_canvas = element;
                jun.camera.stop_stream();
                var div = document.getElementById("docs-jun.CustomersPhotoWinPanel");
                div.innerHTML = '';
                div.appendChild(element);
            });
            snapshotThumb.get_canvas(function (element) {
                Ext.getCmp('docs-jun.CustomersPhotoWin').thumb_canvas = element;
            });
            snapshotThumb.get_blob(function (element) {
                Ext.getCmp('docs-jun.CustomersPhotoWin').thumb_blob = element;
            });
            snapshot.get_blob(function (element) {
                Ext.getCmp('docs-jun.CustomersPhotoWin').photo_blob = element;
            });
        }
        else {
            // <canvas> is not supported in this browser. We'll use anonymous
            // graphic instead.
            var image = document.createElement("img");
            image.src = "no_canvas_photo.jpg";
            setTimeout(function () {
                add_snapshot.call(snapshot, image)
            }, 1);
        }
    },
    onbtnStreamclick: function () {
        this.showStream();
    },
    showStream: function () {
        var options = {
            shutter_ogg_url: SHUTTER_OGG_URL,
            shutter_mp3_url: SHUTTER_MP3_URL,
            swf_url: SWF_URL,
            quality: 0.9
        };
        if(jun.camera) jun.camera.audio_context.close();
        jun.camera = new JpegCamera("#docs-jun.CustomersPhotoWinPanel", options);
        jun.camera.show_stream();
    },
    onbtnSaveCloseClick: function () {
        if (this.thumb_canvas != null) {
            this.thumb_canvas.style.width = '100%';
            this.thumb_canvas.style.height = '100%';
            //var div = document.getElementById("docs-jun.CustomersWinPreview");
            var div = document.getElementById(Ext.getCmp('docs-jun.CustomersWin').target_view);
            div.innerHTML = '';
            div.appendChild(this.thumb_canvas);
            
            var readerThumb = new FileReader();
            readerThumb.onload = function () {
                var dataURL = readerThumb.result;
                //Ext.getCmp('docs-jun.CustomersWin').thumb.setValue(dataURL);
                Ext.getCmp('docs-jun.CustomersWin').target_thumb.setValue(dataURL);
            };
            readerThumb.readAsDataURL(this.thumb_blob);
            
            var readerPhoto = new FileReader();
            readerPhoto.onload = function () {
                var dataURL = readerPhoto.result;
                //Ext.getCmp('docs-jun.CustomersWin').photo.setValue(dataURL);
                Ext.getCmp('docs-jun.CustomersWin').target_photo.setValue(dataURL);
            };
            readerPhoto.readAsDataURL(this.photo_blob);
        }
        this.close();
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
//jun.UploadHistoryManual = Ext.extend(Ext.Window, {
//    title: "Upload History Manual",
//    iconCls: "silk13-report",
//    modez: 1,
//    width: 400,
//    height: 170,
//    layout: "form",
//    modal: !0,
//    padding: 5,
//    closeForm: !1,
//    resizable: !1,
//    iswin: !0,
//    initComponent: function () {
//        this.items = [
//            {
//                xtype: "form",
//                frame: !1,
//                bodyStyle: "background-color: #E4E4E4;padding: 10px",
//                id: "form-UploadHistoryManual",
//                labelWidth: 100,
//                labelAlign: "left",
//                layout: "form",
//                ref: "formz",
//                border: !1,
//                items: [
//                    {
//                        fieldLabel: 'From Date',
//                        xtype: 'xdatefield',
//                        format: 'd M Y',
//                        name: "tglfrom",
//                        ref: '../tglfrom',
//                        anchor: "100%"
//                    },
//                    {
//                        fieldLabel: 'To Date',
//                        xtype: 'xdatefield',
//                        format: 'd M Y',
//                        name: "tglto",
//                        ref: '../tglto',
//                        anchor: "100%"
//                    },
////                    {
////                        xtype: 'combo',
////                        typeAhead: true,
////                        triggerAction: 'all',
////                        lazyRender: true,
////                        mode: 'local',
////                        forceSelection: true,
////                        fieldLabel: 'Group',
////                        store: jun.rztGrupCmp,
////                        ref: '../grup',
////                        name: 'grup_id',
////                        valueField: 'nama_grup',
////                        displayField: 'nama_grup',
////                        allowBlank: true,
////                        emptyText: 'ALL Group',
////                        anchor: '100%'
////                    },
//                    {
//                        xtype: "hidden",
//                        name: "format",
//                        ref: "../format"
//                    }
//                ]
//            }
//        ];
//        this.fbar = {
//            xtype: "toolbar",
//            items: [
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_excel",
//                    text: "Eksekusi",
//                    ref: "../btnSave"
//                }
//            ]
//        };
//        jun.UploadHistoryManual.superclass.initComponent.call(this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
//    },
//    onbtnSaveclick: function () {
//            this.btnDisabled(true);
//            var urlz = 'Customers/UploadHistoryManual/';
//            Ext.getCmp('form-UploadHistoryManual').getForm().submit({
//                url: urlz,
//                timeOut: 1000,
//                scope: this,
//                success: function (f, a) {
//                    var response = Ext.decode(a.response.responseText);
//                    Ext.MessageBox.show({
//                        title: 'Info',
//                        msg: response.msg,
//                        buttons: Ext.MessageBox.OK,
//                        icon: Ext.MessageBox.INFO
//                    });
//                },
//                failure: function (f, a) {
//                    switch (a.failureType) {
//                        case Ext.form.Action.CLIENT_INVALID:
//                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
//                            break;
//                        case Ext.form.Action.CONNECT_FAILURE:
//                            Ext.Msg.alert('Failure', 'Ajax communication failed');
//                            break;
//                        case Ext.form.Action.SERVER_INVALID:
//                            Ext.Msg.alert('Failure', a.result.msg);
//                    }
//                }
//            });
//        }
//});
//WOMEN
//appendEPCL('+RIB');
//appendEPCL('+C 4');
//appendEPCL('F');
//appendEPCL('T 50 400 0 1 0 75 1 SUB01 777777');
//appendEPCL('T 50 470 0 1 0 65 1 STEPHANUS NOVE ANANDO');
//appendEPCL('T 170 580 0 1 0 45 1 09 2014');
//appendEPCL('T 650 580 0 1 0 45 1 09 2016');
//appendEPCL('I');
//MEN
//appendEPCL('+RIB');
//appendEPCL('+C 4');
//appendEPCL('F');
//appendEPCL('T 50 450 0 1 0 75 1 SUB01 777777');
//appendEPCL('T 50 520 0 1 0 65 1 STEPHANUS NOVE ANANDO');
//appendEPCL('T 535 575 0 1 0 45 1 09 2014');
//appendEPCL('T 785 575 0 1 0 45 1 09 2016');
//appendEPCL('I');
//girls sampe 19 tahun
//appendEPCL('+RIB');
//appendEPCL('+C 4');
//appendEPCL('F');
//appendEPCL('T 50 450 0 1 0 75 1 SUB01 777777');
//appendEPCL('T 50 520 0 1 0 65 1 STEPHANUS NOVE ANANDO');
//appendEPCL('T 170 575 0 1 0 45 1 09 2016');
//appendEPCL('T 785 575 0 1 0 45 1 09 2014');
//appendEPCL('I');