jun.KasWin = Ext.extend(Ext.Window, {
    title: 'Kas/Bank Masuk',
    modez: 1,
    width: 610,
    height: 465,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Kas',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        //readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Pay Method:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'bank_id',
                        store: jun.rztBankTransCmpPusat,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        value: SYSTEM_BANK_CASH,
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: 'hidden',
                        name: 'store',
                        value: STORE
                    },
//                    {
//                        xtype: "label",
//                        text: "From:",
//                        x: 295,
//                        y: 5
//                    },
//                    {
//                        xtype: 'uctextfield',
//                        name: 'who',
//                        id: 'whoid',
//                        ref: '../who',
//                        maxLength: 100,
//                        x: 400,
//                        y: 2,
//                        height: 20,
//                        width: 175
//                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'keperluan',
                        id: 'keperluanid',
                        ref: '../keperluan',
                        width: 175,
                        height: 52,
                        x: 400,
                        y: 2
                    },
                    new jun.KasDetailGrid({
                        x: 5,
                        y: 95,
                        height: 260,
                        ref: '../kasDetailGrid',
                        frameHeader: !1,
                        header: !1,
                        mode: jun.rztChartMasterCmpPendapatan
                    }),
//                    {
//                        xtype: 'checkbox',
//                        boxLabel: '<b>M  O  D  A  L</b>',
////                        height: 30,
//                        hideLabel: false,
//                        value: 1,
//                        inputValue: 1,
//                        uncheckedValue: 0,
//                        name: 'modal',
//                        ref: '../modal',
//                        x: 5,
//                        y: 365
//                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 295,
                        y: 365
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'amount',
                        id: 'amount_kasid',
                        ref: '../amount',
                        maxLength: 30,
                        readOnly: true,
                        width: 175,
                        x: 400,
                        y: 362
                    },
                    {
                        xtype: 'hidden',
                        id: 'total_debit_kasid',
                        name: 'total_debit',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        id: 'total_kredit_kasid',
                        name: 'total_kredit',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        name: 'arus',
                        id: 'arus_kasid',
                        value: '1'
                    },
                    {
                        xtype: 'hidden',
                        name: 'store',
                        value: STORE
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.KasWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        this.tgl.setReadOnly(!EDIT_TGL);
    },
    onWinClose: function () {
        this.kasDetailGrid.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (this.kasDetailGrid.store.data.length === 0) {
            Ext.MessageBox.alert("Error", "Must have at least 1 item details!");
            this.btnDisabled(!1);
            return;
        }
        print(PRINTER_RECEIPT, __openCashDrawer);
        var urlz = 'Kas/create/';
        Ext.getCmp('form-Kas').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    this.kasDetailGrid.store.data.items, "data")),
                id: this.idkas,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztKas.reload();
                var response = Ext.decode(a.response.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    console.log("Printer not ready");
                } else {
                    //qz.append(chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r");
                    //qz.print();
                    //while (!qz.isDonePrinting()) {}
                    //qz.appendHTML('<html><pre>'+response.print+'</pre></html>');
                    //qz.printHTML();
                    // opencashdrawer();
                    // printHTML(PRINTER_RECEIPT, '<html><pre>' + response.print + '</pre></html>');
                    var msg = [{type: 'raw', data: response.print}];
                    var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                    print(PRINTER_RECEIPT, printData);
                }
                if (this.modez == 0) {
                    Ext.getCmp('form-Kas').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.KasWinOut = Ext.extend(Ext.Window, {
    title: 'Kas/Bank Keluar',
    modez: 1,
    width: 610,
    height: 495,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Kas',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        //readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Pay Method:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'bank_id',
                        store: jun.rztBankTransCmpPusat,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        value: SYSTEM_PETTY_CASH,
                        readOnly: true,
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Receipt No:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_kwitansi',
                        id: 'no_kwitansiid',
                        ref: '../no_kwitansi',
                        maxLength: 20,
                        width: 175,
                        x: 85,
                        y: 92
                    },
                    {
                        xtype: 'hidden',
                        name: 'store',
                        value: STORE
                    },
//                    {
//                        xtype: "label",
//                        text: "To:",
//                        x: 295,
//                        y: 5
//                    },
//                    {
//                        xtype: 'uctextfield',
//                        name: 'who',
//                        id: 'whoid',
//                        ref: '../who',
//                        maxLength: 100,
//                        x: 400,
//                        y: 2,
//                        height: 20,
//                        width: 175
//                    },
                    {
                        xtype: "label",
                        text: "Type:",
                        x: 295,
                        y: 5
                    },
                    new jun.cmbJenisKas({
                        x: 400,
                        y: 2,
                        height: 20,
                        width: 175
                    }),
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textarea',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'keperluan',
                        id: 'keperluanid',
                        ref: '../keperluan',
                        width: 175,
                        height: 52,
                        x: 400,
                        y: 32
                    },
                    new jun.KasDetailGrid({
                        x: 5,
                        y: 125,
                        height: 260,
                        ref: '../kasDetailGrid',
                        frameHeader: !1,
                        header: !1,
                        mode: jun.rztChartMasterCmpBiaya
                    }),
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 295,
                        y: 395
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'amount',
                        id: 'amount_kasid',
                        ref: '../amount',
                        maxLength: 30,
                        readOnly: true,
                        width: 175,
                        x: 400,
                        y: 392
                    },
                    {
                        xtype: 'hidden',
                        id: 'total_debit_kasid',
                        name: 'total_debit',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        id: 'total_kredit_kasid',
                        name: 'total_kredit',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        name: 'arus',
                        id: 'arus_kasid',
                        value: '-1'
                    },
                    {
                        xtype: 'hidden',
                        name: 'store',
                        value: STORE
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.KasWinOut.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        this.tgl.setReadOnly(!EDIT_TGL);
        //if (this.modez == 0 || this.modez == 2) {
        //    Ext.Ajax.request({
        //        url: 'site/GetDateTime',
        //        method: 'POST',
        //        scope: this,
        //        success: function (f, a) {
        //            var response = Ext.decode(f.responseText);
        //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
        //        },
        //        failure: function (f, a) {
        //            switch (a.failureType) {
        //                case Ext.form.Action.CLIENT_INVALID:
        //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                    break;
        //                case Ext.form.Action.CONNECT_FAILURE:
        //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
        //                    break;
        //                case Ext.form.Action.SERVER_INVALID:
        //                    Ext.Msg.alert('Failure', a.result.msg);
        //            }
        //        }
        //    });
        //}
    },
    onWinClose: function () {
        this.kasDetailGrid.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (this.kasDetailGrid.store.data.length === 0) {
            Ext.MessageBox.alert("Error", "Item details must set.");
            this.btnDisabled(!1);
            return;
        }
        print(PRINTER_RECEIPT, __openCashDrawer);
        var urlz = 'Kas/create/';
        Ext.getCmp('form-Kas').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    this.kasDetailGrid.store.data.items, "data")),
                id: this.idkas,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztKasKeluar.reload();
                var response = Ext.decode(a.response.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    console.log("Printer not ready");
                } else {
                    //qz.append(chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r");
                    //qz.print();
                    //while (!qz.isDonePrinting()) {}
                    //qz.appendHTML('<html><pre>'+response.print+'</pre></html>');
                    //qz.printHTML();
                    // opencashdrawer();
                    // printHTML(PRINTER_RECEIPT, '<html><pre>' + response.print + '</pre></html>');
                    var msg = [{type: 'raw', data: response.print}];
                    var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                    print(PRINTER_RECEIPT, printData);
                }
                if (this.modez == 0) {
                    Ext.getCmp('form-Kas').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.KasWinPusat = Ext.extend(Ext.Window, {
    title: 'Deposit',
    modez: 1,
    width: 900,
    height: 495,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Kas',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        //readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        x: 85,
                        y: 32,
                        minValue: RES_DATE
                    },
                    {
                        xtype: "label",
                        text: "Pay Method:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'bank_id',
                        store: jun.rztBankTransCmpPusat,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Amount:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        id: 'amount_kaspusatid',
                        name: 'amount',
                        ref: '../amount',
                        maxLength: 30,
                        value: 0,
                        width: 175,
                        x: 85,
                        y: 92
                    },
//                    {
//                        xtype: "label",
//                        text: "From:",
//                        x: 295,
//                        y: 5
//                    },
//                    {
//                        xtype: 'uctextfield',
//                        name: 'who',
//                        id: 'whoid',
//                        ref: '../who',
//                        maxLength: 100,
//                        x: 400,
//                        y: 2,
//                        height: 20,
//                        width: 175
//                    },
                    {
                        xtype: "label",
                        text: "Branch:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        readOnly: !HEADOFFICE,
                        width: 175,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textarea',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'keperluan',
                        id: 'keperluanid',
                        ref: '../keperluan',
                        width: 175,
                        height: 52,
                        x: 400,
                        y: 32
                    },
                    //{
                    //    xtype: "label",
                    //    text: "Store:",
                    //    x: 295,
                    //    y: 65
                    //},
                    //{
                    //    xtype: 'combo',
                    //    //typeAhead: true,
                    //    fieldLabel: 'Branch',
                    //    ref: '../cmbStore',
                    //    triggerAction: 'all',
                    //    lazyRender: true,
                    //    mode: 'local',
                    //    store: jun.rztStoreCmp,
                    //    hiddenName: 'store',
                    //    name: 'store',
                    //    valueField: 'store_kode',
                    //    displayField: 'store_kode',
                    //    allowBlank: false,
                    //    width: 175,
                    //    x: 400,
                    //    y: 62
                    //},
                    new jun.KasDetailPusatGrid({
                        frameHeader: !1,
                        header: !1,
                        mode: jun.rztChartMasterCmp,
                        ref: "../kasDetailGrid",
                        x: 5,
                        y: 122,
                        height: 230
                    }),
                    {
                        xtype: 'checkbox',
                        boxLabel: "UNTUK ALL CABANG",
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: "all_store",
                        x: 5,
                        y: 362
                    },
                    {
                        xtype: "label",
                        text: "Total Debit:",
                        x: 295,
                        y: 365
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total_debit',
                        id: 'total_debit_kasid',
                        ref: '../total_debit',
                        maxLength: 30,
                        readOnly: true,
                        width: 175,
                        x: 400,
                        y: 362
                    },
                    {
                        xtype: "label",
                        text: "Total Kredit:",
                        x: 295,
                        y: 395
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total_kredit',
                        id: 'total_kredit_kasid',
                        ref: '../total_kredit',
                        maxLength: 30,
                        readOnly: true,
                        width: 175,
                        x: 400,
                        y: 392
                    },
                    {
                        xtype: 'hidden',
                        name: 'arus',
                        id: 'arus_kasid',
                        value: '1'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.KasWinPusat.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        //this.store.on("select", this.onStoreSelect, this);
        this.amount.on("change", this.onAmountChange, this);
        //this.cmbStore.on("select", this.onCmbStoreSelect, this);
        this.on("close", this.onWinClose, this);
        this.tgl.setReadOnly(!EDIT_TGL);
        //if (this.modez == 0) {
        //    Ext.Ajax.request({
        //        url: 'site/GetDateTime',
        //        method: 'POST',
        //        scope: this,
        //        success: function (f, a) {
        //            var response = Ext.decode(f.responseText);
        //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
        //        },
        //        failure: function (f, a) {
        //            switch (a.failureType) {
        //                case Ext.form.Action.CLIENT_INVALID:
        //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                    break;
        //                case Ext.form.Action.CONNECT_FAILURE:
        //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
        //                    break;
        //                case Ext.form.Action.SERVER_INVALID:
        //                    Ext.Msg.alert('Failure', a.result.msg);
        //            }
        //        }
        //    });
        //}
    },
    onActivate: function () {
        if (this.modez !== 0) {
            this.btnSaveClose.setVisible(false);
        }
    },
    onWinClose: function () {
        this.kasDetailGrid.store.removeAll();
        jun.rztBankTransCmpPusat.clearFilter();
    },
    onAmountChange: function (c, n, o) {
        this.total_debit.setValue(n + this.kasDetailGrid.store.sum("debit"));
    },
    onStoreSelect: function (combo, record, index) {
        this.kasDetailGrid.store.each(function (r) {
            r.set('store', combo.getValue());
        });
        this.kasDetailGrid.store.commitChanges();
    },
    onCmbStoreSelect: function (combo, record, index) {
        jun.rztBankTransCmpPusat.filter([
            {
                property: 'store',
                value: record.data.store_kode,
                anyMatch: false
            }]);
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (this.kasDetailGrid.store.data.length === 0) {
            Ext.MessageBox.alert("Error", "Item details must set.");
            this.btnDisabled(!1);
            return;
        }
        if (this.total_debit.getValue() != this.total_kredit.getValue()) {
            Ext.MessageBox.alert("Error", "Total Debit must be equal to Total Kredit.");
            this.btnDisabled(!1);
            return;
        }
        var urlz = 'Kas/create/';
        Ext.getCmp('form-Kas').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    this.kasDetailGrid.store.data.items, "data")),
                id: this.idkas,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztKasPusat.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Kas').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.KasWinPusatOut = Ext.extend(Ext.Window, {
    title: 'Expenses',
    modez: 1,
    width: 900,
    height: 525,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Kas',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        //readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        x: 85,
                        y: 32,
                        minValue: REST_START,
                        maxValue: REST_END
                    },
                    {
                        xtype: "label",
                        text: "Pay Method:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'bank_id',
                        store: jun.rztBankTransCmpPusat,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Amount:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        id: 'amount_kaspusatid',
                        name: 'amount',
                        ref: '../amount',
                        maxLength: 30,
                        value: 0,
                        width: 175,
                        x: 85,
                        y: 92
                    },
                    {
                        xtype: "label",
                        text: "Receipt No:",
                        x: 5,
                        y: 125
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_kwitansi',
                        id: 'no_kwitansiid',
                        ref: '../no_kwitansi',
                        maxLength: 20,
                        width: 175,
                        x: 85,
                        y: 122
                    },
//                    {
//                        xtype: "label",
//                        text: "To:",
//                        x: 295,
//                        y: 5
//                    },
//                    {
//                        xtype: 'uctextfield',
//                        name: 'who',
//                        id: 'whoid',
//                        ref: '../who',
//                        maxLength: 100,
//                        x: 400,
//                        y: 2,
//                        height: 20,
//                        width: 175
//                    },
                    {
                        xtype: "label",
                        text: "Branch:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        readOnly: !HEADOFFICE,
                        width: 175,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Type:",
                        x: 295,
                        y: 35
                    },
                    new jun.cmbJenisKas({
                        x: 400,
                        y: 32,
                        height: 20,
                        width: 175
                    }),
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'keperluan',
                        id: 'keperluanid',
                        ref: '../keperluan',
                        width: 175,
                        height: 52,
                        x: 400,
                        y: 62
                    },
                    //{
                    //    xtype: "label",
                    //    text: "Store:",
                    //    x: 295,
                    //    y: 92
                    //},
                    //{
                    //    xtype: 'combo',
                    //    //typeAhead: true,
                    //    fieldLabel: 'Branch',
                    //    ref: '../cmbStore',
                    //    triggerAction: 'all',
                    //    lazyRender: true,
                    //    mode: 'local',
                    //    store: jun.rztStoreCmp,
                    //    hiddenName: 'store',
                    //    name: 'store',
                    //    valueField: 'store_kode',
                    //    displayField: 'store_kode',
                    //    allowBlank: false,
                    //    width: 175,
                    //    x: 400,
                    //    y: 92
                    //},
                    new jun.KasDetailPusatGrid({
                        frameHeader: !1,
                        header: !1,
                        mode: jun.rztChartMasterCmp,
                        ref: "../kasDetailGrid",
                        x: 5,
                        y: 152,
                        height: 230
                    }),
                    {
                        xtype: 'checkbox',
                        boxLabel: "UNTUK ALL CABANG",
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: "all_store",
                        x: 5,
                        y: 392
                    },
                    {
                        xtype: "label",
                        text: "Total Debit:",
                        x: 295,
                        y: 395
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total_debit',
                        id: 'total_debit_kasid',
                        ref: '../total_debit',
                        maxLength: 30,
                        readOnly: true,
                        width: 175,
                        x: 400,
                        y: 392
                    },
                    {
                        xtype: "label",
                        text: "Total Kredit:",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total_kredit',
                        id: 'total_kredit_kasid',
                        ref: '../total_kredit',
                        maxLength: 30,
                        readOnly: true,
                        width: 175,
                        x: 400,
                        y: 422
                    },
                    {
                        xtype: 'hidden',
                        name: 'arus',
                        id: 'arus_kasid',
                        value: '-1'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.KasWinPusatOut.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        //this.store.on("select", this.onStoreSelect, this);
        this.amount.on("change", this.onAmountChange, this);
        //this.cmbStore.on("select", this.onCmbStoreSelect, this);
        this.on("close", this.onWinClose, this);
        this.tgl.setReadOnly(!EDIT_TGL);
        //if (this.modez == 0) {
        //    Ext.Ajax.request({
        //        url: 'site/GetDateTime',
        //        method: 'POST',
        //        scope: this,
        //        success: function (f, a) {
        //            var response = Ext.decode(f.responseText);
        //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
        //        },
        //        failure: function (f, a) {
        //            switch (a.failureType) {
        //                case Ext.form.Action.CLIENT_INVALID:
        //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                    break;
        //                case Ext.form.Action.CONNECT_FAILURE:
        //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
        //                    break;
        //                case Ext.form.Action.SERVER_INVALID:
        //                    Ext.Msg.alert('Failure', a.result.msg);
        //            }
        //        }
        //    });
        //}
    },
    onActivate: function () {
        if (this.modez !== 0) {
            this.btnSaveClose.setVisible(false);
        }
    },
    onWinClose: function () {
        this.kasDetailGrid.store.removeAll();
    },
    onAmountChange: function (c, n, o) {
        this.total_kredit.setValue(n + this.kasDetailGrid.store.sum("kredit"));
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    onStoreSelect: function (combo, record, index) {
        this.kasDetailGrid.store.each(function (r) {
            r.set('store', combo.getValue());
        });
        this.kasDetailGrid.store.commitChanges();
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (this.kasDetailGrid.store.data.length === 0) {
            Ext.MessageBox.alert("Error", "Item details must set.");
            this.btnDisabled(!1);
            return;
        }
        if (this.total_debit.getValue() != this.total_kredit.getValue()) {
            Ext.MessageBox.alert("Error", "Total Debit must be equal to Total Kredit.");
            this.btnDisabled(!1);
            return;
        }
        var urlz = 'Kas/create/';
        Ext.getCmp('form-Kas').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    this.kasDetailGrid.store.data.items, "data")),
                id: this.idkas,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztKasPusatKeluar.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Kas').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});