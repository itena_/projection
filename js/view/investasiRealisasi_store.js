jun.InvestasiRealisasistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.InvestasiRealisasistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'InvestasiRealisasiStoreId',
            url: 'projection/InvestasiRealisasi',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name:'investasi_id'},
                {name:'account_id'},
                {name:'account_code'},
                {name:'account_name'},
                {name:'name'},
                {name:'description'},
                {name:'qty'},
                {name:'amount'},
                {name:'tdate'},
                {name:'tdatef'},
                {name:'total'},
                {name:'created_at'},
                {name:'businessunit_id'},
                {name:'uploadfile'},
                {name:'uploaddate'}
                
            ]
        }, cfg));
    }
});
jun.rztInvestasiRealisasi = new jun.InvestasiRealisasistore();
//jun.rztInvestasiRealisasi.load();
