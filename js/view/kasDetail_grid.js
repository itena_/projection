jun.KasDetailPusatGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Kas Detail",
    id: "docs-jun.KasDetailPusatGrid",
    stripeRows: true,
    viewConfig: {
        forceFit: !0
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: !0
    }),
    columns: [
        {
            header: "Accounts Code",
            resizable: !0,
            dataIndex: "account_code",
            width: 100
        },
        {
            header: "Accounts Name",
            resizable: !0,
            dataIndex: "account_code",
            width: 250,
            renderer: jun.renderCoa
        },
        {
            header: "Note",
            resizable: !0,
            dataIndex: "item_name",
            width: 250
        },
        {
            header: "Debit",
            resizable: !0,
            dataIndex: "debit",
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            width: 100
        },
        {
            header: "Kredit",
            resizable: !0,
            dataIndex: "kredit",
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            width: 100
        },
        {
            header: "Branch",
            resizable: !0,
            dataIndex: "store",
            width: 75
        }
    ],
    initComponent: function () {
        this.store = jun.rztKasDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'COA :'
                        },
                        {
                            xtype: 'combo',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            enableKeyEvents: true,
                            forceSelection: true,
                            fieldLabel: 'COA',
                            store: jun.rztChartMasterCmp,
                            ref: '../../account_code',
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                                "</div></tpl>"),
                            listWidth: 500,
                            valueField: 'account_code',
                            displayField: 'account_code',
                            width: 150
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Debit :'
                        },
                        {
                            xtype: 'numericfield',
                            //id: 'debitid',
                            ref: '../../debit',
                            width: 75,
                            value: 0,
                            enableKeyEvents: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Kredit :'
                        },
                        {
                            xtype: 'numericfield',
                            //id: 'kreditid',
                            ref: '../../kredit',
                            width: 75,
                            value: 0,
                            enableKeyEvents: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Branch :'
                        },
                        {
                            xtype: 'combo',
                            fieldLabel: 'Branch',
                            ref: '../../store_',
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            store: jun.rztStoreCmp,
                            // hiddenName: 'store',
                            // name: 'store',
                            valueField: 'store_kode',
                            displayField: 'store_kode',
                            allowBlank: false,
                            width: 75
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Area :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztAreaCmp,
                            ref: '../../area',
                            valueField: 'area_id',
                            displayField: 'area_name',
                            readOnly: !HEADOFFICE,
                            width: 100
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../item_name',
                            width: 660,
                            colspan: 9,
                            maxLength: 150
                        },
                        {
                            xtype: 'numericfield',
                            //id: 'totalid',
                            ref: '../../total',
                            width: 75,
                            value: 0,
                            hidden: true
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.KasDetailPusatGrid.superclass.initComponent.call(this);
        this.btnAdd.on("Click", this.loadForm, this);
        this.btnEdit.on("Click", this.onClickbtnEdit, this);
        this.btnDelete.on("Click", this.deleteRec, this);
        this.kredit.on('keyup', this.kreditOnChange, this);
        this.debit.on('keyup', this.debitOnChange, this);
        this.store_.on('select', this.storeOnSelect, this);
        this.area.on('select', this.areaOnSelect, this);
        this.getSelectionModel().on("rowselect", this.getrow, this);
        this.store.removeAll();
    },
    storeOnSelect: function (t, e) {
        this.area.clearValue();
    },
    areaOnSelect: function (t, e) {
        this.store_.clearValue();
    },
    kreditOnChange: function (t, e) {
        if (t.getValue() != 0) {
            this.debit.setValue('0');
        }
        this.total.setValue(-t.getValue());
    },
    debitOnChange: function (t, e) {
        if (t.getValue() != 0) {
            this.kredit.setValue('0');
        }
        this.total.setValue(t.getValue());
    },
    getrow: function (a, b, c) {
        this.record = c;
        var d = this.sm.getSelections();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    resetForm: function (s) {
        this.account_code.reset();
        this.item_name.reset();
        this.store_.reset();
        this.total.reset();
        this.kredit.reset();
        this.debit.reset();
        this.store_.reset();
        this.area.reset();
        this.btnDisable(false);
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.account_code.setValue(record.data.account_code);
            this.item_name.setValue(record.data.item_name);
            this.store_.setValue(record.data.store);
            this.total.setValue(record.data.total);
            this.kredit.setValue(record.data.kredit);
            this.debit.setValue(record.data.debit);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadForm: function () {
        this.btnDisable(true);
        
        var account_code = this.account_code.getValue();
        var item_name = this.item_name.getValue();
        var store = this.store_.getValue();
        var area = this.area.getValue();
        
        if (!account_code) {
            Ext.MessageBox.alert("Error", "COA must be selected.");
            this.btnDisable(false);
            return;
        }
        if (!item_name) {
            Ext.MessageBox.alert("Error", "Note can not be blank.");
            this.btnDisable(false);
            return;
        }
        if (!store && !area) {
            Ext.MessageBox.alert("Error", "Branch/area must be selected.");
            this.btnDisable(false);
            return;
        }
        
        var total = parseFloat(this.total.getValue());
        var kredit = parseFloat(this.kredit.getValue());
        var debit = parseFloat(this.debit.getValue());
        
        if (store) {
            this.save({
                account_code: account_code,
                item_name: item_name,
                store: store,
                debit: debit,
                kredit: kredit,
                total: total
            });
        } else if (area) {
            var arrStore = new jun.StoreAreastore();
            arrStore.load({
                params: { area_id: area },
                scope: this,
                callback: function(r){
                    var account_code = this.account_code.getValue();
                    var item_name = this.item_name.getValue();
                    var totalKredit = parseFloat(this.kredit.getValue());
                    var totalDebit = parseFloat(this.debit.getValue());
                    var totalTotal = totalKredit? -totalKredit : totalDebit;

                    var lastStore = '',
                        kredit = 0,
                        debit = 0,
                        total = 0,
                        sumKredit = 0,
                        sumDebit = 0,
                        sumTotal = 0;

                    
                    (totalKredit > 0) && (kredit = jun.roundDown(totalKredit / r.length,2));
                    (totalDebit > 0) && (debit = jun.roundDown(totalDebit / r.length,2));
                    total = kredit? -kredit : debit;

                    for(i=0; i<r.length; i++){
                        lastStore = r[i].get('store');
                        this.save({
                            account_code: account_code,
                            item_name: item_name,
                            store: lastStore,
                            debit: debit,
                            kredit: kredit,
                            total: total
                        });
                        sumKredit += kredit;
                        sumDebit += debit;
                        sumTotal += total;
                    }

                    /* save rounding */
                    var roundingTotal = totalTotal - sumTotal;
                    if(roundingTotal){
                        this.save({
                            account_code: COA_ROUNDING,
                            item_name: 'ROUNDING : '+item_name,
                            store: lastStore,
                            debit: roundingTotal>0?Math.abs(roundingTotal):0,
                            kredit: roundingTotal<0?Math.abs(roundingTotal):0,
                            total: roundingTotal
                        });
                    }
                }
            });
        }
    },
    save: function(data){
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('account_code', data.account_code);
            record.set('item_name', data.item_name);
            record.set('store', data.store);
            record.set('debit', data.debit);
            record.set('kredit', data.kredit);
            record.set('total', data.total);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c(data);
            this.store.add(d);
        }
        
        this.resetForm();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm("Pertanyaan", "Apakah anda yakin ingin menghapus data ini?", this.deleteRecYes, this);
    },
    deleteRecYes: function (a) {
        if (a == "no") return;
        var b = this.sm.getSelected();
        if (b == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Item");
            return;
        }
        this.store.remove(b);
    }
});


jun.KasDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "KasDetail",
    id: 'docs-jun.KasDetailGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'COA',
            resizable: true,
            dataIndex: 'account_code',
            menuDisabled: true,
            width: 100
        },
        {
            header: 'Note',
            resizable: true,
            dataIndex: 'item_name',
            menuDisabled: true,
            width: 100
        },
        {
            header: 'Total',
            resizable: true,
            dataIndex: 'debit',
            menuDisabled: true,
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztKasDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 6,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'COA :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            forceSelection: true,
                            fieldLabel: 'COA',
                            //store: this.mode,
                            store: jun.rztChartMasterCmp,
                            ref: '../../account_code',
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                                "</div></tpl>"),
                            listWidth: 500,
                            hiddenName: 'account_code',
                            valueField: 'account_code',
                            displayField: 'account_code',
                            width: 150
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Total :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'totalid',
                            ref: '../../total',
                            width: 75,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Branch :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztStoreCmp,
                            ref: '../../branch',
                            valueField: 'store_kode',
                            displayField: 'store_kode',
                            value: STORE,
                            readOnly: !HEADOFFICE,
                            width: 80
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'uctextfield',
                            ref: '../../item',
                            width: 400,
                            colspan: 5,
                            maxLength: 100
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.KasDetailGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on('afterrender', this.afterrender, this);
    },
    afterrender: function () {
        this.getColumnModel().setDataIndex(2, (Ext.getCmp('arus_kasid').getValue()==1?'kredit':'debit'));
    },
    onStoreChange: function () {
        this.store.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var item_name = this.item.getValue();
        var account_code = this.account_code.getValue();
        var total = parseFloat(this.total.getValue());
        var branch = this.branch.getValue();
        if (account_code == "" || account_code == undefined) {
            Ext.MessageBox.alert("Error", "COA must selected.");
            return;
        }
        if (branch == "" || branch == undefined) {
            Ext.MessageBox.alert("Error", "Branch must selected.");
            return;
        }
        if (total < 0) {
            Ext.MessageBox.alert("Error", "Total must greater than 0.");
            return;
        }
        
        var arus = Ext.getCmp('arus_kasid').getValue();
        
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('account_code', account_code);
            record.set('item_name', item_name);
            if(arus==1){
                record.set('kredit', total);
                record.set('debit', 0);
            }else{
                record.set('kredit', 0);
                record.set('debit', total);
            }
            record.set('store', branch);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = arus==1?
                new c({
                    account_code: account_code,
                    item_name: item_name,
                    kredit: total,
                    debit: 0,
                    store: branch
                })
                :
                new c({
                    account_code: account_code,
                    item_name: item_name,
                    kredit: 0,
                    debit: total,
                    store: branch
                });
            this.store.add(d);

        }
        this.account_code.reset();
        this.item.reset();
        this.total.reset();
        this.branch.reset();
    },
    btnDisable: function (s){
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        var arus = Ext.getCmp('arus_kasid').getValue();
        if (btn.text == 'Edit') {
            this.account_code.setValue(record.data.account_code);
            this.item.setValue(record.data.item_name);
            this.total.setValue(arus==1?record.data.kredit:record.data.debit);
            this.branch.setValue(record.data.store);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }
});
