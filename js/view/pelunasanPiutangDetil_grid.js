jun.PelunasanPiutangDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PelunasanPiutangDetil",
    id: 'docs-jun.PelunasanPiutangDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        //{
        //    header: 'Invoice Date',
        //    sortable: true,
        //    resizable: true,
        //    dataIndex: 'tgl',
        //    width: 100
        //},
        {
            header: 'Invoice No.',
            sortable: true,
            resizable: true,
            dataIndex: 'no_faktur',
            width: 100
        },
        {
            header: 'Invoice Value',
            sortable: true,
            resizable: true,
            dataIndex: 'nilai',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Payment',
            sortable: true,
            resizable: true,
            dataIndex: 'kas_dibayar',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Remains',
            sortable: true,
            resizable: true,
            dataIndex: 'sisa',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztPelunasanPiutangDetil;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    text: 'Invoice No:\xA0'
                },
                {
                    xtype: "combo",
                    typeAhead: !0,
                    triggerAction: "all",
                    lazyRender: !0,
                    mode: "local",
                    store: jun.rztFakturPiutang,
                    forceSelection: !0,
                    valueField: "transfer_item_id",
                    matchFieldWidth: !1,
                    itemSelector: "div.search-item",
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                        // '<span>{no_faktur} | Tgl: {tgl} | Nilai: {nilai:number("0,0.00")} | Sisa: {sisa:number("0,0.00")}</span>',
                        '<span>Doc.ref: {doc_ref}</span>','<span> | Invoice: {no_faktur}</span>','<span> | Date: {tgl:date("M j, Y")}</span><br>',
                        '<span>Value: {nilai:number("0,0.00")}</span>','<span>| Remains: {sisa:number("0,0.00")}</span>',
                        "</div></tpl>"),
                    displayField: "no_faktur",
                    listWidth: 650,
                    editable: !0,
                    ref: "../jual",
                    lastQuery: ""
                },
                {
                    xtype: 'label',
                    text: '\xA0Payment Total :\xA0'
                },
                {
                    xtype: 'numericfield',
                    ref: '../kas',
                    value: 0,
                    width: 100
                },
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    ref: '../btnDel'
                }
            ]
        };
        jun.PelunasanPiutangDetilGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadEditForm, this);
        this.btnDel.on('Click', this.deleteRec, this);
        this.jual.on('select', this.selectJual, this);
        this.store.on('add', this.onStoreChange, this);
        this.store.on('remove', this.onStoreChange, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    selectJual: function( combo, record, index){
        this.kas.setValue(record.data.sisa);
    },
    onStoreChange: function () {
        jun.rztPelunasanPiutangDetil.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PelunasanPiutangDetilWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var jual = this.jual.getValue();
        if (jual == "") {
            Ext.MessageBox.alert("Error", "Invoice must selected.");
            return
        }
        var kas = parseFloat(this.kas.getValue());
        if (kas < 0) {
            Ext.MessageBox.alert("Error", "Payment total must more than 0.");
            return;
        }
        var faktur_id = jun.rztFakturPiutang.findExact('transfer_item_id', jual);
        if (faktur_id == -1) {
            Ext.MessageBox.alert("Error", "Fatal Error.");
            return
        }
        var faktur = jun.rztFakturPiutang.getAt(faktur_id);
        var a = this.store.findExact("transfer_item_id", faktur.data.transfer_item_id);
        if (a > -1) {
            Ext.MessageBox.alert("Error", "Invoice already added.");
            return;
        }
        if (kas > 0 && kas > faktur.data.sisa) {
            Ext.MessageBox.alert("Error", "Payment total can't more than invoice total.");
            return;
        }
        var sisa = faktur.data.sisa - kas;
        var c = jun.rztPelunasanPiutangDetil.recordType,
            d = new c({
                transfer_item_id: faktur.data.transfer_item_id,
                kas_dibayar: kas,
                tgl: faktur.data.tgl,
                no_faktur: faktur.data.no_faktur,
                nilai: faktur.data.nilai,
                sisa: sisa
            });
        jun.rztPelunasanPiutangDetil.add(d);
        this.jual.reset();
        this.kas.reset();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Invoive must selected.");
            return;
        }
        Ext.MessageBox.confirm('Confirmation', 'Are you sure want delete invoice payment?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
    }
});