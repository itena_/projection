jun.LahaImportBankFeeGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "LahaImportBankFee",
    id: 'docs-jun.LahaImportBankFeeGrid',
    // iconCls: "silk-grid",
    // viewConfig: {
    //     forceFit: true
    // },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    view: new Ext.grid.GroupingView({
        forceFit: true,
        groupTextTpl: '({[values.rs.length]} {[values.rs.length > 1 ? "Banks" : "Bank"]})',
        showGroupName: false,
        enableNoGroups: false,
        enableGroupingMenu: false,
        hideGroupedColumn: true
    }),
    plugins: new Ext.ux.grid.GroupSummary(this),
    columns: [
        {
            header: 'laha_import_bank_fee_id',
            sortable: true,
            resizable: true,
            dataIndex: 'laha_import_bank_fee_id'
        },
        {
            header: 'Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'bank_id',
            renderer: jun.renderBank
        },
        {
            header: 'Nominal',
            sortable: true,
            resizable: true,
            dataIndex: 'bank',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            summaryType: 'sum'
        }
    ],
    initComponent: function () {
        this.store = jun.rztLahaImportBankFee;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Bank :'
                },
                {
                    xtype: 'combo',
                    //typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    ref: '../bank_id',
                    store: jun.rztBankTransCmpPusat,
                    valueField: 'bank_id',
                    width: 150,
                    displayField: 'nama_bank'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Nominal :'
                },
                {
                    xtype: 'numericfield',
                    ref: '../bank_amount',
                    width: 75,
                    value: 0,
                    minValue: 0
                },
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    ref: '../btnDelete'
                }
            ]
        };
        jun.LahaImportBankFeeGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        jun.rztLahaImportBankFee.on('add', this.refreshData, this);
        jun.rztLahaImportBankFee.on('update', this.refreshData, this);
        jun.rztLahaImportBankFee.on('remove', this.refreshData, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    refreshData: function (t, r) {
        var total = t.sum("bank");
        var piutang = parseFloat(Ext.getCmp('p_d_piutangcard_n_id').getValue());
        var fee = piutang - total;
        Ext.getCmp('piu_d_total_bank_id').setValue(total);
        Ext.getCmp('piu_d_fee_n_id').setValue(fee);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var bank_id = this.bank_id.getValue();
        var bank_amount = parseFloat(this.bank_amount.getValue());
        if (bank_id == "" || bank_id == undefined) {
            Ext.MessageBox.alert("Error", "Bank harus dipilih.");
            return;
        }
        if (bank_id == "" || bank_id == undefined) {
            Ext.MessageBox.alert("Error", "Bank harus dipilih.");
            return;
        }
        if (bank_amount <= 0) {
            Ext.MessageBox.alert("Error", "Nominal Bank harus lebih dari 0.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('bank_id', bank_id);
            record.set('bank', bank_amount);
            record.commit();
        } else {
            var c = jun.rztLahaImportBankFee.recordType,
                d = new c({
                    bank_id: bank_id,
                    bank: bank_amount
                });
            jun.rztLahaImportBankFee.add(d);
        }
        this.bank_id.reset();
        this.bank_amount.reset();
    },
    btnDisable: function (ss) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.bank_id.setValue(record.data.bank_id);
            this.bank_amount.setValue(record.data.bank);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }
});
jun.LahaImportBankSetorGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "LahaImportBankSetor",
    id: 'docs-jun.LahaImportBankSetorGrid',
    // iconCls: "silk-grid",
    // viewConfig: {
    //     forceFit: true
    // },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    view: new Ext.grid.GroupingView({
        forceFit: true,
        groupTextTpl: '({[values.rs.length]} {[values.rs.length > 1 ? "Banks" : "Bank"]})',
        showGroupName: false,
        enableNoGroups: false,
        enableGroupingMenu: false,
        hideGroupedColumn: true
    }),
    plugins: new Ext.ux.grid.GroupSummary(this),
    columns: [
        {
            header: 'laha_import_bank_fee_id',
            sortable: true,
            resizable: true,
            dataIndex: 'laha_import_bank_fee_id',
            width: 100
        },
        {
            header: 'Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'bank_id',
            renderer: jun.renderBank,
            width: 100
        },
        {
            header: 'No Slip',
            sortable: true,
            resizable: true,
            dataIndex: 'note_',
            width: 100
        },
        {
            header: 'Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'bank',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            summaryType: 'sum'
        }
    ],
    initComponent: function () {
        this.store = jun.rztLahaImportBankSetor;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Bank :'
                },
                {
                    xtype: 'combo',
                    //typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    ref: '../bank_id',
                    store: jun.rztBankTransCmpPusat,
                    valueField: 'bank_id',
                    width: 150,
                    displayField: 'nama_bank'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'No Slip :'
                },
                {
                    xtype: 'textfield',
                    hideLabel: false,
                    //hidden:true,
                    ref: '../note_',
                    width: 75,
                    maxLength: 600
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Amount :'
                },
                {
                    xtype: 'numericfield',
                    ref: '../bank_amount',
                    width: 75,
                    value: 0,
                    minValue: 0
                },
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    ref: '../btnDelete'
                }
            ]
        };
        jun.LahaImportBankFeeGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        jun.rztLahaImportBankSetor.on('add', this.refreshData, this);
        jun.rztLahaImportBankSetor.on('update', this.refreshData, this);
        jun.rztLahaImportBankSetor.on('remove', this.refreshData, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    refreshData: function (t, r) {
        var total = t.sum("bank");
        Ext.getCmp('s_total_setor_id').setValue(total);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var bank_id = this.bank_id.getValue();
        var note_ = this.note_.getValue();
        var bank_amount = parseFloat(this.bank_amount.getValue());
        if (bank_id == "" || bank_id == undefined) {
            Ext.MessageBox.alert("Error", "Bank harus dipilih.");
            return;
        }
        if (bank_amount <= 0) {
            Ext.MessageBox.alert("Error", "Nominal Bank harus lebih dari 0.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('bank_id', bank_id);
            record.set('bank', bank_amount);
            record.set('note_', note_);
            record.commit();
        } else {
            var c = jun.rztLahaImportBankSetor.recordType,
                d = new c({
                    bank_id: bank_id,
                    bank: bank_amount,
                    note_: note_
                });
            jun.rztLahaImportBankSetor.add(d);
        }
        this.bank_id.reset();
        this.bank_amount.reset();
        this.note_.reset();
    },
    btnDisable: function (ss) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.bank_id.setValue(record.data.bank_id);
            this.bank_amount.setValue(record.data.bank);
            this.note_.setValue(record.data.note_);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }
})

