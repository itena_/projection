jun.BarangAssetstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BarangAssetstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangAssetStoreId',
            url: 'BarangAsset',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_asset_id'},
                {name: 'kode_barang_asset'},
                {name: 'nama_barang_asset'},
                {name: 'ket_asset'},
                {name: 'satuan'},
                {name: 'up'},
                {name: 'group_asset_id'},

            ]
        }, cfg));
    }
});
jun.rztBarangAsset = new jun.BarangAssetstore();
jun.rztBarangAssetLib = new jun.BarangAssetstore();
//jun.rztBarangAsset.load();
