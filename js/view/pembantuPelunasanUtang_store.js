jun.PembantuPelunasanUtangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PembantuPelunasanUtangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PembantuPelunasanUtangStoreId',
            url: 'PembantuPelunasanUtang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pembantu_pelunasan_utang_id'},
                {name: 'total', type:'float'},
                {name: 'rounding', type:'float'},
                {name: 'amount_bank', type:'float'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'bank_id'},
                {name: 'store'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'up'},
                {name: 'total_detail_1'},
                {name: 'total_detail_2'},
                {name: 'void_user_id'},
                {name: 'void_date_time'},

            ]
        }, cfg));
    }
});
jun.rztPembantuPelunasanUtang = new jun.PembantuPelunasanUtangstore();
//jun.rztPembantuPelunasanUtang.load();
