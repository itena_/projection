jun.PaymentJournalGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Account Receiveable",
    id: 'docs-jun.PaymentJournalGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield", filterName: "doc_ref"}
        },
        {
            header: 'No. Payment',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_other',
            width: 100,
            filter: {xtype: "textfield", filterName: "doc_ref_other"}
        },
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            filter: {
                xtype: "xdatefield",
                format: 'd/m/Y'
            }
        },
        {
            header: 'Jatuh Tempo',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_jatuh_tempo',
            width: 100
        },
        {
            header: 'Total Hutang',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100
        },
        {
            header: 'Posting',
            sortable: true,
            resizable: true,
            dataIndex: 'p',
            width: 100,
            renderer: function (v, m, r) {
                if (r.get("p") == 0) {
                    return 'BELUM DIPOSTING';
                }
                else {
                    return 'SUDAH DIPOSTING';
                }
            }
        }
        /*
         {
         header:'note',
         sortable:true,
         resizable:true,
         dataIndex:'note',
         width:100
         },
         {
         header:'user_id',
         sortable:true,
         resizable:true,
         dataIndex:'user_id',
         width:100
         },
         {
         header:'supplier_id',
         sortable:true,
         resizable:true,
         dataIndex:'supplier_id',
         width:100
         },
         {
         header:'total',
         sortable:true,
         resizable:true,
         dataIndex:'total',
         width:100
         },
         {
         header:'lunas',
         sortable:true,
         resizable:true,
         dataIndex:'lunas',
         width:100
         },
         {
         header:'store_kode',
         sortable:true,
         resizable:true,
         dataIndex:'store_kode',
         width:100
         },
         {
         header:'p',
         sortable:true,
         resizable:true,
         dataIndex:'p',
         width:100
         },
         {
         header:'up',
         sortable:true,
         resizable:true,
         dataIndex:'up',
         width:100
         },
         {
         header:'editable',
         sortable:true,
         resizable:true,
         dataIndex:'editable',
         width:100
         },
         */

    ],
    initComponent: function () {
        this.store = jun.rztPaymentJournal;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Piutang',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Piutang',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Un-Posting Piutang',
                    ref: '../btnUnposting'
                }
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete'
                // }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.PaymentJournalGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnUnposting.on('Click', this.unPostingRec, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },

    getrow: function (sm, idx, r) {
        this.record = r;

        var selectedz = this.sm.getSelections();
    },

    loadForm: function () {
        jun.rztCustomersPiutangCmp.baseParams = {
            customer_id: POSCUSTOMERDEFAULT
        };
        jun.rztCustomersPiutangCmp.load();
        jun.rztCustomersPiutangCmp.baseParams = {};

        var form = new jun.PaymentJournalWin({modez: 0});
        form.show();
    },

    loadEditForm: function () {

        var selectedz = this.sm.getSelected();

        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.payment_journal_id;
        var pos = selectedz.json.p;
        var form = new jun.PaymentJournalWin({modez: 1, id: idz, pos: pos});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPaymentJournalDetail.baseParams = {
            payment_journal_id: idz
        };
        jun.rztPaymentJournalDetail.load();
        jun.rztPaymentJournalDetail.baseParams = {};
    },

    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },

    deleteRecYes: function (btn) {

        if (btn == 'no')
            return;

        var record = this.sm.getSelected();

        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }

        Ext.Ajax.request({
            url: 'PaymentJournal/delete/id/' + record.json.payment_journal_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPaymentJournal.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
})
