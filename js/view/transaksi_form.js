jun.TransaksiWin = Ext.extend(Ext.Window, {
    title: 'Sales',
    modez: 1,
    width: 900,
    height: 565,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        if (jun.rztUnit.getTotalCount() === 0) {
            jun.rztUnit.load();
        }
        if (jun.rztGroupproduk.getTotalCount() === 0) {
            jun.rztGroupproduk.load();
        }
        if (jun.rztOutlet.getTotalCount() === 0) {
            jun.rztOutlet.load();
        }
        if (jun.rztProduk.getTotalCount() === 0) {
            jun.rztProduk.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-Transaksi',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        readOnly: true,
                        maxLength: 50,
                        width: 200,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tdate',
                        name: 'tdate',
                        format: 'd M Y',
                        value: DATE_NOW,
                        readOnly: !EDIT_TGL,
                        width: 200,
                        x: 85,
                        y: 32
                    },

                    {
                        xtype: "label",
                        text: "Outlet:",
                        id: 'lblsalesoutlet',
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        id: 'cmbsalesoutlet',
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztOutlet,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kodeoutlet} - {namaoutlet}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'outlet_id',
                        valueField: 'outlet_id',
                        allowBlank: false,
                        ref:'../outlet',
                        displayField: 'kodeoutlet',
                        emptyText: "Outlet",
                        width: 200,
                        x: 85,
                        y: 62
                    },

                    /*{
                        xtype: "label",
                        text: "Product Group:",
                        x: 340,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztGroupproduk,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kode} - {nama}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'groupproduk_id',
                        //name: 'store',
                        valueField: 'groupproduk_id',
                        displayField: 'kode',
                        allowBlank: false,
                        readOnly: !HEADOFFICE,
                        ref: '../groupproduk',
                        emptyText: "Product Group",
                        width: 200,
                        x: 420,
                        y: 2
                    },*/
                    {
                        xtype: "label",
                        text: "Description:",
                        x: 340,
                        y: 5
                        //x: 340,
                        //y: 35
                    },
                    {
                        xtype: 'textarea',
                        name: 'note',

                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        ref: '../note',
                        maxLength: 600,
                        width: 300,
                        height: 50,
                        x: 420,
                        y: 2
                        //x: 420,
                        //y: 32
                    },

                    new jun.TransaksiDetailGrid({
                        height: 320,
                        frameHeader: !1,
                        header: !1,
                        ref: '../gridDetail',
                        parent:this,
                        x: 5,
                        y: 65 + 30
                    }),
                    {
                        xtype: "label",
                        text: "Total Sales Qty:",
                        x: 615 - 25,
                        y: 450 - 20
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        ref: "../salesqty",
                        name: "salesqty",
                        value: 0,
                        id: "salesqty_id",
                        readOnly: !0,
                        width: 175,
                        x: 715 - 25,
                        y: 450 - 20
                    },
                    {
                        xtype: "label",
                        text: "Total Sales Rp:",
                        x: 615 - 25,
                        y: 482 - 20
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        ref: "../salesrp",
                        name: "salesrp",
                        id: "salesrp_id",
                        readOnly:true,
                        value: 0,
                        width: 175,
                        x: 715 - 25,
                        y: 480 - 20
                    }
                    /*                                                      {
                         xtype: 'textfield',
                         fieldLabel: 'outlet_id',
                         hideLabel:false,
                         //hidden:true,
                         name:'outlet_id',
                         id:'outlet_idid',
                         ref:'../outlet_id',
                         maxLength: 50,
                         //allowBlank: ,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'produk_id',
                         hideLabel:false,
                         //hidden:true,
                         name:'produk_id',
                         id:'produk_idid',
                         ref:'../produk_id',
                         maxLength: 50,
                         //allowBlank: ,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'groupproduk_id',
                         hideLabel:false,
                         //hidden:true,
                         name:'groupproduk_id',
                         id:'groupproduk_idid',
                         ref:'../groupproduk_id',
                         maxLength: 50,
                         //allowBlank: ,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'unit_id',
                         hideLabel:false,
                         //hidden:true,
                         name:'unit_id',
                         id:'unit_idid',
                         ref:'../unit_id',
                         maxLength: 50,
                         //allowBlank: ,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'businessunit_id',
                         hideLabel:false,
                         //hidden:true,
                         name:'businessunit_id',
                         id:'businessunit_idid',
                         ref:'../businessunit_id',
                         maxLength: 50,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'namaoutlet',
                         hideLabel:false,
                         //hidden:true,
                         name:'namaoutlet',
                         id:'namaoutletid',
                         ref:'../namaoutlet',
                         maxLength: 100,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'kodeoutlet',
                         hideLabel:false,
                         //hidden:true,
                         name:'kodeoutlet',
                         id:'kodeoutletid',
                         ref:'../kodeoutlet',
                         maxLength: 50,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'kodeproduk',
                         hideLabel:false,
                         //hidden:true,
                         name:'kodeproduk',
                         id:'kodeprodukid',
                         ref:'../kodeproduk',
                         maxLength: 50,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'kodegroupproduk',
                         hideLabel:false,
                         //hidden:true,
                         name:'kodegroupproduk',
                         id:'kodegroupprodukid',
                         ref:'../kodegroupproduk',
                         maxLength: 20,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'harga',
                         hideLabel:false,
                         //hidden:true,
                         name:'harga',
                         id:'hargaid',
                         ref:'../harga',
                         maxLength: 50,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'hargabeli',
                         hideLabel:false,
                         //hidden:true,
                         name:'hargabeli',
                         id:'hargabeliid',
                         ref:'../hargabeli',
                         maxLength: 50,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'jmloutlet',
                         hideLabel:false,
                         //hidden:true,
                         name:'jmloutlet',
                         id:'jmloutletid',
                         ref:'../jmloutlet',
                         maxLength: 11,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'persentase',
                         hideLabel:false,
                         //hidden:true,
                         name:'persentase',
                         id:'persentaseid',
                         ref:'../persentase',
                         maxLength: 11,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'qty',
                         hideLabel:false,
                         //hidden:true,
                         name:'qty',
                         id:'qtyid',
                         ref:'../qty',
                         maxLength: 11,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'satuan',
                         hideLabel:false,
                         //hidden:true,
                         name:'satuan',
                         id:'satuanid',
                         ref:'../satuan',
                         maxLength: 50,
                         //allowBlank: ,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'salesqty',
                         hideLabel:false,
                         //hidden:true,
                         name:'salesqty',
                         id:'salesqtyid',
                         ref:'../salesqty',
                         maxLength: 50,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'salesrp',
                         hideLabel:false,
                         //hidden:true,
                         name:'salesrp',
                         id:'salesrpid',
                         ref:'../salesrp',
                         maxLength:50 ,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'salesrpbeli',
                         hideLabel:false,
                         //hidden:true,
                         name:'salesrpbeli',
                         id:'salesrpbeliid',
                         ref:'../salesrpbeli',
                         maxLength:50 ,
                         //allowBlank: 1,
                         anchor: '100%'
                     },

                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'created_at',
                         hideLabel:false,
                         //hidden:true,
                         name:'created_at',
                         id:'created_atid',
                         ref:'../created_at',
                         maxLength: 6,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'uploadfile',
                         hideLabel:false,
                         //hidden:true,
                         name:'uploadfile',
                         id:'uploadfileid',
                         ref:'../uploadfile',
                         maxLength: 100,
                         //allowBlank: 1,
                         anchor: '100%'
                     },
                                                          {
                         xtype: 'textfield',
                         fieldLabel: 'uploaddate',
                         hideLabel:false,
                         //hidden:true,
                         name:'uploaddate',
                         id:'uploaddateid',
                         ref:'../uploaddate',
                         maxLength: 6,
                         //allowBlank: 1,
                         anchor: '100%'
                     }, */
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.TransaksiWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        }
        else if(this.modez == 3)
        {
            this.btnSaveClose.setVisible(false);
            this.btnSave.setVisible(false);
            //this.gridDetail.toolbardetail.setVisible(false);

            //Ext.getCmp('lbloutlet').setVisible(false);
            //Ext.getCmp('cmbsalesoutlet').setVisible(false);
            Ext.getCmp('toolbardetail').setVisible(false);
            this.btnCancel.text = "Close";
        }
        else {
            this.btnSave.setVisible(true);
        }
        //this.groupproduk.on('select', this.groupprodukonselect, this);
    },

    onWinClose: function () {
        this.gridDetail.store.removeAll();
    },

    /*groupprodukonselect: function(c,r,i)
    {
        this.gridDetail.produk.store.reload({params:{groupproduk_id:r.get('groupproduk_id')}});
    },*/

    btnDisabled:function(status)
    {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm : function()
    {
        this.btnDisabled(true);
        var urlz= 'projection/Transaksi/create/';

        Ext.getCmp('form-Transaksi').getForm().submit({
            url:urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(
                    this.gridDetail.store.data.items, "data"))
            },
            success: function(f,a){
                jun.rztTransaksi.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    this.formz.getForm().reset();
                    this.gridDetail.store.removeAll();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }

});

