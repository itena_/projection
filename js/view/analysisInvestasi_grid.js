jun.AnalysisInvestasiGrid=Ext.extend(Ext.grid.GridPanel ,{
	title:"Invesment Analysis",
        id:'docs-jun.AnalysisInvestasiGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
                                {
			header:'Account Code',
			sortable:true,
			resizable:true,                        
            dataIndex:'AccountCode',
			width:100,
            //filter: {xtype: "textfield"}
		},
                                {
			header:'Account Name',
			sortable:true,
			resizable:true,                        
            dataIndex:'AccountName',
			width:100,
            //filter: {xtype: "textfield"}
		},
                                {
			header:'Amount Plan',
			sortable:true,
			resizable:true,                        
            dataIndex:'AmountBudget',
			width:100,
            //filter: {xtype: "textfield"}
		},
                                {
			header:'Amount Realization',
			sortable:true,
			resizable:true,                        
            dataIndex:'AmountRealization',
			width:100,
            //filter: {xtype: "textfield"}
		},
        {
            header:'Achievement',
            sortable:true,
            resizable:true,
            dataIndex:'Achievement',
            width:100,
            //filter: {xtype: "textfield"}
        },
        {
            header:'(%)',
            sortable:true,
            resizable:true,
            dataIndex:'AchievementPercent',
            width:100,
            //filter: {xtype: "textfield"}
        },

		
	],
	initComponent: function(){
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
	this.store = jun.rztAnalysisInvestasi;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'label',
                        ref: '../lbldate',
                        text: 'Show By ',
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['Y', 'Year'],
                                ['MY', 'Month & Year'],
                                ['D', 'Date'],
                                ['A', 'Account'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../cmbShowBy'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../cmbAccount',
                        displayField: 'account_code',
                        emptyText: "Account...",

                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Month',
                        hiddenName: 'month',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['1', 'January'],
                                ['2', 'February'],
                                ['3', 'March'],
                                ['4', 'April'],
                                ['5', 'May'],
                                ['6', 'June'],
                                ['7', 'July'],
                                ['8', 'August'],
                                ['9', 'September'],
                                ['10', 'Okteber'],
                                ['11', 'November'],
                                ['12', 'December'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Month...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../cmbMonth'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Year',
                        hiddenName: 'year',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['2018', '2018'],
                                ['2019', '2019'],
                                ['2020', '2020'],
                                ['2021', '2021'],
                                ['2022', '2022'],
                                ['2023', '2023'],
                                ['2024', '2024'],
                                ['2025', '2025'],
                                ['2026', '2026'],
                                ['2027', '2027'],
                                ['2028', '2028'],
                                ['2029', '2029'],
                                ['2030', '2030'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Year...",
                        mode : 'local',
                        allowBlank: true,
                        width:'60',
                        ref: '../cmbYear'
                    },
                    {
                        xtype: 'xdatefield',
                        ref:'../date_start',
                        //fieldLabel: 'Date Acquisition',
                        name:'date_start',
                        id:'date_startid',
                        format: 'd M Y',
                        //value: DATE_NOW,
                        emptyText: "Start Date...",
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref:'../date_end',
                        //fieldLabel: 'Date Acquisition',
                        name:'date_end',
                        id:'date_endid',
                        format: 'd M Y',
                        //value: DATE_NOW,
                        emptyText: "End Date...",
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'button',
                        text: 'Show',
                        ref: '../btnShow',
                        iconCls: 'silk13-magnifier'
                    },
                    {
                        xtype:'tbseparator',
                        ref: '../separator',
                    },
                    {
                        xtype: 'button',
                        text: 'Export',
                        ref: '../btnExport',
                        iconCls: 'silk13-page_white_excel'
                    }
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};

		        jun.AnalysisInvestasiGrid.superclass.initComponent.call(this);
                this.on("activate", this.onActivate, this);
                this.btnShow.on('Click', this.ShowAnalysis, this);
                this.btnExport.on('Click', this.Export, this);
                this.cmbShowBy.on('Select', this.ShowBy, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},
    Export: function(){
        var form = new jun.ReportAnalysis({modez:0});
        form.show();
    },

        getrow: function(sm, idx, r){
            this.record = r;
            var selectedz = this.sm.getSelections();
        },

        onActivate: function(){
            this.cmbAccount.setVisible(false);
            this.cmbMonth.setVisible(false);
            this.cmbYear.setVisible(false);
            this.date_start.setVisible(false);
            this.date_end.setVisible(false);

            this.btnShow.setVisible(false);

            this.btnExport.setVisible(false);
            this.separator.setVisible(false);

        },

        ShowBy: function(){

            var selectedz = this.cmbShowBy.getValue();

            if(selectedz == 'Y')
            {
                this.cmbYear.setVisible(true);
                this.cmbMonth.setVisible(false);
                this.cmbAccount.setVisible(false);
                this.date_start.setVisible(false);
                this.date_end.setVisible(false);

            }
            else if(selectedz == 'MY')
            {
                this.cmbYear.setVisible(true);
                this.cmbMonth.setVisible(true);
                this.cmbAccount.setVisible(false);
                this.date_start.setVisible(false);
                this.date_end.setVisible(false);
            }
            else if(selectedz == 'A')
            {
                this.cmbYear.setVisible(false);
                this.cmbMonth.setVisible(false);
                this.cmbAccount.setVisible(true);
                this.date_start.setVisible(false);
                this.date_end.setVisible(false);
            }
            else if(selectedz == 'D')
            {
                this.cmbYear.setVisible(false);
                this.cmbMonth.setVisible(false);
                this.cmbAccount.setVisible(false);

                this.date_start.setVisible(true);
                this.date_end.setVisible(true);
            }

            this.btnShow.setVisible(true);
            this.btnExport.setVisible(true);
            this.separator.setVisible(true);
        },

    ShowAnalysis: function(){

            var selectedz = this.cmbShowBy.getValue();
            var acc = this.cmbAccount.getValue();
            var month = this.cmbMonth.getValue();
            var year = this.cmbYear.getValue();
            var datestart = this.date_start.getValue();
            var dateend = this.date_end.getValue();
            var urlz = "";

            if(selectedz == 'Y')
            {
                var acc = this.cmbAccount.reset();
                var month = this.cmbMonth.reset();
                var datestart = this.date_start.reset();
                var dateend = this.date_end.reset();
                urlz = 'projection/analysisInvestasi/index';
            }
            else if(selectedz == 'MY')
            {
                var acc = this.cmbAccount.reset();
                var datestart = this.date_start.reset();
                var dateend = this.date_end.reset();
                urlz = 'projection/analysisInvestasi/index';
            }
            else if(selectedz == 'A')
            {
                var month = this.cmbMonth.reset();
                var year = this.cmbYear.reset();
                var datestart = this.date_start.reset();
                var dateend = this.date_end.reset();
                urlz = 'projection/analysisInvestasi/index';
            }
            else if(selectedz == 'D')
            {
                var acc = this.cmbAccount.reset();
                var month = this.cmbMonth.reset();
                var year = this.cmbYear.reset();
                urlz = 'projection/analysisInvestasi/index';
            }

            this.store.setBaseParam("acc",acc);
            this.store.setBaseParam("month",month);
            this.store.setBaseParam("year",year);
            this.store.setBaseParam("date_start",datestart);
            this.store.setBaseParam("date_end",dateend);
            this.store.reload();


        }


})
