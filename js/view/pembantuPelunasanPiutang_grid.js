jun.PembantuPelunasanPiutangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PembantuPelunasanPiutang",
    id: 'docs-jun.PembantuPelunasanPiutangGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
//        {
//            header: 'Supplier',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'supplier_id',
//            width: 100,
//            renderer: jun.renderSupplier
//        },
//        {
//            header: 'Receipt No.',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'no_bukti',
//            width: 100
//        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {

        jun.rztCustomersPiutangCmp.baseParams = {
            customer_id: POSCUSTOMERDEFAULT
        };
        jun.rztCustomersPiutangCmp.load();
        jun.rztCustomersPiutangCmp.baseParams = {};

        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        jun.rztPembantuPelunasanPiutang.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    //b.params.tgl = Ext.getCmp('tglPelunasanUtangid').getValue();
                    var tgl = Ext.getCmp('tglPembantuPelunasanPiutangid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });

        this.store = jun.rztPembantuPelunasanPiutang;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Debt Payment',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show Debt Payment',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglPembantuPelunasanPiutangid',
                    ref: '../tgl'
                }
            ]
        };
        jun.PembantuPelunasanPiutangGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
//                this.btnDelete.on('Click', this.deleteRec, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();

    },

    getrow: function (sm, idx, r) {
        this.record = r;

        var selectedz = this.sm.getSelections();
    },

    loadForm: function () {
        var form = new jun.PembantuPelunasanPiutangWin({modez: 0});
        form.show();
    },

    loadEditForm: function () {

        var selectedz = this.sm.getSelected();

        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.pembantu_pelunasan_piutang_id;
        var form = new jun.PembantuPelunasanPiutangWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPembantuPelunasanPiutangDetil.baseParams = {
            pembantu_pelunasan_piutang_id: idz
        };
        jun.rztPembantuPelunasanPiutangDetil.load();
        jun.rztPembantuPelunasanPiutangDetil.baseParams = {};
    },
    refreshTgl: function () {
        this.store.reload();
    },

    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },

    deleteRecYes: function (btn) {

        if (btn == 'no') {
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }

        Ext.Ajax.request({
            url: 'PembantuPelunasanPiutang/delete/id/' + record.json.pembantu_pelunasan_piutang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPembantuPelunasanPiutang.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
})
