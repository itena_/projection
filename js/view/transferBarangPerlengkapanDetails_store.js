jun.TransferBarangPerlengkapanDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferBarangPerlengkapanDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferBarangPerlengkapanDetailsStoreId',
            url: 'TransferBarangPerlengkapanDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'transfer_perlengkapan_details_id'},
                {name: 'qty'},
                {name: 'barang_perlengkapan_id'},
                {name: 'transfer_perlengkapan_id'},
                {name: 'price',type:'float'},
                {name: 'total', type: 'float'},
                {name: 'disc', type: 'float'},
                {name: 'discrp', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'vat', type: 'float'},
                {name: 'vatrp', type: 'float'},
                {name: 'disc1', type: 'float'},
                {name: 'discrp1', type: 'float'},
                {name: 'total_pot', type: 'float'},
                {name: 'account_code'}
            ]
        }, cfg));
    } ,
    refreshTotal: function () {
//        var totalBruto=0,totalVat=0;
//
//        this.each(function(r){
//            qty = Number(r.get('qty'));
//            price = Number(r.get('price'));
//            vat = Number(r.get('vat'));
//
//            totalBruto += (qty * price);
//            totalVat += (qty * price * vat/100);
//        });
        Ext.getCmp('TransferBarangPerlengkapan_totalbrutoid').setValue(this.sum('bruto'));
        Ext.getCmp('TransferBarangPerlengkapan_totalvatid').setValue(this.sum('vatrp'));
        Ext.getCmp('TransferBarangPerlengkapan_totalpriceid').setValue(this.sum('total'));
    }
});

jun.rztTransferBarangPerlengkapanDetails = new jun.TransferBarangPerlengkapanDetailsstore();
jun.rztTransferOutBarangPerlengkapanDetails = new jun.TransferBarangPerlengkapanDetailsstore({url: 'TransferBarangPerlengkapanDetails/IndexOut'});
//jun.rztTransferBarangPerlengkapanDetails.load();
