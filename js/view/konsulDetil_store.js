jun.KonsulDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KonsulDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KonsulDetilStoreId',
            url: 'KonsulDetil',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'konsul_detil_id'},
                {name: 'barang_id'},
                {name: 'qty'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'ketpot'},
                {name: 'vat'},
                {name: 'vatrp'},
                {name: 'bruto'},
                {name: 'total'},
                {name: 'total_pot'},
                {name: 'price'},
                {name: 'disc_name'},
                {name: 'hpp'},
                {name: 'cost'},
                {name: 'disc1'},
                {name: 'discrp1'},
                {name: 'konsul_id'}
            ]
        }, cfg));
    }
});
jun.rztKonsulDetil = new jun.KonsulDetilstore();
jun.rztKonsulDetilCounter = new jun.KonsulDetilstore();
jun.rztKonsulDetilTambahan = new jun.KonsulDetilstore();
//jun.rztKonsulDetil.load();
