jun.AssetDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetDetailStoreId',
            url: 'asset/AssetDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'asset_trans_id'},
                {name:'asset_id'},
                {name:'asset_group_id'},
                {name:'barang_id'},
                {name:'ati'},
                {name:'docref'},
                {name:'docref_other'},
                {name:'asset_trans_name'},
                {name:'asset_trans_branch'},
                {name:'asset_trans_price'},
                {name:'asset_trans_new_price'},
                {name:'asset_trans_date'},
                {name:'description'},
                {name:'penyusutanperbulan'},
                {name:'penyusutanpertahun'},
                {name:'class'},
                {name:'period'},
                {name:'tariff'},
                {name:'status'},
                {name:'created_at'},
                {name:'updated_at'},
            ]
        }, cfg));
    }
});
jun.rztAssetDetail = new jun.AssetDetailstore();
//jun.rztAssetDetail.load();
