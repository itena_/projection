jun.Analysisstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Analysisstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AnalysisStoreId',
            url: 'projection/Analysis',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'AccountCode'},
                {name:'AccountName'},
                {name:'AmountBudget'},
                {name:'AmountRealization'},
                {name:'Achievement'},
                {name:'AchievementPercent'},
            ]
        }, cfg));
    }
});
jun.rztAnalysis = new jun.Analysisstore();
//jun.rztBudget.load();
