jun.AntrianGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Antrian",
    id: 'docs-jun.AntrianGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor'
        },
        {
            header: 'No. Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'no_member',
            width: 100
        }//,
        // {
        //     header: 'kartu',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'kartu',
        //     width: 100
        // },
        /* {
         header: 'Bagian',
         sortable: true,
         resizable: true,
         dataIndex: 'bagian',
         width: 100
         },
         {
         header: 'tujuan',
         sortable: true,
         resizable: true,
         dataIndex: 'tujuan',
         width: 100
         },
         
         {
         header:'picked',
         sortable:true,
         resizable:true,
         dataIndex:'picked',
         width:100
         },
         {
         header:'tanggal',
         sortable:true,
         resizable:true,
         dataIndex:'tanggal',
         width:100
         },
         {
         header:'timestamp',
         sortable:true,
         resizable:true,
         dataIndex:'timestamp',
         width:100
         },
         {
         header:'spesial',
         sortable:true,
         resizable:true,
         dataIndex:'spesial',
         width:100
         },
         */
    ],
    initComponent: function () {
        this.store = jun.rztAntrian;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    scale: 'large',
                    style: 'margin:5px',
                    text: 'A N T R I A N'
                },
                '->',
                {
                    xtype: 'button',
                    scale: 'large',
                    text: 'P E N D I N G >>>',
                    ref: '../btnPending'
                }
            ]
        };
        jun.AntrianGrid.superclass.initComponent.call(this);
        this.btnPending.on('click', this.pending, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },

    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },

    pending: function () {
        this.btnPending.setDisabled(true);
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            this.btnPending.setDisabled(false);
            return;
        }
        Ext.Ajax.request({
            url: 'Antrian/update/',
            method: 'POST',
            scope: this,
            params: {
                id: record.json.id_antrian,
                mode: 'pending'
            },
            success: function (f, a) {
                this.btnPending.setDisabled(false);
                jun.rztAntrian.reload();
                jun.rztAntrianPending.reload();
            },
            failure: function (f, a) {
                this.btnPending.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.AntrianPendingGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pending",
    id: 'docs-jun.AntrianPendingGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor',
            width: 100
        },
        {
            header: 'No. Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'no_member',
            width: 100
        }
        /*{
         header: 'kartu',
         sortable: true,
         resizable: true,
         dataIndex: 'kartu',
         width: 100
         },
         {
         header: 'bagian',
         sortable: true,
         resizable: true,
         dataIndex: 'bagian',
         width: 100
         },
         {
         header: 'tujuan',
         sortable: true,
         resizable: true,
         dataIndex: 'tujuan',
         width: 100
         },
         
         {
         header:'picked',
         sortable:true,
         resizable:true,
         dataIndex:'picked',
         width:100
         },
         {
         header:'tanggal',
         sortable:true,
         resizable:true,
         dataIndex:'tanggal',
         width:100
         },
         {
         header:'timestamp',
         sortable:true,
         resizable:true,
         dataIndex:'timestamp',
         width:100
         },
         {
         header:'spesial',
         sortable:true,
         resizable:true,
         dataIndex:'spesial',
         width:100
         },
         */
    ],
    initComponent: function () {
        this.store = jun.rztAntrianPending;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    scale: 'large',
                    text: '<<< A N T R I A N',
                    ref: '../btnAntrian'
                },
                '->',
                {
                    xtype: 'label',
                    scale: 'large',
                    style: 'margin:5px',
                    text: 'P E N D I N G A N'
                }
            ]
        };
        jun.AntrianPendingGrid.superclass.initComponent.call(this);
        this.btnAntrian.on('click', this.antrian, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    antrian: function () {
        this.btnAntrian.setDisabled(true);
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            this.btnAntrian.setDisabled(false);
            return;
        }
        Ext.Ajax.request({
            url: 'Antrian/update/',
            method: 'POST',
            scope: this,
            params: {
                id: record.json.id_antrian,
                mode: 'antrian'
            },
            success: function (f, a) {
                this.btnAntrian.setDisabled(false);
                jun.rztAntrian.reload();
                jun.rztAntrianPending.reload();
            },
            failure: function (f, a) {
                this.btnAntrian.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.AntrianFOGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "AntrianFO",
    id: 'docs-jun.AntrianFOGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    selectedRecord: null,
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_antrian',
            width: 100,
            hidden: true
        },
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'id_antrian',
            width: 100,
            renderer: function(value, metaData, record, rowIndex){
                var mark = record.get('counter_asal');
                if(!mark){
                    return record.get('nomor_antrian');
                } else
                return record.get('nomor_antrian') + record.get('counter_asal');
            }
        },
        {
            header: 'No. Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_pasien',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztAntrianFO;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    scale: 'small',
                    style: 'margin:5px',
                    text: 'A N T R I A N'
                },
                '->',
                {
                    xtype: 'button',
                    scale: 'small',
                    text: 'P E N D I N G >>>',
                    ref: '../btnPending'
                }
            ]
        };
        jun.AntrianFOGrid.superclass.initComponent.call(this);
        this.btnPending.on('click', this.pending, this);
        this.store.on('load', this.onStoreLoad, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        this.selectedRecords = this.sm.getSelected();
    },
    onStoreLoad: function (a, b) {
        if (this.selectedRecords != null) {
            this.sm.selectRow(a.indexOf(this.selectedRecords), true);
            this.selectedRecords = null;
        }
        jun.AntrianFOCount = this.store.getTotalCount();
        Ext.getCmp('docs-jun.AntrianCounterWin').setTitle('Counter [' +
                jun.AntrianFOCount + '] Kasir [' + jun.AntrianKasirCount + ']');
    },
    pending: function () {
        this.btnPending.setDisabled(true);
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            this.btnPending.setDisabled(false);
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                id_antrian: record.json.id_antrian,
                bagian: 'counter',
                mode: 'pending'
            },
            success: function (f, a) {
                this.btnPending.setDisabled(false);
                jun.rztAntrianFO.reload();
                jun.rztAntrianFOPending.reload();
            },
            failure: function (f, a) {
                this.btnPending.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.AntrianFOPendingGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pending",
    id: 'docs-jun.AntrianFOPendingGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    selectedRecord: null,
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_antrian',
            width: 100,
            hidden: true
        },
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'id_antrian',
            width: 100,
            renderer: function(value, metaData, record, rowIndex){
                var mark = record.get('counter_asal');
                if(!mark){
                    return record.get('nomor_antrian');
                } else
                return record.get('nomor_antrian') + record.get('counter_asal');
            }
        },
        {
            header: 'No. Pasien',
            resizable: true,
            dataIndex: 'nomor_pasien',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztAntrianFOPending;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    scale: 'small',
                    text: '<<< A N T R I A N',
                    ref: '../btnAntrian'
                },
                '->',
                {
                    xtype: 'label',
                    scale: 'small',
                    style: 'margin:5px',
                    text: 'P E N D I N G A N'
                }
            ]
        };
        jun.AntrianFOPendingGrid.superclass.initComponent.call(this);
        this.btnAntrian.on('click', this.antrian, this);
        this.store.on('load', this.onStoreLoad, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        this.selectedRecords = this.sm.getSelected();
    },
    onStoreLoad: function (a, b) {
        if (this.selectedRecords != null) {
            this.sm.selectRow(a.indexOf(this.selectedRecords), true);
            this.selectedRecords = null;
        }
    },
    antrian: function () {
        this.btnAntrian.setDisabled(true);
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            this.btnAntrian.setDisabled(false);
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                id_antrian: record.json.id_antrian,
                mode: 'unpending'
            },
            success: function (f, a) {
                this.btnAntrian.setDisabled(false);
                jun.rztAntrianFO.reload();
                jun.rztAntrianFOPending.reload();
            },
            failure: function (f, a) {
                this.btnAntrian.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.AntrianKasirGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "AntrianKasir",
    id: 'docs-jun.AntrianKasirGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    selectedRecord: null,
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_antrian',
            width: 100,
            hidden: true
        },
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'id_antrian',
            width: 100,
            renderer: function(value, metaData, record, rowIndex){
                var mark = record.get('counter_asal');
                if(!mark){
                    return record.get('nomor_antrian');
                } else
                return record.get('nomor_antrian') + record.get('counter_asal');
            }
        },
        {
            header: 'No. Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_pasien',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztAntrianKasir;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    scale: 'small',
                    style: 'margin:5px',
                    text: 'A N T R I A N'
                },
                '->',
                {
                    xtype: 'button',
                    scale: 'small',
                    text: 'P E N D I N G >>>',
                    ref: '../btnPending'
                }
            ]
        };
        jun.AntrianKasirGrid.superclass.initComponent.call(this);
        this.btnPending.on('click', this.pending, this);
        this.store.on('load', this.onStoreLoad, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        this.selectedRecords = this.sm.getSelected();
    },
    onStoreLoad: function (a, b) {
        if (this.selectedRecords != null) {
            this.sm.selectRow(a.indexOf(this.selectedRecords), true);
            this.selectedRecords = null;
        }
        jun.AntrianKasirCount = this.store.getTotalCount();
        Ext.getCmp('docs-jun.AntrianCounterWin').setTitle('Counter [' +
                jun.AntrianFOCount + '] Kasir [' + jun.AntrianKasirCount + ']');
    },
    pending: function () {
        this.btnPending.setDisabled(true);
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            this.btnPending.setDisabled(false);
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                id_antrian: record.json.id_antrian,
                bagian: 'kasir',
                mode: 'pending'
            },
            success: function (f, a) {
                this.btnPending.setDisabled(false);
                jun.rztAntrianKasir.reload();
                jun.rztAntrianKasirPending.reload();
            },
            failure: function (f, a) {
                this.btnPending.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.AntrianKasirPendingGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pending",
    id: 'docs-jun.AntrianKasirPendingGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    selectedRecord: null,
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_antrian',
            width: 100,
            hidden: true
        },
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'id_antrian',
            width: 100,
            renderer: function(value, metaData, record, rowIndex){
                var mark = record.get('counter_asal');
                if(!mark){
                    return record.get('nomor_antrian');
                } else
                return record.get('nomor_antrian') + record.get('counter_asal');
            }
        },
        {
            header: 'No. Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_pasien',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztAntrianKasirPending;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    scale: 'small',
                    text: '<<< A N T R I A N',
                    ref: '../btnAntrian'
                },
                '->',
                {
                    xtype: 'label',
                    scale: 'small',
                    style: 'margin:5px',
                    text: 'P E N D I N G A N'
                }
            ]
        };
        jun.AntrianKasirPendingGrid.superclass.initComponent.call(this);
        this.btnAntrian.on('click', this.antrian, this);
        this.store.on('load', this.onStoreLoad, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        this.selectedRecords = this.sm.getSelected();
    },
    onStoreLoad: function (a, b) {
        if (this.selectedRecords != null) {
            this.sm.selectRow(a.indexOf(this.selectedRecords), true);
            this.selectedRecords = null;
        }
    },
    antrian: function () {
        this.btnAntrian.setDisabled(true);
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            this.btnAntrian.setDisabled(false);
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                id_antrian: record.json.id_antrian,
                mode: 'unpending'
            },
            success: function (f, a) {
                this.btnAntrian.setDisabled(false);
                jun.rztAntrianKasir.reload();
                jun.rztAntrianKasirPending.reload();
            },
            failure: function (f, a) {
                this.btnAntrian.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.AntrianDokterGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "AntrianDokter",
    id: 'docs-jun.AntrianDokterGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    selectedRecord: null,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_antrian',
            width: 100,
            hidden: true
        },
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'id_antrian',
            width: 100,
            renderer: function(value, metaData, record, rowIndex){
                var mark = record.get('counter_asal');
                if(!mark){
                    return record.get('nomor_antrian');
                } else
                return record.get('nomor_antrian') + record.get('counter_asal');
            }
        },
        {
            header: 'No. Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_pasien',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztAntrianDokter;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    scale: 'small',
                    style: 'margin:5px',
                    text: 'A N T R I A N'
                },
                '->', '-', '-', '-',
                {
                    xtype: 'button',
                    scale: 'small',
                    text: 'B A T A L - K O N S U L',
                    ref: '../btnBatal'
                },
                '-', '-', '-',
                {
                    xtype: 'button',
                    scale: 'small',
                    text: 'P E N D I N G >>>',
                    ref: '../btnPending'
                }
            ]
        };
        jun.AntrianDokterGrid.superclass.initComponent.call(this);
        this.btnPending.on('click', this.pending, this);
        this.btnBatal.on('click', this.batal, this);
        this.store.on('load', this.onStoreLoad, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onStoreLoad: function (a, b) {
        if (this.selectedRecords != null) {
            this.sm.selectRow(a.indexOf(this.selectedRecords), true);
            this.selectedRecords = null;
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        this.selectedRecords = this.sm.getSelected();
    },
    batal: function () {
        this.btnBatal.setDisabled(true);
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Antrian");
            this.btnPending.setDisabled(false);
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                id_antrian: record.json.id_antrian,
                bagian: 'medis',
                mode: 'batal'
            },
            success: function (f, a) {
                this.btnBatal.setDisabled(false);
                jun.rztAntrianDokter.reload();
                jun.rztAntrianDokterPending.reload();
//                jun.rztAntrianDokterPending.reload({baseParams: {medis: jun.Counter}});
            },
            failure: function (f, a) {
                this.btnBatal.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    pending: function () {
        this.btnPending.setDisabled(true);
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            this.btnPending.setDisabled(false);
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                id_antrian: record.json.id_antrian,
                bagian: 'medis',
                mode: 'pending'
            },
            success: function (f, a) {
                this.btnPending.setDisabled(false);
                jun.rztAntrianDokter.reload();
                jun.rztAntrianDokterPending.reload({baseParams: {medis: jun.Counter}});
            },
            failure: function (f, a) {
                this.btnPending.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.AntrianDokterPendingGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pending",
    id: 'docs-jun.AntrianDokterPendingGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    selectedRecord: null,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_antrian',
            width: 100,
            hidden: true
        },
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'id_antrian',
            width: 100,
            renderer: function(value, metaData, record, rowIndex){
                var mark = record.get('counter_asal');
                if(!mark){
                    return record.get('nomor_antrian');
                } else
                return record.get('nomor_antrian') + record.get('counter_asal');
            }
        },
        {
            header: 'No. Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_pasien',
            width: 100
        }
    ],
    initComponent: function () {
//        this.store = jun.rztAntrianDokterPending;
        jun.rztAntrianDokterPending = new jun.AishaAntrianstore({
                        url: 'AishaAntrian/AntrianPendingDokter',
                        baseParams: {bagian: "medis", f: "cmp", c: jun.Counter},
                        method: 'POST'
                    }); 
        this.store = jun.rztAntrianDokterPending;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    scale: 'small',
                    text: '<<< A N T R I A N',
                    ref: '../btnAntrian'
                },
                '->',
                {
                    xtype: 'label',
                    scale: 'small',
                    style: 'margin:5px',
                    text: 'P E N D I N G A N'
                }
            ]
        };
        jun.AntrianDokterPendingGrid.superclass.initComponent.call(this);
        this.btnAntrian.on('click', this.antrian, this);
        this.store.on('load', this.onStoreLoad, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        this.selectedRecords = this.sm.getSelected();
    },
    onStoreLoad: function (a, b) {
        if (this.selectedRecords != null) {
            this.sm.selectRow(a.indexOf(this.selectedRecords), true);
            this.selectedRecords = null;
        }
//        console.log();
    },
    antrian: function () {
        this.btnAntrian.setDisabled(true);
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            this.btnAntrian.setDisabled(false);
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                id_antrian: record.json.id_antrian,
                mode: 'unpending'
            },
            success: function (f, a) {
                this.btnAntrian.setDisabled(false);
                jun.rztAntrianDokter.reload();
                jun.rztAntrianDokterPending.reload({baseParams: {medis: jun.Counter}});
            },
            failure: function (f, a) {
                this.btnAntrian.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.AntrianMonitorGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Monitoring Antrian",
    id: 'docs-jun.AntrianMonitorGrid',
    // iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    // plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_antrian',
            width: 100,
            hidden: true
        },
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'id_antrian',
            width: 100,
            renderer: function(value, metaData, record, rowIndex){
                var mark = record.get('counter_asal');
                if(!mark){
                    return record.get('nomor_antrian');
                } else
                return record.get('nomor_antrian') + record.get('counter_asal');
            }
        },
        {
            header: 'No. Pasien',
            resizable: true,
            dataIndex: 'nomor_pasien',
            width: 100
        },
        {
            header: 'Register',
            resizable: true,
            dataIndex: 'daftar'
        },
        {
            header: 'Counter',
            resizable: true,
            dataIndex: 'counter'
        },
        {
            header: 'Konsul',
            resizable: true,
            dataIndex: 'konsul'
        },
        {
            header: 'Kasir',
            resizable: true,
            dataIndex: 'kasir'
        },
        {
            header: 'Keterangan',
            resizable: true,
            dataIndex: 'alasan'
        }
    ],
    initComponent: function () {
        this.store = jun.rztAntrianMonitor;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        jun.AntrianMonitorGrid.superclass.initComponent.call(this);
        // this.btnPending.on('click', this.pending, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    }
});