jun.SecurityRolesWin = Ext.extend(Ext.Window, {
    title: "Security Role",
    modez: 1,
    width: 800,
    height: 600,
    layout: "form",
    modal: !0,
    padding: 3,
    resizable: 1,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-SecurityRoles",
                labelWidth: 100,
                labelAlign: "left",
                layout: "accordion",
                ref: "formz",
                border: !1,
                anchor: "100% 100%",
                items: [
                    {
                        xtype: "panel",
                        title: "Description",
                        layout: "form",
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        items: [
                            {
                                xtype: "textfield",
                                fieldLabel: "Role Name",
                                hideLabel: !1,
                                name: "role",
                                id: "roleid",
                                ref: "../role",
                                maxLength: 30,
                                anchor: "100%"
                            },
                            {
                                xtype: "textarea",
                                fieldLabel: "Description",
                                enableKeyEvents: true,
                                style: {textTransform: "uppercase"},
                                listeners: {
                                    change: function (field, newValue, oldValue) {
                                        field.setValue(newValue.toUpperCase());
                                    }
                                },
                                hideLabel: !1,
                                name: "ket",
                                id: "ketid",
                                ref: "../ket",
                                maxLength: 255,
                                anchor: "100%"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Master Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                        defaultType: "checkbox",
                        items: [
                            /*{
                                columnWidth: .33,
                                boxLabel: "Country",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "100"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Province",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "101"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "City",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "102"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Sub District",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "103"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Doctor",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "104"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Bank",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "105"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Group",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "106"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Items",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "107"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Resep",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "133"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Customers",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "108"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Beautician",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "109"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Beauty Tips Class",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "110"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Value Tips Class",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "111"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Beauty Onduty",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "112"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Apoteker",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "134"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Front Office",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "135"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Status Customers",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "113"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Promosi",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "136"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Discount",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "114"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Store",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "115"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Supplier",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "116"
                            },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Chart Of Account Class",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "131"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Chart Of Account Type",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "132"
                            // },
                            {
                                columnWidth: .33,
                                boxLabel: "Chart Of Account",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "117"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Default Purchase Price",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "118"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Region",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "119"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Card Type",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "121"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Keterangan Transaksi",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "120"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Group Attribute",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "122"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Selling Price",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "123"
                            },

                            {
                                columnWidth: .33,
                                boxLabel: "Clinical Item",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "125"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Clinical Category",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "126"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Info",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "127"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Souvenir",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "128"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Promotion",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "129"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Packages",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "130"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Barang Aset",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "131"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Barang Perlengkapan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "132"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Group Barang Asset",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "133"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Store Area",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "137"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Bonus name",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "138"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Bonus Jual",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "139"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Tag",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "141"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Tag Product/Treatment",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "142"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Assign Employees",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "143"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Racikan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "144"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Bonus Template",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "145"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Referral",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "146"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Fee Referral",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "147"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Ship",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "148"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Asset Group",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "504"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Asset",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "505"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Asset Detail",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "506"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Show Asset",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "507"
                            },*/

                            {
                                columnWidth: .33,
                                boxLabel: "Product Groups",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "801"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Products",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "802"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Outlets",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "803"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Units",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "804"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Accounts",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "805"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Account Groups",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "806"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Account Categories",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "807"
                            },
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Transaction Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            /*{
                                columnWidth: .33,
                                boxLabel: "Sales",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "200"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Import Sales",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "255"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Return Sales",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "201"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Edit Service",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "202"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Kas/Bank Masuk',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "203"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Kas/Bank Keluar',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "204"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Invoice Supplier",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "205"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Invoice Supplier (set LUNAS)",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "2051"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Return Supplier Item",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "206"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Tender Declaration",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "207"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Audit",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "208"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Override Price & Discount",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "209"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "History",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "210"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "General Journal",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "211"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Kas/Bank Masuk (HO)',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "212"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Kas/Bank Keluar (HO)',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "213"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Cash/Bank Transfer",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "214"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Generate Profit Lost",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "215"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Yearly Closing",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "216"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Absent Transaction',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "217"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Production",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "218"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Production Return',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "219"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Clinical In',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "220"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Clinical Out',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "221"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Souvenir In',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "222"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Souvenir Out',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "223"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Point In',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "224"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Point Out',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "225"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Member Get Member',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "226"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'My Special Day',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "227"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Pelunasan Hutang',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "230"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Transfer Barang Masuk',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "228"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Transfer Barang Keluar',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "229"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Purchase Barang Asset',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "240"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Purchase Barang Perlengkapan',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "241"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Transfer Out Barang Perlengkapan',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "242"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Input Stock Opname',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "243"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Retur Sales All Item',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "244"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Tender Declaration (Status)',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "245"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Invoice Journal',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "231"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Pembantu Pelunasan Hutang',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "232"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Edit Tanggal',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "246"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'ULPT',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "247"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'RPJ',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "248"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Purchase Order',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "249"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Penerimaan Barang',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "250"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Transfer Request',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "251"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Transfer',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "252"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Transfer Receive',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "253"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Transfer Approval',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "257"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Transfer Recall',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "264"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Pemutihan ULPT',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "254"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Antrian',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "256"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Input Penerimaan Barang',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "260"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Import Laha Natasha',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "261"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Retur Produksi',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "262"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'All Tipe Barang',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "263"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Module Counter',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "9996"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Module Dokter',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "9997"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Modul Perawatan',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "9998"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Quick Sales Transaction',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "9999"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Restriction Date Input',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "501"
                            }*/
                            {
                                columnWidth: .33,
                                boxLabel: 'Sales',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "808"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Sales Override',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "900"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Budget Plans',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "809"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Budget Realizations',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "810"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Budget Analysis',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "811"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Investment Plans',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "818"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Investment Realizations',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "819"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Investment Analysis',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "820"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Report Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            /*{
                                columnWidth: .33,
                                boxLabel: 'Sales Summary',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "313"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Sales Details',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "316"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Customer Details',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "317"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Payments',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "321"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Sales Summary Group",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "300"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Sales Details Group",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "301"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Sales and Return Sales',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "331"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Inventory Movement",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "302"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Inventory Card",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "303"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Inventory Movements Perlengkapan',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "333"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Inventory Card Perlengkapan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "350"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Show Cost Price on Inventory Card",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "351"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Beautician Services Summary",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "304"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Beautician Services Details",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "305"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Doctors Services Summary",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "311"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Doctors Services Details",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "312"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "New Customers",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "306"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Category Customers",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "332"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Tenders",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "307"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Daily Report",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "308"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Return Sales Summary",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "309"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Return Sales Details",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "310"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "General Ledger",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "314"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "General Journal",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "315"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Profit Lost",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "318"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Balance Sheet",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "319"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Neraca",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "320"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Payment",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "321"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Sales Price",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "322"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Purchase Price',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "330"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Rekening Koran",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "323"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Kartu Hutang',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "324"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Buku Hutang',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "345"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Chart Customers Attendance',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "325"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Chart New Customers',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "326"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Chart Sales Group',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "327"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Chart Top Customers',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "328"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Chart Top Sales Group',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "329"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'BirthDay Customers',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "334"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Sales Item Details',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "335"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Fee Card',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "336"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Real Customers',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "337"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Rekap Penjualan',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "338"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Info Efektivitas',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "339"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Transfer Barang Masuk',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "340"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Pasien Baru Riil Faktur',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "341"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Stock In Transit',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "342"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Rekap Purchase Order',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "343"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Rekap Transfer Request',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "344"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Invoice Supplier',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "346"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Bonus',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "347"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report CashFlow',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "348"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Cashier Turnover',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "349"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Produksi',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "352"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Omset Per Periode',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "353"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'nox customer',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "354"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Fee Referral',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "355"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Efektivitas Doctor',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "356"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Efektivitas Doctor Service',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "357"
                            } */
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Sales',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "812"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Sales Plans',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "821"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Sales Realizations',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "831"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Budget Plans',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "813"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Budget Realizations',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "814"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Budget Analysis',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "815"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Investment Plans',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "816"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Investment Realizations',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "822"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Investment Analysis',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "823"
                            },

                            //ALL BU
                            {
                                columnWidth: .33,
                                boxLabel: 'Report All Budget Plans',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "824"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report All Budget Realizations',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "825"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report All Budget Analysis',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "826"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report All Investment Plans',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "827"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report All Investment Realizations',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "828"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report All Investment Analysis',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "829"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Laba Rugi (Plans)',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "830"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Laba Rugi (Realizations)',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "832"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Administration Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Business Units",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "800"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "User Management",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "400"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Security Roles",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "401"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Backup / Restore",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "402"
                            },
                            /*{
                                columnWidth: .33,
                                boxLabel: "Import",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "403"
                            },*/
                            {
                                columnWidth: .33,
                                boxLabel: "Setting Client",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "404"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Preferences",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "405"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Employee",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "124"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "User Employee",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "406"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Restrict Date",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "502"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Import Data",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "817"
                            },
                            /*{
                                columnWidth: .33,
                                boxLabel: "Upload History",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "503"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Sync",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "508"
                            }*/

                        ]
                    },
                    {
                        xtype: "panel",
                        title: "General Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Change Password",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "000"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Logout",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "001"
                            }
                            ,
                            {
                                columnWidth: .33,
                                boxLabel: "Super User (SU)",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "9999"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.SecurityRolesWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSaveClose.setDisabled(a);
    },
    onActivate: function () {
        this.btnSaveClose.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        var a;
        this.modez == 1 || this.modez == 2 ? a = "SecurityRoles/update/id/" + this.id : a = "SecurityRoles/create/",
            Ext.getCmp("form-SecurityRoles").getForm().submit({
                url: a,
                scope: this,
                success: function (a, b) {
//                    var a = BASE_URL;
                    window.location = BASE_URL;
//                    jun.rztSecurityRoles.reload();
//                    jun.sidebar.getRootNode().reload();
//                    var c = Ext.decode(b.response.responseText);
//                    this.close();
//                    this.closeForm ? this.close() : (c.data != undefined && Ext.MessageBox.alert("Pelayanan", c.data.msg),
//                        this.modez == 0 && Ext.getCmp("form-SecurityRoles").getForm().reset());
                },
                failure: function (a, b) {
                    Ext.MessageBox.alert("Error", "Can't Communicate With The Server");
                    this.btnDisabled(!1);
                }
            });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0;
        this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1;
        this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.SettingClientsWin = Ext.extend(Ext.Window, {
    title: "Setting Client",
    modez: 1,
    width: 350,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 3,
    resizable: !1,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-SettingClients",
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Setting Name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'name_',
                        id: 'name_id',
                        ref: '../name_',
                        maxLength: 50,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Value',
                        hideLabel: false,
                        //hidden:true,
                        name: 'value_',
                        ref: '../value_',
                        maxLength: 50,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.SettingClientsWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSaveClose.setDisabled(a);
    },
    onActivate: function () {
        this.btnSaveClose.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        if (this.modez == 0) {
            var c = jun.rztSettingClients.recordType,
                d = new c({
                    name_: this.name_.getValue(),
                    value_: this.value_.getValue()
                });
            jun.rztSettingClients.add(d);
        } else {
            this.r.set('name_', this.name_.getValue());
            this.r.set('value_', this.value_.getValue());
            this.r.commit();
        }
        var data = {};
        var arr = [];
        jun.rztSettingClients.each(function (rec) {
                arr.push(rec.data);
            }
        );
        data.data = arr;
        // Lockr.set('settingClient', data);
        localStorage.setItem("settingClient", JSON.stringify(data));
        this.close();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0;
        this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1;
        this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});