jun.AssetDetailWin = Ext.extend(Ext.Window, {
    title: 'Asset Detail',
    modez: 1,
    width: 945,
    height: 595,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    iswin: true,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-AssetDetail',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    /*{
                        xtype: "label",
                        text: "Docref Detail:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'docref',
                        id: 'docrefid',
                        ref: '../docref',
                        readOnly: true,
                        width: 175,
                        x: 85,
                        y: 2
                    },*/
                    {
                        xtype: "label",
                        text: "No. Activa:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'ati',
                        id: 'atiid',
                        ref: '../ati',
                        readOnly: true,
                        width: 175,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Docref Asset:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'docref_other',
                        id: 'docref_otherid',
                        ref: '../docref_other',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Asset Name:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'asset_trans_name',
                        id: 'asset_trans_nameid',
                        ref: '../asset_trans_name',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 62
                    },

                    {
                        xtype: "label",
                        text: "Price:",
                        x: 290,
                        y: 5
                    },

                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'asset_trans_price',
                        id: 'asset_trans_priceid',
                        ref: '../asset_trans_price',
                        width: 175,
                        readOnly: true,
                        x: 370,
                        y: 2,
                    },

                    {
                        xtype: "label",
                        text: "New Price:",
                        x: 290,
                        y: 35
                    },

                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'asset_trans_new_price',
                        id: 'asset_trans_new_priceid',
                        ref: '../asset_trans_new_price',
                        width: 175,
                        readOnly: true,
                        x: 370,
                        y: 32
                    },

                    {
                        xtype: "label",
                        text: "Date:",
                        x: 290,
                        y: 65
                    },
                    {
                        xtype: "xdatefield",
                        ref: "../asset_trans_date",
                        name: "asset_trans_date",
                        id: "asset_trans_dateid",
                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        format: "d M Y",
                        width: 175,
                        x: 370,
                        y: 62,
                        minValue: RES_DATE
                    },

                    {
                        xtype: "label",
                        text: "Branch:",
                        x: 615,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'asset_trans_branch',
                        id: 'asset_trans_branchid',
                        ref: '../asset_trans_branch',
                        value: STORE,
                        readOnly: true,
                        maxLength: 20,
                        width: 175,
                        x: 715,
                        y: 2
                    },

                    {
                        xtype: "label",
                        text: "Branch:",
                        x: 615,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'asset_trans_branch',
                        id: 'asset_trans_branchid',
                        ref: '../asset_trans_branch',
                        value: STORE,
                        readOnly: true,
                        maxLength: 20,
                        width: 175,
                        x: 715,
                        y: 2
                    },

                    {
                        xtype: "label",
                        text: "Description:",
                        x: 615,
                        y: 35
                    },
                    {
                        xtype: 'textarea',
                        hideLabel: false,
                        name: 'description',
                        id: 'descriptionid',
                        ref: '../description',
                        //value: STORE,
                        //readOnly: true,
                        //maxLength: 20,
                        width: 175,
                        height: 50,
                        x: 715,
                        y: 32
                    },

                    new jun.AssetPeriodeGrid({
                        height: 405 - 30,
                        frameHeader: !1,
                        header: !1,
                        x: 5,
                        y: 65 + 30
                    }),

                    {
                        xtype: "label",
                        text: "Total Depreciation:",
                        x: 615,
                        y: 482
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        ref: "../penyusutanpertahun",
                        name: "penyusutanpertahun",
                        value: 0,
                        readOnly: true,
                        width: 175,
                        x: 715,
                        y: 480
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                /*{
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },*/
                {
                    xtype: 'button',
                    text: 'Close',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetDetailWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        //this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        //this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'asset/AssetDetail/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'asset/AssetDetail/create/';
                }
             
            Ext.getCmp('form-AssetDetail').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztAssetDetail.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-AssetDetail').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    /*onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },*/
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});