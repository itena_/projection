jun.LahaImportWin = Ext.extend(Ext.Window, {
    title: 'Import Laha',
    id: 'docs-jun.LahaImportWin',
    modez: 1,
    width: 1120,
    height: 620,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;',
                id: 'form-LahaImport',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'fit',
                ref: 'formz',
                border: false,
                items: [
                    {
                        layout: 'hbox',
                        border: false,
                        bodyStyle: 'background-color: #E4E4E4;',
                        frame: false,
                        layoutConfig: {
                            padding: 2,
                            align: 'stretch'
                        },
                        items: [
                            {
                                layout: 'vbox',
                                border: false,
                                // bodyStyle: 'background-color: #E4E4E4; padding: 2px',
                                frame: false,
                                layoutConfig: {
                                    padding: 1,
                                    align: 'stretch'
                                },
                                flex: 5,
                                items: [
                                    {
                                        bodyStyle: 'background-color: #E4E4E4; padding: 2px',
                                        labelWidth: 130,
                                        labelAlign: 'left',
                                        layout: 'form',
                                        ref: 'formz',
                                        border: true,
                                        items: [
                                            {
                                                xtype: 'compositefield',
                                                fieldLabel: 'Doc. Ref',
                                                msgTarget: 'side',
                                                anchor: '100%',
                                                // anchor: '-20',
                                                defaults: {
                                                    flex: 1
                                                },
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Doc. Ref',
                                                        hideLabel: false,
                                                        //hidden:true,
                                                        name: 'doc_ref',
                                                        // id: 'doc_refid',
                                                        ref: '../../../../../doc_ref',
                                                        maxLength: 50,
                                                        //allowBlank: ,
                                                        readOnly: true,
                                                        anchor: '50%'
                                                    },
                                                    {
                                                        xtype: 'label',
                                                        style: 'margin:5px',
                                                        width: 100,
                                                        text: 'Date :'
                                                    },
                                                    {
                                                        xtype: 'xdatefield',
                                                        ref: '../../../../../tgl',
                                                        fieldLabel: 'Date',
                                                        name: 'tgl',
                                                        format: 'd/m/Y',
                                                        anchor: '50%',
                                                        //readOnly: true,
                                                        allowBlank: false,
                                                        value: DATE_NOW,
                                                        minValue: RES_DATE
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: 'compositefield',
                                                fieldLabel: 'Branch',
                                                msgTarget: 'side',
                                                anchor: '100%',
                                                // anchor: '-20',
                                                defaults: {
                                                    flex: 1
                                                }
                                                ,
                                                items: [
                                                    {
                                                        xtype: 'combo',
                                                        //typeAhead: true,
                                                        fieldLabel: 'Branch',
                                                        ref: '../../../../../store',
                                                        triggerAction: 'all',
                                                        lazyRender: true,
                                                        mode: 'local',
                                                        store: jun.rztStoreCmp,
                                                        hiddenName: 'store',
                                                        name: 'store',
                                                        valueField: 'store_kode',
                                                        displayField: 'store_kode',
                                                        emptyText: "Branch",
                                                        //value: STORE,
                                                        readOnly: !HEADOFFICE,
                                                        anchor: '50%'
                                                    },
                                                    {
                                                        xtype: 'label',
                                                        style: 'margin:5px',
                                                        width: 100,
                                                        text: 'Kas Cabang :'
                                                    },
                                                    {
                                                        xtype: 'combo',
                                                        //typeAhead: true,
                                                        ref: '../../../../../store',
                                                        fieldLabel: 'Kas Cabang',
                                                        triggerAction: 'all',
                                                        lazyRender: true,
                                                        mode: 'local',
                                                        forceSelection: true,
                                                        store: jun.rztBankTransCmpPusat,
                                                        hiddenName: 'p_d_kas_bank_id',
                                                        valueField: 'bank_id',
                                                        displayField: 'nama_bank',
                                                        anchor: '50%'
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        bodyStyle: 'background-color: #E4E4E4; padding: 2px',
                                        title: 'Penjualan',
                                        labelWidth: 130,
                                        labelAlign: 'left',
                                        layout: 'form',
                                        ref: 'formz',
                                        border: true,
                                        items: [
                                            {
                                                xtype: 'compositefield',
                                                fieldLabel: 'Kas Cabang',
                                                msgTarget: 'side',
                                                anchor: '100%',
                                                // anchor: '-20',
                                                defaults: {
                                                    flex: 1
                                                },
                                                items: [
                                                    {
                                                        xtype: 'numericfield',
                                                        hideLabel: false,
                                                        //hidden:true,
                                                        name: 'p_d_kas_n',
                                                        ref: '../../../../../p_d_kas_n',
                                                        anchor: '50%'
                                                    },
                                                    {
                                                        xtype: 'label',
                                                        style: 'margin:5px',
                                                        width: 100,
                                                        text: 'Piutang Card :'
                                                    },
                                                    {
                                                        xtype: 'numericfield',
                                                        hideLabel: false,
                                                        fieldLabel: 'Piutang Card',
                                                        name: 'p_d_piutangcard_n',
                                                        id: 'p_d_piutangcard_n_id',
                                                        enableKeyEvents: true,
                                                        ref: '../../../../../p_d_piutangcard_n',
                                                        anchor: '50%'
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: 'numericfield',
                                                hideLabel: false,
                                                fieldLabel: 'Potongan Penjualan',
                                                name: 'p_d_pot_jual_n',
                                                ref: '../../../../p_d_pot_jual_n',
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'compositefield',
                                                fieldLabel: 'Penjualan Obat',
                                                msgTarget: 'side',
                                                anchor: '100%',
                                                // anchor: '-20',
                                                defaults: {
                                                    flex: 1
                                                },
                                                items: [
                                                    {
                                                        xtype: 'numericfield',
                                                        hideLabel: false,
                                                        fieldLabel: 'Penjualan Obat',
                                                        //hidden:true,
                                                        name: 'p_k_obat_n',
                                                        enableKeyEvents: true,
                                                        ref: '../../../../../p_k_obat_n',
                                                        anchor: '50%'
                                                    },
                                                    {
                                                        xtype: 'label',
                                                        style: 'margin:5px',
                                                        width: 100,
                                                        text: 'PPn Obat :'
                                                    },
                                                    {
                                                        xtype: 'numericfield',
                                                        hideLabel: false,
                                                        fieldLabel: 'PPn Obat',
                                                        //hidden:true,
                                                        name: 'p_k_ppn_obat_n',
                                                        ref: '../../../../../p_k_ppn_obat_n',
                                                        // readOnly: true,
                                                        anchor: '50%'
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: 'compositefield',
                                                fieldLabel: 'Penjualan Apotik',
                                                msgTarget: 'side',
                                                anchor: '100%',
                                                // anchor: '-20',
                                                defaults: {
                                                    flex: 1
                                                },
                                                items: [
                                                    {
                                                        xtype: 'numericfield',
                                                        hideLabel: false,
                                                        fieldLabel: 'Penjualan Apotik',
                                                        //hidden:true,
                                                        name: 'p_k_apotik_n',
                                                        enableKeyEvents: true,
                                                        ref: '../../../../../p_k_apotik_n',
                                                        anchor: '50%'
                                                    },
                                                    {
                                                        xtype: 'label',
                                                        style: 'margin:5px',
                                                        width: 100,
                                                        text: 'PPn Apotik :'
                                                    },
                                                    {
                                                        xtype: 'numericfield',
                                                        hideLabel: false,
                                                        fieldLabel: 'PPn Apotik',
                                                        //hidden:true,
                                                        // readOnly: true,
                                                        name: 'p_k_ppn_apotik_n',
                                                        ref: '../../../../../p_k_ppn_apotik_n',
                                                        anchor: '50%'
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: 'compositefield',
                                                fieldLabel: 'Penjualan Perawatan',
                                                msgTarget: 'side',
                                                anchor: '100%',
                                                // anchor: '-20',
                                                defaults: {
                                                    flex: 1
                                                },
                                                items: [
                                                    {
                                                        xtype: 'numericfield',
                                                        hideLabel: false,
                                                        fieldLabel: 'Penjualan Perawatan',
                                                        //hidden:true,
                                                        name: 'p_k_jasa_n',
                                                        ref: '../../../../../p_k_jasa_n',
                                                        anchor: '50%'
                                                    },
                                                    {
                                                        xtype: 'label',
                                                        style: 'margin:5px',
                                                        width: 100,
                                                        text: 'Pembulatan :'
                                                    },
                                                    {
                                                        xtype: 'numericfield',
                                                        hideLabel: false,
                                                        fieldLabel: 'Pembulatan',
                                                        //hidden:true,
                                                        name: 'p_k_round_n',
                                                        ref: '../../../../../p_k_round_n',
                                                        anchor: '50%'
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: 'compositefield',
                                                fieldLabel: 'Pendapatan Lain',
                                                msgTarget: 'side',
                                                anchor: '100%',
                                                // anchor: '-20',
                                                defaults: {
                                                    flex: 1
                                                },
                                                items: [
                                                    {
                                                        xtype: 'numericfield',
                                                        hideLabel: false,
                                                        fieldLabel: 'Pendapatan Lain',
                                                        //hidden:true,
                                                        name: 'p_k_lain_lain_n',
                                                        ref: '../../../../../p_k_lain_lain_n',
                                                        anchor: '50%'
                                                    },
                                                    {
                                                        xtype: 'label',
                                                        style: 'margin:5px',
                                                        width: 100,
                                                        text: 'Pendapatan di Muka :'
                                                    },
                                                    {
                                                        xtype: 'numericfield',
                                                        hideLabel: false,
                                                        fieldLabel: 'Pendapatan di Muka',
                                                        //hidden:true,
                                                        name: 'p_k_pendmuka_n',
                                                        ref: '../../../../../p_k_pendmuka_n',
                                                        anchor: '50%'
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        layout: 'vbox',
                                        border: true,
                                        frame: false,
                                        layoutConfig: {
                                            padding: 1,
                                            align: 'stretch'
                                        },
                                        bodyStyle: 'background-color: #E4E4E4; padding: 2px',
                                        title: 'Piutang Card',
                                        flex: 1,
                                        items: [
                                            new jun.LahaImportBankFeeGrid({
                                                ref: '../../../../bankfeegrid',
                                                frame: false,
                                                frameHeader: !1,
                                                header: !1,
                                                flex: 1
                                            }),
                                            {
                                                bodyStyle: 'background-color: #E4E4E4; padding: 2px',
                                                title: 'Penjualan',
                                                labelWidth: 130,
                                                labelAlign: 'left',
                                                layout: 'form',
                                                ref: 'formz',
                                                border: true,
                                                items: [
                                                    {
                                                        xtype: 'numericfield',
                                                        hideLabel: false,
                                                        fieldLabel: 'Fee Card',
                                                        //hidden:true,
                                                        readOnly: true,
                                                        name: 'piu_d_fee_n',
                                                        id: 'piu_d_fee_n_id',
                                                        ref: '../../../../../piu_d_fee_n',
                                                        anchor: '100%'
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                layout: 'vbox',
                                border: false,
                                frame: false,
                                layoutConfig: {
                                    padding: 1,
                                    align: 'stretch'
                                }
                                ,
                                flex: 5,
                                items: [
                                    {
                                        layout: 'vbox',
                                        border: true,
                                        frame: false,
                                        layoutConfig: {
                                            padding: 1,
                                            align: 'stretch'
                                        },
                                        bodyStyle: 'background-color: #E4E4E4; padding: 2px',
                                        title: 'Biaya',
                                        flex: 1,
                                        items: [
                                            new jun.LahaImportBiayaGrid({
                                                ref: '../../../../biayagrid',
                                                frame: false,
                                                frameHeader: !1,
                                                header: !1,
                                                flex: 5
                                            }),
                                            {
                                                bodyStyle: 'background-color: #E4E4E4; padding: 2px',
                                                labelWidth: 90,
                                                labelAlign: 'left',
                                                layout: 'form',
                                                ref: 'formz',
                                                border: false,
                                                items: [
                                                    {
                                                        xtype: 'compositefield',
                                                        fieldLabel: 'Kas Cabang',
                                                        msgTarget: 'side',
                                                        anchor: '100%',
                                                        // anchor: '-20',
                                                        defaults: {
                                                            flex: 1
                                                        },
                                                        items: [
                                                            {
                                                                xtype: 'numericfield',
                                                                hideLabel: false,
                                                                fieldLabel: 'Kas Cabang',
                                                                name: 'b_k_kas_n',
                                                                ref: '../../../../../b_k_kas_n',
                                                                anchor: '50%'
                                                            },
                                                            {
                                                                xtype: 'label',
                                                                style: 'margin:5px',
                                                                width: 90,
                                                                text: 'Pembulatan :'
                                                            },
                                                            {
                                                                xtype: 'numericfield',
                                                                hideLabel: false,
                                                                fieldLabel: 'Pembulatan',
                                                                //hidden:true,
                                                                name: 'b_k_round_n',
                                                                ref: '../../../../../b_k_round_n',
                                                                anchor: '100%'
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'compositefield',
                                                        fieldLabel: 'Hutang PPH 21',
                                                        msgTarget: 'side',
                                                        anchor: '100%',
                                                        // anchor: '-20',
                                                        defaults: {
                                                            flex: 1
                                                        },
                                                        items: [
                                                            {
                                                                xtype: 'numericfield',
                                                                fieldLabel: 'Hutang PPH 21',
                                                                name: 'b_k_pph21_n',
                                                                ref: '../../../../../b_k_pph21_n',
                                                                anchor: '50%'
                                                            },
                                                            {
                                                                xtype: 'label',
                                                                style: 'margin:5px',
                                                                width: 90,
                                                                text: 'Note PPH 21:'
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                name: 'b_k_pph21_note',
                                                                anchor: '50%'
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'compositefield',
                                                        fieldLabel: 'Hutang PPH 22',
                                                        msgTarget: 'side',
                                                        anchor: '100%',
                                                        // anchor: '-20',
                                                        defaults: {
                                                            flex: 1
                                                        },
                                                        items: [
                                                            {
                                                                xtype: 'numericfield',
                                                                hideLabel: false,
                                                                fieldLabel: 'Hutang PPH 22',
                                                                name: 'b_k_pph22_n',
                                                                ref: '../../../../../b_k_pph22_n',
                                                                anchor: '50%'
                                                            },
                                                            {
                                                                xtype: 'label',
                                                                style: 'margin:5px',
                                                                width: 90,
                                                                text: 'Note PPH 22:'
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                name: 'b_k_pph22_note',
                                                                anchor: '50%'
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'compositefield',
                                                        fieldLabel: 'Hutang PPH 23',
                                                        msgTarget: 'side',
                                                        anchor: '100%',
                                                        // anchor: '-20',
                                                        defaults: {
                                                            flex: 1
                                                        },
                                                        items: [
                                                            {
                                                                xtype: 'numericfield',
                                                                hideLabel: false,
                                                                fieldLabel: 'Hutang PPH 23',
                                                                name: 'b_k_pp23_n',
                                                                ref: '../../../../../b_k_pp23_n',
                                                                anchor: '50%'
                                                            },
                                                            {
                                                                xtype: 'label',
                                                                style: 'margin:5px',
                                                                width: 90,
                                                                text: 'Note PPH 23:'
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                name: 'b_k_pp23_note',
                                                                anchor: '50%'
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'compositefield',
                                                        fieldLabel: 'Hutang PPH 4(2)',
                                                        msgTarget: 'side',
                                                        anchor: '100%',
                                                        // anchor: '-20',
                                                        defaults: {
                                                            flex: 1
                                                        },
                                                        items: [
                                                            {
                                                                xtype: 'numericfield',
                                                                hideLabel: false,
                                                                fieldLabel: 'Hutang PPH 4(2)',
                                                                name: 'b_k_pph4_2_n',
                                                                ref: '../../../../../b_k_pph4_2_n',
                                                                anchor: '100%'
                                                            },
                                                            {
                                                                xtype: 'label',
                                                                style: 'margin:5px',
                                                                width: 90,
                                                                text: 'Note PPH 4(2):'
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                name: 'b_k_pph4_2_note',
                                                                anchor: '50%'
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    new jun.LahaImportBankSetorGrid({
                                        title: 'Setoran',
                                        height: 200
                                    })
                                ]
                            }
                        ]
                    },
                    {
                        xtype: 'hidden',
                        name: 'piu_d_total_bank',
                        id: 'piu_d_total_bank_id'
                    },
                    {
                        xtype: 'hidden',
                        name: 'b_d_total_biaya',
                        id: 'b_d_total_biaya_id'
                    },
                    {
                        xtype: 'hidden',
                        name: 's_total_setor',
                        id: 's_total_setor_id'
                    },
                    {
                        xtype: 'hidden',
                        name: 'p',
                        ref: '../posting_laha'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Check Balance',
                    hidden: false,
                    ref: '../btnBalance'
                }, {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.LahaImportWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.on('close', this.onClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnBalance.on('click', this.saveCheckBalance, this);
        this.p_k_obat_n.on('keyup', this.onObatChange, this);
        this.p_k_apotik_n.on('keyup', this.onApotikChange, this);
        this.p_d_piutangcard_n.on('keyup', this.onPiutangChange, this);
        if (this.modez == 1) {
            this.btnSave.setVisible(false);
        } else if (this.modez == 2) {
            this.btnBalance.setVisible(false);
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnBalance.setVisible(false);
            this.btnSave.setVisible(true);
        }
    },
    onClose: function () {
        jun.rztLahaImportBiaya.removeAll();
        jun.rztLahaImportBankFee.removeAll();
        jun.rztLahaImportBankSetor.removeAll();
    },
    onPiutangChange: function (a, e) {
        var total_bank = parseFloat(Ext.getCmp('piu_d_total_bank_id').getValue());
        var piutang = a.getValue();
        var fee = piutang - total_bank;
        Ext.getCmp('piu_d_fee_n_id').setValue(fee);
    },
    onObatChange: function (a, e) {
        this.p_k_ppn_obat_n.setValue(a.getValue() * 0.1);
    },
    onApotikChange: function (a, e) {
        this.p_k_ppn_apotik_n.setValue(a.getValue() * 0.1);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveCheckBalance: function () {
        this.btnBalance.setDisabled(true);
        var urlz = 'LahaImport/CekBalance/';
        Ext.getCmp('form-LahaImport').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                biaya: Ext.encode(Ext.pluck(
                        jun.rztLahaImportBiaya.data.items, "data")),
                bankfee: Ext.encode(Ext.pluck(
                        jun.rztLahaImportBankFee.data.items, "data")),
                setor: Ext.encode(Ext.pluck(
                        jun.rztLahaImportBankSetor.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                jun.rztCekBalance.loadData(response.msg);
                var form = new jun.LahaImportCekBalanceWin({modez: 1, ids: this.id, p: this.posting_laha.getValue()});
                form.show();
//                this.btnBalance.setDisabled(true);
//                this.btnSaveClose.setDisabled(true);
                this.btnBalance.setDisabled(false);
//                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnBalance.setDisabled(false);
            }
        });
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'LahaImport/create/';
        Ext.getCmp('form-LahaImport').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                posting: false,
                biaya: Ext.encode(Ext.pluck(
                        jun.rztLahaImportBiaya.data.items, "data")),
                bankfee: Ext.encode(Ext.pluck(
                        jun.rztLahaImportBankFee.data.items, "data")),
                setor: Ext.encode(Ext.pluck(
                        jun.rztLahaImportBankSetor.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztLahaImport.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-LahaImport').getForm().reset();
                    this.onClose();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.LahaImportCekBalanceWin = Ext.extend(Ext.Window, {
    title: 'Import Laha',
    modez: 1,
    width: 450,
    height: 520,
    layout: 'fit',
    modal: true,
    // padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            new jun.LahaImportCekBalanceGrid({
                frameHeader: !1,
                header: !1
            })
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Posting',
                    ref: '../btnPosting'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.LahaImportCekBalanceWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnPosting.on('click', this.onbtnPostingclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
    },
    onActivate: function () {
        var d = jun.rztCekBalance.sum('d');
        var k = jun.rztCekBalance.sum('k');
//        if (this.p != 0) {
//            this.btnPosting.setVisible(false);
//        } else {
//            this.btnPosting.setVisible(d === k);
//        }
        var total = Math.round(d-k,2);
        this.btnPosting.setVisible(total === 0.00);
    },
    onbtnPostingclick: function () {
        this.btnPosting.setDisabled(true);
        var urlz = 'LahaImport/create/';
        Ext.getCmp('form-LahaImport').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                biaya: Ext.encode(Ext.pluck(
                        jun.rztLahaImportBiaya.data.items, "data")),
                bankfee: Ext.encode(Ext.pluck(
                        jun.rztLahaImportBankFee.data.items, "data")),
                setor: Ext.encode(Ext.pluck(
                        jun.rztLahaImportBankSetor.data.items, "data")),
                posting: true,
                id: this.ids,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztLahaImport.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                Ext.getCmp(this.ids).close();
                this.btnPosting.setDisabled(false);
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnPosting.setDisabled(false);
            }
        });
//        this.btnPosting.setDisabled(true);
//        this.close();
    },
    onbtnCancelclick: function () {
        this.close();
    }
});