jun.PurchaseCoastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PurchaseCoastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PurchaseCoaStoreId',
            url: 'purchase/PurchaseCoa',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'purchase_coa_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tdate'},
                {name: 'doc_ref_other'},
                {name: 'user_id'},
                {name: 'store'},
                {name: 'supplier_id'},
//                {name: 'vat'},
                {name: 'up'},
                {name: 'tgl_jatuh_tempo'},
                {name: 'lunas'},
                {name: 'p'},
                {name: 'total', type: 'float'}
                
            ]
        }, cfg));
    }
});
jun.rztPurchaseCoa = new jun.PurchaseCoastore();
//jun.rztPurchaseCoa.load();
