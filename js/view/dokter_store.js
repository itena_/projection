jun.Dokterstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Dokterstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'DokterStoreId',
            url: 'Dokter',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'dokter_id'},
                {name: 'nama_dokter'},
                {name: 'gol_id'},
                {name: 'kode_dokter'},
                {name: 'store'},
                {name: 'active'}
            ]
        }, cfg));
    }
});
jun.rztDokter = new jun.Dokterstore();
jun.rztDokterLib = new jun.Dokterstore({
    baseParams: {mode: "lib"},
    method: 'POST'
});
jun.rztDokterCmp = new jun.Dokterstore({baseParams: {f: "cmp"}});
jun.rztDokterStatus = new jun.Dokterstore();
//jun.rztDokter.load();
