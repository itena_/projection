jun.LahaImportstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.LahaImportstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LahaImportStoreId',
            url: 'LahaImport',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'laha_import_id'},
                {name: 'doc_ref'},
                {name: 'store'},
                {name: 'p_d_kas_bank_id'},
                {name: 'p_d_kas_n'},
                {name: 'p_d_piutangcard_coa'},
                {name: 'p_d_piutangcard_n'},
                {name: 'p_k_obat_coa'},
                {name: 'p_k_jasa_coa'},
                {name: 'p_k_obat_n'},
                {name: 'p_k_jasa_n'},
                {name: 'p_k_ppn_obat_coa'},
                {name: 'p_k_ppn_obat_n'},
                {name: 'p_k_pendmuka_n'},
                {name: 'p_k_lain_lain_n'},
                {name: 'b_d_total_biaya'},
                {name: 'b_k_pph21_coa'},
                {name: 'b_k_pph22_coa'},
                {name: 'b_k_pph23_coa'},
                {name: 'b_k_pph4_2_coa'},
                {name: 'b_k_kas_n'},
                {name: 'b_k_pph21_n'},
                {name: 'b_k_pph22_n'},
                {name: 'b_k_pp23_n'},
                {name: 'b_k_pph4_2_n'},
                {name: 's_total_setor'},
                {name: 'tgl'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'p_d_pot_jual_coa'},
                {name: 'p_d_pot_jual_n'},
                {name: 'p_k_apotik_coa'},
                {name: 'p_k_apotik_n'},
                {name: 'p_k_ppn_apotik_n'},
                {name: 'p_k_ppn_apotik_coa'},
                {name: 'p_k_round_coa'},
                {name: 'p_k_round_n'},
                {name: 'p'},
                {name: 'piu_d_total_bank'},
                {name: 'piu_d_fee_n'},
                {name: 'piu_d_fee_coa'},
                {name: 'b_k_round_coa'},
                {name: 'b_k_round_n'},
                
                {name: 'b_k_pph21_note'},
                {name: 'b_k_pph22_note'},
                {name: 'b_k_pp23_note'},
                {name: 'b_k_pph4_2_note'}
            ]
        }, cfg));
    }
});
jun.rztLahaImport = new jun.LahaImportstore();
//jun.rztLahaImport.load();
var renderCekBalance = new Ext.data.JsonReader({
    root: 'results',
    totalProperty: 'total',
    fields: [
        {name: 'd', type: 'float'},
        {name: 'k', type: 'float'},
        {name: 'coa'},
        {name: 'grup'}
    ]
});
jun.rztCekBalance = new Ext.data.GroupingStore({
    reader: renderCekBalance,
    groupDir: undefined,
    // url: 'LahaImportBankFee',
    // sortInfo: {field: 'kode_material', direction: 'ASC'},
    groupField: 'grup'
});