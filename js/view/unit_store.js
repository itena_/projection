jun.Unitstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Unitstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'UnitStoreId',
            url: 'projection/Unit',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'unit_id'},
                {name:'name'},
                {name:'code'},
                {name:'description'},
                {name:'created_at'},
                {name:'businessunit_id'},
                {name:'uploadfile'},
                {name:'uploaddate'},
                
            ]
        }, cfg));
    }
});
jun.rztUnit = new jun.Unitstore();
jun.rztUnitLib = new jun.Unitstore();
jun.rztUnitLib.load();
