jun.AssetWin = Ext.extend(Ext.Window, {
    title: 'Asset',
    modez:1,
    width: 523,
    height: 389,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        if (jun.rztAssetGroup.getTotalCount() === 0) {
            jun.rztAssetGroup.load();
        }
        if (jun.rztBarangAsst.getTotalCount() === 0) {
            jun.rztBarangAsst.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }


        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-Asset',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Doc Ref',
                        hideLabel:false,
                        //hidden:true,
                        name:'doc_ref',
                        id:'doc_refid',
                        ref:'../doc_ref',
                        maxLength: 100,
                        //allowBlank: ,
                        readOnly:true,
                        anchor: '100%'
                    },
                    /*{
                        xtype: 'textfield',
                        fieldLabel: 'No. Activa',
                        hideLabel:false,
                        //hidden:true,
                        name:'ati',
                        id:'atiid',
                        ref:'../ati',
                        maxLength: 100,
                        //allowBlank: ,
                        readOnly:false,
                        emptyText: "No Activa",
                        anchor: '100%'
                    },*/
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Asset Name',
                        ref: '../barang_id',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztBarangAsst,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kode_barang} </br> {nama_barang} </br> {ket} </span></h3>',
                            "</div></tpl>"),
                        listWidth: 450,
                        lastQuery: "",
                        pageSize: 20,
                        forceSelection: true,
                        autoSelect: false,
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        hiddenName: 'barang_id',
                        name: 'barang_id',
                        valueField: 'barang_id',
                        displayField: 'kode_barang',
                        emptyText: "Asset Name",

                        //value: STORE,
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    /*{
                        xtype: 'textfield',
                        fieldLabel: 'Asset Name',
                        hideLabel:false,
                        //hidden:true,
                        name:'asset_name',
                        id:'asset_nameid',
                        ref:'../asset_name',
                        maxLength: 150,
                        //allowBlank: ,
                        anchor: '100%'
                    },*/
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Qty',
                        hideLabel:false,
                        //hidden:true,
                        name:'qty',
                        id:'qtyid',
                        ref:'../qty',
                        maxLength: 10,
                        emptyText: "Qty",
                        //allowBlank: ,
                        anchor: '100%'
                    },

                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        //emptyText: "Asset Name",
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../date_acquisition',
                            fieldLabel: 'Date Acquisition',
                            name:'date_acquisition',
                            id:'date_acquisitionid',
                            format: 'd M Y',
                            value: DATE_NOW,
                            //allowBlank: ,
                            anchor: '100%'                            
                        }, 
                                                                     {
                                    xtype: 'numericfield',
                                    fieldLabel: 'Price Acquisition',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'price_acquisition',
                                    id:'price_acquisitionid',
                                    ref:'../price_acquisition',
                                    maxLength: 30,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'numericfield',
                                    fieldLabel: 'New Price Acquisition',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'new_price_acquisition',
                                    id:'new_price_acquisitionid',
                                    ref:'../new_price_acquisition',
                                    maxLength: 30,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender:true,
                            mode: 'local',
                            forceSelection: true,
                            fieldLabel: 'Class',
                            store: jun.rztAssetGroup,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                                    '<h3><span">{golongan} - {desc} - period ({period})</span></h3>',
                                                    "</div></tpl>"),
                            hiddenName:'asset_group_id',
                            valueField: 'asset_group_id',
                            emptyText: "All Class",
                            displayField: 'golongan',
                            anchor: '100%'
                        },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Description',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style : {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'description',
                        id: 'descriptionid',
                        ref: '../description',
                        maxLength: 255,
                        //allowBlank: 1,
                        anchor: '100%'
                    }
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            /*var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'asset/Asset/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'asset/Asset/create/';
                }
             */
        var urlz = 'asset/Asset/create/';
            Ext.getCmp('form-Asset').getForm().submit({
                url:urlz,
                params: {
                    detil: Ext.encode(Ext.pluck(
                        jun.rztAssetDetail.data.items, "data")),
                    id: this.id,
                    mode: this.modez
                },
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztAsset.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-Asset').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});