jun.Beautystore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Beautystore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BeautyStoreId',
            url: 'Beauty',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'beauty_id'},
                {name: 'nama_beauty'},
                {name: 'gol_id'},
                {name: 'kode_beauty'},
                {name: 'active'},
                {name: 'store'}
            ]
        }, cfg));
    }
});
jun.rztBeauty = new jun.Beautystore();
jun.rztBeautyLib = new jun.Beautystore({
    baseParams: {mode: "lib"},
    method: 'POST'
});
jun.rztBeautyLib.load();
jun.rztBeautyCmp = new jun.Beautystore({baseParams: {f: "cmp"}});
jun.rztBeauty1Cmp = new jun.Beautystore({baseParams: {f: "cmp"}});
jun.rztBeauty2Cmp = new jun.Beautystore({baseParams: {f: "cmp"}});
jun.rztBeauty3Cmp = new jun.Beautystore({baseParams: {f: "cmp"}});
jun.rztBeauty4Cmp = new jun.Beautystore({baseParams: {f: "cmp"}});
jun.rztBeauty5Cmp = new jun.Beautystore({baseParams: {f: "cmp"}});
jun.BeautyOffdutystore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BeautyOffdutystore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BeautyOffdutyStoreId',
            url: 'Beauty/Offduty',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'beauty_id'},
                {name: 'nama_beauty'},
                {name: 'gol_id'},
                {name: 'kode_beauty'},
                {name: 'active'},
                {name: 'store'},
                {name: 'up'},
                {name: 'tipe'},
                {name: 'start_at'},
                {name: 'end_at'},
                {name: 'pending'},
                {name: 'end_estimate'},
                {name: 'total_durasi'},
                {name: 'off_'}
            ]
        }, cfg));
    }
});
//jun.rztBeautyOffduty = new jun.BeautyOffdutystore();
jun.rztBeautyOffduty1Cmp = new jun.BeautyOffdutystore({baseParams: {f: "cmp"}});
jun.rztBeautyOffduty2Cmp = new jun.BeautyOffdutystore({baseParams: {f: "cmp"}});
jun.rztBeautyOffduty3Cmp = new jun.BeautyOffdutystore({baseParams: {f: "cmp"}});
jun.rztBeautyOffduty4Cmp = new jun.BeautyOffdutystore({baseParams: {f: "cmp"}});
jun.rztBeautyOffduty5Cmp = new jun.BeautyOffdutystore({baseParams: {f: "cmp"}});