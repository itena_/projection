jun.StockMovesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.StockMovesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StockMovesStoreId',
            url: 'StockMoves',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'stock_moves_id'},
                {name: 'type_no'},
                {name: 'trans_no'},
                {name: 'tran_date'},
                {name: 'price'},
                {name: 'reference'},
                {name: 'qty'},
                {name: 'barang_id'},
            ]
        }, cfg));
    }
});
jun.rztStockMoves = new jun.StockMovesstore();
//jun.rztStockMoves.load();
