jun.Transaksistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Transaksistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransaksiStoreId',
            url: 'projection/Transaksi',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'transaksi_id'},
                {name:'outlet_id'},
                {name:'produk_id'},
                {name:'groupproduk_id'},
                {name:'unit_id'},
                {name:'businessunit_id'},
                {name:'doc_ref'},
                {name:'namaoutlet'},
                {name:'kodeoutlet'},
                {name:'kodeproduk'},
                {name:'kodegroupproduk'},
                {name:'harga'},
                {name:'hargabeli'},
                {name:'jmloutlet'},
                {name:'persentase'},
                {name:'qty'},
                {name:'satuan'},
                {name:'salesqty'},
                {name:'salesrp'},
                {name:'salesrpbeli'},
                {name:'tdate'},
                {name:'note'},
                {name:'created_at'},
                {name:'uploadfile'},
                {name:'uploaddate'},
                
            ]
        }, cfg));

    },

});
jun.rztTransaksi = new jun.Transaksistore();
//jun.rztTransaksi.load();
