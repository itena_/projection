jun.TransferBarangAssetDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "TransferBarangAssetDetails",
    id: 'docs-jun.TransferBarangAssetDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Barang Asset Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_asset_id',
            width: 150,
            renderer: jun.renderKodeBarangAsset
        },
        {
            header: 'Barang Asset Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_asset_id',
            width: 300,
            renderer: jun.renderBarangAsset
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 60,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Price Per Item',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 140,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Vat (%)',
            sortable: true,
            resizable: true,
            dataIndex: 'vat',
            width: 60,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztTransferBarangAssetDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 8,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            style: 'margin-bottom:2px',
                            forceSelection: true,
                            store: jun.rztBarangAssetLib,
                            hiddenName: 'barang_asset_id',
                            valueField: 'barang_asset_id',
                            ref: '../../barangAsset',
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">',
                                '<span style=" font-weight: bold">{kode_barang_asset}</span><br />{nama_barang_asset}',
                                "</div></tpl>"
                            ),
                            displayField: 'kode_barang_asset'
//                            colspan: 3
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0\xA0\xA0Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        // {
                        //     xtype: 'label',
                        //     style: 'margin:5px',
                        //     ref: '../../sat',
                        //     text: '\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0'
                        // },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0\xA0\xA0Price (Rp) :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'priceid',
                            ref: '../../price',
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0\xA0\xA0Vat (%) :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'vatid',
                            ref: '../../vat',
                            width: 50,
                            value: 0,
                            //readOnly:true,
                            minValue: 0
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 5,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            text: 'Delete',
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.TransferBarangAssetDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        // this.barangAsset.on('Change', this.onItemChange, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.on('add', this.onStoreChange, this);
        this.store.on('update', this.onStoreChange, this);
        this.store.on('remove', this.onStoreChange, this);
        // this.store.on('load', this.onStoreChange, this);
    },
    // onItemChange: function () {
    //     var barang_asset_id = this.barangAsset.getValue();
    //     var barang = jun.getBarangAsset(barang_asset_id);
    //     this.sat.setText(barang.data.sat);
    // },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    onStoreChange: function () {
        jun.rztTransferBarangAssetDetails.refreshTotal();
    },
    loadForm: function () {
        var barang_asset_id = this.barangAsset.getValue();
        if (barang_asset_id == "") {
            Ext.MessageBox.alert("Error", "You have not selected an item");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
        if (this.btnEdit.text != 'Save') {
            var a = jun.rztTransferBarangAssetDetails.findExact("barang_asset_id", barang_asset_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Item already added");
                return
            }
        }
        var qty = parseFloat(this.qty.getValue());
        var price = parseFloat(this.price.getValue());
        var vat = parseFloat(this.vat.getValue());
        var bruto = price*qty;
        var vatrp = vat/100*price*qty;
        var total = bruto+vatrp;
        
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_asset_id', barang_asset_id);
            record.set('qty', qty);
            record.set('price', price);
            record.set('vat', vat);
            record.set('bruto', bruto);
            record.set('vatrp', vatrp);
            record.set('total', total);
            record.commit();
        } else {
            var c = jun.rztTransferBarangAssetDetails.recordType,
                d = new c({
                    barang_asset_id: barang_asset_id,
                    qty: qty,
                    price: price,
                    vat: vat,
                    bruto: bruto,
                    vatrp: vatrp,
                    total: total
                });
            jun.rztTransferBarangAssetDetails.add(d);
        }
        this.barangAsset.reset();
        this.qty.reset();
        this.price.reset();
        this.vat.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected an item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barangAsset.setValue(record.data.barang_asset_id);
            this.qty.setValue(record.data.qty);
            this.price.setValue(record.data.price);
            this.vat.setValue(record.data.vat);
            btn.setText("Save");
            this.btnDisable(true);
            this.onItemChange();
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected an item");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected an item");
            return;
        }
        this.store.remove(record);
    }
})
