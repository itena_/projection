jun.TransaksiCategorystore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.TransaksiCategorystore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransaksiCategoryStoreId',
            url: 'projection/TransaksiCategory',
            root: 'results',
            totalProperty: 'total',
            fields:
                [
                    {name:'category_id'},
                    {name:'category_name'},
                    {name:'category_code'},
                    {name:'description'}
                
            ]
        }, cfg));
    }
});
jun.rztTransaksiCategory = new jun.TransaksiCategorystore();
//jun.rztTransaksiCategory.load();
