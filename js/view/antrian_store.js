jun.Antrianstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Antrianstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AntrianStoreId',
            url: 'Antrian',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'id_antrian'},
                {name: 'nomor'},
                {name: 'no_member'},
                {name: 'kartu'},
                {name: 'bagian'},
                {name: 'tujuan'},
                {name: 'picked'},
                {name: 'tanggal'},
                {name: 'timestamp'},
                {name: 'spesial'}
            ]
        }, cfg));
    }
});
jun.rztAntrian = new jun.Antrianstore({
    url: 'Antrian/AntrianIndex'
});
jun.rztAntrianPending = new jun.Antrianstore({
    url: 'Antrian/AntrianPendingIndex'
});
//jun.rztAntrian.load();
