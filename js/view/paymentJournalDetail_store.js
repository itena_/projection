jun.PaymentJournalDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.PaymentJournalDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PaymentJournalDetailStoreId',
            url: 'PaymentJournalDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'payment_journal_detail_id'},
                {name:'payment_journal_id'},
                {name:'store_kode'},
                {name:'debit'},
                {name:'kredit'},
                {name:'account_code'},
                {name:'memo_'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        var total = this.sum("debit") - this.sum("kredit");
//        console.log(total);
        Ext.getCmp("tot_debit_id").setValue(this.sum("debit"));
        Ext.getCmp("tot_kredit_id").setValue(this.sum("kredit"));
        Ext.getCmp("hutangBalance").setValue(total);
    }
});
jun.rztPaymentJournalDetail = new jun.PaymentJournalDetailstore();
//jun.rztAccountPayableDetail.load();
