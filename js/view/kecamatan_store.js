jun.Kecamatanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Kecamatanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KecamatanStoreId',
            url: 'Kecamatan',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kecamatan_id'},
                {name: 'nama_kecamatan'},
                {name: 'kota_id'}
            ]
        }, cfg));
    }
});
jun.rztKecamatan = new jun.Kecamatanstore();
jun.rztKecamatanLib = new jun.Kecamatanstore();
jun.rztKecamatanCmp = new jun.Kecamatanstore();
//jun.rztKecamatan.load();
