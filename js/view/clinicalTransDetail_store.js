jun.ClinicalTransDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ClinicalTransDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ClinicalTransDetailStoreId',
            url: 'ClinicalTransDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'clinical_trans_detail_id'},
                {name: 'qty'},
                {name: 'note'},
                {name: 'clinical_trans_id'},
                {name: 'barang_clinical'},
                {name: 'sisa'},
                {name: 'awal'}
            ]
        }, cfg));
    }
});
jun.rztClinicalTransDetail = new jun.ClinicalTransDetailstore();
//jun.rztClinicalTransDetail.load();
