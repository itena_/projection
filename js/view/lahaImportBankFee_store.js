jun.LahaImportBankFeestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.LahaImportBankFeestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LahaImportBankFeeStoreId',
            url: 'LahaImportBankFee',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'laha_import_bank_fee_id'},
                {name: 'bank', type: 'float'},
                {name: 'laha_import_id'},
                {name: 'bank_id'},
                {name: 'fee'},
                {name: 'piutang_card'}
            ]
        }, cfg));
    }
});
// var renderLahaImportBankFee= new jun.LahaImportBankFeestore();
var renderLahaImportBankFee = new Ext.data.JsonReader({
    idProperty: 'laha_import_bank_fee_id',
    root: 'results',
    totalProperty: 'total',
    fields: [
        {name: 'laha_import_bank_fee_id'},
        {name: 'bank', type: 'float'},
        {name: 'laha_import_id'},
        {name: 'bank_id'},
        {name: 'note_'},
        {name: 'type_'}
    ]
});
jun.rztLahaImportBankFee = new Ext.data.GroupingStore({
    reader: renderLahaImportBankFee,
    url: 'LahaImportBankFee',
    // sortInfo: {field: 'kode_material', direction: 'ASC'},
    groupField: 'laha_import_bank_fee_id'
});
jun.rztLahaImportBankSetor = new Ext.data.GroupingStore({
    reader: renderLahaImportBankFee,
    url: 'LahaImportBankFee',
    // sortInfo: {field: 'kode_material', direction: 'ASC'},
    groupField: 'laha_import_bank_fee_id'
});
