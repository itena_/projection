jun.BonusTemplatestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.BonusTemplatestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BonusTemplateStoreId',
            url: 'BonusTemplate',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'bonus_template_id'},
{name:'grup_id'},
{name:'nama_grup'},
{name:'bonus_name_id'},
{name:'persen_bonus'},
                
            ]
        }, cfg));
    }
});
jun.rztBonusTemplate = new jun.BonusTemplatestore();
//jun.rztBonusTemplate.load();
