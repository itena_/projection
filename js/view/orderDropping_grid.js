jun.OrderDroppingGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Transfer Requests",
    id: 'docs-jun.OrderDroppingGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100,
            renderer : Ext.util.Format.dateRenderer('d-M-Y'),
            filter: {xtype: "datefield", format: "d-M-Y"}
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Pengirim',
            sortable: true,
            resizable: true,
            dataIndex: 'store_pengirim',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'lunas',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case PR_DRAFT :
                        return 'DRAFT';
                    case PR_OPEN :
                        return 'OPEN';
                    case PR_PROCESS :
                        metaData.style += "background-color: #FCFC98;";
                        return 'PROCESS';
                    case PR_CLOSED :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'CLOSE';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [-1, 'DRAFT'], [0, 'OPEN'], [1, 'PROCESS'], [2, 'CLOSE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Approved',
            sortable: true,
            resizable: true,
            dataIndex: 'approved',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                value && (metaData.css += ' silk13-tick ');
                return '';
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NEEDS APPROVAL'], [1, 'APPROVED']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Up',
            sortable: true,
            resizable: true,
            dataIndex: 'up',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case DR_PENDING :
                        metaData.style += "background-color: #FF1A1A;";
                        return 'PENDING';
                    case DR_SEND :
                        metaData.style += "background-color: #FFFF33;";
                        return 'SEND';
                    case DR_APPROVE :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'APPROVE';
                    case DR_PROCESS :
                        metaData.style += "background-color: #00FF00;";
                        return 'PROCESS';
                    case DR_RECEIVE :
                        metaData.style += "background-color: #FF4D88;";
                        return 'RECEIVE';
                    case DR_CLOSE :
                        metaData.style += "background-color: #ADAD85;";
                        return 'CLOSE';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'PENDING'], [1, 'SEND'], [2, 'APPROVE'], [3, 'PROCESS'], [4, 'RECEIVE'], [5, 'CLOSE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        }
    ],
    initComponent: function () {
        if(jun.rztStoreCmp.getTotalCount()==0) jun.rztStoreCmp.load();
        if(jun.rztBarangNonJasaAll.getTotalCount()==0) jun.rztBarangNonJasaAll.load();
        if(jun.rztBarangLib.getTotalCount()==0) jun.rztBarangLib.load();
        
        this.store = jun.rztOrderDropping;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Create Transfer Requests',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Transfer Requests',
                    ref: '../btnEdit'
                },
                { xtype: 'tbseparator'},
                {
                    xtype: 'button',
                    text: 'Resend',
                    ref: '../btnCheck',
                    iconCls: 'silk13-arrow_refresh'
                },
                { xtype: 'tbseparator'},
                {
                    xtype: 'button',
                    text: 'Open',
                    ref: '../btnOpen',
                    iconCls: 'silk13-flag_green'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Order',
                    ref: '../btnPrintOrderDropping',
                    iconCls: 'silk13-page_white_excel'
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-OrderDroppingGrid",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "order_dropping_id",
                            ref: "../../order_dropping_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                },
                { xtype: 'tbseparator'},
                {
                    xtype: 'button',
                    text: 'History',
                    ref: '../btnHistory',
                    iconCls: 'silk13-clock'
                },
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        
        jun.OrderDroppingGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnCheck.on('Click', this.loadCheck, this);
        this.btnOpen.on('Click', this.btnOpenOnClick, this);
        this.btnPrintOrderDropping.on('Click', this.printOrderDropping, this);
        this.btnHistory.on('Click', this.loadHistory, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        
        var filter = r.get('lunas') == PR_DRAFT && r.get('store') == STORE;
        this.btnEdit.setText(filter ?"Edit Transfer Requests":"Show Transfer Requests");
        this.btnOpen.setDisabled(!filter);
        
        this.btnPrintOrderDropping.setDisabled((r.get('store_pengirim') != STORE || r.get('lunas') == PR_DRAFT));
    },
    loadForm: function () {
        var form = new jun.OrderDroppingWin({
            modez: 0,
            title: 'Create Transfer Requests'
        });
        form.show();
    },
    loadHistory: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }

        var idz = selectedz.json.order_dropping_id;
        //Ext.MessageBox.alert("Warning", idz);

        var form = new jun.DroppingHistoryWin({
            modez: 0,
            historyid: {order_dropping_id :idz},
            title: 'Dropping History '
        });
        form.show();
    },
    loadCheck: function () {

        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Transfer Requests");
            return;
        }

        var up = record.json.up;
        /*if(up >= DR_SEND )
         {
         Ext.MessageBox.alert("Warning", "Transfer Requests ini telah di terkirim ditujuan.");
         return;
         }*/

        var apv = record.json.approved;
        if(apv == 1)
        {
             Ext.MessageBox.alert("Warning", "Transfer Requests ini telah di approve.");
            return;
        }

        var cab = record.json.store_penerima;

        if(cab == STORE)
        {
            Ext.MessageBox.alert("Warning", "Anda sebagai penerima tidak dapat melakukan resend.");
            return;
        }


        Ext.Ajax.request({
            url: 'OrderDropping/Resend',
            method: 'POST',
            params: {
                id: record.json.order_dropping_id
            },
            scope: this,
            success: function (f, a) {
                this.store.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    },

    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Transfer Requests");
            return;
        }
        var idz = selectedz.json.order_dropping_id;
        var filter = this.record.get('lunas') == PR_DRAFT && this.record.get('store') == STORE;
        var form = new jun.OrderDroppingWin({
            modez: (filter?1:2),
            id: idz,
            title: (filter?"Edit Transfer Requests":'Show Transfer Requests')
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztOrderDroppingDetails.baseParams = {
            order_dropping_id: idz
        };
        jun.rztOrderDroppingDetails.load();
        jun.rztOrderDroppingDetails.baseParams = {};
    },
    btnOpenOnClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Transfer Requests.");
            return;
        }
        if (this.record.get('store') != STORE){
            Ext.MessageBox.alert("Warning", "Anda tidak diperbolehkan untuk memposting Order ini.");
            return;
        }
        if (this.record.get('lunas') != PR_DRAFT){
            Ext.MessageBox.alert("Warning", "Order telah diposting sebelumnya.");
            return;
        }
        Ext.MessageBox.confirm('Open Transfer Requests', 'Apakah anda yakin ingin memposting Order ini?', this.openOrder, this);
    },
    openOrder: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'OrderDropping/Open',
            method: 'POST',
            params: {
                id: record.json.order_dropping_id
            },
            scope: this,
            success: function (f, a) {
                this.store.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    printOrderDropping: function (){
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Transfer Requests.");
            return;
        }
        if (this.record.get('store_pengirim') != STORE || this.record.get('lunas') == PR_DRAFT) {
            Ext.MessageBox.alert("Warning", "Tidak dapat print data ini.");
            return;
        }
        if (this.record.get('approved') == 0) {
            Ext.MessageBox.alert("Warning", "Transfer Requests belum di-approve.");
            return;
        }
        Ext.getCmp("form-OrderDroppingGrid").getForm().standardSubmit = !0;
        Ext.getCmp("form-OrderDroppingGrid").getForm().url = "Report/PrintOrderDropping";
        this.order_dropping_id.setValue(selectedz.json.order_dropping_id);
        var form = Ext.getCmp('form-OrderDroppingGrid').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
