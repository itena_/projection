jun.EmployeeWin = Ext.extend(Ext.Window, {
    title: 'Employee',
    modez: 1,
    width: 400,
    height: 245,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Employee',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Nama Lengkap',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_employee',
                        id: 'nama_employee_id',
                        ref: '../nama_employee',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'NIK',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kode_employee',
                        id: 'kode_employeeid',
                        ref: '../kode_employee',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Role',
                        store: jun.rztTipeEmployee,
                        hiddenName: 'tipe',
                        valueField: 'tipe_employee_id',
                        displayField: 'nama_',
                        ref: '../tipe',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Gol',
                        store: jun.rztGolLib,
                        hiddenName: 'gol_id',
                        valueField: 'gol_id',
                        displayField: 'nama_gol',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Ijin',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_ijin',
                        id: 'no_ijinid',
                        ref: '../no_ijin',
                        maxLength: 100,
//                        value: STORE,
                        disabled: true,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Cabang',
                        hideLabel: false,
                        //hidden:true,
                        name: 'store',
                        id: 'storeid',
                        ref: '../store',
                        maxLength: 50,
                        value: STORE,
                        readOnly: true,
                        //allowBlank: 1,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.EmployeeWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.tipe.on('select', this.tipeOnSelect, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    tipeOnSelect: function (c,r,i) {
        this.no_ijin.setValue("");
        var tipe = c.store.getAt(c.store.findExact("tipe_employee_id", c.getValue())).get('kode');
        switch (tipe){
            case 'APT': this.no_ijin.setDisabled(false);break;
            case 'DK': this.no_ijin.setDisabled(false);break;
            default : this.no_ijin.setDisabled(true);                    
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Employee/update/id/' + this.id;
        } else {
            urlz = 'Employee/create/';
        }
        Ext.getCmp('form-Employee').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztEmployee.reload();
                jun.rztEmployeeCmp.reload();
                jun.rztEmployeeLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Employee').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});