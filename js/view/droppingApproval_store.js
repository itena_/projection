jun.DroppingApprovalstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.DroppingApprovalstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'DroppingApprovalStoreId',
            url: 'Dropping/ApprovalList',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'jenis'},
                {name: 'id_order_dropping_id'},
                {name: 'id_dropping_id'},
                {name: 'tgl', type: 'date'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'type_', type: 'int'},
                {name: 'store'},
                {name: 'order_dropping_id'},
                {name: 'store_penerima'},
                {name: 'store_pengirim'},
                {name: 'approved', type: 'int'},
                {name: 'approved_user_id'},
                {name: 'approved_date', type: 'date'},
                {name: 'lunas', type: 'int'},
                {name: 'up'}
            ]
        }, cfg));
    }
});
jun.rztDroppingApproval = new jun.DroppingApprovalstore();
