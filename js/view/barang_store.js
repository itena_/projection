jun.Barangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Barangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangStoreId',
            url: 'Barang',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'barcode'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'ket'},
                {name: 'grup_id'},
                {name: 'active'},
                {name: 'sat'},
                {name: 'store'},
                {name: 'tipe_barang_id'},
                {name: 'price', type: 'float'}
            ]
        }, cfg));
    }
});
jun.rztBarang = new jun.Barangstore();
jun.rztBarangLib = new jun.Barangstore();
jun.rztBarangCmp = new jun.Barangstore({baseParams: {f: "cmp", tipe_barang_id: 0, price: 1000000000000}});
jun.rztBarangPaketCmp = new jun.Barangstore({baseParams: {f: "cmp", tipe_barang_id: 0}});
jun.rztBarangNoJasaMedis = new jun.Barangstore({baseParams: {mode: "nojasamedis", f: "cmp"}});
jun.rztBarangJasa = new jun.Barangstore(
    {
        baseParams: {mode: "jasa", f: "cmp"},
        method: 'POST'
    });
jun.rztBarangNonJasa = new jun.Barangstore(
    {
        baseParams: {mode: "nonjasa", f: "cmp", tipe_barang_id: 0},
        method: 'POST'
    });
jun.rztBarangRaw = new jun.Barangstore(
    {
        baseParams: {mode: "nonjasa", f: "cmp", tipe_barang_id: 1},
        method: 'POST'
    });
jun.rztBarangNonJasaAll = new jun.Barangstore(
    {
        baseParams: {mode: "nonjasa", f: "cmp"},
        method: 'POST'
    });
jun.rztBarangNonJasaTransfer = new jun.Barangstore(
    {
        baseParams: {mode: "nonjasatransfer", f: "cmp"},
        method: 'POST'
    });
jun.rztBarangPurchasable = new jun.Barangstore(
    {
        baseParams: {mode: "purchasable", f: "cmp"},
        method: 'POST'
    });

jun.rztBarangAsst = new jun.Barangstore(
    {
        baseParams: {f: "cmp", tipe_barang_id: 2},
        method: 'POST'
    });
jun.rztBarangJual = new jun.Barangstore(
    {
        baseParams: {order: "asc", tipe_barang_id: 0, mode: "grid"}
    });
jun.rztBarangBonusJual = new jun.Barangstore(
    {
        baseParams: {order: "asc", tipe_barang_id: 0}
    });


jun.rztBarangLib.load();
