jun.TransferBarangPerlengkapanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferBarangPerlengkapanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferBarangPerlengkapanStoreId',
            url: 'TransferBarangPerlengkapan',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'transfer_perlengkapan_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tdate'},
                {name: 'doc_ref_other'},
                {name: 'user_id'},
                {name: 'type_'},
                {name: 'store'},
                {name: 'supplier_id'},
                {name: 'total'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'bruto'},
                {name: 'vat'},
                {name: 'up'},
                {name: 'total_pot'},
                {name: 'total_discrp1'},
                {name: 'tgl_jatuh_tempo'},
                {name: 'lunas'}

            ]
        }, cfg));
    }
});
jun.rztTransferBarangPerlengkapan = new jun.TransferBarangPerlengkapanstore({url: 'TransferBarangPerlengkapan/IndexIn'});
jun.rztTransferBarangPerlengkapanOut = new jun.TransferBarangPerlengkapanstore({url: 'TransferBarangPerlengkapan/IndexOut'});
//jun.rztTransferBarangPerlengkapanLib = new jun.TransferBarangPerlengkapanstore();
//jun.rztTransferBarangPerlengkapan.load();
