/*
 * - Menu untuk melihat invoice/return
 * - Menu untuk divisi Purchasing
 */
jun.TransferItemGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Invoice Supplier",
    id: 'docs-jun.TransferItemGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Tipe',
            sortable: true,
            resizable: true,
            dataIndex: 'type_',
            width: 40,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case 0 : return 'INVOICE';
                    case 1 : return 'RETURN';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'INVOICE'], [1, 'RETURN']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 50,
            renderer : Ext.util.Format.dateRenderer('d-M-Y'),
            filter: {xtype: "datefield", format: "d-M-Y"}
        },
        {
            header: 'No. PO',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_po',
            width: 80,
            filter: {xtype: "textfield"}
        },
        {
            header: 'No. Surat Jalan',
            sortable: true,
            resizable: true,
            dataIndex: 'no_sj',
            width: 80,
            filter: {xtype: "textfield"}
        },
        {
            header: 'No. Invoice',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_other',
            width: 80,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 150,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 40,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 40,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case STATUS_OPEN :
                        //metaData.style += "background-color: #AAFFD4;";
                        return 'OPEN';
                    case STATUS_CLOSE :
                        metaData.style += "background-color: #AAD4FF;";
                        return 'LUNAS';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable: false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: ['myId','displayText'],
                    data: [['all', 'ALL'], [0, 'OPEN'], [1, 'LUNAS']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangPurchasable.getTotalCount() === 0) {
            jun.rztBarangPurchasable.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        
        this.store = jun.rztTransferItem;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    ref: '../botbar',
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'LUNAS',
                    ref: '../btnClose',
                    iconCls: 'silk13-flag_blue'
                },
                { xtype: 'tbseparator'},
                {
                    xtype: 'button',
                    text: 'Print',
                    ref: '../btnPrint',
                    iconCls: 'silk13-page_white_excel'
                },
                {
                    xtype: "form",
                    frame: !1,
                    ref: '../formPrintInvoice',
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "transfer_item_id",
                            ref: "../../transfer_item_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel"
                        }
                    ]
                }
            ]
        };
        jun.TransferItemGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnClose.on('Click', this.btnCloseOnClick, this);
        this.btnPrint.on('Click', this.btnPrintOnClick, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        
        this.store.load();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        
        this.btnEdit.setText((r.get('status') == STATUS_OPEN)?"Edit":"View");
        this.btnClose.setDisabled(r.get('status') == STATUS_CLOSE);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.transfer_item_id;
        var filter = this.record.get('status') == STATUS_OPEN;
        var form = new jun.TransferItemWin({
            modez: filter?1:2,
            transfer_item_id: idz,
            terima_barang_id: selectedz.json.terima_barang_id,
            title: (filter?"Edit":"View")+" Invoice",
            parent: this,
            storeGridDetil: new jun.TransferItemDetailsstore({url: (this.record.get('type_')?'TransferItemDetails/IndexOut':'TransferItemDetails/IndexIn')})
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);

        form.griddetils.store.load({
            params: {transfer_item_id: idz},
            // callback: function(){
            //     this.calculateItem();
            // },
            scope: form.griddetils.store
        });
    },
    btnCloseOnClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data Invoice.");
            return;
        }
        if (Number(this.sm.getSelected().json.status) == STATUS_CLOSE){
            Ext.MessageBox.alert("Warning", "Invoice telah LUNAS.");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin akan set LUNAS Invoice <b>'+this.sm.getSelected().json.doc_ref_other+'</b>?', this.closeInvoice, this);
    },
    closeInvoice: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'TransferItem/Close',
            method: 'POST',
            scope: this,
            params: {
                transfer_item_id: record.json.transfer_item_id
            },
            success: function (f, a) {
                this.store.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnPrintOnClick: function(){
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data.");
            return;
        }
        this.printInvoice(selectedz.json.transfer_item_id)
    },
    printInvoice: function(transfer_item_id){
        var form = this.formPrintInvoice.getForm();
        form.standardSubmit = !0;
        form.url = "Report/PrintSupplierInvoice";
        this.transfer_item_id.setValue(transfer_item_id);
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

/*
 * - Menu untuk membuat return
 * - Menu untuk divisi WH
 */
jun.ReturnTransferItemGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Return Supplier Item",
    id: 'docs-jun.ReturnTransferItemGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100,
            renderer : Ext.util.Format.dateRenderer('d-M-Y'),
            filter: {xtype: "datefield", format: "d-M-Y"}
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 40,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case STATUS_OPEN :
                        metaData.style += "background-color: #AAFFD4;";
                        return 'OPEN';
                    case STATUS_CLOSE :
                        metaData.style += "background-color: #AAD4FF;";
                        return 'CLOSED';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable: false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: ['myId','displayText'],
                    data: [['all', 'ALL'], [0, 'OPEN'], [1, 'CLOSED']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangPurchasable.getTotalCount() === 0) {
            jun.rztBarangPurchasable.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        
        this.store = jun.rztReturnTransferItem;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    ref: '../botbar',
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'View Return Item',
                    ref: '../btnEdit'
                }
            ]
        };
        jun.ReturnTransferItemGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        
        this.store.setBaseParam('type_', 1);
        this.store.load();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        this.btnEdit.setText((r.get('status') == STATUS_OPEN)?"Edit Return Item":"View Return Item");
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        
        var idz = selectedz.json.transfer_item_id;
        var filter = this.record.get('status') == STATUS_OPEN;
        var form = new jun.ReturnTransferItemWin({
            modez: filter?1:2,
            transfer_item_id: idz,
            terima_barang_id: this.record.get('terima_barang_id'),
            title: (filter?"Edit":"View")+" Return Supplier Item",
            storeGridDetil: new jun.TransferItemDetailsstore({url: 'TransferItemDetails/IndexOut'})
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);

        form.storeGridDetil.load({
            params: {transfer_item_id: idz}
        });
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'ReturnTransferItem/delete/id/' + record.json.transfer_item_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztReturnTransferItem.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});

