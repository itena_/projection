jun.PoInDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PoInDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PoInDetailsStoreId',
            url: 'PoInDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'po_details_id'},
                {name: 'qty', type: 'float'},
                {name: 'description'},
                {name: 'price', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'barang_id'},
                {name: 'po_id'},
                {name: 'disc', type: 'float'},
                {name: 'disc_rp', type: 'float'},
                {name: 'sub_total', type: 'float'},
                {name: 'ppn', type: 'float'},
                {name: 'ppn_rp', type: 'float'},
                {name: 'pph', type: 'float'},
                {name: 'pph_rp', type: 'float'},
                {name: 'seq', type: 'float'},
                {name: 'total_disc', type: 'float'},
                {name: 'total_dpp', type: 'float'},
                {name: 'charge'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        var sub_total = this.sum("sub_total");
        var tax_rp = this.sum("ppn_rp");
        var total = this.sum("total");
        var disc_rp = this.sum("disc_rp");
        var total_pph_rp = this.sum("pph_rp");
        var total_dpp = this.sum("total_dpp");
        Ext.getCmp('total_dppid').setValue(total_dpp);
        Ext.getCmp('total_disc_rpid').setValue(disc_rp);
        Ext.getCmp('sub_totalid').setValue(sub_total);
        Ext.getCmp('totalprid').setValue(total);
        Ext.getCmp('total_pph_rpid').setValue(total_pph_rp);
        Ext.getCmp('tax_rpid').setValue(tax_rp);
        this.refreshSortData();
    },
    refreshSortData: function () {
        this.each(function (item, index, totalItems) {
            //console.log(index);
            item.data.seq = index;
        },this);
    }
});
jun.rztPoInDetails = new jun.PoInDetailsstore();
//jun.rztPoInDetails.load();
