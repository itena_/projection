jun.ShipToWin = Ext.extend(Ext.Window, {
    title: 'ShipTo',
    modez: 1,
    width: 450,
    height: 280,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-ShipTo',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Attn. Name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama',
                        id: 'namaid',
                        ref: '../nama',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Company Name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ship_to_company',
                        id: 'ship_to_companyid',
                        ref: '../ship_to_company',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Address',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ship_to_address',
                        id: 'ship_to_addressid',
                        ref: '../ship_to_address',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Company City',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ship_to_city',
                        id: 'ship_to_cityid',
                        ref: '../ship_to_city',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Company Country',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ship_to_country',
                        id: 'ship_to_countryid',
                        ref: '../ship_to_country',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Company Phone',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ship_to_phone',
                        id: 'ship_to_phoneid',
                        ref: '../ship_to_phone',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Company Fax',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ship_to_fax',
                        id: 'ship_to_faxid',
                        ref: '../ship_to_fax',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ShipToWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm: function ()
    {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {

            urlz = 'ShipTo/update/id/' + this.id;

        } else {

            urlz = 'ShipTo/create/';
        }

        Ext.getCmp('form-ShipTo').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztShipTo.reload();
                jun.rztShipToCmp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-ShipTo').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function ()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function ()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});