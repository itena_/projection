jun.TransferBarangPerlengkapanGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Purchase Barang Perlengkapan",
    id: 'docs-jun.TransferBarangPerlengkapanGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 60
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 70
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 160
        },
        {
            header:'Branch',
            sortable:true,
            resizable:true,
            dataIndex:'store',
            width:100,
            renderer: jun.renderStore
        },
    ],
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) jun.rztStoreCmp.load();
        if (jun.rztBarangPerlengkapanLib.getTotalCount() === 0) jun.rztBarangPerlengkapanLib.load();
        if (jun.rztSupplierCmp.getTotalCount() === 0) jun.rztSupplierCmp.load();
        if (jun.rztStoreLib.getTotalCount() === 0) jun.rztStoreLib.load();
        
        jun.rztTransferBarangPerlengkapan.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    //b.params.tgl = Ext.getCmp('tgltransferBarangPerlengkapangrid').getValue();
                    var tgl = Ext.getCmp('tgltransferbarangperlengkapangrid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztTransferBarangPerlengkapan;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Purchase',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Purchase',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tgltransferbarangperlengkapangrid'
                }
            ]
        };
        jun.TransferBarangPerlengkapanGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },

    loadForm: function () {
        var form = new jun.TransferBarangPerlengkapanWin({modez: 0});
        form.show();
    },

    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected any Purchase Data");
            return;
        }
        var idz = selectedz.json.transfer_perlengkapan_id;
        var form = new jun.TransferBarangPerlengkapanWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztTransferBarangPerlengkapanDetails.proxy.setUrl('TransferBarangPerlengkapanDetails/IndexIn');
        jun.rztTransferBarangPerlengkapanDetails.baseParams = {
            transfer_perlengkapan_id: idz
        };
        jun.rztTransferBarangPerlengkapanDetails.load();
        jun.rztTransferBarangPerlengkapanDetails.baseParams = {};
    }
})

jun.TransferBarangPerlengkapanOutGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Transfer Out Barang Perlengkapan",
    id: 'docs-jun.TransferBarangPerlengkapanOutGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100,
            renderer: jun.renderStore
        }

    ],
    initComponent: function () {
        if (jun.rztBarangPerlengkapanLib.getTotalCount() === 0) {
            jun.rztBarangPerlengkapanLib.load();
        }
        jun.rztTransferBarangPerlengkapanOut.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglTransferBarangPerlengkapanOutGrid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztTransferBarangPerlengkapanOut;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Transfer Out',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Transfer Out',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglTransferBarangPerlengkapanOutGrid'
                }
            ]
        };
        jun.TransferBarangPerlengkapanOutGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.TransferBarangPerlengkapanOutWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.transfer_perlengkapan_id;
        var form = new jun.TransferBarangPerlengkapanOutWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztTransferOutBarangPerlengkapanDetails.proxy.setUrl('TransferBarangPerlengkapanDetails/IndexOut');            // <<<<<<indexout??
        jun.rztTransferOutBarangPerlengkapanDetails.baseParams = {
            transfer_perlengkapan_id: idz
        };
        jun.rztTransferOutBarangPerlengkapanDetails.load();
        jun.rztTransferOutBarangPerlengkapanDetails.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'TransferBarangPerlengkapanOut/delete/id/' + record.json.transfer_perlengkapan_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztTransferBarangPerlengkapanOut.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
