jun.BeautyServicesWin = Ext.extend(Ext.Window, {
    title: 'BeautyServices',
    modez: 1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-BeautyServices',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'total',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'beauty_id',
                        store: jun.rztBeautyCmp,
                        hiddenName: 'beauty_id',
                        valueField: 'beauty_id',
                        displayField: 'nama_beauty',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'salestrans_details',
                        store: jun.rztSalestransDetails,
                        hiddenName: 'salestrans_details',
                        valueField: 'salestrans_details',
                        displayField: 'ketpot',
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BeautyServicesWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'BeautyServices/update/id/' + this.id;
        } else {
            urlz = 'BeautyServices/create/';
        }
        Ext.getCmp('form-BeautyServices').getForm().submit({
            url: urlz,
            scope: this,
            success: function (f, a) {
                jun.rztBeautyServices.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-BeautyServices').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.beautytransWin = Ext.extend(Ext.Window, {
    title: 'Service',
    modez: 1,
    width: 435,
    height: 310,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-beautytrans',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: 'Beauty 1',
                        ref: '../beauty1',
                        // triggerAction: 'all',
                        // lazyRender: true,
                        mode: 'local',
                        store: jun.rztBeauty1Cmp,
                        hiddenName: 'beauty_id',
                        valueField: 'beauty_id',
                        displayField: 'nama_beauty',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: 'Beauty 2',
                        ref: '../beauty2',
                        // triggerAction: 'all',
                        // lazyRender: true,
                        mode: 'local',
                        store: jun.rztBeauty2Cmp,
                        hiddenName: 'beauty2_id',
                        valueField: 'beauty_id',
                        displayField: 'nama_beauty',
                        allowBlank: true,
                        anchor: '100%',
                        id: 'beauty2wkid'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: 'Beauty 3',
                        ref: '../beauty3',
                        // triggerAction: 'all',
                        // lazyRender: true,
                        mode: 'local',
                        store: jun.rztBeauty3Cmp,
                        hiddenName: 'beauty3_id',
                        valueField: 'beauty_id',
                        displayField: 'nama_beauty',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: 'Beauty 4',
                        ref: '../beauty4',
                        // triggerAction: 'all',
                        // lazyRender: true,
                        mode: 'local',
                        store: jun.rztBeauty4Cmp,
                        hiddenName: 'beauty4_id',
                        valueField: 'beauty_id',
                        displayField: 'nama_beauty',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: 'Beauty 5',
                        ref: '../beauty5',
                        // triggerAction: 'all',
                        // lazyRender: true,
                        mode: 'local',
                        store: jun.rztBeauty5Cmp,
                        hiddenName: 'beauty5_id',
                        valueField: 'beauty_id',
                        displayField: 'nama_beauty',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: 'Doctor',
                        ref: '../dokter',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztDokterCmp,
                        hiddenName: 'dokter_id',
                        valueField: 'dokter_id',
                        displayField: 'nama_dokter',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        fieldLabel: 'Doctor Tip',
                        name: 'jasa_dokter',
                        id: 'jasa_dokterid',
                        ref: '../jasa_dokter',
                        value: 0,
                        maxLength: 30,
                        anchor: '100%',
                        hidden: true
                    },
                    {
                        xtype: 'hidden',
                        name: 'salestrans_details'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.beautytransWin.superclass.initComponent.call(this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnSave.setVisible(this.final == 0);
        this.beauty1.on('expand', this.oncmbExpand, this);
        this.beauty2.on('expand', this.oncmbExpand, this);
        this.beauty3.on('expand', this.oncmbExpand, this);
        this.beauty4.on('expand', this.oncmbExpand, this);
        this.beauty5.on('expand', this.oncmbExpand, this);
        this.beauty1.on('collapse', this.oncmbCollapse, this);
        this.beauty2.on('collapse', this.oncmbCollapse, this);
        this.beauty3.on('collapse', this.oncmbCollapse, this);
        this.beauty4.on('collapse', this.oncmbCollapse, this);
        this.beauty5.on('collapse', this.oncmbCollapse, this);
    },
    oncmbCollapse: function (c) {
        c.store.clearFilter();
    },
    oncmbExpand: function (c) {
        c.store.filter('active', '1');
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
    },
    onbtnSaveclick: function () {
        this.btnDisabled(true);
        var urlz = 'SalestransDetails/update/';
        Ext.getCmp('form-beautytrans').getForm().submit({
            scope: this,
            url: urlz,
            success: function (f, a) {
                jun.rztBeutytrans.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.BeautyPerawatanWin = Ext.extend(Ext.Window, {
    title: 'Perawatan',
    modez: 1,
    width: 935,
    height: 310,
    layout: 'fit',
    id: 'docs-jun.BeautyPerawatanWin',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-BeautyPerawatan',
                // labelWidth: 100,
                // labelAlign: 'left',
                // layout: 'form',
                // anchor: "100% 100%",
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                // frameHeader: !1,
                // header: !1,
                // border: false,
                defaults: {
                    // margins: '0 0 5 0',
                    border: false,
                    bodyStyle: 'background-color: #E4E4E4;'
                },
                ref: 'formz',
                items: [
                    {
                        xtype: 'panel',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        frameHeader: !1,
                        header: !1,
                        border: false,
                        height: 90,
                        defaults: {
                            xtype: 'panel',
                            layout: 'form',
                            anchor: "100% 100%",
                            padding: 5,
                            bodyStyle: 'background-color: #E4E4E4;',
                            labelWidth: 100,
                            labelAlign: 'left',
                            flex: 1
                        },
                        items: [
                            {
                                frameHeader: !1,
                                header: !1,
                                border: false,
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'No. Receipt',
                                        hideLabel: false,
                                        name: 'doc_ref',
                                        id: 'doc_refid',
                                        ref: '../../doc_ref',
                                        readOnly: true,
                                        anchor: "100%"
                                    },
                                    {
                                        xtype: 'timefield',
                                        fieldLabel: 'Waktu Mulai',
                                        hideLabel: false,
                                        format: 'H:i',
                                        name: 'start_at',
                                        ref: '../../start_at',
                                        readOnly: true,
                                        anchor: "100%"
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Extend (Menit)',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'add_durasi',
                                        ref: '../../add_durasi',
                                        readOnly: true,
                                        maxLength: 30,
                                        value: 0,
                                        //allowBlank: 1,
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                frameHeader: !1,
                                header: !1,
                                border: false,
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'No Pasien',
                                        hideLabel: false,
                                        name: 'no_customer',
                                        ref: '../../no_customer',
                                        readOnly: true,
                                        anchor: "100%"
                                    },
                                    {
                                        xtype: 'timefield',
                                        fieldLabel: 'Estimasi Selesai',
                                        hideLabel: false,
                                        format: 'H:i',
                                        name: 'end_estimate',
                                        ref: '../../end_estimate',
                                        readOnly: false,
                                        anchor: "100%"
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Perawatan',
                                        hideLabel: false,
                                        name: 'add_jasa',
                                        ref: '../../add_jasa',
                                        readOnly: true,
                                        anchor: "100%"
                                    }
                                ]
                            },
                            {
                                frameHeader: !1,
                                header: !1,
                                border: false,
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Nama Pasien',
                                        hideLabel: false,
                                        name: 'nama_customer',
                                        ref: '../../nama_customer',
                                        readOnly: true,
                                        anchor: "100%"
                                    },
                                    {
                                        xtype: 'timefield',
                                        fieldLabel: 'Waktu Selesai',
                                        hideLabel: false,
                                        format: 'H:i:s',
                                        name: 'end_at',
                                        ref: '../../end_at',
                                        readOnly: true,
                                        anchor: "100%"
                                    }
                                ]
                            }
                        ]
                    },
                    new jun.BeautyPerawatanGrid({
                        flex: 1,
                        frameHeader: !1,
                        header: !1,
                        ref: '../grid'
                    }),
                    {
                        xtype: 'hidden',
                        name: 'salestrans_id',
                        ref: '../salestrans_id'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'PANGGIL',
                    modez: 0,
                    ref: '../btnPanggil'
                },
                {
                    xtype: 'button',
                    text: 'ULANGI',
                    disabled:true,
                    modez: 0,
                    ref: '../btnUlang'
                },
                {
                    xtype: 'button',
                    text: 'S A V E',
                    hidden: false,
                    modez: 2,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'S T A R T',
                    disabled:true,
                    modez: 0,
                    ref: '../btnStart'
                },
                {
                    xtype: 'button',
                    text: 'S T O P',
                    modez: 1,
                    ref: '../btnStop'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BeautyPerawatanWin.superclass.initComponent.call(this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnStart.on('click', this.onbtnSaveclick, this);
        this.btnStop.on('click', this.onbtnSaveclick, this);
        this.btnPanggil.on('click', this.onPanggilClick, this);
        this.btnUlang.on('click', this.onUlangiClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on('show', this.onShow, this);
        this.on('beforeclose', this.onBeforeClose, this);
    },
    onPendingClick: function () {
        var storedNames = Locstor.get('AntrianPerawatan');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                mode: 'pending',
                bagian: 'perawatan',
                nomor: storedNames.nomor,
                no_base: storedNames.no_base,
                id_antrian: storedNames.id_antrian,
                customer_id: storedNames.customer_id,
                nama_customer: storedNames.nama_customer
            },
            success: function (f, a) {
                // this.refreshStoreAntrian();
                Locstor.remove("AntrianPerawatan");
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onUlangiClick: function () {
        var storedNames = Locstor.get('AntrianPerawatan');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/UlangiCounter/',
            method: 'POST',
            scope: this,
            params: {
                nomor: storedNames.nomor,
                bagian: 'perawatan',
                no_base: storedNames.no_base,
                id_antrian: storedNames.id_antrian,
                customer_id: storedNames.customer_id,
                nama_customer: storedNames.nama_customer
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (!response.success) {
                    Ext.Msg.alert('Information', response.msg);
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onPanggilClick: function () {
        var storedNames = Locstor.get('AntrianPerawatan');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            return;
        }

        Ext.Ajax.request({
            url: 'AishaAntrian/PanggilCounter/',
            method: 'POST',
            scope: this,
            params: {
                bagian: 'perawatan',
                id_antrian: storedNames.id_antrian//selectedz.data.id_antrian
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success) {
                    if (response.msg != "Tidak ada antrian.") {
                        var person = {
                            nomor: response.msg[0],
                            bagian: response.msg[1],
                            counter: response.msg[2],
                            no_base: response.msg[3],
                            id_antrian: response.msg[4],
                            customer_id: response.msg[5],
                            nama_customer: response.msg[6]
                        };
                        this.btnUlang.setDisabled(false);
                        this.btnStart.setDisabled(false);
                        Locstor.set('AntrianPerawatan', person);
                        return;
                    } else {
                        //Locstor.remove("AntrianPerawatan");
                        Ext.Msg.alert('Information', response.msg);
                    }
                } else {
                    Ext.Msg.alert('Information', response.msg);
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onBeforeClose: function () {
        this.hide();
        return false;
    },
    onShow: function () {
        switch (this.modez) {
            case 0 :
                this.btnSave.setVisible(false);
                this.btnStart.setVisible(true);
                this.btnStop.setVisible(false);
                break;
            case 1 :
                this.btnSave.setVisible(false);
                this.btnStart.setVisible(false);
                this.btnStop.setVisible(true);
                break;
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
    },
    onbtnSaveclick: function (t, e) {
        this.btnDisabled(true);
        var rawat = '';
        jun.rztBeautyPerawatan.each(function (r) {
            if (Ext.isEmpty(r.data.beauty_id, false)) {
                rawat = r.data.kode_barang;
                return false;
            }
        }, this);
        if (rawat != '') {
            Ext.Msg.alert('Failed', 'Perawatan ' + rawat + ' belum ada beauty.');
            this.btnDisabled(false);
            return;
        }
        this.btnStart.setDisabled(true);
        this.btnStop.setDisabled(true);
        this.modez = t.modez;
        var urlz = 'BeautyServices/create/';
        Ext.getCmp('form-BeautyPerawatan').getForm().submit({
            scope: this,
            url: urlz,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztBeautyPerawatan.data.items, "data")),
                mode: this.modez
            },
            success: function (f, a) {
                // jun.reloadAllStorePerawatan();
                if (this.modez == 0) {
                    var storedNames = Locstor.get('AntrianPerawatan');
                    if (storedNames != null) {
                        Locstor.remove('AntrianPerawatan');
                        var g = Ext.getCmp('docs-jun.AntrianPerawatWin');
                        if (g !== undefined) {
                            g.updateLabel();
                        }
                    }
                    this.printBeauty2(this.salestrans_id.getValue());
                }
                var response = Ext.decode(a.response.responseText);
                // Ext.MessageBox.show({
                //     title: 'Info',
                //     msg: response.msg,
                //     buttons: Ext.MessageBox.OK,
                //     icon: Ext.MessageBox.INFO
                // });
                this.hide();
                this.btnDisabled(false);
                jun.rztBeautyOffduty1Cmp.reload();
                this.btnStart.setDisabled(false);
                this.btnStop.setDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    printBeauty2: function (salestrans_id) {
        jun.indexPrint = 0;
        jun.rztSalestransDetails.load({
            callback: function () {
                jun.indexMax = jun.rztSalestransDetails.getTotalCount();
                this.printBeauty();
            },
            scope: this,
            params: {
                salestrans_id: salestrans_id
            }
        });
    },
    cetakBeauty: function (response, callback) {
        if (response.success !== false) {
            var msg = [{type: 'raw', data: response.msg}];
            var printData = __printDataBeauty.concat(msg, __feedPaper, __cutPaper);
            print(PRINTER_BEAUTY, printData);
        }
        callback();
    },
    printBeauty: function () {
        var b = jun.rztSalestransDetails.getAt(jun.indexPrint);
        if (!b) {
            return;
        }
        if (jun.indexMax <= jun.indexPrint) {
            return;
        }
        console.log(jun.indexPrint);
        console.log(b);
        var salestrans_details = b.get('salestrans_details');
        console.log(salestrans_details);
        Ext.Ajax.request({
            url: 'Salestrans/PrintBeauty',
            method: 'POST',
            scope: this,
            params: {
                id: salestrans_details,
                index: jun.indexPrint
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    return;
                }
                jun.indexPrint++;
                Ext.getCmp('docs-jun.BeautyPerawatanWin').cetakBeauty(response, Ext.getCmp('docs-jun.BeautyPerawatanWin').printBeauty);
                // print(PRINTER_STOCKER, printData);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onbtnCancelclick: function () {
        this.hide();
    }
});
jun.indexPrint = 0;
jun.indexMax = 0;
