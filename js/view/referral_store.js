jun.Referralstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Referralstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReferralStoreId',
            url: 'Referral',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'referral_id'},
                {name: 'referral_nama'},
                {name: 'referral_no_hp'},
                {name: 'referral_alamat'},
                {name: 'referral_sosmed'}

            ]
        }, cfg));
    }
});
jun.rztReferral = new jun.Referralstore();
//jun.rztReferral.load();
