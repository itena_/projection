jun.GlTransWin = Ext.extend(Ext.Window, {
    title: 'General Journal',
    modez: 1,
    width: 945,
    height: 555,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    iswin: true,
    initComponent: function () {
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-GlTrans',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: "xdatefield",
                        ref: "../tran_date",
                        name: "tran_date",
                        id: "tran_dateid",
                        //readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        format: "d M Y",
                        width: 175,
                        x: 85,
                        y: 2,
                        minValue: REST_START,
                        maxValue: REST_END
                    },
                    {
                        xtype: "label",
                        text: "Total Debit:",
                        x: 290,
                        y: 5
                    },
                    {
                        xtype: "numericfield",
                        hideLabel: !1,
                        name: "tot_debit",
                        id: "tot_debit_id",
                        readOnly: !0,
                        ref: "../TotDebit",
                        value: 0,
                        width: 175,
                        x: 370,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Branch:",
                        hidden: true,
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: "label",
                        text: "Total Credit:",
                        x: 290,
                        y: 35
                    },
                    {
                        xtype: "numericfield",
                        hideLabel: !1,
                        name: "tot_kredit",
                        id: "tot_kredit_id",
                        readOnly: !0,
                        ref: "../TotKredit",
                        value: 0,
                        width: 175,
                        x: 370,
                        y: 32
                    },
                    new Ext.form.ComboBox({
                        store: new Ext.data.ArrayStore({
                            fields: ["code", "label"],
                            data: [
                                [0, 'Manual Input'],
                                [1, 'All Cabang Debit'],
                                [2, 'All Cabang Kredit'],
                                [3, 'All Cabang Debit - Kredit']
                            ]
                        }),
                        hiddenName: "all_store",
                        ref: '../cmbAllStore',
                        editable: false,
                        id: 'cmbAllStoreid',
                        typeAhead: false,
                        valueField: 'code',
                        displayField: 'label',
                        mode: 'local',
                        value: 0,
                        triggerAction: 'all',
                        selectOnFocus: true,
                        hidden: true,
                        width: 175,
                        x: 85,
                        y: 32
                    }),
                    new jun.GlTransGrid({
                        height: 405,
                        frameHeader: !1,
                        header: !1,
                        x: 5,
                        y: 65
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.GlTransWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        //this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        //if (this.modez == 1 || this.modez == 2) {
        //    this.btnSave.setVisible(false);
        //} else {
        //    this.btnSave.setVisible(true);
        //}
        //this.setDateTime();
    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tran_date.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    onActivate: function () {
        if (this.modez !== 0) {
            this.btnSaveClose.setVisible(false);
        }
    },
    btnDisabled: function (status) {
        //this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (this.TotDebit.value != this.TotKredit.value) {
            Ext.MessageBox.alert("Error", "Total debit and credit must equal");
            this.btnDisabled(!1);
            return;
        }
        if (parseFloat(this.TotDebit.value) === 0 || parseFloat(this.TotKredit.value) === 0) {
            Ext.MessageBox.alert("Error", "Total debit or total credit must greater than 0.");
            this.btnDisabled(!1);
            return;
        }
        var urlz = 'GlTrans/create/';
        Ext.getCmp('form-GlTrans').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztGlTrans.data.items, "data")),
                id: this.idju,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztJurnalUmum.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-GlTrans').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.ImportJurnalUmum = Ext.extend(Ext.Window, {
    title: "Import Jurnal Umum",
    iconCls: "silk13-report",
    modez: 1,
    width: 500,
    height: 400,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    group: null,
    initComponent: function () {
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        this.items = [
            {
                xtype: "panel",
                bodyStyle: "background-color: #E4E4E4;padding: 5px; margin-bottom: 5px;",
                html: '<span style="color:red;font-weight:bold">PERHATIAN !!!!</span><br><span style="color:red;font-weight:bold">Konsultasi dengan IT jika akan melakukan import !!!!!</span>'
            },
            {
                html: '<input type="file" name="xlfile" id="inputFile" />',
                name: 'file',
                xtype: "panel",
                listeners: {
                    render: function (c) {
                        new Ext.ToolTip({
                            target: c.getEl(),
                            html: 'Format file : Excel (.xlsx)'
                        });
                    },
                    afterrender: function () {
                        itemFile = document.getElementById("inputFile");
                        itemFile.addEventListener('change', readFileExcel, false);
                    }
                }
            },
            {
                xtype: "panel",
                bodyStyle: "margin-top: 5px;",
                height: 250,
                layout: 'fit',
                items: {
                    xtype: "textarea",
                    readOnly: true,
                    ref: '../log_msg',
                    style: {
                        'fontFamily': 'courier new',
                        'fontSize': '12px'
                    }
                }
            },
            {
                xtype: "form",
                frame: !1,
                id: "form-ImportJurnalUmum",
                border: !1
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Import",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ImportJurnalUmum.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnCheckclick: function () {
        if (jun.dataStockOpname == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih file importKas.");
            return;
        }
        jun.importKasStore.loadData(jun.dataStockOpname[jun.namasheet]);
        var win = new jun.CheckImportInvoiceJournalWin({generateQuery: 0});
        win.show(this);
    },
    onbtnSaveclick: function () {
        if (document.getElementById("inputFile").value == '') return;
        // this.btnSave.setDisabled(true);
        this.log_msg.setValue('');
        this.writeLog('\n----- MULAI IMPORT JURNAL UMUM -----');
        this.rowLoopCounter = 0;
        this.headerCounter = 0;
        this.dataSubmit = {};
        this.dataSubmit['detil'] = [];
        this.dataSubmit['all_store'] = 0;
        this.arrData = jun.dataStockOpname[jun.namasheet];
        this.loopPost();
    },
    headerCounter: 0,
    dataSubmit: null,
    arrData: null,
    loopPost: function () {
        if (this.rowLoopCounter >= this.arrData.length) {
            /*
             * loop berakhir
             */
            this.writeLog('\n----- IMPORT JURNAL UMUM SELESAI -----'
                + '\nJumlah Baris : ' + this.rowLoopCounter
                + '\nJumlah data : ' + this.headerCounter
                + '\n-------------------------------------------\n');
            document.getElementById("inputFile").value = '';
            this.btnSave.setDisabled(false);
            this.arrData = null;
            jun.dataStockOpname = null;
            return;
        }
        var d = this.arrData[this.rowLoopCounter];
        // this.writeLog('\n============== Data ke- ' + this.headerCounter + ' ==============\n');
        // this.loopLog('Data Header nomor : ' + this.headerCounter);
        this.group = d.GRUP;
        this.dataSubmit['mode'] = 0;
        this.dataSubmit['tran_date'] = d.TGL;
        this.dataSubmit['detil'].push('{"account_code":"' + d.COA + '","debit":"' + d.DEBIT + '","kredit":"' + d.KREDIT + '","memo_":"' + d.NOTE + '","store":"' + d.STORE + '"}');
        if (!this.dataSubmit) {
            this.nextRow();
            return;
        }
        this.loopLog('Data detail dari Header nomor : ' + (this.headerCounter + 1));
        var nextRound = this.arrData[this.rowLoopCounter + 1];
        if (!nextRound || nextRound.GRUP != this.group) {
            this.headerCounter++;
            this.writeLog('>>> Submit : Data Header nomor : ' + this.headerCounter);
            if (this.dataSubmit['detil'].length > 0) {
                this.dataSubmit['detil'] = '[' + this.dataSubmit['detil'].join(',') + ']';
            }
            Ext.getCmp('form-ImportJurnalUmum').getForm().submit({
                url: 'GlTrans/Create',
                timeOut: 1000,
                scope: this,
                params: this.dataSubmit,
                success: function (f, a) {
                    var response = Ext.decode(a.response.responseText);
                    var msg = '';
                    if (response.success) {
                        msg = response.msg;
                        this.group = null;
                        this.dataSubmit = {};
                        this.dataSubmit['detil'] = [];
                        this.dataSubmit['all_store'] = 0;
                    } else {
                        msg = '***** GAGAL ****\n' + response.msg;
                        this.btnSave.setDisabled(false);
                    }
                    this.writeLog('<<< Result : dari Header nomor : ' + this.headerCounter + '\n' + msg);
                    if (response.success) this.nextRow();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Connection failure has occured.');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        } else {
            this.nextRow();
        }
    },
    nextRow: function () {
        this.rowLoopCounter++;
        this.loopPost();
    },
    loopLog: function (msg) {
        this.writeLog('Baris ke- ' + (this.rowLoopCounter + 1) + ' dari ' + jun.dataStockOpname[jun.namasheet].length + ' --> ' + msg);
    },
    writeLog: function (msg) {
        this.log_msg.setValue(msg + '\n' + this.log_msg.getValue());
    }
});