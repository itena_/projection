<?php
Yii::import('application.modules.projection.models._base.BaseTransaksiDetailView');

class TransaksiDetailView extends BaseTransaksiDetailView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->transaksi_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->transaksi_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}