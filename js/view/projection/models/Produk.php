<?php
Yii::import('application.modules.projection.models._base.BaseProduk');

class Produk extends BaseProduk
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->produk_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->produk_id = $uuid;
        }
        return parent::beforeValidate();
    }
}