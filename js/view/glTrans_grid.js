jun.GlTransDetilWin = Ext.extend(Ext.Window, {
    title: "General Journal",
    modez: 1,
    width: 640,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    debit: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-GlTransDetil",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'COA',
                        store: jun.rztChartMasterCmp,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        forceSelection: true,
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 300,
                        displayField: 'account_code',
                        ref: '../cmbkode',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Note',
                        hideLabel: false,
                        //hidden:true,
                        name: 'memo_',
                        id: 'memo_id',
                        ref: '../memo_',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: "numericfield",
                        fieldLabel: "Amount",
                        hideLabel: !1,
                        name: "amount",
                        id: "amountid",
                        ref: "../amount",
                        maxLength: 30,
                        value: 0,
                        anchor: "100%"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save",
                    hidden: !1,
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.GlTransDetilWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
        this.cmbkode.on("focus", this.onLoadChartMaster, this);
        this.on("close", this.onWinClose, this);
    },
    onLoadChartMaster: function () {
        jun.rztChartMasterCmp.FilterData();
    },
    onWinClose: function () {
        jun.rztChartMasterCmp.clearFilter();
    },
    onActivate: function () {
        this.btnSave.setVisible(false);
    },
    saveForm: function () {
        if (parseFloat(this.amount.value) === 0) {
            Ext.MessageBox.alert("Error", "Amount must greater than 0");
            return;
        }
        //var a = jun.rztGlTrans.find("account", this.cmbkode.value);
        //if (a > -1 && this.modez == 0) {
        //    Ext.MessageBox.show({
        //        title: "Error",
        //        msg: "Kode rekening sudah di dipakai!",
        //        buttons: Ext.MessageBox.OK,
        //        icon: Ext.MessageBox.ERROR
        //    });
        //    return;
        //}
        var b = jun.rztGlTrans.recordType, c = new b({
            account: this.cmbkode.value,
            memo_: this.memo_.getValue(),
            debit: this.debit ? parseFloat(this.amount.value) : 0,
            kredit: this.debit ? 0 : parseFloat(this.amount.value)
        });
        jun.rztGlTrans.insert(jun.rztGlTrans.getCount(), c);
        Ext.getCmp("form-GlTransDetil").getForm().reset();
        this.closeForm && this.close();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0, this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1, this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.GlTransGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "GlTrans",
    id: "docs-jun.GlTransGrid",
    stripeRows: true,
    viewConfig: {
        forceFit: !0
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: !0
    }),
    columns: [
        {
            header: "Accounts Code",
            resizable: !0,
            dataIndex: "account_code",
            width: 100
        },
        {
            header: "Accounts Name",
            resizable: !0,
            dataIndex: "account_code",
            width: 250,
            renderer: jun.renderCoa
        },
        {
            header: "Note",
            resizable: !0,
            dataIndex: "memo_",
            width: 250
        },
        {
            header: "Debit",
            resizable: !0,
            dataIndex: "debit",
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            width: 100
        },
        {
            header: "Kredit",
            resizable: !0,
            dataIndex: "kredit",
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            width: 100
        },
        {
            header: "Branch",
            resizable: !0,
            dataIndex: "store",
            width: 75
        }
    ],
    initComponent: function () {
        this.store = jun.rztGlTrans;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'COA :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
//                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            enableKeyEvents: true,
                            forceSelection: true,
                            fieldLabel: 'COA',
                            store: jun.rztChartMasterCmp,
                            ref: '../../account_code',
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                                "</div></tpl>"),
                            listWidth: 500,
//                            hiddenName: 'account_code',
                            valueField: 'account_code',
                            displayField: 'account_code',
                            width: 150
//                            anchor: '100%',
//                            listeners: {
//                                buffer: 50,
//                                keyup: function(txt,e) {
//                                    var searchValue = txt.getRawValue().toUpperCase();
//                                    if (!!searchValue){
//                                        var store = txt.store;
//                                        //store.suspendEvents();
//                                        store.clearFilter();
//                                        //store.resumeEvents();
//                                        store.filterBy(function(record,id){
//                                            return record.data.account_code.indexOf(searchValue) > -1 ||
//                                                record.data.account_name.indexOf(searchValue) > -1;
//                                        });
//                                    }
//
//                                }
//                            }
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Debit :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'debitid',
                            ref: '../../debit',
                            width: 75,
                            value: 0,
                            enableKeyEvents: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Kredit :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'kreditid',
                            ref: '../../kredit',
                            width: 75,
                            value: 0,
                            enableKeyEvents: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Branch :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            fieldLabel: 'Branch',
                            ref: '../../store_',
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            store: jun.rztStoreCmp,
                            hiddenName: 'store',
                            name: 'store',
                            valueField: 'store_kode',
                            displayField: 'store_kode',
                            allowBlank: false,
                            width: 75
                            // x: 85,
                            // y: 32
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Area :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztAreaCmp,
                            ref: '../../area',
                            valueField: 'area_id',
                            displayField: 'area_name',
                            readOnly: !HEADOFFICE,
                            width: 100
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../memo_',
                            width: 660,
                            colspan: 9,
                            maxLength: 150
                        },
                        {
                            xtype: 'numericfield',
                            id: 'amountid',
                            ref: '../../amount',
                            width: 75,
                            value: 0,
                            hidden: true
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
//            xtype: "toolbar",
//            items: [
//
//                {
//                    xtype: "button",
//                    text: "Add Debit",
//                    ref: "../btnAdd"
//                },
//                {
//                    xtype: "tbseparator"
//                },
//                {
//                    xtype: "button",
//                    text: "Add Credit",
//                    ref: "../btnEdit"
//                },
//                {
//                    xtype: "tbseparator"
//                },
//                {
//                    xtype: "button",
//                    text: "Delete Item",
//                    ref: "../btnDelete"
//                }
//            ]
        };
        jun.GlTransGrid.superclass.initComponent.call(this);
        this.btnAdd.on("Click", this.loadForm, this);
        this.btnEdit.on("Click", this.onClickbtnEdit, this);
        this.btnDelete.on("Click", this.deleteRec, this);
        this.kredit.on('keyup', this.kreditOnChange, this);
        this.kredit.on('change', this.kreditOnChange, this);
        this.debit.on('keyup', this.debitOnChange, this);
        this.debit.on('change', this.debitOnChange, this);
        this.store_.on('select', this.storeOnSelect, this);
        this.area.on('select', this.areaOnSelect, this);
        this.getSelectionModel().on("rowselect", this.getrow, this);
        this.store.removeAll();
    },
    storeOnSelect: function (t, e) {
        this.area.clearValue();
    },
    areaOnSelect: function (t, e) {
        this.store_.clearValue();
    },
    kreditOnChange: function (t, e) {
        if (t.getValue() != 0) {
            this.debit.setValue('0');
        }
        this.amount.setValue(-t.getValue());
    },
    debitOnChange: function (t, e) {
        if (t.getValue() != 0) {
            this.kredit.setValue('0');
        }
        this.amount.setValue(t.getValue());
    },
    getrow: function (a, b, c) {
        this.record = c;
        var d = this.sm.getSelections();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    resetForm: function (s) {
        this.account_code.reset();
        this.memo_.reset();
        this.amount.reset();
        this.kredit.reset();
        this.debit.reset();
        this.store_.reset();
        this.area.reset();
        this.btnDisable(false);
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.account_code.setValue(record.data.account_code);
            this.memo_.setValue(record.data.memo_);
            this.store_.setValue(record.data.store);
            this.amount.setValue(record.data.amount);
            this.kredit.setValue(record.data.kredit);
            this.debit.setValue(record.data.debit);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadForm: function () {
        this.btnDisable(true);
        
        var valkredit = parseFloat(this.kredit.getValue());
        var valdebit = parseFloat(this.debit.getValue());
//        
//        if(valkredit !== 0){
//            this.amount.setValue(valkredit);
//            return;
//        }
//        if(valdebit !== 0){
//            this.amount.setValue(valdebit);
//            return;
//        }
        var account_code = this.account_code.getValue();
        var memo_ = this.memo_.getValue();
        var store = this.store_.getValue();
        var area = this.area.getValue();
        var amount = parseFloat(this.amount.getValue());
        var debit = valdebit;
        var kredit = valkredit;
        
        if (!store && !area) {
            Ext.MessageBox.alert("Error", "Branch/area must be selected.");
            return;
        }
        
        if (store) {
            this.save({
                account_code: account_code,
                memo_: memo_,
                store: store,
                debit: debit,
                kredit: kredit,
                amount: amount
            });
        } else if (area) {
            var arrStore = new jun.StoreAreastore();
            arrStore.load({
                params: { area_id: area },
                scope: this,
                callback: function(r){
                    var account_code = this.account_code.getValue();
                    var memo_ = this.memo_.getValue();
                    var totalKredit = parseFloat(this.kredit.getValue());
                    var totalDebit = parseFloat(this.debit.getValue());
                    var totalAmount = totalKredit? -totalKredit : totalDebit;

                    var lastStore = '',
                        kredit = 0,
                        debit = 0,
                        amount = 0,
                        sumKredit = 0,
                        sumDebit = 0,
                        sumAmount = 0;

                    (totalKredit > 0) && (kredit = jun.roundDown(totalKredit / r.length,2));
                    (totalDebit > 0) && (debit = jun.roundDown(totalDebit / r.length,2));
                    amount = kredit? -kredit : debit;

                    for(i=0; i<r.length; i++){
                        lastStore = r[i].get('store');
                        this.save({
                            account_code: account_code,
                            memo_: memo_,
                            store: r[i].get('store'),
                            debit: debit,
                            kredit: kredit,
                            amount: amount
                        });
                        sumKredit += kredit;
                        sumDebit += debit;
                        sumAmount += amount;
                    }

                    /* save rounding */
                    var roundingAmount = totalAmount - sumAmount;
                    if(roundingAmount){
                        this.save({
                            account_code: COA_ROUNDING,
                            memo_: 'ROUNDING : '+memo_,
                            store: lastStore,
                            debit: roundingAmount>0?Math.abs(roundingAmount):0,
                            kredit: roundingAmount<0?Math.abs(roundingAmount):0,
                            amount: roundingAmount
                        });
                    }

                }
            });
        }
    },
    save: function(data){
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('account_code', data.account_code);
            record.set('memo_', data.memo_);
            record.set('store', data.store);
            record.set('debit', data.debit);
            record.set('kredit', data.kredit);
            record.set('amount', data.amount);
            record.commit();
        } else {
            var c = jun.rztGlTrans.recordType,
                d = new c(data);
            jun.rztGlTrans.add(d);
        }
        
        this.resetForm();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm("Pertanyaan", "Apakah anda yakin ingin menghapus data ini?", this.deleteRecYes, this);
    },
    deleteRecYes: function (a) {
        if (a == "no") return;
        var b = this.sm.getSelected();
        if (b == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Item");
            return;
        }
        this.store.remove(b);
    }
});
jun.JurnalUmum = Ext.extend(Ext.grid.GridPanel, {
    title: "General Journal",
    id: 'docs-jun.JurnalUmum',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        // {
        //     header: 'Date',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'tran_date',
        //     width: 100
        // },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'reference',
            width: 100,
            filter: {xtype: "textfield", filterName: "reference"}
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'memo_',
            width: 100,
            filter: {xtype: "textfield", filterName: "memo_"}
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'tot_debit',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
            //filter: {xtype: "textfield", filterName: "tot_debit"}
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100,
            filter: {xtype: "textfield", filterName: "store"}
        }
    ],
    initComponent: function () {
        if (jun.rztAreaCmp.getTotalCount() === 0) {
            jun.rztAreaCmp.load();
        }
        jun.rztJurnalUmum.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    //b.params.tgl = Ext.getCmp('tgljurnalumumgridid').getValue();
                    var tgl = Ext.getCmp('tgljurnalumumgridid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztJurnalUmum;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add General Journal',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show General Journal',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                // {
                //     xtype: 'button',
                //     text: 'Delete General Journal',
                //     ref: '../btnDel'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tgljurnalumumgridid'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Import General Journal',
                //     ref: '../btnImportMasuk'
                // }
            ]
        };
        jun.JurnalUmum.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDel.on('Click', this.deleteRec, this);
        this.tgl.on('select', this.refreshTgl, this);
        // this.btnImportMasuk.on('Click', this.importKasMasuk, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    importKasMasuk: function (){
        var form = new jun.ImportJurnalUmum();
        form.show();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.GlTransWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a doctor");
            return;
        }
        var idz = selectedz.json.type_no;
        var form = new jun.GlTransWin({modez: 1, idju: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztGlTrans.baseParams = {
            type_no: idz
        };
        jun.rztGlTrans.load();
        jun.rztGlTrans.baseParams = {};
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'GlTrans/delete',
            method: 'POST',
            scope: this,
            params: {
                type_no: record.json.type_no
            },
            success: function (f, a) {
                this.refreshTgl();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
