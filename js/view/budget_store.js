jun.Budgetstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Budgetstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BudgetStoreId',
            url: 'projection/Budget',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'budget_id'},
                {name:'account_id'},
                {name:'account_code'},
                {name:'account_name'},
                {name:'amount'},
                {name:'description'},
                {name:'tdate'},
                {name:'tdatef'},
                {name:'created_at'},
                {name:'businessunit_id'},
                {name:'uploadfile'},
                {name:'uploaddate'},

            ]
        }, cfg));
    }
});
jun.rztBudget = new jun.Budgetstore();
//jun.rztBudget.load();
