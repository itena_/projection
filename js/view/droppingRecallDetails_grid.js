var checkboxSelectionModelDroppingRecallAllDetailsGrid = new Ext.grid.CheckboxSelectionModel();
jun.DroppingRecallAllDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    viewConfig: { forceFit: true },
    sm: checkboxSelectionModelDroppingRecallAllDetailsGrid,
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        checkboxSelectionModelDroppingRecallAllDetailsGrid,
        {
            header: 'Transfer Ref.',
            sortable: true,
            resizable: true,
            dataIndex: 'dropping_doc_ref',
            width: 200,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Penerima',
            sortable: true,
            resizable: true,
            dataIndex: 'store_penerima',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_barang',
            width: 150,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_barang',
            width: 250,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 50,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Transfer',
            sortable: true,
            resizable: true,
            dataIndex: 'qty_dropping',
            align: 'right',
            width: 100,
            renderer: Ext.util.Format.numberRenderer('0,0')
        },
        {
            header: 'Received',
            sortable: true,
            resizable: true,
            dataIndex: 'qty_received',
            align: 'right',
            width: 100,
            renderer: Ext.util.Format.numberRenderer('0,0')
        },
        {
            header: 'Recalled',
            sortable: true,
            resizable: true,
            dataIndex: 'qty_recalled',
            align: 'right',
            width: 100,
            renderer: Ext.util.Format.numberRenderer('0,0')
        },
        {
            header: 'Intransit',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            align: 'right',
            width: 100,
            renderer: Ext.util.Format.numberRenderer('0,0')
        },
        {
            header: 'Qty Ditarik',
            sortable: true,
            resizable: true,
            dataIndex: 'qty_recall',
            align: 'right',
            width: 100,
            renderer : function(value, metaData, record, rowIndex){
                if(value > 0){
                    if(value > record.get('qty'))
                        metaData.style += ';background-color: #FFFF99;';
                    else
                        metaData.style += ';background-color: #99FF99;';
                }
                return Ext.util.Format.number(value, "0,0");
            }
        }
    ],
    initComponent: function () {
        this.store = new jun.DroppingRecallDetailsStore({
            url: 'DroppingRecall/AllDetail'
        });
        
        if(!this.readOnly){
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Refresh',
                        ref: '../btnRefresh',
                        iconCls: 'silk13-arrow_refresh'
                    },
                    '-',
                    {
                        xtype: 'button',
                        text: 'Buat penarikan',
                        ref: '../btnMake',
                        iconAlign: 'right',
                        iconCls: 'silk13-bullet_go'
                    }
                ]
            };
        }
        jun.DroppingRecallAllDetailsGrid.superclass.initComponent.call(this);
        if(!this.readOnly){
            this.btnRefresh.on('click', this.store.reload, this.store);
            this.btnMake.on('click', this.goToGridDetail, this);
            this.store.on('load', this.afterStoreLoad, this);
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    afterStoreLoad: function(){
        this.parent.gridDetail.store.each(function (rec, idx) {
            var matchIdx = this.store.findBy(function(r, id){
                return rec.get('dropping_id') == r.get('dropping_id') && rec.get('product_id') == r.get('product_id');
            });
            if(matchIdx>-1){
                var matchRecord = this.store.getAt(matchIdx);
                matchRecord.set('qty_recall', rec.get('qty'));
                matchRecord.commit();
            }
        }, this);
    },
    goToGridDetail: function (btn) {
        /*add selected item */
        var s = this.getSelectionModel().getSelections();
        for(var i = 0, r; r = s[i]; i++){
            this.parent.gridDetail.addNewRecord(r.data);
        }

        this.parent.card1.layout.setActiveItem(1);
        this.parent.btnVisibled(true);
    }
});

var checkboxSelectionModelDroppingRecallDetailsGrid = new Ext.grid.CheckboxSelectionModel();
jun.DroppingRecallDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    viewConfig: { forceFit: true },
    sm: checkboxSelectionModelDroppingRecallDetailsGrid,
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        checkboxSelectionModelDroppingRecallDetailsGrid,
        new Ext.grid.RowNumberer(),
        {
            header: 'Transfer Ref.',
            sortable: true,
            resizable: true,
            dataIndex: 'dropping_doc_ref',
            width: 200
        },
        {
            header: 'Penerima',
            sortable: true,
            resizable: true,
            dataIndex: 'store_penerima',
            width: 100
        },
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_barang',
            width: 150
        },
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_barang',
            width: 250
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            align: 'right',
            width: 100,
            renderer: Ext.util.Format.numberRenderer('0,0')
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'sat',
            width: 50
        }
    ],
    initComponent: function () {
        this.store = new jun.DroppingRecallDetailsStore();

        if(!this.readOnly){
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 6,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Item :'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: false,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                readOnly: true,
                                store: jun.rztBarangLib,
                                valueField: 'barang_id',
                                ref: '../../barang',
                                displayField: 'kode_barang',
                                listWidth: 300
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty :'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../qty',
                                width: 50,
                                minValue: 1
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        defaults: {
                            scale: 'small',
                            width: 50
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Add',
                                ref: '../../btnAdd',
                                iconCls: 'silk13-add'
                            },
                            {
                                xtype: 'button',
                                text: 'Edit',
                                ref: '../../btnEdit',
                                iconCls: 'silk13-pencil'
                            },
                            {
                                xtype: 'button',
                                text: 'Del',
                                ref: '../../btnDelete',
                                iconCls: 'silk13-delete'
                            }
                        ]
                    }
                ]
            };
        }
        jun.DroppingRecallDetailsGrid.superclass.initComponent.call(this);
        if(!this.readOnly){
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.loadEditForm, this);
            this.btnDelete.on('Click', this.deleteRec, this);
            this.getSelectionModel().on('rowselect', this.getrow, this);

            this.store.on('add', this.onStoreChange, this);
            this.store.on('update', this.onStoreChange, this);
            this.store.on('remove', this.onStoreChange, this);
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    onStoreChange: function (s, b, d) {
        this.store.setSequence();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadForm: function () {
        this.parent.card1.layout.setActiveItem(0);
        this.parent.gridAllDetail.store.reload();
        this.parent.btnVisibled(false);
    },
    loadEditForm: function (btn) {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            this.record.set('qty', parseFloat(this.qty.getValue()));
            this.record.commit();

            btn.setText("Edit");
            this.btnEdit.setIconClass('silk13-pencil');
            this.btnDisable(false);

            this.barang.reset();
            this.qty.reset();
        }else if (btn.text == 'Edit') {
            this.barang.setValue(this.record.data.barang_id);
            this.qty.setValue(this.record.data.qty);
            btn.setText("Save");
            this.btnEdit.setIconClass('silk13-disk');
            this.btnDisable(true);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Delete item', 'Apakah anda yakin ingin menghapus data yang terpilih?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') { return; }

        var s = this.getSelectionModel().getSelections();
        for(var i = 0, r; r = s[i]; i++){
            this.store.remove(r);
        }
    },
    addNewRecord: function(data){
        var i = this.store.findBy(function(r){
            return r.get('dropping_id') == data.dropping_id && r.get('barang_id') == data.barang_id
        });
        if(i<0){
            var d = new this.store.recordType(data);
            this.store.add(d);
        }
    }
});
