jun.AssignEmployeesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.AssignEmployeesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssignEmployeesStoreId',
            url: 'AssignEmployees',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'assign_employee_id'},
                {name: 'employee_id'},
                {name: 'tipe_employee_id'},
                {name: 'tgl'},
                {name: 'tdate'}
            ]
        }, cfg));
    }
});
jun.rztAssignEmployees = new jun.AssignEmployeesstore();
//jun.rztAssignEmployees.load();
