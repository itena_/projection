jun.TagBarangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TagBarangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TagBarangStoreId',
            url: 'TagBarang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tag_barang_id'},
                {name: 'barang_id'},
                {name: 'tag_id'},
                {name: 'security_roles_id'}

            ]
        }, cfg));
    }
});
jun.rztTagBarang = new jun.TagBarangstore();
//jun.rztTagBarang.load();
