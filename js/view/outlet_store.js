jun.Outletstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Outletstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'OutletStoreId',
            url: 'projection/Outlet',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'outlet_id'},
                {name:'kodeoutlet'},
                {name:'namaoutlet'},
                {name:'description'},
                {name:'jumlahoutlet'},
                {name:'created_at'},
                {name:'businessunit_id'},
                {name:'uploadfile'},
                {name:'uploaddate'},
                
            ]
        }, cfg));
    }
});
jun.rztOutlet = new jun.Outletstore();
jun.rztOutletLib = new jun.Outletstore();
jun.rztOutletLib.load();
//jun.rztOutlet.load();
