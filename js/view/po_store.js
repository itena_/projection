jun.PoStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PoStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PoStoreId',
            url: 'PoIn',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'po_id'},
                {name: 'doc_ref'},
                {name: 'tgl', type: 'date'},
                {name: 'tgl_delivery', type: 'date'},
                {name: 'note'},
                {name: 'acc_charge'},
                
                {name: 'pr_id'},
                {name: 'doc_ref_pr'},
                {name: 'divisi'},
                {name: 'store'},
                
                {name: 'status'},
                {name: 'id_user'},
                {name: 'tdate'},
                
                {name: 'nama'},
                {name: 'ship_to_company'},
                {name: 'ship_to_address'},
                {name: 'ship_to_city'},
                {name: 'ship_to_country'},
                {name: 'ship_to_phone'},
                {name: 'ship_to_fax'},
                
                {name: 'supplier_id'},
                {name: 'supp_nama'},
                {name: 'supp_company'},
                {name: 'supp_address'},
                {name: 'supp_city'},
                {name: 'supp_country'},
                {name: 'supp_phone'},
                {name: 'supp_fax'},
                {name: 'supp_email'},
                
                {name: 'termofpayment', type: 'int'},
                {name: 'nama_rek'},
                {name: 'no_rek'},
                {name: 'bank_rek'},
                
                {name: 'sub_total', type: 'float'},
                {name: 'disc', type: 'float'},
                {name: 'total_disc_rp', type: 'float'},
                {name: 'total_dpp', type: 'float'},
                {name: 'ppn'},
                {name: 'tax_rp', type: 'float'},
                {name: 'total_pph_rp', type: 'float'},
                {name: 'total', type: 'float'},
                
                {name: 'closed_id_user'},
                {name: 'closed_tdate'},
                
                /*
                 * type :
                 * 1 : PO normal
                 * -1 : PO retur/minus
                 */
                {name: 'type_', type: 'int'}
            ]
        }, cfg));
    }
});
jun.rztPO = new jun.PoStore();
