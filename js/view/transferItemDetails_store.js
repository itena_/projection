jun.TransferItemDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferItemDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferItemDetailsStoreId',
            url: 'TransferItemDetails',
            root: 'results',
            autoLoad: !1,
            autoSave: !1,
            totalProperty: 'total',
            fields: [
                {name: 'transfer_item_details_id'},
                {name: 'qty', type: 'float'},
                {name: 'barang_id'},
                {name: 'transfer_item_id'},
                {name: 'price', type: 'float'},
                {name: 'sub_total', type: 'float'},
                {name: 'disc', type: 'float'},
                {name: 'disc_rp', type: 'float'},
                {name: 'total_disc', type: 'float'},
                {name: 'total_dpp', type: 'float'},
                {name: 'ppn', type: 'float'},
                {name: 'ppn_rp', type: 'float'},
                {name: 'pph_id', type: 'int'},
                {name: 'pph', type: 'float'},
                {name: 'pph_rp', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'charge'},
                {name: 'item_id'}
            ]
        }, cfg));
    },
    calculateItem: function () {
        this.each(function(r){

            var result = jun.calculateItemInvoice(r.get('qty'), r.get('price'), r.get('disc'), r.get('ppn'), r.get('pph_id'), r.get('pph'));
            var sub_total = result['bruto'];
            var disc_rp = result['disc_rp'];
            var total_dpp = result['dpp'];
            var ppn_rp = result['ppn_rp'];
            var pph_rp = result['pph_rp'];
            var total = result['total'];
            
            r.set('sub_total', sub_total);
            r.set('disc_rp', disc_rp);
            r.set('total_disc', disc_rp);
            r.set('total_dpp', total_dpp);
            r.set('ppn_rp', ppn_rp);
            r.set('pph_rp', Math.abs(pph_rp));
            r.set('total', total);
        });
        this.commitChanges();
        this.refreshData();
    },
    refreshData: function () {
        var sub_total = this.sum("sub_total");
        var tax_rp = this.sum("ppn_rp");
        var total = this.sum("total");
        var disc_rp = this.sum("disc_rp");
        var total_pph_rp = this.sum("pph_rp");
        var total_dpp = this.sum("total_dpp");
        Ext.getCmp('total_dpp_id').setValue(total_dpp);
        Ext.getCmp('total_disc_rp_id').setValue(disc_rp);
        Ext.getCmp('sub_total_id').setValue(sub_total);
        Ext.getCmp('totalpr_id').setValue(total);
        Ext.getCmp('total_pph_rp_id').setValue(total_pph_rp);
        Ext.getCmp('tax_rp_id').setValue(tax_rp);
        this.refreshSortData();
    },
    refreshSortData: function () {
        this.each(function (item, index, totalItems) {
            item.data.seq = index;
        },this);
    }
});
jun.rztTransferItemDetails = new jun.TransferItemDetailsstore();
