jun.ResepDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ResepDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ResepDetailsStoreId',
            url: 'ResepDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'resep_details_id'},
                {name: 'nama_bahan'},
                {name: 'qty'},
                {name: 'sat'},
                {name: 'resep_id'}
            ]
        }, cfg));
    }
});
jun.rztResepDetails = new jun.ResepDetailsstore();
//jun.rztResepDetails.load();
