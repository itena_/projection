jun.PaymentJournalDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Account Receiveable Detail",
    id: "docs-jun.PaymentJournalDetailGrid",
    stripeRows: true,
    viewConfig: {
        forceFit: !0
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: !0
    }),
    columns: [
        {
            header: "Accounts Code",
            resizable: !0,
            dataIndex: "account_code",
            width: 100
        },
        {
            header: "Accounts Name",
            resizable: !0,
            dataIndex: "account_code",
            width: 250,
            renderer: jun.renderCoa
        },
        {
            header: "Note",
            resizable: !0,
            dataIndex: "memo_",
            width: 250
        },
        {
            header: "Debit",
            resizable: !0,
            dataIndex: "debit",
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            width: 100
        },
        {
            header: "Kredit",
            resizable: !0,
            dataIndex: "kredit",
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            width: 100
        },
        {
            header: "Branch",
            resizable: !0,
            dataIndex: "store_kode",
            width: 75
        }
//        {
//            header: "Grup",
//            resizable: !0,
//            dataIndex: "grup",
//            width: 75
//        }
    ],
    initComponent: function () {
        this.store = jun.rztPaymentJournalDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 8,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'COA :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
//                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            enableKeyEvents: true,
                            forceSelection: true,
                            fieldLabel: 'COA',
                            store: jun.rztChartMasterCmp,
                            ref: '../../account_code',
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                                "</div></tpl>"),
                            listWidth: 500,
//                            hiddenName: 'account_code',
                            valueField: 'account_code',
                            displayField: 'account_code'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Debit :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'debitid',
                            ref: '../../debit',
                            width: 75,
                            value: 0,
                            enableKeyEvents: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Kredit :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'kreditid',
                            ref: '../../kredit',
                            width: 75,
                            value: 0,
                            enableKeyEvents: true
                        },
//                        {
//                            xtype: 'label',
//                            style: 'margin:5px',
//                            text: 'Grup  :'
//                        },
//                        {
//                            xtype: 'numericfield',
//                            ref: '../../grup',
//                            width: 75,
//                            maxLength: 75
//                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Branch :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            fieldLabel: 'Branch',
                            ref: '../../store_kode',
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            store: jun.rztStoreCmp,
                            hiddenName: 'store',
                            name: 'store_kode',
                            valueField: 'store_kode',
                            displayField: 'store_kode',
                            allowBlank: false,
                            width: 100
                            // x: 85,
                            // y: 32
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../memo_',
                            width: 600,
                            colspan: 8,
                            maxLength: 150
                        },
                        {
                            xtype: 'numericfield',
                            id: 'amountid',
                            ref: '../../amount',
                            width: 75,
                            value: 0,
                            hidden: true
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.PaymentJournalDetailGrid.superclass.initComponent.call(this);
        this.btnAdd.on("Click", this.loadForm, this);
        this.btnEdit.on("Click", this.onClickbtnEdit, this);
        this.btnDelete.on("Click", this.deleteRec, this);
        this.kredit.on('keyup', this.kreditOnChange, this);
        this.debit.on('keyup', this.debitOnChange, this);
        this.getSelectionModel().on("rowselect", this.getrow, this);
        this.store.removeAll();
    },
    kreditOnChange: function (t, e) {
        if (t.getValue() != 0) {
            this.debit.setValue('0');
        }
        this.amount.setValue(-t.getValue());
    },
    debitOnChange: function (t, e) {
        if (t.getValue() != 0) {
            this.kredit.setValue('0');
        }
        this.amount.setValue(t.getValue());
    },
    getrow: function (a, b, c) {
        this.record = c;
        var d = this.sm.getSelections();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.account_code.setValue(record.data.account_code);
            this.memo_.setValue(record.data.memo_);
            this.store_kode.setValue(record.data.store_kode);
//            this.grup.setValue(record.data.grup);
            this.amount.setValue(record.data.amount);
            this.kredit.setValue(record.data.kredit);
            this.debit.setValue(record.data.debit);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadForm: function () {
        var valkredit = parseFloat(this.kredit.getValue());
        var valdebit = parseFloat(this.debit.getValue());
        var store_kode = this.store_kode.getValue();
//        var grup = this.grup.getValue();

        if(store_kode == "" || store_kode == undefined){
            Ext.MessageBox.alert("Warning", "Branch tidak boleh kosong!");
            return;
        }
//        if(grup <= 0 || grup == "" || grup == undefined){
//            Ext.MessageBox.alert("Warning", "Grup harus berupa angka yang lebih besar dari 0");
//            return;
//        }
        var account_code = this.account_code.getValue();
        var memo_ = this.memo_.getValue();
        var amount = parseFloat(this.amount.getValue());
        var debit = valdebit;
        var kredit = valkredit;
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('account_code', account_code);
            record.set('memo_', memo_);
            record.set('store_kode', store_kode);
//            record.set('grup', grup);
            record.set('debit', debit);
            record.set('kredit', kredit);
            record.set('amount', amount);
            record.commit();
        } else {
            var c = jun.rztPaymentJournalDetail.recordType,
                d = new c({
                    account_code: account_code,
                    memo_: memo_,
//                    grup: grup,
                    store_kode: store_kode,
                    debit: debit,
                    kredit: kredit,
                    amount: amount
                });
            jun.rztPaymentJournalDetail.add(d);
        }
        this.account_code.reset();
        this.memo_.reset();
//        this.grup.reset();
        this.store_kode.reset();
        this.amount.reset();
        this.kredit.reset();
        this.debit.reset();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm("Pertanyaan", "Apakah anda yakin ingin menghapus data ini?", this.deleteRecYes, this);
    },
    deleteRecYes: function (a) {
        if (a == "no") return;
        var b = this.sm.getSelected();
        if (b == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Item");
            return;
        }
        this.store.remove(b);
    }
});