jun.ReceiveDroppingDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.ReceiveDroppingDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReceiveDroppingDetailsStoreId',
            url: 'ReceiveDroppingDetails',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'receive_dropping_detail_id'},
                {name:'qty'},
                {name:'barang_id'},
                {name:'visible'},
                {name:'receive_dropping_id'},
                {name: 'price'}
            ]
        }, cfg));
    }
});
jun.rztReceiveDroppingDetails = new jun.ReceiveDroppingDetailsstore();
//jun.rztReceiveDroppingDetails.load();
