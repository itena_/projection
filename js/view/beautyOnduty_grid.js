jun.BeautyOndutyGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Beauty Onduty",
    id: 'docs-jun.BeautyOndutyGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Nama Beauty',
            sortable: true,
            resizable: true,
            dataIndex: 'beauty_id',
            renderer: jun.renderBeauty,
            width: 100
        },
        {
            header: 'Keterangan',
            sortable: true,
            resizable: true,
            dataIndex: 'note_',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBeautyLib.getTotalCount() === 0) {
            jun.rztBeautyLib.load();
        }
        this.store = jun.rztBeautyOnduty;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Beautician :'
                },
                {
                    xtype: 'combo',
                    ref: '../beauty',
                    triggerAction: 'all',
                    editable: false,
                    lazyRender: true,
                    mode: 'local',
                    store: jun.rztBeautyCmp,
                    valueField: 'beauty_id',
                    displayField: 'nama_beauty',
                    allowBlank: true,
                    listWidth: 300,
                    lastQuery: ''
                },
                {
                    xtype: 'button',
                    text: 'Add Beauty',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Delete Beauty',
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Toggle ISHOMA',
                    ref: '../btnEdit'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.BeautyOndutyGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var beauty_id = this.beauty.getValue();
        if (beauty_id == '') {
            Ext.MessageBox.alert("Warning", "Anda harus memilih beauty yang akan di add.");
            return;
        }
        var pos = jun.rztBeautyOnduty.findExact('beauty_id', beauty_id);
        if (pos != -1) {
            Ext.MessageBox.alert("Warning", "Beautician sudah di add.");
            return;
        }
        Ext.Ajax.request({
            url: 'BeautyOnduty/create/',
            method: 'POST',
            params: {
                beauty_id: beauty_id,
                note_: 'KOSONG'
            },
            success: function (f, a) {
                jun.rztBeautyOnduty.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    loadEditForm: function (t, e) {
        t.setDisabled(true);
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih beauty");
            t.setDisabled(false);
            return;
        }
        if (selectedz.json.note_ != 'ISHOMA' && selectedz.json.note_ != 'KOSONG') {
            Ext.MessageBox.alert("Warning", "Beauty tidak dalam status KOSONG atau ISHOMA");
            t.setDisabled(false);
            return;
        }
        var beauty_onduty_id = selectedz.json.beauty_onduty_id;
        Ext.Ajax.request({
            url: 'BeautyOnduty/update/',
            method: 'POST',
            params: {
                beauty_onduty_id: beauty_onduty_id
            },
            success: function (f, a) {
                t.setDisabled(false);
                jun.rztBeautyOnduty.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                t.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'BeautyOnduty/delete/id/' + record.json.beauty_onduty_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBeautyOnduty.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
