jun.UlptGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "ULPT",
    id: 'docs-jun.UlptGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    status: 0,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. ULPT',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'No. Customer',
            sortable: true,
            resizable: true,
            dataIndex: 'no_customer',
            width: 100
        },
        {
            header: 'Nama Customer',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 100
        },
        {
            header: 'Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Jenis ULPT',
            sortable: true,
            resizable: true,
            dataIndex: 'tipe_ulpt',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztUlpt;
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztCardCmp.getTotalCount() === 0) {
            jun.rztCardCmp.load();
        }
        // jun.rztUlpt.on({
        //     scope: this,
        //     beforeload: {
        //         fn: function (a, b) {
        //             b.params.status = this.status;
        //             b.params.mode = "grid";
        //         }
        //     }
        // });
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Create ULPT',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit ULPT',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print ULPT',
                    ref: '../btnPrintUlpt'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Create RPJ',
                    ref: '../btnRpg'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Status : <b>ALL</b>',
                    ref: '../btnFilter',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'ALL',
                                // status_pr: 'all',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        jun.rztUlpt.setBaseParam('status', 0);
                                        jun.rztUlpt.load();
                                    }
                                }
                            },
                            {
                                text: 'OPEN',
                                // status_pr: PR_OPEN,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        jun.rztUlpt.setBaseParam('status', 1);
                                        jun.rztUlpt.load();
                                    }
                                }
                            },
                            {
                                text: 'CLOSED',
                                // status_pr: PR_CLOSED,
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('Status : <b>' + m.text + '</b>');
                                        jun.rztUlpt.setBaseParam('status', 2);
                                        jun.rztUlpt.load();
                                    }
                                }
                            }
                        ]
                    }
                }
            ]
        };
        this.store.baseParams = {mode: "grid", status: 0};
        this.store.load();
        jun.UlptGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrintUlpt.on('Click', this.printUlpt, this);
        this.btnRpg.on('Click', this.loadEditRpg, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    printUlpt: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        Ext.Ajax.request({
            url: 'ulpt/print',
            method: 'POST',
            scope: this,
            params: {
                id: record.json.ulpt_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    return;
                }
//                qz.appendImage(getPath() + "images/img_receipt.png", "ESCP");
//                while (!qz.isDoneAppending()) {}
//                qz.append("\x1B\x40"); // 1
//                //qz.append("\x1B\x21\x08"); // 2
//                qz.append("\x1B\x21\x01"); // 3
//                qz.append(response.msg);
//                qz.append("\x1D\x56\x41"); // 4
//                qz.append("\x1B\x40"); // 5
//                qz.print();
//                qz.appendHTML('<html><pre>' + response.msg + '</pre></html>');
//                qz.printHTML();
//                 printHTML(PRINTER_RECEIPT, '<html><pre>' + response.msg + '</pre></html>');
//                 print(PRINTER_RECEIPT, __openCashDrawer);
                var msg = [{type: 'raw', data: response.msg}];
                var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                print(PRINTER_RECEIPT, printData);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (selectedz == undefined) {
            return;
        }
        jun.rztSalestransULPTCmp.baseParams = {
            salestrans_id: this.record.data.salestrans_id
        };
        jun.rztSalestransULPTCmp.load();
        jun.rztSalestransULPTCmp.baseParams = {};
    },
    loadForm: function () {
        var form = new jun.UlptWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.ulpt_id;
        var form = new jun.UlptWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    loadEditRpg: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        if (selectedz.json.lunas != 0) {
            Ext.MessageBox.alert("Error", "ULPT sudah lunas.");
            return;
        }
        var idz = selectedz.json.ulpt_id;
        var form = new jun.RpgWin({modez: 1});
        form.parent_id.setValue(idz);
        form.ulpt_doc_ref.setValue(selectedz.json.doc_ref);
        form.amount.setMaxValue(selectedz.json.amount);
        form.show(this);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Ulpt/delete/id/' + record.json.ulpt_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztUlpt.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
