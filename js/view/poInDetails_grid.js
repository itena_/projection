jun.PoInDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PoInDetails",
    id: 'docs-jun.PoInDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarang
        },
        {
            header: 'Description',
            sortable: true,
            resizable: true,
            dataIndex: 'description',
            width: 100
        },
        {
            header: 'Charge',
            sortable: true,
            resizable: true,
            dataIndex: 'charge',
            width: 100
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 75,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        //{
        //    header: 'Sat',
        //    sortable: true,
        //    resizable: true,
        //    dataIndex: 'sat',
        //    width: 100,
        //    renderer: jun.renderSatBarang
        //},
        {
            header: 'Price',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Disc',
            sortable: true,
            resizable: true,
            dataIndex: 'disc',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'PPN',
            sortable: true,
            resizable: true,
            dataIndex: 'ppn',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'PPH',
            sortable: true,
            resizable: true,
            dataIndex: 'pph',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztPoInDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 14,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Barang :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztBarangCmp,
                            //hiddenName: 'barang_id',
                            valueField: 'barang_id',
                            ref: '../../barang',
                            displayField: 'kode_barang'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Charge :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../charge',
                            width: 100,
                            style: 'margin-bottom:2px'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Price :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../price',
                            width: 75,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../disc',
                            width: 50,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'PPN :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../ppn',
                            width: 50,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'PPH :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../pph',
                            width: 50,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Description :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../ket',
                            width: 801,
                            colspan: 13,
                            maxLength: 255
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.PoInDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.barang.on('select', this.onChangeBarang, this);
        this.on('sortchange', this.onShortChange, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onShortChange: function (g, i) {
        jun.rztPoInDetails.refreshSortData()
    },
    onChangeBarang: function (c, r, i) {
        this.price.setValue(r.data.price);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        var charge = this.charge.getValue();
        var ket = this.ket.getValue();
        var disc = parseFloat(this.disc.getValue());
        var qty = parseFloat(this.qty.getValue());
        var price = parseFloat(this.price.getValue());
        var ppn = parseFloat(this.ppn.getValue());
        var pph = parseFloat(this.pph.getValue());
        var sub_total = round(qty * price,2);
        var disc_rp = round(sub_total * (disc / 100),2);
        var total_disc = disc_rp;
        var total_dpp = sub_total - total_disc;
        var ppn_rp = round(total_dpp * (ppn / 100),2);
        var pph_rp = round(total_dpp * (pph / 100),2);
        var total = total_dpp + ppn_rp - pph_rp;
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('description', ket);
            record.set('charge', charge);
            record.set('qty', qty);
            record.set('price', price);
            record.set('sub_total', sub_total);
            record.set('total_dpp', total_dpp);
            record.set('disc', disc);
            record.set('disc_rp', disc_rp);
            record.set('ppn', ppn);
            record.set('ppn_rp', ppn_rp);
            record.set('pph', pph);
            record.set('pph_rp', pph_rp);
            record.set('total', total);
            record.commit();
        } else {
            var c = jun.rztPoInDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    description: ket,
                    charge: charge,
                    qty: qty,
                    price: price,
                    sub_total: sub_total,
                    total_dpp: total_dpp,
                    disc: disc,
                    disc_rp: disc_rp,
                    ppn: ppn,
                    ppn_rp: ppn_rp,
                    pph: pph,
                    pph_rp: pph_rp,
                    total: total
                });
            jun.rztPoInDetails.add(d);
        }
        this.barang.reset();
        this.charge.reset();
        this.ket.reset();
        this.disc.reset();
        this.qty.reset();
        this.ppn.reset();
        this.pph.reset();
        this.price.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            this.qty.setValue(record.data.qty);
            this.price.setValue(record.data.price);
            this.disc.setValue(record.data.disc);
            this.ket.setValue(record.data.description);
            this.charge.setValue(record.data.charge);
            this.ppn.setValue(record.data.ppn);
            this.pph.setValue(record.data.pph);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});
