jun.AssetDetailGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"Asset Detail",
        id:'docs-jun.AssetDetailGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
        /*                {
			header:'asset_trans_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_trans_id',
			width:100
		},
                                {
			header:'asset_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_id',
			width:100
		},
                                {
			header:'asset_group_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_group_id',
			width:100
		},*/
        /*{
            header:'Doc Ref Other',
            sortable:true,
            resizable:true,
            dataIndex:'docref_other',
            width:100
        },*/
                                {
			header:'Doc Ref',
			sortable:true,
			resizable:true,                        
            dataIndex:'docref_other',
			width:100,
            filter: {xtype: "textfield"}
		},
        {
            header:'No. Activa',
            sortable:true,
            resizable:true,
            dataIndex:'ati',
            width:100,
            filter: {xtype: "textfield"}
        },

                                {
			header:'Asset Name',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_trans_name',
			width:100,
            filter: {xtype: "textfield"}
		},
        {
            header:'Branch',
            sortable:true,
            resizable:true,
            dataIndex:'asset_trans_branch',
            width:100,
            filter: {xtype: "textfield"}
        },
                                {
			header:'Price',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_trans_price',
			width:100
		},
                                {
			header:'New Price',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_trans_new_price',
			width:100
		},

        {
            header:'Class',
            sortable:true,
            resizable:true,
            dataIndex:'class',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Tariff',
            sortable:true,
            resizable:true,
            dataIndex:'tariff',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Period',
            sortable:true,
            resizable:true,
            dataIndex:'period',
            width:100,
            filter: {xtype: "textfield"}
        },
        /*{
            header:'Penyusutan Perbulan',
            sortable:true,
            resizable:true,
            dataIndex:'penyusutanperbulan',
            width:100
        },
        {
            header:'Penyusutan PerTahun',
            sortable:true,
            resizable:true,
            dataIndex:'penyusutanpertahun',
            width:100
        },*/

                                {
			header:'Description',
			sortable:true,
			resizable:true,                        
            dataIndex:'description',
			width:100,
            filter: {xtype: "textfield"}
		},
        {
            header:'Date',
            sortable:true,
            resizable:true,
            dataIndex:'asset_trans_date',
            width:100,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            filter: {
                xtype: "xdatefield",
                format: 'd/m/Y'
            }
        },


		
	],
	initComponent: function(){
	this.store = jun.rztAssetDetail;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    /*{
                        xtype: 'button',
                        text: 'Tambah',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },*/
                    {
                        xtype: 'button',
                        text: 'Show Depreciation',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Show Report',
                        ref: '../btnShow'
                    },
                    /*{
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Show Excel',
                        ref: '../btnShowExcel'
                    },*/
                    /*{
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Hapus',
                        ref: '../btnDelete'
                    }*/
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
                jun.AssetDetailGrid.superclass.initComponent.call(this);
	        //this.btnAdd.on('Click', this.loadForm, this);
                this.btnEdit.on('Click', this.loadEditForm, this);
                this.btnShow.on('Click', this.ShowForm, this);
                //this.btnShowExcel.on('Click', this.ShowExcel, this);
                //this.btnDelete.on('Click', this.deleteRec, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},
        
        getrow: function(sm, idx, r){
            this.record = r;
            var selectedz = this.sm.getSelections();
        },
        
        loadForm: function(){
            var form = new jun.AssetDetailWin({modez:0});
            form.show();
        },

        ShowForm: function(){

            var selectedz = this.sm.getSelected();
            if(selectedz == undefined){
                Ext.MessageBox.alert("Warning","Anda belum memilih Data");
                return;
            }
            var idz = selectedz.json.asset_trans_id;
            var form = new jun.ReportAssetDetails({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);

        },

        loadEditForm: function(){
            var selectedz = this.sm.getSelected();
            if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Data");
                 return;
            }
            var idz = selectedz.json.asset_trans_id;
            var form = new jun.AssetDetailWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
            jun.rztAssetPeriode.baseParams = {
                asset_trans_id: idz
            };
            jun.rztAssetPeriode.load();
            jun.rztAssetPeriode.baseParams = {};
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'asset/AssetDetail/delete/id/' + record.json.asset_trans_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztAssetDetail.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });
        
        }
})

jun.ShowAssetDetailGrid=Ext.extend(Ext.grid.GridPanel ,{
    title:"Show Assets",
    id:'docs-jun.AssetDetailGrid',
    iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
    plugins:[new Ext.ux.grid.GridHeaderFilters],
    columns:[

        {
            header:'Doc Ref',
            sortable:true,
            resizable:true,
            dataIndex:'docref_other',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Activa',
            sortable:true,
            resizable:true,
            dataIndex:'ati',
            width:100,
            filter: {xtype: "textfield"}
        },

        {
            header:'Asset Name',
            sortable:true,
            resizable:true,
            dataIndex:'asset_trans_name',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Class',
            sortable:true,
            resizable:true,
            dataIndex:'class',
            width:100,
            filter: {xtype: "textfield"}
        },
        /*{
            header:'Branch',
            sortable:true,
            resizable:true,
            dataIndex:'asset_trans_branch',
            width:100,
            filter: {xtype: "textfield"}
        },*/
        {
            header:'Description',
            sortable:true,
            resizable:true,
            dataIndex:'description',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Date',
            sortable:true,
            resizable:true,
            dataIndex:'asset_trans_date',
            width:100,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            filter: {
                xtype: "xdatefield",
                format: 'd/m/Y'
            }
        },
    ],
    initComponent: function(){
        this.store = jun.rztAssetDetail;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    ref: '../lbldate',
                    text: 'Date',
                },
                ' ',
                {
                    xtype: "xdatefield",
                    format: 'd/m/Y',
                    ref: '../date',
                    value: DATE_NOW,
                },
                ' ',
                {
                    xtype: 'button',
                    text: 'Print',
                    ref: '../btnPrint',
                    iconCls: 'silk13-printer'
                },
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Export Excel',
                    ref: '../btnExport',
                    iconCls: 'silk13-page_white_excel'
                },
                {
                    xtype: "form",
                    frame: !1,
                    border: !1,
                    ref: '../formz',
                    items: [
                        {
                            xtype: "hidden",
                            name: "asset_id",
                            ref: "../../asset_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }

            ]
        };
        this.store.baseParams = {mode: "grid"};
         this.store.reload();
         this.store.baseParams = {};
        jun.AssetDetailGrid.superclass.initComponent.call(this);
        //this.btnAdd.on('Click', this.loadForm, this);
        this.btnPrint.on('Click', this.Print, this);
        this.btnExport.on('Click', this.Export, this);
        //this.btnShowExcel.on('Click', this.ShowExcel, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        //this.getSelectionModel().on('rowselect', this.getrow, this);
        this.getSelectionModel().on('doubleclick', this.rowdblclick, this);
    },

    rowdblclick: function(){
        isDblClick = true;
        log.innerHTML += '<div>row double click</div>';
        Ext.MessageBox.alert("Warning","dbl");
        window.setTimeout(function(){
            isDblClick = false;
        }, 0);
    },
    btnVisibled: function (v) {
        this.lbldate.setVisible(v);
        this.date.setVisible(v);
    },
    btnDisabled: function (status) {
        /*this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);*/
    },
    getrow: function(sm, idx, r){
        this.record = r;
        var selectedz = this.sm.getSelections();

        /*this.btnVisibled(true);
        this.lbldate.setVisible(false);
        this.date.setVisible(false);*/
        //Ext.MessageBox.alert("Warning","Anda belum memilih Data");
        /*var singleClickTask = new Ext.util.DelayedTask(singleClickAction),  // our delayed task for single click
            singleClickDelay = 100; // delay in milliseconds

        function onClick() {
            singleClickTask.delay(singleClickDelay);
        }

        function onDblClick() {
            // double click detected - trigger double click action
            doubleClickAction();
            // and don't forget to cancel single click task!
            singleClickTask.cancel();
        }

        function singleClickAction() {
            Ext.MessageBox.alert("Warning","Anda belum memilih Data");
        }

        function doubleClickAction() {
            // something useful...
        }


// setting event handlers on an Element
        AssetDetailGrid.on('click', onClick);
        AssetDetailGrid.on('dblclick', onDblClick);*/


    },

    getrowdbl: function(sm, idx, r){
        this.record = r;
        var selectedz = this.sm.getSelections();

        Ext.MessageBox.alert("Warning","Anda belum memilih Data");
    },

    loadForm: function(){
        var form = new jun.AssetDetailWin({modez:0});
        form.show();
    },

    Export: function(){

        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data "+this.title+".");
            return;
        }
        var idz = selectedz.json.asset_id;
        //Ext.MessageBox.alert(idz);


        this.asset_id.setValue(idz);
        var form = this.formz.getForm();
        form.standardSubmit = !0;
        form.url = "Report/PrintAsset";
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },

    Print: function(){
        Ext.Ajax.request({
            url: 'asset/Asset/PrintAsset/',
            method: 'POST',
            //timeOut: 1000,
            scope: this,
            params: {
                id: this.id,
                date: this.date.getValue(),
            },
            success: function (f, a) {

                var response = Ext.decode(f.responseText);
                var printWindow = window.open('', '', 'height=0px ,width=0px');

                printWindow.document.write('<html>');
                printWindow.document.write('<head>');
                printWindow.document.write('<style>table {font-family: arial, sans-serif; border: 3px;border-collapse: collapse;width: 100%;} td, th {border: 1px solid #dddddd;text-align: center;padding: 8px;}tr:nth-child(even) {background-color: #dddddd;} </style>');
                printWindow.document.write('</head>');

                printWindow.document.write('<body>');
                printWindow.document.write('<div>');
                printWindow.document.write('<table>');
                printWindow.document.write('<tr>');
                    printWindow.document.write('<th>Name</th>');
                    printWindow.document.write('<th>No.Activa</th>');
                printWindow.document.write('</tr>');

                    for (var i = 0, len = response.data.length; i < len; i++)
                    {
                        printWindow.document.write('<tr>');
                            printWindow.document.write('<td>'+response.data[i].asset_trans_name+'</td>');
                            printWindow.document.write('<td>'+response.data[i].ati+'</td>');
                        printWindow.document.write('</tr>');
                    }

                printWindow.document.write('</table>');
                printWindow.document.write('</div>');

                printWindow.document.write('</body>');
                printWindow.document.write('</html>');

                setTimeout(function () {
                    printWindow.print();
                    printWindow.close();
                }, 500);

            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },

    deleteRec : function(){
        Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },

    deleteRecYes : function(btn){

        if (btn == 'no') {
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
            return;
        }

        Ext.Ajax.request({
            url: 'asset/AssetDetail/delete/id/' + record.json.asset_trans_id,
            method: 'POST',
            success:function (f, a) {
                jun.rztAssetDetail.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
})
