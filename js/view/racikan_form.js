jun.RacikanWin = Ext.extend(Ext.Window, {
    title: 'Formula',
    modez: 1,
    width: 605,
    height: 445,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Racikan',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Kode Barang',
                        store: jun.rztBarang,
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        displayField: 'kode_barang',
//                        anchor: '100%',
                        ref: '../barang',
                        emptyText: 'untuk membuat ..',
//                        itemSelector: "div.search-item-table",
//                        tpl: new Ext.XTemplate('<div class="container">','<tpl for="."><div class="search-item-table">',
//                            '<div class="cell4" style="width: 150px;text-align: right;font-size: 120%;"><h3>{kode_barang}</div></h3>',
//                            '<div class="cell4" style="width: 350px;font-size: 130%;"><h3>{nama_barang}</div></h3>',
//                            "</div></tpl>",'</div>'),
                        width: 150    
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Qty Hasil',
                        hideLabel: false,
                        //hidden:true,
                        name: 'qty',
//                        id: 'qtyid',
                        emptyText: 'sebanyak ..',
                        ref: '../qty',
                        maxLength: 30,
                        decimalPrecision: 3,
                        //allowBlank: ,
//                        anchor: '100%',
                        width: 150
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Satuan',
                        store: [
                            'pot', 'tube'
                        ],
                        hiddenName: 'satuan',
                        valueField: 'satuan',
                        displayField: 'satuan',
                        ref: '../satuan',
//                        readOnly: true,
//                        anchor: '100%',
                        width: 150
                    },
//                    {
//                        xtype: 'xdatefield',
//                        ref: '../tdate',
//                        fieldLabel: 'tdate',
//                        name: 'tdate',
//                        id: 'tdateid',
//                        format: 'd M Y',
//                        //allowBlank: ,
//                        anchor: '100%'
//                    },
//                    {
//                        xtype: 'textfield',
//                        fieldLabel: 'user_id',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'user_id',
//                        id: 'user_idid',
//                        ref: '../user_id',
//                        maxLength: 50,
//                        //allowBlank: ,
//                        anchor: '100%'
//                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Note',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'note',
//                        id: 'noteid',
                        ref: '../note',
//                        anchor: '100%',
                        width: 200
                    },
                    new jun.RacikanDetailsGrid({
                        height: 260,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.RacikanWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        } else {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function () {
        this.griddetils.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function ()
    {
        this.btnDisabled(true);
        if (this.griddetils.store.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            return;
        }
        var sat = this.satuan.getValue();
        var qty = this.qty.getValue();
//        var qty = jun.konversiSatuanDisimpan(parseFloat(this.qty.getValue()), sat);
        var urlz = 'Racikan/create/';
        Ext.getCmp('form-Racikan').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                id: this.id,
                mode: this.modez,
//                barang_id: this.barang.getValue(),
//                satuan_id: this.satuan.getValue(),
                qty_konv: qty,
                detil: Ext.encode(Ext.pluck(
                        jun.rztRacikanDetails.data.items, "data"))
            },
            success: function (f, a) {
                //jun.TransferBarangstore.reload();
                jun.rztRacikan.reload();
//                jun.rztKonversiFormula.reload();
//                jun.rztBarang2RoastingAll.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.closeForm) {
                    this.close();
                } else {
                    Ext.getCmp('form-Racikan').getForm().reset();
                    this.btnDisabled(false);
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },
    onbtnSaveCloseClick: function ()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function ()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});