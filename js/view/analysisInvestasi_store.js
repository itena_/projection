jun.AnalysisInvestasistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Analysisstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AnalysisStoreId',
            url: 'projection/AnalysisInvestasi',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'AccountCode'},
                {name:'AccountName'},
                {name:'AmountBudget'},
                {name:'AmountRealization'},
                {name:'Achievement'},
                {name:'AchievementPercent'},
            ]
        }, cfg));
    }
});
jun.rztAnalysisInvestasi = new jun.AnalysisInvestasistore();
//jun.rztBudget.load();
