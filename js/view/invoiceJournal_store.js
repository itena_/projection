jun.InvoiceJournalstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.InvoiceJournalstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'InvoiceJournalStoreId',
            url: 'InvoiceJournal',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'invoice_journal_id'},
                {name: 'doc_ref'},
                {name: 'doc_ref_other'},
                {name: 'tgl'},
                {name: 'tdate'},
                {name: 'tgl_jatuh_tempo'},
                {name: 'note'},
                {name: 'user_id'},
                {name: 'supplier_id'},
                {name: 'total', type: 'float'},
                {name: 'lunas'},
                {name: 'store_kode'},
                {name: 'p'},
                {name: 'up'},
                {name: 'editable'}
            ]
        }, cfg));
    }
});
jun.rztInvoiceJournal = new jun.InvoiceJournalstore();
//jun.rztInvoiceJournal.load();
