jun.RacikanDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Formula Detail",
    id: 'docs-jun.RacikanDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Item Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 150,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Item Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderBarang
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
//            renderer: function (v, m, r) {
//                return Ext.util.Format.number(r.get('satuan_id')), '0.00';
//            }
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'satuan',
            width: 50
//            renderer: jun.renderSatuan
        }
    ],
    initComponent: function () {
        this.store = jun.rztRacikanDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 6,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
//                            style: 'margin-bottom:2px',
                            forceSelection: true,
//                            store: jun.rztBarang2Lib,
                            store: jun.rztBarang,
                            valueField: 'barang_id',
                            ref: '../../barang',
                            displayField: 'kode_barang',
//                            itemSelector: "div.search-item-table",
//                            tpl: new Ext.XTemplate('<div class="container">', '<tpl for="."><div class="search-item-table">',
//                                    '<div class="cell4" style="width: 150px;text-align: right;font-size: 120%;"><h3>{kode_barang}</div></h3>',
//                                    '<div class="cell4" style="width: 350px;font-size: 130%;"><h3>{nama_barang}</div></h3>',
//                                    "</div></tpl>", '</div>'),
                            width: 150
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Satuan :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
//                            fieldLabel: 'Satuan',
                            store: [
                                'pot','tube'
                            ],
                            valueField: 'satuan',
                            displayField: 'satuan',
                            ref: '../../satuan',
//                            readOnly: true,
//                        anchor: '100%',
                            width: 150
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
//                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
//                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
//                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.RacikanDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
//        this.barang.on('select', this.onItemChange, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;

        var selectedz = this.sm.getSelections();
    },
//    onItemChange: function () {
//        var barang_id = this.barang.getValue();
//        var barang = jun.getBarang(barang_id);
//        this.sat.setText(barang.data.satuan);
//    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "You have not selected a item");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
        if (this.btnEdit.text != 'Save') {
            var a = jun.rztRacikanDetails.findExact("barang_id", barang_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Item already inputted");
                return
            }
        }
        var sat = this.satuan.getValue();
        var qty = this.qty.getValue();
//        var qty = jun.konversiSatuanDisimpan(parseFloat(this.qty.getValue()), sat);

        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.set('satuan', sat);
            record.commit();
        } else {
            var c = jun.rztRacikanDetails.recordType,
                    d = new c({
                        barang_id: barang_id,
                        qty: qty,
                        satuan: sat
                    });
            jun.rztRacikanDetails.add(d);
        }
        // this.store.reset();
        this.barang.reset();
        this.qty.reset();
        this.satuan.reset();

    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            this.qty.setValue(record.data.qty);
            this.satuan.setValue(record.data.satuan_id);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }
});
