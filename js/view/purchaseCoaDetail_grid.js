jun.PurchaseCoaDebitGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Debit",
    id: 'docs-jun.PurchaseCoaDebitGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Dpp',
            sortable: true,
            resizable: true,
            dataIndex: 'dpp',
            width: 100
        },
        {
            header: 'PPN',
            sortable: true,
            resizable: true,
            dataIndex: 'vat',
            width: 100
        },
        {
            header: 'DP PPh22',
            sortable: true,
            resizable: true,
            dataIndex: 'dppph22',
            width: 100
        },
        {
            header: 'DP PPh25',
            sortable: true,
            resizable: true,
            dataIndex: 'dppph25',
            width: 100
        },
        {
            header: 'DP PPh21',
            sortable: true,
            resizable: true,
            dataIndex: 'dppph21',
            width: 100
        },
        {
            header: 'DP PPh23',
            sortable: true,
            resizable: true,
            dataIndex: 'dppph23',
            width: 100
        },
        {
            header: 'DP PPh42',
            sortable: true,
            resizable: true,
            dataIndex: 'dppph42',
            width: 100
        },
        {
            header: 'DP PPh26',
            sortable: true,
            resizable: true,
            dataIndex: 'dppph26',
            width: 100
        },
        {
            header: 'Beban PPN',
            sortable: true,
            resizable: true,
            dataIndex: 'beban_ppn',
            width: 100
        },
        {
            header: 'Beban PPh',
            sortable: true,
            resizable: true,
            dataIndex: 'beban_pph',
            width: 100
        },
        {
            header: 'Pajak Lainnya',
            sortable: true,
            resizable: true,
            dataIndex: 'other_tax',
            width: 100
        },
        {
            header: 'Materai/Ekspedisi',
            sortable: true,
            resizable: true,
            dataIndex: 'biaya_materai_ekspedisi',
            width: 100
        },
        {
            header: 'Forex Loss',
            sortable: true,
            resizable: true,
            dataIndex: 'forex_loss',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztPurchaseCoaDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 14,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        /*
                         * atas
                         */
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Dpp :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'dppidkredit',
                            ref: '../../dpp',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'PPn :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'vatidkredit',
                            ref: '../../vat',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'DP PPh22 :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'dppph22idkredit',
                            ref: '../../dppph22',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'DP PPh25 :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'dppph25idkredit',
                            ref: '../../dppph25',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'DP PPh21 :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'dppph21idkredit',
                            ref: '../../dppph21',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'DP PPh23 :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'dppph23idkredit',
                            ref: '../../dppph23',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'DP PPh42 :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'dppph42idkredit',
                            ref: '../../dppph42',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        /*
                         * bawah
                         */
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'DP PPh26 :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'dppph26idkredit',
                            ref: '../../dppph26',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Beban PPN :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'beban_ppnidkredit',
                            ref: '../../beban_ppn',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Beban PPh :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'beban_pphidkredit',
                            ref: '../../beban_pph',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Pajak Lainnya :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'other_taxidkredit',
                            ref: '../../other_tax',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Materai/Ekspedisi :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'biaya_materai_ekspedisiidkredit',
                            ref: '../../biaya_materai_ekspedisi',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Forex Loss :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'forex_lossidkredit',
                            ref: '../../forex_loss',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        }
                    ]}
//                {
//                    xtype: 'buttongroup',
//                    columns: 3,
//                    id: 'btnsalesdetilid',
//                    defaults: {
//                        scale: 'large'
//                    },
//                    items: [
//                        {
//                            xtype: 'button',
//                            text: 'Add',
//                            height: 44,
//                            ref: '../../btnAdd'
//                        },
//                        {
//                            xtype: 'button',
//                            text: 'Edit',
//                            height: 44,
//                            ref: '../../btnEdit'
//                        },
//                        {
//                            xtype: 'button',
//                            text: 'Del',
//                            height: 44,
//                            ref: '../../btnDelete'
//                        }
//                    ]
//                }
            ]
        };
        jun.PurchaseCoaDebitGrid.superclass.initComponent.call(this);
//        this.btnAdd.on('Click', this.loadForm, this);
//        this.btnEdit.on('Click', this.loadEditForm, this);
//        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
//    loadForm: function () {
//        var dpp = this.dpp.getValue();
//        var vat = this.vat.getValue();
//        var dppph21 = this.dppph21.getValue();
//        var dppph23 = this.dppph23.getValue();
//        var dppph25 = this.dppph25.getValue();
//        var dppph26 = this.dppph26.getValue();
//        var dppph42 = this.dppph42.getValue();
//        var dppph22 = this.dppph22.getValue();
//        var beban_ppn = this.beban_ppn.getValue();
//        var beban_pph = this.beban_pph.getValue();
//        var other_tax = this.other_tax.getValue();
//        var biaya_materai_ekspedisi = this.biaya_materai_ekspedisi.getValue();
//        var forex_loss = this.forex_loss.getValue();
//        if (this.btnEdit.text == 'Save') {
//            var record = this.sm.getSelected();
//            record.set('dpp', dpp);
//            record.set('vat', vat);
//            record.set('dppph21', dppph21);
//            record.set('dppph23', dppph23);
//            record.set('dppph25', dppph25);
//            record.set('dppph26', dppph26);
//            record.set('dppph42', dppph42);
//            record.set('dppph22', dppph22);
//            record.set('beban_ppn', beban_ppn);
//            record.set('beban_pph', beban_pph);
//            record.set('other_tax', other_tax);
//            record.set('biaya_materai_ekspedisi', biaya_materai_ekspedisi);
//            record.set('forex_loss', forex_loss);
//            record.commit();
//        } else {
//            var c = this.store.recordType,
//                d = new c({
//                    dpp: dpp,
//                    vat: vat,
//                    dppph21: dppph21,
//                    dppph23: dppph23,
//                    dppph25: dppph25,
//                    dppph26: dppph26,
//                    dppph42: dppph42,
//                    dppph22: dppph22,
//                    beban_ppn: beban_ppn,
//                    beban_pph: beban_pph,
//                    other_tax: other_tax,
//                    biaya_materai_ekspedisi: biaya_materai_ekspedisi,
//                    forex_loss: forex_loss
//                });
//            this.store.add(d);
//        }
//        this.dpp.reset();
//        this.vat.reset();
//        this.dppph21.reset();
//        this.dppph23.reset();
//        this.dppph25.reset();
//        this.dppph26.reset();
//        this.dppph42.reset();
//        this.dppph22.reset();
//        this.beban_ppn.reset();
//        this.beban_pph.reset();
//        this.other_tax.reset();
//        this.biaya_materai_ekspedisi.reset();
//        this.forex_loss.reset();
//    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.dpp.setValue(record.data.dpp);
            this.vat.setValue(record.data.vat);
            this.dppph21.setValue(record.data.dppph21);
            this.dppph23.setValue(record.data.dppph23);
            this.dppph25.setValue(record.data.dppph25);
            this.dppph26.setValue(record.data.dppph26);
            this.dppph42.setValue(record.data.dppph42);
            this.dppph22.setValue(record.data.dppph22);
            this.beban_ppn.setValue(record.data.beban_ppn);
            this.beban_pph.setValue(record.data.beban_pph);
            this.other_tax.setValue(record.data.other_tax);
            this.biaya_materai_ekspedisi.setValue(record.data.biaya_materai_ekspedisi);
            this.forex_loss.setValue(record.data.forex_loss);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});

jun.PurchaseCoaKreditGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Kredit",
    id: 'docs-jun.PurchaseCoaKreditGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'PPh 21',
            sortable: true,
            resizable: true,
            dataIndex: 'pph_21',
            width: 50
        },
        {
            header: 'PPh 23',
            sortable: true,
            resizable: true,
            dataIndex: 'pph23',
            width: 50
        },
        {
            header: 'PPh 42',
            sortable: true,
            resizable: true,
            dataIndex: 'pph42',
            width: 50
        },
        {
            header: 'PPh 25/29',
            sortable: true,
            resizable: true,
            dataIndex: 'pph25_29',
            width: 50
        },
        {
            header: 'Forex Gain',
            sortable: true,
            resizable: true,
            dataIndex: 'forex_gain',
            width: 50
        },
        {
            header: 'Rounding',
            sortable: true,
            resizable: true,
            dataIndex: 'round',
            width: 50
        },
        {
            header: 'Total hutang',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store_kode',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztPurchaseCoaDetail;
        this.tbar = {xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 14,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        /*
                         * bawah
                         */
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'PPh 21 :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'pph_21id',
                            ref: '../../pph_21',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'PPh 23 :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'pph23id',
                            ref: '../../pph23',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'PPh 42 :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'pph42id',
                            ref: '../../pph42',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'PPh 25/29 :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'pph25_29id',
                            ref: '../../pph25_29',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Forex Gain :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'forex_gainid',
                            ref: '../../forex_gain',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Rounding :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'roundid',
                            ref: '../../round',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Total Hutang :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'totalid',
                            ref: '../../total',
                            style: 'margin-bottom:2px',
                            width: 100,
                            value: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Branch :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztStoreCmp,
                            hiddenName: 'store_kode',
                            valueField: 'store_kode',
                            displayField: 'store_kode',
                            ref: '../../store_kode',
                            colspan: 4
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../note',
                            width: 480+50,
                            colspan: 8,
                            maxLength: 255
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.PurchaseCoaKreditGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        /*
         * Debit
         */
        var dpp = Ext.getCmp('dppidkredit').getValue();
        var vat = Ext.getCmp('vatidkredit').getValue();
        var dppph21 = Ext.getCmp('dppph21idkredit').getValue();
        var dppph23 = Ext.getCmp('dppph23idkredit').getValue();
        var dppph25 = Ext.getCmp('dppph25idkredit').getValue();
        var dppph26 = Ext.getCmp('dppph26idkredit').getValue();
        var dppph42 = Ext.getCmp('dppph42idkredit').getValue();
        var dppph22 = Ext.getCmp('dppph22idkredit').getValue();
        var beban_ppn = Ext.getCmp('beban_ppnidkredit').getValue();
        var beban_pph = Ext.getCmp('beban_pphidkredit').getValue();
        var other_tax = Ext.getCmp('other_taxidkredit').getValue();
        var biaya_materai_ekspedisi = Ext.getCmp('biaya_materai_ekspedisiidkredit').getValue();
        var forex_loss = Ext.getCmp('forex_lossidkredit').getValue();
        /*
         * Kredit
         */
        var pph_21 = this.pph_21.getValue();
        var pph23 = this.pph23.getValue();
        var pph42 = this.pph42.getValue();
        var pph25_29 = this.pph25_29.getValue();
        var forex_gain = this.forex_gain.getValue();
        var round = this.round.getValue();
        var total = this.total.getValue();
        var store_kode = this.store_kode.getValue();
        var note = this.note.getValue();
        
        if (store_kode == '' || store_kode == undefined) {
            Ext.Msg.alert('Failure', 'Branch tidak boleh kosong!');
            return;
        }
        
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            /*
             * Debit
             */
            record.set('dpp', dpp);
            record.set('vat', vat);
            record.set('dppph21', dppph21);
            record.set('dppph23', dppph23);
            record.set('pph25_29', pph25_29);
            record.set('dppph26', dppph26);
            record.set('dppph42', dppph42);
            record.set('dppph22', dppph22);
            record.set('beban_ppn', beban_ppn);
            record.set('beban_pph', beban_pph);
            record.set('other_tax', other_tax);
            record.set('biaya_materai_ekspedisi', biaya_materai_ekspedisi);
            record.set('forex_loss', forex_loss);
            /*
             * Kredit
             */
            record.set('pph_21', pph_21);
            record.set('pph23', pph23);
            record.set('pph42', pph42);
            record.set('pph25_29', pph25_29);
            record.set('forex_gain', forex_gain);
            record.set('round', round);
            record.set('total', total);
            record.set('store_kode', store_kode);
            record.set('note', note);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    /*
                     * Debit
                     */
                    dpp: dpp,
                    vat: vat,
                    dppph21: dppph21,
                    dppph23: dppph23,
                    dppph25: dppph25,
                    dppph26: dppph26,
                    dppph42: dppph42,
                    dppph22: dppph22,
                    beban_ppn: beban_ppn,
                    beban_pph: beban_pph,
                    other_tax: other_tax,
                    biaya_materai_ekspedisi: biaya_materai_ekspedisi,
                    forex_loss: forex_loss,
                    /*
                     * Kredit
                     */
                    pph_21: pph_21,
                    pph23: pph23,
                    pph42: pph42,
                    pph25_29: pph25_29,
                    forex_gain: forex_gain,
                    round: round,
                    total: total,
                    store_kode: store_kode,
                    note: note
                });
            this.store.add(d);
        }
        /*
         * Debit
         */
        Ext.getCmp('dppidkredit').reset();
        Ext.getCmp('vatidkredit').reset();
        Ext.getCmp('dppph21idkredit').reset();
        Ext.getCmp('dppph23idkredit').reset();
        Ext.getCmp('dppph25idkredit').reset();
        Ext.getCmp('dppph26idkredit').reset();
        Ext.getCmp('dppph42idkredit').reset();
        Ext.getCmp('dppph22idkredit').reset();
        Ext.getCmp('beban_ppnidkredit').reset();
        Ext.getCmp('beban_pphidkredit').reset();
        Ext.getCmp('other_taxidkredit').reset();
        Ext.getCmp('biaya_materai_ekspedisiidkredit').reset();
        Ext.getCmp('forex_lossidkredit').reset();
        /*
         * Kedit
         */        
        this.pph_21.reset();
        this.pph23.reset();
        this.pph42.reset();
        this.pph25_29.reset();
        this.forex_gain.reset();
        this.round.reset();
        this.total.reset();
        this.store_kode.reset();
        this.note.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            /*
             * Debit
             */
            Ext.getCmp('dppidkredit').setValue(record.data.dpp);
            Ext.getCmp('vatidkredit').setValue(record.data.vat);
            Ext.getCmp('dppph21idkredit').setValue(record.data.dppph21);
            Ext.getCmp('dppph23idkredit').setValue(record.data.dppph23);
            Ext.getCmp('dppph25idkredit').setValue(record.data.dppph25);
            Ext.getCmp('dppph26idkredit').setValue(record.data.dppph26);
            Ext.getCmp('dppph42idkredit').setValue(record.data.dppph42);
            Ext.getCmp('dppph22idkredit').setValue(record.data.dppph22);
            Ext.getCmp('beban_ppnidkredit').setValue(record.data.beban_ppn);
            Ext.getCmp('beban_pphidkredit').setValue(record.data.beban_pph);
            Ext.getCmp('other_taxidkredit').setValue(record.data.other_tax);
            Ext.getCmp('biaya_materai_ekspedisiidkredit').setValue(record.data.biaya_materai_ekspedisi);
            Ext.getCmp('forex_lossidkredit').setValue(record.data.forex_loss);
            /*
             * Kredit
             */
            this.pph_21.setValue(record.data.pph_21);
            this.pph23.setValue(record.data.pph23);
            this.pph42.setValue(record.data.pph42);
            this.pph25_29.setValue(record.data.pph25_29);
            this.forex_gain.setValue(record.data.forex_gain);
            this.round.setValue(record.data.round);
            this.total.setValue(record.data.total);
            this.store_kode.setValue(record.data.store_kode);
            this.note.setValue(record.data.note);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});
