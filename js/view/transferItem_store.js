jun.TransferItemstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferItemstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferItemStoreId',
            url: 'TransferItem',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'transfer_item_id'},
                {name: 'tgl', type: 'date'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tdate', type: 'date'},
                {name: 'doc_ref_other'},
                {name: 'user_id'},
                {name: 'type_', type: 'int'},
                {name: 'store'},
                {name: 'supplier_id'},
                
                {name: 'sub_total', type: 'float'},
                {name: 'disc', type: 'float'},
                {name: 'total_disc_rp', type: 'float'},
                {name: 'total_dpp', type: 'float'},
                {name: 'ppn'},
                {name: 'tax_rp', type: 'float'},
                {name: 'total_pph_rp', type: 'float'},
                {name: 'total', type: 'float'},
                
                {name: 'tgl_jatuh_tempo', type: 'date'},
                {name: 'terima_barang_id'},
                {name: 'no_fakturpajak'},
                {name: 'status', type: 'int'},
                
                {name: 'closed_id_user'},
                {name: 'closed_tdate', type: 'date'},

                {name: 'doc_ref_po'},
                {name: 'no_sj'}
            ]
        }, cfg));
    }
});
jun.rztTransferItem = new jun.TransferItemstore();
jun.rztReturnTransferItem = new jun.TransferItemstore({baseParams: {type_: 1}});
