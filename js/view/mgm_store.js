jun.Mgmstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Mgmstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MgmStoreId',
            url: 'Mgm',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'mgm_id'},
                {name: 'customer_new_id'},
                {name: 'customer_friend_id'},
                {name: 'salestrans_id'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'up'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'store'},
                {name: 'customer_new_name'},
                {name: 'customer_friend_name'},
                {name: 'sales_doc_ref'}
            ]
        }, cfg));
    }
});
jun.rztMgm = new jun.Mgmstore();
//jun.rztMgm.load();
