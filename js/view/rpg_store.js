jun.Rpgstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Rpgstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'RpgStoreId',
            url: 'Rpg',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'rpg_id'},
                {name: 'doc_ref'},
                {name: 'salestrans_id'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'tgl'},
                {name: 'amount'},
                {name: 'note'},
                {name: 'bank_id'},
                {name: 'account_code'},
                {name: 'lunas'},
                {name: 'parent_id'},
                {name: 'tipe'},
                {name: 'store'},
                {name: 'nama_customer'},
                {name: 'ulpt_doc_ref'},
                {name: 'ulpt_salestrans_id'},
                {name: 'customer_id'}
            ]
        }, cfg));
    }
});
jun.rztRpg = new jun.Rpgstore();
