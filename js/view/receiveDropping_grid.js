jun.ReceiveDroppingGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Transfer Receive",
    id: 'docs-jun.ReceiveDroppingGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 50,
            renderer : Ext.util.Format.dateRenderer('d-M-Y'),
            filter: {xtype: "datefield", format: "d-M-Y"}
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Pengirim',
            sortable: true,
            resizable: true,
            dataIndex: 'store_pengirim',
            width: 50,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'lunas',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case PR_OPEN :
                        return 'OPEN';
                    case PR_CLOSED :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'CLOSE';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'OPEN'], [1, 'PROCESS'], [1, 'CLOSE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Up',
            sortable: true,
            resizable: true,
            dataIndex: 'up',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case DR_PENDING :
                        metaData.style += "background-color: #FF1A1A;";
                        return 'PENDING';
                    case DR_SEND :
                        metaData.style += "background-color: #FFFF33;";
                        return 'SEND';
                    case DR_APPROVE :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'APPROVE';
                    case DR_PROCESS :
                        metaData.style += "background-color: #00FF00;";
                        return 'PROCESS';
                    case DR_RECEIVE :
                        metaData.style += "background-color: #FF4D88;";
                        return 'RECEIVE';
                    case DR_CLOSE :
                        metaData.style += "background-color: #ADAD85;";
                        return 'CLOSE';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'PENDING'], [1, 'SEND'], [2, 'APPROVE'], [3, 'PROCESS'], [4, 'RECEIVE'], [5, 'CLOSE']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        }
    ],
    initComponent: function () {
        this.store = jun.rztReceiveDropping;
        if(jun.rztBarangNonJasaAll.getTotalCount()==0) jun.rztBarangNonJasaAll.load();
        if(jun.rztBarangLib.getTotalCount()==0) jun.rztBarangLib.load();
        
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Create Transfer Receive',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Transfer Receive',
                    ref: '../btnEdit'
                },
                { xtype: 'tbseparator'},
                {
                    xtype: 'button',
                    text: 'Resend',
                    ref: '../btnResend',
                    iconCls: 'silk13-arrow_refresh'
                },
                { xtype: 'tbseparator'},
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnClose',
                    iconCls: 'silk13-flag_green'
                },
                { xtype: 'tbseparator'},
                {
                    xtype: 'button',
                    text: 'History',
                    ref: '../btnHistory',
                    iconCls: 'silk13-clock'
                },
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.ReceiveDroppingGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnResend.on('Click', this.loadResend, this);
        this.btnClose.on('Click', this.btnCloseOnClick, this);
        this.btnHistory.on('Click', this.loadHistory, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        
        var filter = r.get('lunas') == PR_OPEN && r.get('store') == STORE;
        this.btnEdit.setText(filter ?"Edit Transfer Receive":"Show Transfer Receive");
        this.btnClose.setDisabled(!filter);
    },
    loadForm: function () {
        var form = new jun.ReceiveDroppingWin({
            modez: 0,
            title: 'Create Transfer Receive'
        });
        form.show();
    },
    loadHistory: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Transfer Receive");
            return;
        }

        var idz = selectedz.json.receive_dropping_id;

        var form = new jun.DroppingHistoryWin({
            modez: 0,
            historyid: {receive_dropping_id :idz},
            title: 'History Dropping'
        });
        form.show();
    },
    loadResend: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Transfer Receive");
            return;
        }

        var idz = selectedz.json.receive_dropping_id;
        /*var up = selectedz.json.up;
        if(up >= DR_RECEIVE )
        {
            Ext.MessageBox.alert("Warning", "Transfer Receive ini telah di terkirim.");
            return;
        }*/
        var cab = selectedz.json.store_pengirim;

        if(cab == STORE)
        {
            Ext.MessageBox.alert("Warning", "Anda sebagai pengirim tidak dapat melakukan resend.");
            return;
        }

        Ext.Ajax.request({
            url: 'ReceiveDropping/Resend/',
            method: 'POST',
            params: {
                id: idz
            },
            scope: this,
            success: function (f, a) {
                this.store.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Transfer Receive");
            return;
        }
        
        jun.rztDroppingCmp.load({
            params: { dropping_id: this.record.get('dropping_id'), query: '' },
            callback: function(r){
                var filter = this.record.get('store') == STORE && parseInt(this.record.get('lunas')) == PR_OPEN;
                var form = new jun.ReceiveDroppingWin({
                    modez: (filter ? 1 : 2),
                    id: this.record.get('receive_dropping_id'),
                    title: (filter ? "Edit" : "Show")+" Transfer Receive"
                });
                form.show(this);
                form.formz.getForm().loadRecord(this.record);
            },
            scope: this
        });
        jun.rztDroppingCmp.baseParams = {};
        
        jun.rztReceiveDroppingDetails.baseParams = {
            receive_dropping_id: this.record.get('receive_dropping_id')
        };
        jun.rztReceiveDroppingDetails.load();
        jun.rztReceiveDroppingDetails.baseParams = {};
    },
    btnCloseOnClick: function () {
        if (this.sm.getSelected() == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data Transfer Receive.");
            return;
        }
        if (this.record.get('store') != STORE){
            Ext.MessageBox.alert("Warning", "Anda tidak diperbolehkan untuk menutup data ini.");
            return;
        }
        if (this.record.get('lunas') != PR_OPEN){
            Ext.MessageBox.alert("Warning", "Data telah di-close.");
            return;
        }
        Ext.MessageBox.confirm('Close Transfer Receive', 'Apakah anda yakin ingin menutup data ini?', this.closeReceiveDropping, this);
    },
    closeReceiveDropping: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        Ext.Ajax.request({
            url: 'ReceiveDropping/Close',
            method: 'POST',
            params: {
                id: record.json.receive_dropping_id
            },
            scope: this,
            success: function (f, a) {
                this.store.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
