jun.PoDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PurchaseOrderDetails",
    id: 'docs-jun.PoDetailGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarang
        },
        {
            header: 'Description',
            sortable: true,
            resizable: true,
            dataIndex: 'description',
            width: 100
        },
        {
            header: 'Charge',
            sortable: true,
            resizable: true,
            dataIndex: 'charge',
            width: 100
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 75,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        //{
        //    header: 'Sat',
        //    sortable: true,
        //    resizable: true,
        //    dataIndex: 'sat',
        //    width: 100,
        //    renderer: jun.renderSatBarang
        //},
        {
            header: 'Price',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc (%)',
            sortable: true,
            resizable: true,
            dataIndex: 'disc',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'PPN (%)',
            sortable: true,
            resizable: true,
            dataIndex: 'ppn',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'PPH',
            sortable: true,
            resizable: true,
            dataIndex: 'pph_id',
            width: 100,
            align: "center",
            renderer: function(value, metaData, record, rowIndex){
                switch(value){
                    case 0 : return '-';
                    case 21 : return 'PPH 21';
                    case 22 : return 'PPH 22';
                    case 23 : return 'PPH 23';
                }
            }
        },
        {
            header: 'PPH (%)',
            sortable: true,
            resizable: true,
            dataIndex: 'pph',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        if(!this.readOnly){
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 14,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Barang :'
                            },
                            {
                                xtype: 'mfcombobox',
                                //xtype: 'combo',
                                style: 'margin-bottom:2px',
                                searchFields: [
                                    'kode_barang',
                                    'nama_barang'
                                ],
                                typeAhead: false,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                        '<span style="font-weight:bold;">{kode_barang}</span><br><span>{nama_barang}</span>',
                                        "</div></tpl>"),
                                forceSelection: true,
                                store: jun.rztBarangPurchasable,
                                //hiddenName: 'barang_id',
                                valueField: 'barang_id',
                                ref: '../../barang',
                                displayField: 'kode_barang',
                                listWidth: 300,
                                width: 175
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Charge :'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                ref: '../../charge',
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                store: jun.rztStoreCmp,
                                valueField: 'store_kode',
                                displayField: 'store_kode',
                                width: 100
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty :'
                            },
                            {
                                xtype: 'numericfield',
                                id: 'qtyid',
                                ref: '../../qty',
                                style: 'margin-bottom:2px',
                                width: 50,
                                value: 1,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Price :'
                            },
                            {
                                xtype: 'numberfield',
                                ref: '../../price',
                                id: 'docs-jun.PoDetailGrid.price',
                                width: 75,
                                value: 0,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Disc :'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../disc',
                                width: 40,
                                value: 0,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'PPN :'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../ppn',
                                width: 40,
                                value: 0,
                                minValue: 0
                            },
                            {
                                xtype: "combo",
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender:true,
                                editable:false,
                                mode: 'local',
                                store: new Ext.data.ArrayStore({
                                    id: 0,
                                    fields: ['pph_id','pph_name'],
                                    data: [[0, 'NO PPH'], [21, 'PPH 21'], [22, 'PPH 22'],  [23, 'PPH 23']]
                                }),
                                value: 0,
                                valueField: 'pph_id',
                                displayField: 'pph_name',
                                ref: '../../pph_id',
                                style: { marginLeft: '3px' },
                                width: 75
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../pph',
                                width: 40,
                                value: 0,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Description :'
                            },
                            {
                                xtype: 'textfield',
                                ref: '../../note',
                                width: 801,
                                colspan: 13,
                                maxLength: 255
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        id: 'btnsalesdetilid',
                        defaults: {
                            scale: 'large'
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Add',
                                height: 44,
                                ref: '../../btnAdd'
                            },
                            {
                                xtype: 'button',
                                text: 'Edit',
                                height: 44,
                                ref: '../../btnEdit'
                            },
                            {
                                xtype: 'button',
                                text: 'Del',
                                height: 44,
                                ref: '../../btnDelete'
                            }
                        ]
                    }
                ]
            };
        }
        jun.PoDetailGrid.superclass.initComponent.call(this);
        if(!this.readOnly){
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.onClickbtnEdit, this);
            this.btnDelete.on('Click', this.deleteRec, this);
            this.barang.on('select', this.onChangeBarang, this);
            this.on('sortchange', this.onShortChange, this);
            
            this.historyHarga = new Ext.ToolTip({
                autoHide: false,
                title: 'History harga',
                items: new jun.HistoryHargaDetailsGrid(),
                width: 440
            });
            this.price.on('focus', this.showHistory, this);
            this.price.on('blur', this.hideHistory, this);
        }
        this.getSelectionModel().on('rowselect', this.getrow, this);
        
        this.store.on('add', this.onStoreChange, this);
        this.store.on('update', this.onStoreChange, this);
        this.store.on('remove', this.onStoreChange, this);
    },
    onStoreChange: function (s, b, d) {
        s.refreshData();
    },
    onShortChange: function (g, i) {
        this.store.refreshSortData();
    },
    onChangeBarang: function (c, r, i) {
        jun.rztHistoryHargaDetails.reload({
            params: {
                barang_id: this.barang.getValue()
            }
        });
    },
    showHistory: function () { console.log('tes');
        this.historyHarga.showBy('docs-jun.PoDetailGrid.price');
    },
    hideHistory: function () {
        this.historyHarga.hide();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    resetForm: function(){
        this.barang.reset();
        this.charge.reset();
        this.note.reset();
        this.disc.reset();
        this.qty.reset();
        this.ppn.reset();
        this.pph_id.reset();
        this.pph.reset();
        this.price.reset();
        jun.rztHistoryHargaDetails.removeAll();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        var charge = this.charge.getValue();
        var note = this.note.getValue();
        var qty = parseFloat(this.qty.getValue());
        var price = parseFloat(this.price.getValue());
        var disc = parseFloat(this.disc.getValue());
        var ppn = parseFloat(this.ppn.getValue());
        var pph_id = this.pph_id.getValue();
        var pph = parseFloat(this.pph.getValue());

        var result = jun.calculateItemInvoice(qty, price, disc, ppn, pph_id, pph);
        var sub_total = result['bruto'];
        var disc_rp = result['disc_rp'];
        var total_dpp = result['dpp'];
        var ppn_rp = result['ppn_rp'];
        var pph_rp = result['pph_rp'];
        var total = result['total'];

        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('description', note);
            record.set('charge', charge);
            record.set('qty', qty);
            record.set('price', price);
            record.set('sub_total', sub_total);
            record.set('total_dpp', total_dpp);
            record.set('disc', disc);
            record.set('disc_rp', disc_rp);
            record.set('ppn', ppn);
            record.set('ppn_rp', ppn_rp);
            record.set('pph_id', pph_id);
            record.set('pph', pph);
            record.set('pph_rp', pph_rp);
            record.set('total', total);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    barang_id: barang_id,
                    description: note,
                    charge: charge,
                    qty: qty,
                    price: price,
                    sub_total: sub_total,
                    total_dpp: total_dpp,
                    disc: disc,
                    disc_rp: disc_rp,
                    ppn: ppn,
                    ppn_rp: ppn_rp,
                    pph_id: pph_id,
                    pph: pph,
                    pph_rp: pph_rp,
                    total: total
                });
            this.store.add(d);
        }
        
        this.resetForm();
        return true;
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            jun.rztHistoryHargaDetails.reload({
                params: {
                    barang_id: record.data.barang_id
                }
            });
            this.barang.setValue(record.data.barang_id);
            this.charge.setValue(record.data.charge);
            this.qty.setValue(record.data.qty);
            this.price.setValue(record.data.price);
            this.disc.setValue(record.data.disc);
            this.ppn.setValue(record.data.ppn);
            this.pph_id.setValue(record.data.pph_id);
            this.pph.setValue(record.data.pph);
            this.note.setValue(record.data.description);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});

jun.HistoryHargaDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Tanggal',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 80,
            renderer: Ext.util.Format.dateRenderer('d M Y')
        },
        {
            header: 'Supplier',
            sortable: true,
            resizable: true,
            dataIndex: 'supplier_name',
            width: 220
        },
        {
            header: 'Price',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztHistoryHargaDetails;
        jun.HistoryHargaDetailsGrid.superclass.initComponent.call(this);
    }
});

jun.calculateItemInvoice = function(qty, price, disc_persen, ppn_persen, pph_id, pph_persen){
    var result = [];
    result['bruto'] = round(parseFloat(qty) * parseFloat(price),2);
    result['disc_rp'] = round(result['bruto'] * (parseFloat(disc_persen) / 100),2);
    result['dpp'] = result['bruto'] - result['disc_rp'];
    result['ppn_rp'] = round(result['dpp'] * (parseFloat(ppn_persen) / 100),2);

    var pph_rp = 0;
    switch(pph_id){
        case 21:
        case 23:
            /*
                PPH 21, PPH 23
                persentase dari DPP
            */
            pph_rp = -round(result['dpp'] * (parseFloat(pph_persen) / 100),2);
            break;
        case 22 :
            /*
                PPH 22
                persentase dari PPN (bukan dari DPP+PPN).
                by Pak Gelung 15 November 2017
            */
            pph_rp = round(result['ppn_rp'] * (parseFloat(pph_persen) / 100),2);
            break;
    }
    result['pph_rp'] = Math.abs(pph_rp);

    result['total'] = result['dpp'] + result['ppn_rp'] + pph_rp;
    return result;
};