jun.AssetGroupstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetGroupstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetGroupStoreId',
            url: 'asset/AssetGroup',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'asset_group_id'},
                {name:'tariff'},
                {name:'period'},
                {name:'desc'},
                {name:'golongan'},
            ]
        }, cfg));
    }
});
jun.rztAssetGroup = new jun.AssetGroupstore();
//jun.rztAssetGroup.load();
