jun.PaketBarangCmpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PaketBarangCmpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PaketBarangCmpStoreId',
            url: 'PaketBarangCmp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'ket'},
                {name: 'grup_id'},
                {name: 'active'},
                {name: 'sat'},
                {name: 'up'},
                {name: 'tipe_barang_id'},
                {name: 'paket_details_id'},
                {name: 'disc'}
            ]
        }, cfg));
    }
});
jun.rztBarangPaketTransCmp = new jun.PaketBarangCmpstore();
//jun.rztPaketBarangCmp.load();
