jun.TransaksiOverrideRealisasiGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"Sales Realizations",
        id:'docs-jun.TransaksiOverrideRealisasiGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
        {
            header:'Doc.Ref',
            sortable:true,
            resizable:true,
            dataIndex:'docref',
            width:100
        },
        {
            header:'Transaction Name',
            sortable:true,
            resizable:true,
            dataIndex:'name',
            width:100
        },
        {
            header:'Amount',
            sortable:true,
            resizable:true,
            dataIndex:'amount',
            width:100
        },
        {
            header:'Description',
            sortable:true,
            resizable:true,
            dataIndex:'description',
            width:100
        },
        {
            header:'Date',
            sortable:true,
            resizable:true,
            dataIndex:'tdate',
            width:100
        },
        /*                {
			header:'transaksi_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'transaksi_id',
			width:100
		},
                                {
			header:'businessunit_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'businessunit_id',
			width:100
		},
                                {
			header:'category_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'category_id',
			width:100
		},
                                {
			header:'docref',
			sortable:true,
			resizable:true,                        
            dataIndex:'docref',
			width:100
		},
                                {
			header:'name',
			sortable:true,
			resizable:true,                        
            dataIndex:'name',
			width:100
		},
                                {
			header:'amount',
			sortable:true,
			resizable:true,                        
            dataIndex:'amount',
			width:100
		},*/
                		/*
                {
			header:'description',
			sortable:true,
			resizable:true,                        
            dataIndex:'description',
			width:100
		},
                                {
			header:'tdate',
			sortable:true,
			resizable:true,                        
            dataIndex:'tdate',
			width:100
		},
                                {
			header:'flag',
			sortable:true,
			resizable:true,                        
            dataIndex:'flag',
			width:100
		},
                                {
			header:'created_at',
			sortable:true,
			resizable:true,                        
            dataIndex:'created_at',
			width:100
		},
                                {
			header:'uploadfile',
			sortable:true,
			resizable:true,                        
            dataIndex:'uploadfile',
			width:100
		},
                                {
			header:'uploaddate',
			sortable:true,
			resizable:true,                        
            dataIndex:'uploaddate',
			width:100
		},
                		*/
		
	],
	initComponent: function(){
	this.store = jun.rztTransaksiOverrideRealisasi;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Edit',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Delete',
                        ref: '../btnDelete'
                    },
                    /*,
                {
                    xtype: 'button',
                    text: 'Show Details',
                    ref: '../btnShow'
                }*/
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		jun.TransaksiOverrideRealisasiGrid.superclass.initComponent.call(this);
	        this.btnAdd.on('Click', this.loadForm, this);
                this.btnEdit.on('Click', this.loadEditForm, this);
                this.btnDelete.on('Click', this.deleteRec, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},
        
        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
        },
        
        loadForm: function(){
            var form = new jun.TransaksiOverrideRealisasiWin({modez:0});
            form.show();
        },
        
        loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();
            
            //var dodol = this.store.getAt(0);
             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.transaksi_id;
            var form = new jun.TransaksiOverrideRealisasiWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'projection/TransaksiOverrideRealisasi/delete/id/' + record.json.transaksi_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztTransaksiOverrideRealisasi.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });
        
        }
})
