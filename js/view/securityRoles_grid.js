jun.SecurityRolesGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Security Roles",
    id: 'docs-jun.SecurityRolesGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Role Name',
            sortable: true,
            resizable: true,
            dataIndex: 'role',
            width: 100
        },
        {
            header: 'Description',
            sortable: true,
            resizable: true,
            dataIndex: 'ket',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztSecurityRoles;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Security Role',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Security Role',
                    ref: '../btnEdit'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.SecurityRolesGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
//        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SecurityRolesWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a security role");
            return;
        }
        var idz = selectedz.json.security_roles_id;
        var form = new jun.SecurityRolesWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'SecurityRoles/delete/id/' + record.json.security_roles_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSecurityRoles.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.SettingsClientsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Setting Client",
    id: 'docs-jun.SettingsClientsGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    // plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Setting Name',
            sortable: true,
            resizable: true,
            dataIndex: 'name_',
            width: 100
        },
        {
            header: 'Value',
            sortable: true,
            resizable: true,
            dataIndex: 'value_',
            width: 100
        }
    ],
    initComponent: function () {
        var data = {'data': []};
        if (localStorage.getItem("settingClient") == null) {
            localStorage.setItem("settingClient", JSON.stringify(data));
        }
        var data_ = Ext.decode(localStorage.getItem("settingClient"));
        if (data_ == "") {
            data_ = {'data': []};
        }
        jun.rztSettingClients.loadData(data_);
        this.store = jun.rztSettingClients;
        // this.store = jun.rztSecurityRoles;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Setting Client',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Setting Client',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Delete Setting Client',
                    ref: '../btnDelete'
                }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.SettingsClientsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SettingClientsWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a security role");
            return;
        }
        var form = new jun.SettingClientsWin({modez: 1, r: this.record});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
        var data = {};
        var arr = [];
        jun.rztSettingClients.each(function (rec) {
                arr.push(rec.data);
            }
        );
        data.data = arr;
        // Lockr.set('settingClient', data);
        localStorage.setItem("settingClient", JSON.stringify(data));
    }
});
