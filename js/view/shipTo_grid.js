jun.ShipToGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "ShipTo",
    id: 'docs-jun.ShipToGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Attn. Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Company Name',
            sortable: true,
            resizable: true,
            dataIndex: 'ship_to_company',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Address',
            sortable: true,
            resizable: true,
            dataIndex: 'ship_to_address',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Company City',
            sortable: true,
            resizable: true,
            dataIndex: 'ship_to_city',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Company Country',
            sortable: true,
            resizable: true,
            dataIndex: 'ship_to_country',
            width: 100,
            filter: {xtype: "textfield"}
        }
                /*
                 {
                 header:'ship_to_phone',
                 sortable:true,
                 resizable:true,                        
                 dataIndex:'ship_to_phone',
                 width:100
                 },
                 {
                 header:'ship_to_fax',
                 sortable:true,
                 resizable:true,                        
                 dataIndex:'ship_to_fax',
                 width:100
                 },
                 {
                 header:'up',
                 sortable:true,
                 resizable:true,                        
                 dataIndex:'up',
                 width:100
                 },
                 */

    ],
    initComponent: function () {
        this.store = jun.rztShipTo;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    iconCls: 'silk13-pencil',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.ShipToGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },

    getrow: function (sm, idx, r) {
        this.record = r;

        var selectedz = this.sm.getSelections();
    },

    loadForm: function () {
        var form = new jun.ShipToWin({modez: 0});
        form.show();
    },

    loadEditForm: function () {

        var selectedz = this.sm.getSelected();

        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.ship_to_id;
        var form = new jun.ShipToWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },

    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },

    deleteRecYes: function (btn) {

        if (btn == 'no') {
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }

        Ext.Ajax.request({
            url: 'ShipTo/delete/id/' + record.json.ship_to_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztShipTo.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
})
