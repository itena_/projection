jun.TransaksiOverrideRealisasistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.TransaksiOverrideRealisasistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransaksiOverrideRealisasiStoreId',
            url: 'projection/TransaksiOverrideRealisasi',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'transaksi_id'},
                {name:'businessunit_id'},
                {name:'category_id'},
                {name:'docref'},
                {name:'name'},
                {name:'amount'},
                {name:'description'},
                {name:'tdate'},
                {name:'flag'},
                {name:'created_at'},
                {name:'uploadfile'},
                {name:'uploaddate'},
                
            ]
        }, cfg));
    }
});
jun.rztTransaksiOverrideRealisasi = new jun.TransaksiOverrideRealisasistore();
//jun.rztTransaksiOverrideRealisasi.load();
