jun.TransaksiOverrideRealisasiWin = Ext.extend(Ext.Window, {
    title: 'Sales Realizations',
    modez:1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        if (jun.rztTransaksiCategory.getTotalCount() === 0) {
            jun.rztTransaksiCategory.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-TransaksiOverrideRealisasi',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'textfield',
                        name: 'docref',
                        ref: '../docref',
                        fieldLabel: 'Doc.Ref',
                        readOnly: true,
                        maxLength: 50,
                        width: 200,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        name: 'name',
                        ref: '../name',
                        fieldLabel: 'Name',
                        //readOnly: true,
                        maxLength: 50,
                        width: 200,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Sales Categories',
                        store: jun.rztTransaksiCategory,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{category_code} - {category_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'category_id',
                        valueField: 'category_id',
                        ref:'../category_id',
                        displayField: 'category_code',
                        emptyText: "Category",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Amount',
                        hideLabel:false,
                        //hidden:true,
                        name:'amount',
                        id:'amountid',
                        ref:'../amount',
                        maxLength: 50,
                        allowBlank: false,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Description',
                        hideLabel:false,
                        //hidden:true,
                        name:'description',
                        id:'descriptionid',
                        ref:'../description',
                        allowBlank: false,
                        anchor: '100%'
                        //allowBlank: 1
                    },
                    {
                        xtype: 'xdatefield',
                        ref:'../tdate',
                        fieldLabel: 'Date',
                        name:'tdate',
                        id:'tdateid',
                        format: 'M Y',
                        allowBlank: false,
                        //allowBlank: 1,
                        anchor: '100%'
                    }
                                /*                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'businessunit_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'businessunit_id',
                                    id:'businessunit_idid',
                                    ref:'../businessunit_id',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'category_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'category_id',
                                    id:'category_idid',
                                    ref:'../category_id',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'docref',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'docref',
                                    id:'docrefid',
                                    ref:'../docref',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'name',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'name',
                                    id:'nameid',
                                    ref:'../name',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'amount',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'amount',
                                    id:'amountid',
                                    ref:'../amount',
                                    maxLength: ,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'description',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'description',
                                    id:'descriptionid',
                                    ref:'../description',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../tdate',
                            fieldLabel: 'tdate',
                            name:'tdate',
                            id:'tdateid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'flag',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'flag',
                                    id:'flagid',
                                    ref:'../flag',
                                    maxLength: 1,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../created_at',
                            fieldLabel: 'created_at',
                            name:'created_at',
                            id:'created_atid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'uploadfile',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'uploadfile',
                                    id:'uploadfileid',
                                    ref:'../uploadfile',
                                    maxLength: 100,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../uploaddate',
                            fieldLabel: 'uploaddate',
                            name:'uploaddate',
                            id:'uploaddateid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        }, */
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.TransaksiOverrideRealisasiWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        }
        else if(this.modez == 3)
        {
            this.btnSaveClose.setVisible(false);
            this.btnSave.setVisible(false);
        }
        else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            //var urlz;
/*            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'TransaksiOverrideRealisasi/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'TransaksiOverrideRealisasi/create/';
                }
             */
            var urlz= 'projection/TransaksiOverrideRealisasi/create/';
            Ext.getCmp('form-TransaksiOverrideRealisasi').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                params: {
                    mode: this.modez,
                    id: this.id,
                },
                success: function(f,a){
                    jun.rztTransaksiOverrideRealisasi.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-TransaksiOverrideRealisasi').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});