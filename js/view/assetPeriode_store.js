jun.AssetPeriodestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetPeriodestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetPeriodeStoreId',
            url: 'asset/AssetPeriode',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'asset_periode_id'},
                {name:'asset_trans_id'},
                {name:'asset_id'},
                {name:'asset_group_id'},
                {name:'barang_id'},
                {name:'ati'},
                {name:'docref'},
                {name:'docref_other'},
                {name:'asset_trans_name'},
                {name:'asset_trans_branch'},
                {name:'asset_trans_price'},
                {name:'asset_trans_new_price'},
                {name:'asset_trans_date'},
                {name:'description'},
                {name:'class'},
                {name:'period'},
                {name:'tariff'},
                {name:'tglpenyusutan'},
                {name:'penyusutanperbulan'},
                {name:'penyusutanpertahun'},
                {name:'balance'},
                {name:'status'},
                {name:'created_at'},
                {name:'updated_at'},
            ]
        }, cfg));
    }
});
jun.rztAssetPeriode = new jun.AssetPeriodestore();
//jun.rztAssetPeriodeBy = new jun.AssetPeriodestore().findBy();
//jun.rztAssetPeriode.load();
