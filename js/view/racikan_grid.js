jun.RacikanGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Racikan",
    id: 'docs-jun.RacikanGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarang
        },
        {
            header: 'Quantity',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 50,
            align: "right"
//            renderer: function (v, m, r) {
//                return Ext.util.Format.number(jun.konversiSatuanDitampilkan(v, r.get('satuan_id')), '0,0.00');
//            }
        },
        {
            header: 'Satuan',
            sortable: true,
            resizable: true,
            dataIndex: 'satuan',
            width: 50
//            renderer: jun.renderSatuan
        },
        {
            header: 'Tanggal Pembuatan Formula',
            sortable: true,
            resizable: true,
            dataIndex: 'tdate',
            width: 100
        }
//        {
//            header: 'Pembuat',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'user_id',
//            width: 100
//        }

    ],
    initComponent: function () {
//        if (jun.rztSatuan.getTotalCount() === 0) {
//            jun.rztSatuan.load();
//        }
        this.store = jun.rztRacikan;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.RacikanGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    getrow: function (sm, idx, r) {
        this.record = r;

        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        jun.rztRacikanDetails.removeAll();
        var form = new jun.RacikanWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {

        var selectedz = this.sm.getSelected();

        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.racikan_id;
        var form = new jun.RacikanWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
//        var sat = form.satuan.getValue();
//        var qty = jun.konversiSatuanDitampilkan(parseFloat(form.qty.getValue()), sat);  
//        form.qty.setValue(qty);
        jun.rztRacikanDetails.baseParams = {
            racikan_id: idz
        };
        jun.rztRacikanDetails.load();
        jun.rztRacikanDetails.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {

        if (btn == 'no') {
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }

        Ext.Ajax.request({
            url: 'Racikan/delete/id/' + record.json.racikan_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztRacikan.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
})
