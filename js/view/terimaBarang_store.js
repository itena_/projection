jun.TerimaBarangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.TerimaBarangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TerimaBarangStoreId',
            url: 'TerimaBarang',           
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name:'terima_barang_id'},
                {name:'po_id'},
                {name:'doc_ref'},
                {name:'id_user'},
                {name:'tdate', type:'date'},
                {name:'tgl', type:'date'},
                {name:'store'},
                {name:'no_sj'},
                {name:'tgl_sj', type:'date'},
                {name:'note'},
                {name:'status', type: 'int'},
                {name:'lock_edit', type: 'int'},
                {name:'up'},

                {name:'doc_ref_po'}
            ]
        }, cfg));
    }
});
jun.rztCreateTerimaBarang = new jun.TerimaBarangstore();
jun.rztTerimaBarang = new jun.TerimaBarangstore();
