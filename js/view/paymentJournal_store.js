jun.PaymentJournalstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.PaymentJournalstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PaymentJournalStoreId',
            url: 'PaymentJournal',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'payment_journal_id'},
                {name:'doc_ref'},
                {name:'doc_ref_other'},
                {name:'tgl'},
                {name:'tdate'},
                {name:'tgl_jatuh_tempo'},
                {name:'note'},
                {name:'user_id'},
                {name:'customer_id'},
                {name:'total'},
                {name:'lunas'},
                {name:'store_kode'},
                {name:'p'},
                {name:'up'},
                {name:'editable'}
            ]
        }, cfg));
    }
});
jun.rztPaymentJournal = new jun.PaymentJournalstore();
//jun.rztAccountPayable.load();
