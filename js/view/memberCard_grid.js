jun.MemberCardGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"Member Card",
        id:'docs-jun.MemberCardGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
                                {
			header:'No Customer',
			sortable:true,
			resizable:true,                        
            dataIndex:'no_card',
			width:70,
            filter: {xtype: "textfield"}
		},
        {
            header:'Branch',
            sortable:true,
            resizable:true,
            dataIndex:'store',
            width:50
        },
        {
            header:'Name',
            sortable:true,
            resizable:true,
            dataIndex:'name',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Category',
            sortable:true,
            resizable:true,
            dataIndex:'category',
            width:50,
            filter: {xtype: "textfield"}
        },
        {
            header:'Type',
            sortable:true,
            resizable:true,
            dataIndex:'type',
            width:50,
            filter: {xtype: "textfield"}
        },
                                {
			header:'Since',
			sortable:true,
			resizable:true,                        
            dataIndex:'since',
			width:60,
                                    filter: {
                                        xtype: "xdatefield",
                                        format: 'd/m/Y'
                                    }
		},
                                {
			header:'Valid',
			sortable:true,
			resizable:true,                        
            dataIndex:'valid',
			width:60,
                                    filter: {
                                        xtype: "xdatefield",
                                        format: 'd/m/Y'
                                    }
		},
        {
            header:'Active',
            sortable:true,
            resizable:true,
            dataIndex:'active',
            width:50,
            filter: {xtype: "textfield"}
        },
        {
            header:'Count',
            sortable:true,
            resizable:true,
            dataIndex:'counter',
            width:50
        },
        {
            header:'Created',
            sortable:true,
            resizable:true,
            dataIndex:'created_at',
            width:100,
            filter: {
                xtype: "xdatefield",
                format: 'd/m/Y'
            }
        },
        {
            header:'Updated',
            sortable:true,
            resizable:true,
            dataIndex:'update_at',
            width:100,
            filter: {
                xtype: "xdatefield",
                format: 'd/m/Y'
            }
        },
	],
	initComponent: function(){
	this.store = jun.rztMemberCard;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Edit',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Delete',
                        ref: '../btnDelete'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Print Card',
                        ref: '../btnPrint'
                    }
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		jun.MemberCardGrid.superclass.initComponent.call(this);
	            this.btnAdd.on('Click', this.loadForm, this);
                this.btnEdit.on('Click', this.loadEditForm, this);
                this.btnDelete.on('Click', this.deleteRec, this);
                this.btnPrint.on('Click', this.Print, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},
        
        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
        },

        Print: function(){

            var selectedz = this.sm.getSelected();

            if(selectedz == undefined){
                Ext.MessageBox.alert("Warning","Anda belum memilih data");
                return;
            }
            var idz = selectedz.json.membercard_id;

            Ext.Ajax.request({
                url: 'MemberCard/ReprintCard/',
                method: 'POST',
                //timeOut: 1000,
                scope: this,
                params: {
                    id: idz,
                    mode: this.modez,
                },
                success: function (f, a) {

                    if (confirm("Are you sure you want to print the card ?"))
                    {
                        var printWindow = window.open('', '', 'height=0px ,width=0px');
                        var response = Ext.decode(f.responseText);

                        printWindow.document.write('<html>');
                        printWindow.document.write('<head>');

                        printWindow.document.write('<script src="js/barcode.js"></script>');
                        //printWindow.document.write('<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/barcodes/JsBarcode.code128.min.js"></script>');

                        printWindow.document.write('<link rel="stylesheet" type="text/css" href="'+STYLECARD+'">');

                        printWindow.document.write('</head>');
                        printWindow.document.write('<body>');

                        printWindow.document.write('<div class="mycard">');

                        printWindow.document.write('<div class="nama" style="">');
                        //printWindow.document.write('ARMAN LAAMA');
                        printWindow.document.write(response.MemberCard.name);
                        printWindow.document.write('</div>');

                        //printWindow.document.write('<svg class="barcode" jsbarcode-format="code128" jsbarcode-value="123JOG02" jsbarcode-textmargin="0" jsbarcode-fontoptions="bold" jsbarcode-displayValue="false" jsbarcode-height= "30" jsbarcode-width= "2"></svg>');
                        if(PT_CARD == "ENA")
                        {
                            printWindow.document.write('<div class="nocustomer">');
                            printWindow.document.write(response.MemberCard.no_card);
                            //printWindow.document.write('2101100115');
                            printWindow.document.write('</div>');

                            printWindow.document.write('<svg class="barcode" jsbarcode-format="CODE128" jsbarcode-value="'+response.MemberCard.no_card + '"jsbarcode-margin="0" jsbarcode-height= "60" jsbarcode-width= "2" jsbarcode-textmargin="0" jsbarcode-fontoptions="" jsbarcode-displayValue="false" ></svg>');
                        }
                        else
                        {
                            printWindow.document.write('<div class="nocustomer">');
                            printWindow.document.write(response.MemberCard.no_card + response.MemberCard.store);
                            //printWindow.document.write('2101100115');
                            printWindow.document.write('</div>');

                            printWindow.document.write('<div class="since">');
                            printWindow.document.write(response.yawal);
                            printWindow.document.write('</div>');

                            printWindow.document.write('<div class="valid">');
                            printWindow.document.write(response.mawal+"/"+response.yakhir);
                            printWindow.document.write('</div>');
                            printWindow.document.write('<svg class="barcode" jsbarcode-format="CODE128" jsbarcode-value="'+response.MemberCard.no_card + response.MemberCard.store+'"jsbarcode-margin="0" jsbarcode-height= "30" jsbarcode-width= "2" jsbarcode-textmargin="0" jsbarcode-fontoptions="" jsbarcode-displayValue="false" ></svg>');
                        }
                        //printWindow.document.write('<svg class="barcode" jsbarcode-format="code128" jsbarcode-value="2101100115" jsbarcode-textmargin="0" jsbarcode-margin="0" jsbarcode-height= "60" jsbarcode-width= "2" jsbarcode-fontoptions="" jsbarcode-displayValue="false" ></svg>');

                        printWindow.document.write('</div>');
                        printWindow.document.write('</body>');
                        printWindow.document.write('<script>JsBarcode(".barcode").init();</script>');
                        printWindow.document.write('</html>');

                        setTimeout(function () {
                            printWindow.print();
                            printWindow.close();
                        }, 500);
                        //return false;

                        Ext.Ajax.request({
                            url: 'MemberCard/UpdateDataCard/',
                            method: 'POST',
                            scope: this,
                            params: {
                                id: idz,
                                mode: this.modez,

                            },
                            success: function (data) {}
                        });

                    } else {
                        alert('Print Card Cancelled.', 'Print Card Cancelled.');
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        },
        
        loadForm: function(){
            var form = new jun.MemberCardWin({modez:0});
            form.show();
        },
        
        loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();

             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Data");
                 return;
             }
            var idz = selectedz.json.membercard_id;
            var form = new jun.MemberCardWin({modez:3, memberid:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'MemberCard/delete/id/' + record.json.membercard_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztMemberCard.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });
        
        }
})
