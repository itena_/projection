jun.SouvenirTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SouvenirTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SouvenirTransStoreId',
            url: 'SouvenirTrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'souvenir_trans_id'},
                {name: 'type_no'},
                {name: 'trans_no'},
                {name: 'ref'},
                {name: 'tgl'},
                {name: 'qty'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'up'},
                {name: 'souvenir_id'}
            ]
        }, cfg));
    }
});
jun.rztSouvenirTrans = new jun.SouvenirTransstore();
//jun.rztSouvenirTrans.load();
