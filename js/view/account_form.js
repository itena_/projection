jun.AccountWin = Ext.extend(Ext.Window, {
    title: 'Account',
    modez:1,
    width: 400,
    height: 230,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        if (jun.rztGroup.getTotalCount() === 0) {
            jun.rztGroup.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-Account',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'Code',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'account_code',
                                    id:'account_codeid',
                                    ref:'../account_code',
                                    maxLength: 15,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'Name',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'account_name',
                                    id:'account_nameid',
                                    ref:'../account_name',
                                    maxLength: 100,
                                    //allowBlank: ,
                                    anchor: '100%'
                                },


                                /*                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'status',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'status',
                                    id:'statusid',
                                    ref:'../status',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, */
                                                                     {
                                    xtype: 'textarea',
                                    fieldLabel: 'Description',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'description',
                                    id:'descriptionid',
                                    ref:'../description',
                                    maxLength: 200,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGroup,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{group_code} - {group_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'group_id',
                        valueField: 'group_id',
                        ref:'../group_id',
                        displayField: 'group_name',
                        emptyText: "Group",
                        anchor: '100%'
                    }
                                 /*                                    {
                                    xtype: 'textfield',
                                    fieldLabel: 'norek',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'norek',
                                    id:'norekid',
                                    ref:'../norek',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                },
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'businessunit_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'businessunit_id',
                                    id:'businessunit_idid',
                                    ref:'../businessunit_id',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                },
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'group_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'group_id',
                                    id:'group_idid',
                                    ref:'../group_id',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                },
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'created_at',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'created_at',
                                    id:'created_atid',
                                    ref:'../created_at',
                                    maxLength: 6,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'uploadfile',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'uploadfile',
                                    id:'uploadfileid',
                                    ref:'../uploadfile',
                                    maxLength: 100,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'uploaddate',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'uploaddate',
                                    id:'uploaddateid',
                                    ref:'../uploaddate',
                                    maxLength: 6,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, */
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AccountWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz= 'projection/Account/create/';
            /*if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'Account/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'Account/create/';
                }*/
             
            Ext.getCmp('form-Account').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                params: {
                    id: this.id,
                    mode: this.modez
                },
                success: function(f,a){
                    jun.rztAccount.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });

                    if(this.modez == 0){
                        Ext.getCmp('form-Account').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});