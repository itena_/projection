jun.BarangClinicalstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BarangClinicalstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangClinicalStoreId',
            url: 'BarangClinical',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_clinical'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'sat'},
                {name: 'kategori_clinical_id'}
            ]
        }, cfg));
    }
});
jun.rztBarangClinical = new jun.BarangClinicalstore();
jun.rztBarangClinicalLib = new jun.BarangClinicalstore();
jun.rztBarangClinicalCmp = new jun.BarangClinicalstore();
jun.rztBarangClinicalLib.load();
