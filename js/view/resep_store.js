jun.Resepstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Resepstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ResepStoreId',
            url: 'Resep',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'resep_id'},
                {name: 'nama_resep'},
                {name: 'barang_id'},
                {name: 'up'}
            ]
        }, cfg));
    }
});
jun.rztResep = new jun.Resepstore();
//jun.rztResep.load();
