jun.BarangPerlengkapanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BarangPerlengkapanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangPerlengkapanStoreId',
            url: 'BarangPerlengkapan',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_perlengkapan_id'},
                {name: 'kode_brg_perlengkapan'},
                {name: 'nama_brg_perlengkapan'},
                {name: 'ket_brg_perlengkapan'},
                {name: 'satuan_perlengkapan'},
                {name: 'harga_perlengkapan'},
                {name: 'up'}

            ]
        }, cfg));
    }
});
jun.rztBarangPerlengkapan = new jun.BarangPerlengkapanstore();
jun.rztBarangPerlengkapanLib = new jun.BarangPerlengkapanstore();
//jun.rztBarangPerlengkapanCmp = new jun.BarangPerlengkapanstore();
//jun.rztBarangPerlengkapan.load();
