<?php
return array(
    'basePath' => dirname(__file__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Projection Analysis ',
    'theme' => 'extjs',
    'preload' => array('log'),
    'sourceLanguage' => 'xx',
    'language' => 'en',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.giix-components.*',
        'application.vendors.*'
    ),
    'modules' => array(
        'projection',
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'admin',
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array(
                'ext.giix-core',
            ),
        ),
    ),
    'components' => array(
        'CGridViewPlus' => array(
            'class' => 'components.CGridViewPlus',
        ),
        'user' => array(
            'loginUrl' => array('login'),
            'class' => 'WebUser',
            'allowAutoLogin' => true,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '<action:(contact|PosMode|login|logout|GetDateTime|ClearCache|dokter|perawatan|counter|serverantrian)>/*' => 'site/<action>',
                '<controller:\w+>/<id:[a-zA-Z0-9-]  >' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:[a-zA-Z0-9-]>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        /*'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=posnsc_cabang;port=3306',
            'emulatePrepare' => true,
            'tablePrefix' => 'nscc_',
            'username' => 'rCpp9uvJpztw',
            'password' => 'feZHrlnzcx+l7E0M',
            'charset' => 'utf8',
        ),*/
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=proyeksi;port=3306',
            'emulatePrepare' => true,
            'tablePrefix' => 'nscc_',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),
        'dbpusat' => array(
            'connectionString' => 'mysql:host=localhost;dbname=posnsc;port=3306',
            'emulatePrepare' => true,
            'tablePrefix' => 'nscc_',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'class' => 'CDbConnection'
        ),
        'dbnars' => array(
            'connectionString' => 'mysql:host=' . NARSDOMAIN . ';dbname=sync;port=3306',
            'attributes' => array(PDO::MYSQL_ATTR_COMPRESS => true),
            'emulatePrepare' => true,
            'username' => 'local',
            'password' => 'local',
            'charset' => 'utf8',
            'class' => 'CDbConnection'
        ),
        'BDG01' => array(
            'connectionString' => 'mysql:host=localhost;dbname=posng;port=3306',
            //'attributes' => array(PDO::MYSQL_ATTR_COMPRESS => true),
            'emulatePrepare' => true,
            'tablePrefix' => 'nscc_',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'class' => 'CDbConnection'
        ),
        'antrian' => array(
            'connectionString' => 'mysql:host=localhost;dbname=antrian_naava;port=3306',
            'attributes' => array(PDO::MYSQL_ATTR_COMPRESS => true),
            'tablePrefix' => '',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
            'class' => 'CDbConnection'
        ),
        /*'dbprojection' => array(
            'connectionString' => 'mysql:host=localhost;dbname=proyeksi;port=3306',
            'attributes' => array(PDO::MYSQL_ATTR_COMPRESS => true),
            'tablePrefix' => '',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'class' => 'CDbConnection'
        ),*/
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'session' => array(
            'sessionName' => 'posng',
            'class' => 'CDbHttpSession',
            'autoCreateSessionTable' => false,
            'connectionID' => 'db',
            'sessionTableName' => 'nscc_session',
            'useTransparentSessionID' => isset($_POST['PHPSESSID']) ? true : false,
            'autoStart' => 'false',
            'cookieMode' => 'only',
            'timeout' => 30000
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error',
                    'filter' => 'CLogFilter',
                    'maxFileSize' => 1024,
                    'maxLogFiles' => 10
                ),
            ),
        ),
//        'log' => array(
//            'class' => 'CLogRouter',
//            'routes' => array(
//                array(
//                    'class' => 'CPhpMailerLogRoute',
//                    'levels' => 'error',
//                    'emails' => 'nscc.error@gmail.com',
//                    'sentFrom' => 'nscc.error@gmail.com',
//                    'subject' => 'Error ' . STOREID . ' ' . date('dmYHis'),
//                    'config' => array(
//                        'SMTP',
//                        'Host' => "smtp.gmail.com",
//                        'SMTPAuth' => true, // enable SMTP authentication
//                        'SMTPSecure' => "tls", // sets the prefix to the server
//                        'Port' => 587,
//                        'Username' => "nscc.error@gmail.com", // GMAIL username
//                        'Password' => 'zaq!@#$%',
//                        'CharSet' => "UTF-8",
//                    )
//                ),
//            ),
//        ),
        'aes256' => array(
            'class' => 'application.extensions.aes256.Aes256',
            'privatekey_32bits_hexadecimal' => '9C9413969BD7524F4CD07FECDEE0ED185D1CB08A16DB4D56B14FF951A64E96AC',
        ),
        'Smtpmail' => array(
            'class' => 'application.extensions.smtpmail.PHPMailer',
            'Host' => "smtp.gmail.com",
            'Username' => 'nscc.sync@gmail.com',
            'Password' => 'zaq!@#$%',
            'Mailer' => 'smtp',
            'Port' => 587,
            'SMTPAuth' => true,
            'SMTPSecure' => 'tls',
        ),
    ),
    'params' => array(
        'Username' => 'nscc.sync@gmail.com',
        'Password' => 'zaq!@#$%',
        'system_title' => 'Point Of Sales Naavagreen',
        'report_title_pt' => 'NAAVAGREEN INDONESIA',
        'system_subtitle' => '',
        'phpass' => array(
            'iteration_count_log2' => 8,
            'portable_hashes' => false,
        ),
    ),
);
