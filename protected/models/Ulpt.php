<?php
Yii::import('application.models._base.BaseUlpt');
class Ulpt extends BaseUlpt
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->ulpt_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->ulpt_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public function get_sisa()
    {
        $command = $this->dbConnection->createCommand("
        SELECT nuv.amount - COALESCE(SUM(nr.amount),0)
        FROM nscc_ulpt_view AS nuv
        INNER JOIN nscc_rpg AS nr ON nr.parent_id = nuv.ulpt_id
        WHERE nuv.ulpt_id = :ulpt_id");
        return $command->queryScalar(array(':ulpt_id' => $this->ulpt_id));
    }
    public static function get_ulpt($tgl, $bank_id = null, $store = STOREID)
    {
        return Ulpt::get_ulpt_rpj($tgl, TIPE_ULPT, $bank_id, $store);
    }
    public static function get_rpj($tgl, $bank_id = null, $store = STOREID)
    {
        return Ulpt::get_ulpt_rpj($tgl, TIPE_RPG, $bank_id, $store);
    }
    public static function get_ulpt_rpj($tgl, $tipe, $bank_id = null, $store = STOREID)
    {
//        $bank_id = Bank::get_bank_cash_id();
        // AND ns.bank_id = :bank_id
        $where = "";
        $param = array(
            ':tgl' => $tgl,
            ':tipe' => $tipe
        );
        if ($bank_id != null) {
            $where .= " AND ns.bank_id = :bank_id";
            $param[':bank_id'] = $bank_id;
        }
        if ($store != null) {
            $where .= " AND store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.amount), 0) total FROM nscc_ulpt ns
    WHERE ns.tipe = :tipe AND ns.tgl = :tgl $where");
        return $comm->queryScalar($param);
    }
    public static function create_ulpt($ulpt, $salestrans_id, $tgl, $amount, $bank_id, $card_id, $note)
    {
        /** @var $model Ulpt */
        $model = $ulpt;
        $fee = 0;
        $amount_ju = $amount;
        $bank = Bank::model()->findByPk($bank_id);
        if (!$bank->is_bank_cash() && $card_id == null) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Sales')) . ' Tipe Card harus dipilih!');
        }
        if ($card_id != null) {
            $card = Card::model()->findByPk($card_id);
            if ($card == null) {
                throw new Exception("Card tidak ditemukan!");
            }
            $fee = ($card->persen_fee / 100) * $amount;
            $fee = round($fee, 2);
            $amount_ju = $amount - $fee;
        }
        $docref = "";
        $ref = null;
        $is_new_ulpt = $model->isNewRecord;
        if ($is_new_ulpt) {
            $ref = new Reference();
            $docref = $ref->get_next_reference(ULPT);
        } else {
            $docref = $model->doc_ref;
        }
        $model->salestrans_id = $salestrans_id;
        $model->tgl = $tgl;
        $model->amount = $amount;
        $model->bank_id = $bank_id;
        $model->card_id = $card_id;
        $model->feecard = $fee;
        $model->note = $note;
        $model->store = STOREID;
        $model->tipe = TIPE_ULPT;
        $model->doc_ref = $docref;
        if (!$model->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'ULPT')) . CHtml::errorSummary($model));
        }
        if ($is_new_ulpt) {
            $ref->save(ULPT, $model->ulpt_id, $docref);
        }
        $gl = new GL();
        /** @var Salestrans $sales */
        $sales = Salestrans::model()->findByPk($salestrans_id);
        $memo = "ULPT $docref ".$sales->customer->nama_customer." ".$note;
        $gl->add_gl(ULPT, $model->ulpt_id, $model->tgl, $docref, COA_FEE_CARD,
            $memo, $memo, $fee, 0, $model->store);
        $gl->add_gl(ULPT, $model->ulpt_id, $model->tgl, $docref, $bank->account_code,
            $memo, $memo, $amount_ju, 0, $model->store);
        $is_bank = $gl->is_bank_account($bank->account_code);
        if ($is_bank) {
            $gl->add_bank_trans(ULPT, $model->ulpt_id, $model->bank_id, $docref, $model->tgl,
                $amount_ju, $model->id_user, $model->store);
        }
        $gl->add_gl(ULPT, $model->ulpt_id, $model->tgl, $docref, COA_ULPT,
            $memo, $memo, -$model->amount, $is_bank ? 1 : 0, $model->store);
        $gl->validate();
    }
    
    public function generateGltransUlpt($val) {
//	public function generateGltransKas() {
		$gl                   = new GL();
		app()->db->autoCommit = false;
		$transaction          = Yii::app()->db->beginTransaction();
		try {
//			$sales = Salestrans::model()->findByPk($salestrans_id);
                    $model = $val;
                    $docref = $model->doc_ref;
                    $memo = $model->note;
                    $card_id = $model->card_id;
                    $amount = $model->amount;
                    $fee = $model->feecard;
                    $bank_id = $model->bank_id;
                    $amount_ju = $amount;
                    
                    if ($card_id != null) {                        
                        $amount_ju = $amount - $fee;
                    }
                    $gl->add_gl(ULPT, $model->ulpt_id, $model->tgl, $docref, COA_FEE_CARD,
                        $memo, $memo, $fee, 0, $model->store);
                    
                    $bank = Bank::model()->findByPk($bank_id);
                    $gl->add_gl(ULPT, $model->ulpt_id, $model->tgl, $docref, $bank->account_code,
                        $memo, $memo, $amount_ju, 0, $model->store);
                    
                    $is_bank = $gl->is_bank_account($bank->account_code);
                    $gl->add_gl(ULPT, $model->ulpt_id, $model->tgl, $docref, COA_ULPT,
                        $memo, $memo, -$model->amount, $is_bank ? 1 : 0, $model->store);
                    $gl->validate();
                    $status = true;
                    $msg = 'sukses';
                    $transaction->commit();
		} catch ( Exception $ex ) {
			$transaction->rollback();
			$status = false;
			$msg    = $ex->getMessage();
                        echo $msg;
		}
		app()->db->autoCommit = true;
		return [
			'status' => $status,
			'msg'    => $msg
		];
	}
}