<?php
Yii::import('application.models._base.BaseDroppingRecall');

class DroppingRecall extends BaseDroppingRecall
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->dropping_recall_id == null) {
            $this->dropping_recall_id = $this->dbConnection->createCommand("SELECT UUID();")->queryScalar();
            $this->tdate = $this->dbConnection->createCommand("SELECT NOW();")->queryScalar();
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
}