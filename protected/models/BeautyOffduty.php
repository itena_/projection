<?php
Yii::import('application.models._base.BaseBeautyOffduty');
class BeautyOffduty extends BaseBeautyOffduty
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'beauty_id';
    }
}