<?php

Yii::import('application.models._base.BaseBarangClinical');
class BarangClinical extends BaseBarangClinical
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->barang_clinical == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->barang_clinical = $uuid;
        }
        return parent::beforeValidate();
    }
    public function get_saldo()
    {
        $command = $this->dbConnection->createCommand("SELECT COALESCE(SUM(nsmc.qty),0) FROM
        nscc_stock_moves_clinical nsmc WHERE nsmc.barang_clinical = :barang_clinical");
        return $command->queryScalar(array(':barang_clinical' => $this->barang_clinical));
    }


}