<?php
Yii::import('application.models._base.BasePembantuPelunasanPiutang');

class PembantuPelunasanPiutang extends BasePembantuPelunasanPiutang
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function beforeValidate(){
		if ($this->pembantu_pelunasan_piutang_id == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->pembantu_pelunasan_piutang_id = $uuid;
		}
		if ($this->tdate == null) {
			$this->tdate = new CDbExpression('NOW()');
		}
		if ($this->store == null) {
			$this->store = STOREID;
		}
		if ($this->user_id == null) {
			$this->user_id = Yii::app()->user->getId();
		}
		return parent::beforeValidate();
	}
}