<?php
Yii::import('application.models._base.BaseTransferBarangAsset');

class TransferBarangAsset extends BaseTransferBarangAsset
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->transfer_asset_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->transfer_asset_id = $uuid;
        }
        return parent::beforeValidate();
    }
}