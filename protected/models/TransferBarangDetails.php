<?php
Yii::import('application.models._base.BaseTransferBarangDetails');
class TransferBarangDetails extends BaseTransferBarangDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->transfer_barang_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->transfer_barang_detail_id = $uuid;
        }
        if ($this->visible == null) {
            $this->visible = 1;
        }
        return parent::beforeValidate();
    }
}