<?php
Yii::import('application.models._base.BaseClearUlpt');
class ClearUlpt extends BaseClearUlpt
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->clear_ulpt_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->clear_ulpt_id = $uuid;
        }
        return parent::beforeValidate();
    }
}