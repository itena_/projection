<?php
Yii::import('application.models._base.BasePromosi');
class Promosi extends BasePromosi
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->promosi_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->promosi_id = $uuid;
        }
        return parent::beforeValidate();
    }
}