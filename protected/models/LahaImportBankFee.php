<?php
Yii::import('application.models._base.BaseLahaImportBankFee');
class LahaImportBankFee extends BaseLahaImportBankFee
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->laha_import_bank_fee_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->laha_import_bank_fee_id = $uuid;
        }
        return parent::beforeValidate();
    }
}