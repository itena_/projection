<?php
Yii::import('application.models._base.BaseKmtMaster');

class KmtMaster extends BaseKmtMaster
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->kmt_master_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kmt_master_id = $uuid;
        }
        return parent::beforeValidate();
    }
}