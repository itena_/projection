<?php
Yii::import('application.models._base.BasePoDetails');

class PoDetails extends BasePoDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->po_details_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->po_details_id = $uuid;
        }
        if (!$this->item_id) {
            $this->item_id = $this->dbConnection->createCommand("SELECT UUID();")->queryScalar();
        }
        return parent::beforeValidate();
    }
}