<?php

/**
 * This is the model base class for the table "{{resep_print_bahan}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ResepPrintBahan".
 *
 * Columns in table "{{resep_print_bahan}}" available as properties of the model,
 * followed by relations of table "{{resep_print_bahan}}" available as properties of the model.
 *
 * @property string $resep_print_bahan_id
 * @property string $resep_print_details_id
 * @property integer $no_urut
 * @property string $kode_bahan
 * @property string $nama_bahan
 * @property double $qty
 * @property string $sat
 *
 * @property ResepPrintDetails $resepPrintDetails
 */
abstract class BaseResepPrintBahan extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{resep_print_bahan}}';
	}

	public static function representingColumn() {
		return 'nama_bahan';
	}

	public function rules() {
		return array(
			array('resep_print_bahan_id, resep_print_details_id, nama_bahan, sat', 'required'),
			array('no_urut', 'numerical', 'integerOnly'=>true),
			array('qty', 'numerical'),
			array('resep_print_bahan_id, resep_print_details_id', 'length', 'max'=>36),
			array('kode_bahan, nama_bahan', 'length', 'max'=>100),
			array('sat', 'length', 'max'=>20),
			array('no_urut, kode_bahan, qty', 'default', 'setOnEmpty' => true, 'value' => null),
			array('resep_print_bahan_id, resep_print_details_id, no_urut, kode_bahan, nama_bahan, qty, sat', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'resepPrintDetails' => array(self::BELONGS_TO, 'ResepPrintDetails', 'resep_print_details_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'resep_print_bahan_id' => Yii::t('app', 'Resep Print Bahan'),
			'resep_print_details_id' => Yii::t('app', 'Resep Print Details'),
			'no_urut' => Yii::t('app', 'No Urut'),
			'kode_bahan' => Yii::t('app', 'Kode Bahan'),
			'nama_bahan' => Yii::t('app', 'Nama Bahan'),
			'qty' => Yii::t('app', 'Qty'),
			'sat' => Yii::t('app', 'Sat'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('resep_print_bahan_id', $this->resep_print_bahan_id, true);
		$criteria->compare('resep_print_details_id', $this->resep_print_details_id);
		$criteria->compare('no_urut', $this->no_urut);
		$criteria->compare('kode_bahan', $this->kode_bahan, true);
		$criteria->compare('nama_bahan', $this->nama_bahan, true);
		$criteria->compare('qty', $this->qty);
		$criteria->compare('sat', $this->sat, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}