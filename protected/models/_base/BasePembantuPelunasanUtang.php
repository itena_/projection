<?php

/**
 * This is the model base class for the table "{{pembantu_pelunasan_utang}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PembantuPelunasanUtang".
 *
 * Columns in table "{{pembantu_pelunasan_utang}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $pembantu_pelunasan_utang_id
 * @property string $tgl
 * @property string $doc_ref
 * @property string $total
 * @property string $store
 * @property string $tdate
 * @property string $user_id
 * @property integer $up
 * @property string $bank_id
 * @property string $amount_bank
 * @property string $rounding
 * @property string $void_tdate
 * @property string $void_user_id
 *
 */
abstract class BasePembantuPelunasanUtang extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{pembantu_pelunasan_utang}}';
	}

	public static function representingColumn() {
		return 'tgl';
	}

	public function rules() {
		return array(
			array('pembantu_pelunasan_utang_id, tgl, doc_ref, store, tdate, user_id, bank_id', 'required'),
			array('up', 'numerical', 'integerOnly'=>true),
			array('pembantu_pelunasan_utang_id, bank_id', 'length', 'max'=>36),
			array('doc_ref, user_id, void_user_id', 'length', 'max'=>50),
			array('total, amount_bank, rounding', 'length', 'max'=>30),
			array('store', 'length', 'max'=>20),
			array('void_tdate', 'safe'),
			array('total, up, amount_bank, rounding, void_tdate, void_user_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('pembantu_pelunasan_utang_id, tgl, doc_ref, total, store, tdate, user_id, up, bank_id, amount_bank, rounding, void_tdate, void_user_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'pembantu_pelunasan_utang_id' => Yii::t('app', 'Pembantu Pelunasan Utang'),
			'tgl' => Yii::t('app', 'Tgl'),
			'doc_ref' => Yii::t('app', 'Doc Ref'),
			'total' => Yii::t('app', 'Total'),
			'store' => Yii::t('app', 'Store'),
			'tdate' => Yii::t('app', 'Tdate'),
			'user_id' => Yii::t('app', 'User'),
			'up' => Yii::t('app', 'Up'),
			'bank_id' => Yii::t('app', 'Bank'),
			'amount_bank' => Yii::t('app', 'Amount Bank'),
			'rounding' => Yii::t('app', 'Rounding'),
			'void_tdate' => Yii::t('app', 'Void Tdate'),
			'void_user_id' => Yii::t('app', 'Void User'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('pembantu_pelunasan_utang_id', $this->pembantu_pelunasan_utang_id, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('doc_ref', $this->doc_ref, true);
		$criteria->compare('total', $this->total, true);
		$criteria->compare('store', $this->store, true);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('user_id', $this->user_id, true);
		$criteria->compare('up', $this->up);
		$criteria->compare('bank_id', $this->bank_id, true);
		$criteria->compare('amount_bank', $this->amount_bank, true);
		$criteria->compare('rounding', $this->rounding, true);
		$criteria->compare('void_tdate', $this->void_tdate, true);
		$criteria->compare('void_user_id', $this->void_user_id, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}