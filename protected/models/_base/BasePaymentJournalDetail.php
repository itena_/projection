<?php

/**
 * This is the model base class for the table "{{payment_journal_detail}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PaymentJournalDetail".
 *
 * Columns in table "{{payment_journal_detail}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $payment_journal_detail_id
 * @property string $payment_journal_id
 * @property string $store_kode
 * @property double $debit
 * @property double $kredit
 * @property string $account_code
 * @property string $memo_
 *
 */
abstract class BasePaymentJournalDetail extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{payment_journal_detail}}';
	}

	public static function representingColumn() {
		return 'payment_journal_id';
	}

	public function rules() {
		return array(
			array('payment_journal_detail_id, payment_journal_id, store_kode, account_code', 'required'),
			array('debit, kredit', 'numerical'),
			array('payment_journal_detail_id, payment_journal_id', 'length', 'max'=>36),
			array('store_kode', 'length', 'max'=>20),
			array('account_code', 'length', 'max'=>15),
			array('memo_', 'length', 'max'=>255),
			array('debit, kredit, memo_', 'default', 'setOnEmpty' => true, 'value' => null),
			array('payment_journal_detail_id, payment_journal_id, store_kode, debit, kredit, account_code, memo_', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'payment_journal_detail_id' => Yii::t('app', 'Payment Journal Detail'),
			'payment_journal_id' => Yii::t('app', 'Payment Journal'),
			'store_kode' => Yii::t('app', 'Store Kode'),
			'debit' => Yii::t('app', 'Debit'),
			'kredit' => Yii::t('app', 'Kredit'),
			'account_code' => Yii::t('app', 'Account Code'),
			'memo_' => Yii::t('app', 'Memo'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('payment_journal_detail_id', $this->payment_journal_detail_id, true);
		$criteria->compare('payment_journal_id', $this->payment_journal_id, true);
		$criteria->compare('store_kode', $this->store_kode, true);
		$criteria->compare('debit', $this->debit);
		$criteria->compare('kredit', $this->kredit);
		$criteria->compare('account_code', $this->account_code, true);
		$criteria->compare('memo_', $this->memo_, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}