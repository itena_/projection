<?php

/**
 * This is the model base class for the table "{{paket_trans}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PaketTrans".
 *
 * Columns in table "{{paket_trans}}" available as properties of the model,
 * followed by relations of table "{{paket_trans}}" available as properties of the model.
 *
 * @property string $paket_trans_id
 * @property string $paket_id
 * @property string $salestrans_id
 * @property integer $qty
 *
 * @property Paket $paket
 * @property Salestrans $salestrans
 */
abstract class BasePaketTrans extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{paket_trans}}';
	}

	public static function representingColumn() {
		return 'paket_trans_id';
	}

	public function rules() {
		return array(
			array('paket_trans_id, paket_id, salestrans_id', 'required'),
			array('qty', 'numerical', 'integerOnly'=>true),
			array('paket_trans_id, paket_id', 'length', 'max'=>36),
			array('salestrans_id', 'length', 'max'=>50),
			array('qty', 'default', 'setOnEmpty' => true, 'value' => null),
			array('paket_trans_id, paket_id, salestrans_id, qty', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'paket' => array(self::BELONGS_TO, 'Paket', 'paket_id'),
			'salestrans' => array(self::BELONGS_TO, 'Salestrans', 'salestrans_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'paket_trans_id' => Yii::t('app', 'Paket Trans'),
			'paket_id' => Yii::t('app', 'Paket'),
			'salestrans_id' => Yii::t('app', 'Salestrans'),
			'qty' => Yii::t('app', 'Qty'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('paket_trans_id', $this->paket_trans_id, true);
		$criteria->compare('paket_id', $this->paket_id);
		$criteria->compare('salestrans_id', $this->salestrans_id);
		$criteria->compare('qty', $this->qty);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}