<?php

/**
 * This is the model base class for the table "pasien".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Pasien".
 *
 * Columns in table "pasien" available as properties of the model,
 * followed by relations of table "pasien" available as properties of the model.
 *
 * @property string $id
 * @property integer $id_cabang
 * @property string $no_kartu
 * @property string $no_base
 * @property integer $no_lokal
 * @property string $nama
 * @property string $nama_depan
 * @property string $alamat
 * @property string $kota
 * @property string $telp
 * @property string $tgl_lhr
 * @property string $jkel
 * @property integer $usia
 * @property string $pekerjaan
 * @property string $awal
 * @property string $akhir
 * @property integer $akhir_ke
 * @property integer $tahun
 * @property integer $bulan
 * @property string $timestamp
 *
 * @property Cabang $idCabang
 */
abstract class BasePasien extends NarsActiveRecord {

	public function getDbConnection()
	{
		return self::getNarsDbConnection();
	}

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'pasien';
	}

	public static function representingColumn() {
		return 'no_kartu';
	}

	public function rules() {
		return array(
			array('id_cabang, no_kartu, no_base, nama, timestamp', 'required'),
			array('id_cabang, no_lokal, usia, akhir_ke, tahun, bulan', 'numerical', 'integerOnly'=>true),
			array('no_kartu, no_base', 'length', 'max'=>36),
			array('nama', 'length', 'max'=>100),
			array('nama_depan', 'length', 'max'=>10),
			array('alamat', 'length', 'max'=>200),
			array('kota, pekerjaan', 'length', 'max'=>60),
			array('telp', 'length', 'max'=>50),
			array('jkel', 'length', 'max'=>20),
			array('tgl_lhr, awal, akhir', 'safe'),
			array('no_lokal, nama_depan, alamat, kota, telp, tgl_lhr, jkel, usia, pekerjaan, awal, akhir, akhir_ke, tahun, bulan', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, id_cabang, no_kartu, no_base, no_lokal, nama, nama_depan, alamat, kota, telp, tgl_lhr, jkel, usia, pekerjaan, awal, akhir, akhir_ke, tahun, bulan, timestamp', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'idCabang' => array(self::BELONGS_TO, 'Cabang', 'id_cabang'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'id_cabang' => Yii::t('app', 'Id Cabang'),
			'no_kartu' => Yii::t('app', 'No Kartu'),
			'no_base' => Yii::t('app', 'No Base'),
			'no_lokal' => Yii::t('app', 'No Lokal'),
			'nama' => Yii::t('app', 'Nama'),
			'nama_depan' => Yii::t('app', 'Nama Depan'),
			'alamat' => Yii::t('app', 'Alamat'),
			'kota' => Yii::t('app', 'Kota'),
			'telp' => Yii::t('app', 'Telp'),
			'tgl_lhr' => Yii::t('app', 'Tgl Lhr'),
			'jkel' => Yii::t('app', 'Jkel'),
			'usia' => Yii::t('app', 'Usia'),
			'pekerjaan' => Yii::t('app', 'Pekerjaan'),
			'awal' => Yii::t('app', 'Awal'),
			'akhir' => Yii::t('app', 'Akhir'),
			'akhir_ke' => Yii::t('app', 'Akhir Ke'),
			'tahun' => Yii::t('app', 'Tahun'),
			'bulan' => Yii::t('app', 'Bulan'),
			'timestamp' => Yii::t('app', 'Timestamp'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('id_cabang', $this->id_cabang);
		$criteria->compare('no_kartu', $this->no_kartu, true);
		$criteria->compare('no_base', $this->no_base, true);
		$criteria->compare('no_lokal', $this->no_lokal);
		$criteria->compare('nama', $this->nama, true);
		$criteria->compare('nama_depan', $this->nama_depan, true);
		$criteria->compare('alamat', $this->alamat, true);
		$criteria->compare('kota', $this->kota, true);
		$criteria->compare('telp', $this->telp, true);
		$criteria->compare('tgl_lhr', $this->tgl_lhr, true);
		$criteria->compare('jkel', $this->jkel, true);
		$criteria->compare('usia', $this->usia);
		$criteria->compare('pekerjaan', $this->pekerjaan, true);
		$criteria->compare('awal', $this->awal, true);
		$criteria->compare('akhir', $this->akhir, true);
		$criteria->compare('akhir_ke', $this->akhir_ke);
		$criteria->compare('tahun', $this->tahun);
		$criteria->compare('bulan', $this->bulan);
		$criteria->compare('timestamp', $this->timestamp, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}