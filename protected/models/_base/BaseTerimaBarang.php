<?php

/**
 * This is the model base class for the table "{{terima_barang}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "TerimaBarang".
 *
 * Columns in table "{{terima_barang}}" available as properties of the model,
 * followed by relations of table "{{terima_barang}}" available as properties of the model.
 *
 * @property string $terima_barang_id
 * @property string $po_id
 * @property string $doc_ref
 * @property string $id_user
 * @property string $tdate
 * @property string $tgl
 * @property string $store
 * @property string $no_sj
 * @property string $tgl_sj
 * @property string $note
 * @property integer $lock_edit
 * @property integer $status
 * @property integer $up
 *
 * @property Po $po
 * @property TerimaBarangDetails[] $terimaBarangDetails
 * @property TransferItem[] $transferItems
 */
abstract class BaseTerimaBarang extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{terima_barang}}';
	}

	public static function representingColumn() {
		return 'doc_ref';
	}

	public function rules() {
		return array(
			array('terima_barang_id, po_id, doc_ref, id_user, tdate, tgl, store', 'required'),
			array('lock_edit, status, up', 'numerical', 'integerOnly'=>true),
			array('terima_barang_id, po_id, id_user', 'length', 'max'=>36),
			array('doc_ref', 'length', 'max'=>50),
			array('store', 'length', 'max'=>20),
			array('no_sj', 'length', 'max'=>45),
			array('note', 'length', 'max'=>100),
			array('tgl_sj', 'safe'),
			array('no_sj, tgl_sj, note, lock_edit, status, up', 'default', 'setOnEmpty' => true, 'value' => null),
			array('terima_barang_id, po_id, doc_ref, id_user, tdate, tgl, store, no_sj, tgl_sj, note, lock_edit, status, up', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'po' => array(self::BELONGS_TO, 'Po', 'po_id'),
			'terimaBarangDetails' => array(self::HAS_MANY, 'TerimaBarangDetails', 'terima_barang_id'),
			'transferItems' => array(self::HAS_MANY, 'TransferItem', 'terima_barang_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'terima_barang_id' => Yii::t('app', 'Terima Barang'),
			'po_id' => Yii::t('app', 'Po'),
			'doc_ref' => Yii::t('app', 'Doc Ref'),
			'id_user' => Yii::t('app', 'Id User'),
			'tdate' => Yii::t('app', 'Tdate'),
			'tgl' => Yii::t('app', 'Tgl'),
			'store' => Yii::t('app', 'Store'),
			'no_sj' => Yii::t('app', 'No Sj'),
			'tgl_sj' => Yii::t('app', 'Tgl Sj'),
			'note' => Yii::t('app', 'Note'),
			'lock_edit' => Yii::t('app', 'Lock Edit'),
			'status' => Yii::t('app', 'Status'),
			'up' => Yii::t('app', 'Up'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('terima_barang_id', $this->terima_barang_id, true);
		$criteria->compare('po_id', $this->po_id);
		$criteria->compare('doc_ref', $this->doc_ref, true);
		$criteria->compare('id_user', $this->id_user, true);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('store', $this->store, true);
		$criteria->compare('no_sj', $this->no_sj, true);
		$criteria->compare('tgl_sj', $this->tgl_sj, true);
		$criteria->compare('note', $this->note, true);
		$criteria->compare('lock_edit', $this->lock_edit);
		$criteria->compare('status', $this->status);
		$criteria->compare('up', $this->up);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}