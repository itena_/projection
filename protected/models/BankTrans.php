<?php
Yii::import( 'application.models._base.BaseBankTrans' );
class BankTrans extends BaseBankTrans {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	protected function afterSave() {
		parent::afterSave();
		LogTrans::saveLog( $this->type_, $this->trans_no,
			$this->isNewRecord ? 'CREATE' : 'UPDATE', json_encode(get_defined_vars()) );
		$limit   = Bank::get_minimal( $this->bank_id );
		$balance = BankTrans::get_balance( $this->bank_id, $this->tgl );
		#$newBal = $balance + abs($limit);
		if ( $this->amount < 0 && $balance < $limit ) {
//			throw new Exception( t( 'save.model.fail', 'app', array( '{model}' => 'Cash' ) ) .
//			                     "Kurang dana. Tersedia " . number_format( $balance + ( $this->amount ), 2 ) . ' diperlukan ' . number_format( $this->amount, 2 ) .
//			                     "\nTotal Kekurangan " . number_format( $balance - abs( $this->amount ), 2 )
//			);
			return false;
		} else {
			return true;
		}
	}
	public function beforeValidate() {
		if ( $this->bank_trans_id == null ) {
			$command             = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                = $command->queryScalar();
			$this->bank_trans_id = $uuid;
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		if ( $this->store == null ) {
			$this->store = STOREID;
		}
		if ( $this->id_user == null ) {
			$this->id_user = Yii::app()->user->getId();
		}
		if ( $this->visible == null ) {
			$this->visible = 1;
		}
		if ( $this->tgl != null ) {
			if ( $this->tgl < U::getResDate() ) {
				throw new Exception( 'error pembatasan tanggal.' );
			}
		}
		return parent::beforeValidate();
	}
	public static function get_balance( $id, $date ) {
		$comm = Yii::app()->db->createCommand( "
        SELECT IFNULL(SUM(nbt.amount),0) AS total
        FROM nscc_bank_trans nbt
        WHERE nbt.visible = 1 AND nbt.bank_id = :bank_id AND nbt.tgl <= :tgl" );
		return $comm->queryScalar( array( ':bank_id' => $id, ':tgl' => $date ) );
	}
	public static function get_list_bank_transfer( $tgl, $limit, $offset ) {
//        $comm = Yii::app()->db->createCommand("SELECT nbt.trans_no,nbt.ref,nbt.tgl trans_date,nbt.amount,
//        (SELECT nbt1.bank_id FROM nscc_bank_trans nbt1 WHERE nbt1.type_ = 11 AND nbt1.amount <= 0 AND nbt1.trans_no = nbt.trans_no LIMIT 1) bank_act_asal,
//        COALESCE((SELECT nbt1.bank_id FROM nscc_bank_trans nbt1 WHERE nbt1.type_ = 11 AND nbt1.amount >= 0 AND nbt1.trans_no = nbt.trans_no LIMIT 1,1),
//         (SELECT nbt1.bank_id FROM nscc_bank_trans nbt1 WHERE nbt1.type_ = 11 AND nbt1.amount >= 0 AND nbt1.trans_no = nbt.trans_no LIMIT 1))bank_act_tujuan,
//        IFNULL((SELECT ABS(nbt1.amount) FROM nscc_bank_trans nbt1 WHERE nbt1.type_ = 12 AND nbt1.trans_no = nbt.trans_no LIMIT 1),0) charge,
//        nc.memo_ memo,nbt.store
//        FROM nscc_bank_trans AS nbt
//        LEFT JOIN nscc_comments nc ON (nbt.trans_no = nc.type_no AND nbt.type_ = nc.type)
//        WHERE nbt.type_ = 11 AND nbt.tgl = :tgl AND nbt.visible = 1
//        GROUP BY trans_no");
		$comm = Yii::app()->db->createCommand( "SELECT a.*,b.bank_act_tujuan,COALESCE(c.charge,0) charge,d.memo_ memo FROM
        (SELECT nbt1.type_,nbt1.trans_no,nbt1.ref,nbt1.tgl trans_date,abs(nbt1.amount) amount,nbt1.bank_id bank_act_asal,nbt1.store
        FROM nscc_bank_trans AS nbt1
        WHERE nbt1.amount < 0 AND nbt1.type_ = 11 AND nbt1.tgl = :tgl AND nbt1.visible = 1) a 
        INNER JOIN (SELECT nbt1.trans_no,nbt1.bank_id bank_act_tujuan FROM nscc_bank_trans AS nbt1
        WHERE nbt1.amount >= 0 AND nbt1.type_ = 11 AND nbt1.tgl = :tgl AND nbt1.visible = 1) b ON a.trans_no = b.trans_no
        LEFT JOIN (SELECT nbt1.trans_no,ABS(nbt1.amount) charge FROM nscc_bank_trans AS nbt1
        WHERE nbt1.type_ = 12 AND nbt1.tgl = :tgl AND nbt1.visible = 1) c ON b.trans_no = c.trans_no 
        LEFT JOIN (SELECT DISTINCT(nc.type_no),nc.memo_ FROM nscc_comments nc WHERE nc.type = 11 AND nc.date_ = :tgl) d 
        ON a.trans_no = d.type_no LIMIT $offset,$limit" );
		return $comm->queryAll( true, array( ':tgl' => $tgl ) );
	}
	public static function count_get_list_bank_transfer( $tgl ) {
		$comm = Yii::app()->db->createCommand( "SELECT count(*) FROM
        (SELECT nbt1.type_,nbt1.trans_no,nbt1.ref,nbt1.tgl trans_date,abs(nbt1.amount) amount,nbt1.bank_id bank_act_asal,nbt1.store
        FROM nscc_bank_trans AS nbt1
        WHERE nbt1.amount < 0 AND nbt1.type_ = 11 AND nbt1.tgl = :tgl AND nbt1.visible = 1) a 
        INNER JOIN (SELECT nbt1.trans_no,nbt1.bank_id bank_act_tujuan FROM nscc_bank_trans AS nbt1
        WHERE nbt1.amount >= 0 AND nbt1.type_ = 11 AND nbt1.tgl = :tgl AND nbt1.visible = 1) b ON a.trans_no = b.trans_no
        LEFT JOIN (SELECT nbt1.trans_no,ABS(nbt1.amount) charge FROM nscc_bank_trans AS nbt1
        WHERE nbt1.type_ = 12 AND nbt1.tgl = :tgl AND nbt1.visible = 1) c ON b.trans_no = c.trans_no 
        LEFT JOIN (SELECT DISTINCT(nc.type_no),nc.memo_ FROM nscc_comments nc WHERE nc.type = 11 AND nc.date_ = :tgl) d 
        ON a.trans_no = d.type_no" );
		return $comm->queryScalar( array( ':tgl' => $tgl ) );
	}
	public static function get_list_bank_transfer_by_trans_no( $trans_no ) {
		$comm = Yii::app()->db->createCommand( "SELECT nbt.trans_no,nbt.ref,nbt.tgl trans_date,nbt.amount,
        (SELECT nbt1.bank_id FROM nscc_bank_trans nbt1 WHERE nbt1.type_ = 11 AND nbt1.amount < 0 AND nbt1.trans_no = nbt.trans_no) bank_act_asal,
        (SELECT nbt1.bank_id FROM nscc_bank_trans nbt1 WHERE nbt1.type_ = 11 AND nbt1.amount > 0 AND nbt1.trans_no = nbt.trans_no) bank_act_tujuan,
        IFNULL((SELECT ABS(nbt1.amount) FROM nscc_bank_trans nbt1 WHERE nbt1.type_ = 12 AND nbt1.trans_no = nbt.trans_no LIMIT 1),0) charge,
        nc.memo_ memo
        FROM nscc_bank_trans AS nbt
        LEFT JOIN nscc_comments nc ON (nbt.trans_no = nc.type_no AND nbt.type_ = nc.type)
        WHERE nbt.type_ = 11 AND nbt.trans_no = :trans_no
        GROUP BY trans_no" );
		$comm->setFetchMode( PDO::FETCH_OBJ );
		return $comm->queryRow( true, array( ':trans_no' => $trans_no ) );
	}

	/**
	 * After save attributes
	 */
	/* protected function afterSave() {
		parent::afterSave();
		U::runCommand('banktrans', '--id=' . $this->bank_trans_id, 'protected/runtime/banktrans_'.$this->bank_trans_id.'.log');
	} */
        public function generateGltransBankTransfer($a) {
//	public function generateGltransKas() {
		$gl                   = new GL();
		app()->db->autoCommit = false;
		$transaction          = Yii::app()->db->beginTransaction();
		try {
//			$model  = $a;
//			$model  = $this;
                        $aa = 0;
                        foreach ($a as $model){
                            $docref = $model->ref;
                        
                            $bank = Bank::model()->findByPk($model->bank_id);
                            $namabank = $bank->nama_bank;
                            $trans_no = $model->trans_no;
                            $trans_date = $model->tgl;
                            $memo = $namabank . " " . $trans_date;
                            $amount = $model->amount;
                            $store = $model->store;


                            if ($model->amount > 0){
                                $bank_account_tujuan = $bank->account_code;
                                $gl->add_gl(BANKTRANSFER, $trans_no, $trans_date, $docref, $bank_account_tujuan,
                                $memo, $memo, $amount, 0, $store);
                            } else {
                                $bank_account_asal = $bank->account_code;
                                $gl->add_gl(BANKTRANSFER, $trans_no, $trans_date, $docref, $bank_account_asal,
                                $memo, $memo, $amount, 0, $store);
                            }
                            $aa += $amount;
                        }			
                        $gl->validate();
                        $status = true;
                        $msg = 'sukses';
                        $transaction->commit();
		} catch ( Exception $ex ) {
			$transaction->rollback();
			$status = false;
			$msg    = $ex->getMessage();
                        echo $msg;
		}
		app()->db->autoCommit = true;
		return [
			'status' => $status,
			'msg'    => $msg
		];
	}
}