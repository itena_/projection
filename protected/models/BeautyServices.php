<?php
Yii::import('application.models._base.BaseBeautyServices');
class BeautyServices extends BaseBeautyServices
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    /**
     * @param SalestransDetails $salestrans_details transaksi details
     * @param string $beauty_id beauti_id
     * @param integer $tipe tipe
     * @throws Exception
     */
    public static function add_beauty_tips($salestrans_details, $beauty_id, $tipe)
    {
//        $beauty = BeautyServices::model()->findByAttributes(array('salestrans_details' => $salestrans_details->salestrans_details,
//            'beauty_id' => $beauty_id, 'tipe' => $tipe));
//        if ($beauty == null)
//        {
//            $beauty = new BeautyServices;
//        }
        if (!$salestrans_details->barang->isPerawatan()) {
            throw new Exception('Tidak bisa buat jasa beauty karena ' .
                $salestrans_details->barang->kode_barang . ' bukan perawatan');
        }
        $beauty = new BeautyServices;
        $beauty->beauty_id = $beauty_id;
        $beauty->tipe = $tipe;
        $beauty->salestrans_details = $salestrans_details->salestrans_details;
        if (!$beauty->save())
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Beauty Services')) . CHtml::errorSummary($beauty));
        
        if(METHODE_BONUS == 1) {
            /*
             * Bonus Aishaderm (Persentase)
             * BEAUTY PERAWATAN
             */
            $salestrans = Salestrans::model()->find('salestrans_id = :salestrans_id', array(':salestrans_id'=>$salestrans_details->salestrans_id));
            Bonus::serviceBeauty(PENJUALAN, $salestrans, $salestrans_details, $beauty_id);

            //di natasha pake onepageeditservice jadi harus ditambahkan di nscc_beauty_service
            if (ONEPAGE_EDITSERVICE)
            {
	            $price = Price::get_price($salestrans_details->barang_id, $beauty->beauty->gol_id);
	            if ($price == null)
		            throw new Exception("Tips value not define");
	            //tidak ada pengali di natasha
	            $total_tips = NATASHA_CUSTOM ? $price->value : ($salestrans_details->qty * $price->value);
	            $beauty->total = $total_tips;
	            if (!$beauty->update())
		            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Beauty Services')) . CHtml::errorSummary($beauty));
            }
        } else {
            /*
             * Bonus Naavagreen (Golongan)
             * BEAUTY PERAWATAN
             */
            $price = Price::get_price($salestrans_details->barang_id, $beauty->beauty->gol_id);
            if ($price == null)
                throw new Exception("Tips value not define");
            //tidak ada pengali di natasha
            $total_tips = NATASHA_CUSTOM ? $price->value : ($salestrans_details->qty * $price->value);
            $beauty->total = $total_tips;
            if (!$beauty->update())
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Beauty Services')) . CHtml::errorSummary($beauty));
        }
            
    }
    public function beforeValidate()
    {
        if ($this->nscc_beauty_services_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->nscc_beauty_services_id = $uuid;
        }
        if ($this->visible == null) {
            $this->visible = 1;
        }
        return parent::beforeValidate();
    }
    public static function getSummary($dari, $sampai)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT nbe.kode_beauty,nbe.nama_beauty,ngo.nama_gol,nb.kode_barang,Sum(nsd.qty) AS qty,Sum(nbs.total) AS tip
          FROM nscc_salestrans_details AS nsd
            INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
            INNER JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
            INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
            INNER JOIN nscc_beauty_services AS nbs ON (nbs.salestrans_details = nsd.salestrans_details AND nbs.visible = 1)
            INNER JOIN nscc_beauty AS nbe ON nbs.beauty_id = nbe.beauty_id
            INNER JOIN nscc_gol AS ngo ON nbe.gol_id = ngo.gol_id 
          WHERE ng.kategori_id = '1' AND ns.tgl >= :tgl_dari AND ns.tgl <= :tgl_sampai
            AND	ns.doc_ref NOT IN (SELECT nsr.doc_ref_sales FROM nscc_salestrans AS nsr WHERE nsr.type_ = -1)
          GROUP BY nbe.nama_beauty,nb.barang_id
          ORDER BY nbe.nama_beauty");
        return $comm->queryAll(true, [':tgl_dari' => $dari, ':tgl_sampai' => $sampai]);
    }
}