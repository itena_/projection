<?php
Yii::import('application.models._base.BaseBank');
class Bank extends BaseBank
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_bank_cash($store = STOREID)
    {
        return Bank::model()->findByPk(SysPrefs::get_val('kas_cabang', $store));
    }
    public static function get_bank_cash_while($store = STOREID)
    {
        return Bank::model()->findByPk(SysPrefs::get_val('kas_cabang_sementara', $store));
    }
/*    public static function get_bank_cash_while_id($store = STOREID)
    {
        $ret = self::get_bank_cash_while($store);
        if ($ret != null) {
            return $ret->bank_id;
        }
        throw new Exception(t('bank.fail.cash', 'app'));
        return -1;
    }
    public static function get_bank_cash_id($store = STOREID)
    {
        $ret = self::get_bank_cash($store);
        if ($ret != null) {
            return $ret->bank_id;
        }
        throw new Exception(t('bank.fail.cash', 'app'));
        return -1;
    }
    public static function get_petty_cash_id($store = STOREID)
    {
        $ret = SysPrefs::get_val('petty_cash', $store);
        if ($ret == null) {
            throw new Exception(t('bank.fail.cash', 'app'));
        }
        return $ret;
    }*/
    public function beforeValidate()
    {
        if ($this->bank_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->bank_id = $uuid;
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        return parent::beforeValidate();
    }
    /*public function is_bank_cash($store = STOREID)
    {
        return self::get_bank_cash_id($store) == $this->bank_id;
    }*/
    public static function get_cash_in($tgl, $bank_id = null, $store = null)
    {
//        $where = "";
//        $param = array(':tgl' => $tgl, ':bank_id' => $this->bank_id);
//        if ($store != null) {
//            $where = "AND ns.store = :store";
//            $param[':store'] = $store;
//        }
//        $comm = Yii::app()->db->createCommand("SELECT
//    IFNULL(SUM(ns.amount), 0) total FROM nscc_bank_trans ns
//    WHERE ns.amount > 0 AND (ns.type_ = 25 OR ns.type_ = 3) AND ns.visible = 1 AND DATE(ns.tgl) = :tgl
//    AND bank_id = :bank_id $where");
//        return $comm->queryScalar($param);
//        $total = 0;
        $kas_in = Kas::get_cash_in($tgl, $bank_id, $store);
        $ulpt = Ulpt::get_ulpt($tgl, $bank_id, $store);
        return $kas_in + $ulpt;
    }
    public static function get_cash_out($tgl, $bank_id = null, $store = null)
    {
//        $where = "";
//        $param = array(':tgl' => $tgl, ':bank_id' => $this->bank_id);
//        if ($store != null) {
//            $where = "AND ns.store = :store";
//            $param[':store'] = $store;
//        }
//        $comm = Yii::app()->db->createCommand("SELECT
//    IFNULL(SUM(ns.amount), 0) total FROM nscc_bank_trans ns
//    WHERE ns.amount < 0 AND (ns.type_ = 26 OR ns.type_ = 2) AND ns.visible = 1 AND DATE(ns.tgl) = :tgl
//    AND bank_id = :bank_id $where");
//        return $comm->queryScalar($param);
        $kas_out = Kas::get_cash_out($tgl, $bank_id, $store);
        $rpj = Ulpt::get_rpj($tgl, $bank_id, $store);
        return $kas_out + $rpj;
    }
    public function get_total_sales_payment($tgl, $store = STOREID)
    {
        $where = "";
        $param = array(':tgl' => $tgl, ':bank' => $this->bank_id);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
//        $amount = self::is_bank_cash() ? "np.amount - np.kembali" : "np.amount";
        $comm = Yii::app()->db->createCommand("SELECT
	IFNULL(SUM(np.amount - np.kembali), 0) total
    FROM nscc_salestrans ns
	INNER JOIN nscc_payment np
	 ON ns.salestrans_id = np.salestrans_id
    WHERE ns.bruto >= 0 AND DATE(ns.tgl) = :tgl AND np.bank_id = :bank $where");
        return $comm->queryScalar($param);
    }
    public function get_total_returnsales_payment_bank($tgl, $store = STOREID)
    {
        $where = "";
        $param = array(':tgl' => $tgl, ':bank' => $this->bank_id);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
	IFNULL(SUM(np.amount- np.kembali), 0) total
    FROM nscc_salestrans ns
	INNER JOIN nscc_payment np
	 ON ns.salestrans_id = np.salestrans_id
    WHERE ns.bruto < 0 AND DATE(ns.tgl) = :tgl AND np.bank_id = :bank $where");
        return $comm->queryScalar($param);
    }

    public static function get_minimal($bank_id)
    {
	    $comm = Yii::app()->db->createCommand("SELECT minimum FROM nscc_bank WHERE bank_id = :bank_id");
	    return $comm->queryScalar(array(':bank_id' => $bank_id));
    }
     /**
     * After save attributes
     */
   /*  protected function afterSave() {
        parent::afterSave();
        U::runCommand('bank', '--id=' . $this->bank_id,  'bank_'.$this->bank_id.'.log');        
    } */

}