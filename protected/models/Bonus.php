<?php
/*
 * Bonus Aishaderm
 * ---------------
 * 
 * dari transaksi 'Sales' :
 * 1. Kasir/FO
 * 2. Beauty (combo beauty di sales header)
 * 3. Dokter Peresepan (combo dokter di sales header)
 * 4. Apotik
 * 5. Stocker
 * 6. Admin
 * 7. Pengelola cabang (PC/kacab)
 * 
 * dari transaksi 'Edit Service' :
 * 8. Dokter Tindakan
 * 9. Beauty perawatan/assisten
 * 
 * 
 * Nilai dasar perhitungan bonus yaitu :
 *  quantity * harga - diskon
 * tetapi jika diskon adalah 100%, maka nilai dasar perhitungan bonus yaitu :
 *  quantity * bonus
 * 
 * besar bonus dperoleh dari :
 * Nilai dasar perhitungan bonus * persen_bonus
 * 
 * 'persen_bonus' diambil dari table 'nscc_bonus_jual'
 * 
 */

Yii::import('application.models._base.BaseBonus');
class Bonus extends BaseBonus
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->bonus_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->bonus_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    
    public static function getDataPersenBonus($barang_id_id, $store, $bonus_name_id = NULL)
    {
        $where = "";
        $param = array(
                ":barang_id"=>$barang_id_id,
                ":store"=>$store
            );
        
        if($bonus_name_id){
            $where .= "AND bonus_name_id = :bonus_name_id";
            $param[':bonus_name_id'] = $bonus_name_id;
        }
        
        return BonusJual::model()->findAll(
            "barang_id = :barang_id AND store = :store $where",
            $param
        );
    }
    
    public static function deleteDataBonus($salestrans_details, $tipe_trans, $store = STOREID)
    {
        Bonus::model()->updateAll(
            array('visible' => 0),
            'trans_no = :trans_no AND tipe_trans = :tipe_trans AND store = :store',
            array(
                ':trans_no' => $salestrans_details,
                ':tipe_trans' => $tipe_trans,
                ':store' => $store
            )
        );
    }
    
    public static function simpan($tipe_trans, $type_, $salestrans, $salestrans_detail, $bonusJual, $employee_id, $employeeCount = 1)
    {
        $model = new Bonus;
        $model->employee_id = $employee_id;
        $model->bonus_jual_id = $bonusJual->bonus_jual_id;
        $model->persen_bonus = $bonusJual->persen_bonus;

        /*
         * hitung nilai dasar penghitungan bonus :
         *      qty * harga - discount
         * jika discount 100%, maka :
         *      qty * harga
         */
        //$amount = $salestrans_detail->bruto - $salestrans_detail->discrp;
        $amount = $salestrans_detail->total;
        $model->amount = $amount <= 0 ? $salestrans_detail->bruto : $amount;

        //hitung bonus
        $model->amount_bonus = round(($model->amount * $model->persen_bonus / 100) / $employeeCount, 2);

        //jika nominal bonus = 0, maka tidak disimpan
        if($model->amount_bonus == 0) return FALSE;

        $model->tgl = $salestrans->tgl;
        $model->type_ = $type_;
        $model->trans_no = $salestrans_detail->salestrans_details;
        $model->barang_id = $salestrans_detail->barang_id;
        $model->store = $salestrans->store;
        $model->tipe_trans = $tipe_trans;

        if ($model->save()) {
            return TRUE;
        } else {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bonus')) . CHtml::errorSummary($model));
        }
        
    }
    public static function simpan_bonus_create($tipe_trans, $type_, $salestrans, $salestrans_detail, $bonusJual, $employee_id, $employeeCount = 1)
    {
        $model = new Bonus;
        $model->employee_id = $employee_id;
        $model->bonus_jual_id = $bonusJual->bonus_jual_id;
        $model->persen_bonus = $bonusJual->persen_bonus;
        $model->id_user = $salestrans->user_id;

        /*
         * hitung nilai dasar penghitungan bonus :
         *      qty * harga - discount
         * jika discount 100%, maka :
         *      qty * harga
         */
        //$amount = $salestrans_detail->bruto - $salestrans_detail->discrp;
        //$amount = $salestrans_detail->total;
        //lebih aman bruto - total_pot karena untuk diskon amount tidak terecord di kolom total
	    $amount = $salestrans_detail->bruto - $salestrans_detail->total_pot;
        $model->amount = $amount <= 0 ? $salestrans_detail->bruto : $amount;

        //hitung bonus
        $model->amount_bonus = round(($model->amount * $model->persen_bonus / 100) / $employeeCount, 2);

        //jika nominal bonus = 0, maka tidak disimpan
        if($model->amount_bonus == 0) return FALSE;

        $model->tgl = $salestrans->tgl;
        $model->type_ = $type_;
        $model->trans_no = $salestrans_detail->salestrans_details;
        $model->barang_id = $salestrans_detail->barang_id;
        $model->store = $salestrans->store;
        $model->tipe_trans = $tipe_trans;

        if ($model->save()) {
            return TRUE;
        } else {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bonus')) . CHtml::errorSummary($model));
        }
        
    }
    
    public static function sales($type_, $salestrans, $salestrans_detail)
    {
        $uemp = UserEmployee::model()->findByAttributes(array('id' => Yii::app()->user->getId()));
        $emp = Employee::model()->findByAttributes(array('employee_id' => $uemp->employee_id));
        /*
        * Untuk Metode Bonus 1
        * Supaya Finance Pusat bisa melakukan transaksi sales
        * 9 ada di nscc_tipe_employee
        */
        if($emp->tipe == EMPLOYEE_FINANCE){
            return;
        }
        $bonusJuals = self::getDataPersenBonus($salestrans_detail->barang_id, $salestrans->store);
        
        /*
         * menyiapkan employee_id
         */
        $kasir_id = UserEmployee::model()->find("id = :id", array(":id"=>$salestrans->user_id))->employee_id;
        $beauty_id = $salestrans->beauty_id;
        $dokter_peresepan_id = $salestrans->dokter_id;
        $apoteker = AssignEmployeesView::onDuty(AssignEmployeesView::APT);
        $stocker = AssignEmployeesView::onDuty(AssignEmployeesView::STK);
        $admin = AssignEmployeesView::onDuty(AssignEmployeesView::ADM);
        $kacab = AssignEmployeesView::onDuty(AssignEmployeesView::PC);
        
//        cek apakah user terdaftar di assign employee
        $ae = AssignEmployeesView::model()->findByAttributes(array('employee_id' => $kasir_id));
        
        foreach ($bonusJuals as $bonusJual) {
            switch ($bonusJual->bonus_name_id){
                case '1': /* KASIR/FO */
                    if(!$kasir_id) continue;
                    if($ae) continue;
                    self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $kasir_id) ;
                    break;
                case '2': /* BEAUTY UPSELLING */
                    if(!$beauty_id) continue;
                    self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $beauty_id);
                    break;
                case '3': /* DOKTER PERESEPAN */
                    if(!$dokter_peresepan_id) continue;
                    self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $dokter_peresepan_id);
                    break;
                case '4': /* APOTEKER */
                    foreach ($apoteker as $apt) {
                        self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $apt->employee_id, count($apoteker));
                    }
                    break;
                case '5': /* STOCKER */
                    foreach ($stocker as $stk) {
                        self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $stk->employee_id, count($stocker));
                    }
                    break;
                case '6': /* ADMIN */
                    foreach ($admin as $adm) {
                        self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $adm->employee_id, count($admin));
                    }
                    break;
                case '7': /* PENGELOLA CABANG */
                    foreach ($kacab as $pc) {
                        self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $pc->employee_id, count($kacab));
                    }
                    break;
                default : continue;
            }
        }
    }
    
    public static function sales_bonus_create($type_, $salestrans, $salestrans_detail, $tgl)
    {
        $uemp = UserEmployee::model()->findByAttributes(array('id' => $salestrans->user_id));
        $emp = Employee::model()->findByAttributes(array('employee_id' => $uemp->employee_id));
        /*
        * Untuk Metode Bonus 1
        * Supaya Finance Pusat bisa melakukan transaksi sales
        * 9 ada di nscc_tipe_employee
        */
        if($emp->tipe == EMPLOYEE_FINANCE){
            return;
        }
//        var_dump($salestrans_detail);
        $bonusJuals = self::getDataPersenBonus($salestrans_detail->barang_id, $salestrans->store);
        
        /*
         * menyiapkan employee_id
         */
        $kasir_id = UserEmployee::model()->find("id = :id", array(":id"=>$salestrans->user_id))->employee_id;
        $beauty_id = $salestrans->beauty_id;
        $dokter_peresepan_id = $salestrans->dokter_id;
        $apoteker = AssignEmployeesView::onDateDuty(AssignEmployeesView::APT, $tgl);
        $stocker = AssignEmployeesView::onDateDuty(AssignEmployeesView::STK, $tgl);
        $admin = AssignEmployeesView::onDateDuty(AssignEmployeesView::ADM, $tgl);
        $kacab = AssignEmployeesView::onDateDuty(AssignEmployeesView::PC, $tgl);
        
//        cek apakah user terdaftar di assign employee
        $ae = AssignEmployeesView::model()->findByAttributes(array('employee_id' => $kasir_id));
        
        foreach ($bonusJuals as $bonusJual) {
            switch ($bonusJual->bonus_name_id){
                case '1': /* KASIR/FO */
                    if(!$kasir_id) continue;
                    if($ae) continue;
                    self::simpan_bonus_create('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $kasir_id) ;
                    break;
                case '2': /* BEAUTY UPSELLING */
                    if(!$beauty_id) continue;
                    self::simpan_bonus_create('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $beauty_id);
                    break;
                case '3': /* DOKTER PERESEPAN */
                    if(!$dokter_peresepan_id) continue;
                    self::simpan_bonus_create('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $dokter_peresepan_id);
                    break;
                case '4': /* APOTEKER */
                    foreach ($apoteker as $apt) {
                        self::simpan_bonus_create('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $apt->employee_id, count($apoteker));
                    }
                    break;
                case '5': /* STOCKER */
                    foreach ($stocker as $stk) {
                        self::simpan_bonus_create('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $stk->employee_id, count($stocker));
                    }
                    break;
                case '6': /* ADMIN */
                    foreach ($admin as $adm) {
                        self::simpan_bonus_create('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $adm->employee_id, count($admin));
                    }
                    break;
                case '7': /* PENGELOLA CABANG */
                    foreach ($kacab as $pc) {
                        self::simpan_bonus_create('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $pc->employee_id, count($kacab));
                    }
                    break;
                default : continue;
            }
        }
    }
    
    public static function salesRetur($type_, $salestrans_detail, $salesRetur, $salesReturDetail)
    {
        $bonusSales = Bonus::model()->findAll(
                "type_ = :type_ AND trans_no = :trans_no AND store = :store AND visible = 1",
                array(
                    ":type_" => PENJUALAN,
                    ":trans_no" => $salestrans_detail->salestrans_details,
                    ":store" => $salesRetur->store
                )
            );
        
        foreach ($bonusSales as $bns) {
            $modelBonus = new Bonus;
            $modelBonus->attributes = $bns->attributes;
            
            $modelBonus->bonus_id = NULL;
            $modelBonus->tdate = NULL;
            $modelBonus->id_user = NULL;
            
            $modelBonus->type_ = $type_;
            $modelBonus->trans_no = $salesReturDetail->salestrans_details;
            $modelBonus->amount = -$modelBonus->amount;
            $modelBonus->amount_bonus = -$modelBonus->amount_bonus;
            
            if (!$modelBonus->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bonus')) . CHtml::errorSummary($modelBonus));
            }
        }
    }
    
    public static function serviceDokter($type_, $salestrans, $salestrans_detail, $employee_id)
    {
        $bonusJuals = self::getDataPersenBonus($salestrans_detail->barang_id, $salestrans->store, '8');
        
        foreach ($bonusJuals as $bonusJual) {
            if ($bonusJual->bonus_name_id == '8') { //DOKTER TINDAKAN
                self::simpan('service', $type_, $salestrans, $salestrans_detail, $bonusJual, $employee_id);
            }
        }
    }
    
    public static function serviceBeauty($type_, $salestrans, $salestrans_detail, $employee_id)
    {
        $bonusJuals = self::getDataPersenBonus($salestrans_detail->barang_id, $salestrans->store, '9');
        
        foreach ($bonusJuals as $bonusJual) {
            if ($bonusJual->bonus_name_id == '9') { //BEAUTY
                self::simpan('service', $type_, $salestrans, $salestrans_detail, $bonusJual, $employee_id);
            }
        }
    }
    public static function serviceDokterCreate($type_, $salestrans, $salestrans_detail, $employee_id)
    {
        $bonusJuals = self::getDataPersenBonus($salestrans_detail->barang_id, $salestrans->store, '8');
        
        foreach ($bonusJuals as $bonusJual) {
            if ($bonusJual->bonus_name_id == '8') { //DOKTER TINDAKAN
                self::simpan_bonus_create('service', $type_, $salestrans, $salestrans_detail, $bonusJual, $employee_id);
            }
        }
    }
    
    public static function serviceBeautyCreate($type_, $salestrans, $salestrans_detail, $employee_id)
    {
        $bonusJuals = self::getDataPersenBonus($salestrans_detail->barang_id, $salestrans->store, '9');
        
        foreach ($bonusJuals as $bonusJual) {
            if ($bonusJual->bonus_name_id == '9') { //BEAUTY
                self::simpan_bonus_create('service', $type_, $salestrans, $salestrans_detail, $bonusJual, $employee_id);
            }
        }
    }
}

/*
 * Sebelum menggunakan model bonus yg di ambil dari Assign Employee,
 * masih menggunakan mode bonus yang ambil dari sys pref
 */

//Yii::import('application.models._base.BaseBonus');
//class Bonus extends BaseBonus
//{
//    public static function model($className = __CLASS__)
//    {
//        return parent::model($className);
//    }
//    public function beforeValidate()
//    {
//        if ($this->bonus_id == null) {
//            $command = $this->dbConnection->createCommand("SELECT UUID();");
//            $uuid = $command->queryScalar();
//            $this->bonus_id = $uuid;
//        }
//        if ($this->tdate == null) {
//            $this->tdate = new CDbExpression('NOW()');
//        }
//        if ($this->id_user == null) {
//            $this->id_user = Yii::app()->user->getId();
//        }
//        return parent::beforeValidate();
//    }
//    
//    public static function getDataPersenBonus($barang_id_id, $store, $bonus_name_id = NULL)
//    {
//        $where = "";
//        $param = array(
//                ":barang_id"=>$barang_id_id,
//                ":store"=>$store
//            );
//        
//        if($bonus_name_id){
//            $where .= "AND bonus_name_id = :bonus_name_id";
//            $param[':bonus_name_id'] = $bonus_name_id;
//        }
//        
//        return BonusJual::model()->findAll(
//            "barang_id = :barang_id AND store = :store $where",
//            $param
//        );
//    }
//    
//    public static function deleteDataBonus($salestrans_details, $tipe_trans, $store = STOREID)
//    {
//        Bonus::model()->updateAll(
//            array('visible' => 0),
//            'trans_no = :trans_no AND tipe_trans = :tipe_trans AND store = :store',
//            array(
//                ':trans_no' => $salestrans_details,
//                ':tipe_trans' => $tipe_trans,
//                ':store' => $store
//            )
//        );
//    }
//    
//    public static function simpan($tipe_trans, $type_, $salestrans, $salestrans_detail, $bonusJual, $employee_id)
//    {
//        $model = new Bonus;
//        $model->employee_id = $employee_id;
//        $model->bonus_jual_id = $bonusJual->bonus_jual_id;
//        $model->persen_bonus = $bonusJual->persen_bonus;
//
//        /*
//         * hitung nilai dasar penghitungan bonus :
//         *      qty * harga - discount
//         * jika discount 100%, maka :
//         *      qty * harga
//         */
//        //$amount = $salestrans_detail->bruto - $salestrans_detail->discrp;
//        $amount = $salestrans_detail->total;
//        $model->amount = $amount <= 0 ? $salestrans_detail->bruto : $amount;
//
//        //hitung bonus
//        $model->amount_bonus = round($model->amount * $model->persen_bonus / 100, 2);
//
//        //jika nominal bonus = 0, maka tidak disimpan
//        if($model->amount_bonus == 0) return FALSE;
//
//        $model->tgl = $salestrans->tgl;
//        $model->type_ = $type_;
//        $model->trans_no = $salestrans_detail->salestrans_details;
//        $model->barang_id = $salestrans_detail->barang_id;
//        $model->store = $salestrans->store;
//        $model->tipe_trans = $tipe_trans;
//
//        if ($model->save()) {
//            return TRUE;
//        } else {
//            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bonus')) . CHtml::errorSummary($model));
//        }
//        
//    }
//    
//    public static function sales($type_, $salestrans, $salestrans_detail)
//    {
//        $uemp = UserEmployee::model()->findByAttributes(array('id' => Yii::app()->user->getId()));
//        $emp = Employee::model()->findByAttributes(array('employee_id' => $uemp->employee_id));
//        /*
//        * Untuk Metode Bonus 1
//        * Supaya Finance Pusat bisa melakukan transaksi sales
//        * 9 ada di nscc_tipe_employee
//        */
//        if($emp->tipe == EMPLOYEE_FINANCE){
//            return;
//        }
//        $bonusJuals = self::getDataPersenBonus($salestrans_detail->barang_id, $salestrans->store);
//        
//        /*
//         * menyiapkan employee_id
//         */
//        $kasir = UserEmployee::model()->find("id = :id", array(":id"=>$salestrans->user_id))->employee_id;
//        $beauty = $salestrans->beauty_id;
//        $dokter_peresepan = $salestrans->dokter_id;
//        $apoteker_id = $salestrans->apoteker_id;
//        $stocker_id = $salestrans->stocker_id;
//        $admin = $salestrans->admin_id;
//        $kacab = $salestrans->pc_id;
//        
//        foreach ($bonusJuals as $bonusJual) {
//            switch ($bonusJual->bonus_name_id){
//                case '1': /* KASIR/FO */
//                    if(!$kasir) continue;
//                    self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $kasir) ;
//                    break;
//                case '2': /* BEAUTY UPSELLING */
//                    if(!$beauty) continue;
//                    self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $beauty);
//                    break;
//                case '3': /* DOKTER PERESEPAN */
//                    if(!$dokter_peresepan) continue;
//                    self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $dokter_peresepan);
//                    break;
//                case '4': /* APOTEKER */
//                    if(!$apoteker_id) continue;
//                    self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $apoteker_id);
//                    break;
//                case '5': /* STOCKER */
//                    if(!$stocker_id) continue;
//                    self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $stocker_id);
//                    break;
//                case '6': /* ADMIN */
//                    if(!$admin) continue;
//                    self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $admin);
//                    break;
//                case '7': /* PENGELOLA CABANG */
//                    if(!$kacab) continue;
//                    self::simpan('sales', $type_, $salestrans, $salestrans_detail, $bonusJual, $kacab);
//                    break;
//                default : continue;
//            }
//        }
//    }
//    
//    public static function salesRetur($type_, $salestrans_detail, $salesRetur, $salesReturDetail)
//    {
//        $bonusSales = Bonus::model()->findAll(
//                "type_ = :type_ AND trans_no = :trans_no AND store = :store AND visible = 1",
//                array(
//                    ":type_" => PENJUALAN,
//                    ":trans_no" => $salestrans_detail->salestrans_details,
//                    ":store" => $salesRetur->store
//                )
//            );
//        
//        foreach ($bonusSales as $bns) {
//            $modelBonus = new Bonus;
//            $modelBonus->attributes = $bns->attributes;
//            
//            $modelBonus->bonus_id = NULL;
//            $modelBonus->tdate = NULL;
//            $modelBonus->id_user = NULL;
//            
//            $modelBonus->type_ = $type_;
//            $modelBonus->trans_no = $salesReturDetail->salestrans_details;
//            $modelBonus->amount = -$modelBonus->amount;
//            $modelBonus->amount_bonus = -$modelBonus->amount_bonus;
//            
//            if (!$modelBonus->save()) {
//                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bonus')) . CHtml::errorSummary($modelBonus));
//            }
//        }
//    }
//    
//    public static function serviceDokter($type_, $salestrans, $salestrans_detail, $employee_id)
//    {
//        $bonusJuals = self::getDataPersenBonus($salestrans_detail->barang_id, $salestrans->store, '8');
//        
//        foreach ($bonusJuals as $bonusJual) {
//            if ($bonusJual->bonus_name_id == '8') { //DOKTER TINDAKAN
//                self::simpan('service', $type_, $salestrans, $salestrans_detail, $bonusJual, $employee_id);
//            }
//        }
//    }
//    
//    public static function serviceBeauty($type_, $salestrans, $salestrans_detail, $employee_id)
//    {
//        $bonusJuals = self::getDataPersenBonus($salestrans_detail->barang_id, $salestrans->store, '9');
//        
//        foreach ($bonusJuals as $bonusJual) {
//            if ($bonusJual->bonus_name_id == '9') { //BEAUTY
//                self::simpan('service', $type_, $salestrans, $salestrans_detail, $bonusJual, $employee_id);
//            }
//        }
//    }
//}