<?php
Yii::import('application.models._base.BasePurchaseCoaDetail');

class PurchaseCoaDetail extends BasePurchaseCoaDetail
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->purchase_coa_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->purchase_coa_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}