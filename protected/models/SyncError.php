<?php
Yii::import('application.models._base.BaseSyncError');

class SyncError extends BaseSyncError
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->sync_id_error == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sync_id_error = $uuid;
        }
        return parent::beforeValidate();
    }
}