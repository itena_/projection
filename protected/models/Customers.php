<?php
Yii::import('application.models._base.BaseCustomers');
class Customers extends BaseCustomers
{
    public function beforeValidate()
    {
        if ($this->customer_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->customer_id = $uuid;
        }
        if ($this->no_customer == null) {
            if ($this->store == null) {
                $this->store = STOREID;
            }
            $this->no_customer = Customers::generete_no_customer($this->store);
        }
        return parent::beforeValidate();
    }
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function generete_no_customer($store)
    {
        $res = Yii::app()->db->createCommand("
        SELECT IFNULL(MAX(CAST(REPLACE(nc.no_customer,:store,'') AS UNSIGNED)),0)
        FROM nscc_customers nc WHERE nc.store = :store");
        $counter = $res->queryScalar(array(':store' => $store));
        $counter++;
        return $counter . $store;
    }
    public static function get_backup()
    {
        $res = Yii::app()->db->createCommand("
        SELECT nc.no_customer AS nobase,nc.nama_customer AS nama_pasien,
  nc.alamat, nc.customer_id noax, nkota.nama_kota AS kota,
  nc.telp, DATE(nc.awal) awal,
  if(nc.sex = 'MALE','PRIA','WANITA') AS jkel,
  nc.tgl_lahir AS tgl_lhr, nc.kerja AS pekerjaan
FROM nscc_customers nc
  LEFT JOIN nscc_kecamatan nk
    ON nc.kecamatan_id = nk.kecamatan_id
  LEFT JOIN nscc_kota nkota
    ON nk.kota_id = nkota.kota_id
    WHERE nc.log = 0 AND nc.store = :store");
        return $res->queryAll(true, array(':store' => STOREID));
    }
    protected function afterSave()
    {
        parent::afterSave();
        if (UPLOAD_NARS) {
            U::runCommandNars('Pasien', '--id=' . $this->customer_id);
        }
    }
	public function total_piutang_before($from,$store = null)
	{
		$where = "WHERE ";
		$param = array(':from' => $from);
		$param[':customer_id'] = $this->customer_id;
		if ($store != null) {
			$where = "WHERE nti.store_kode = :store AND ";
			$param[':store'] = $store;
		}
		$res = app()->db->createCommand("SELECT SUM(c.total) FROM (
(SELECT SUM(nti.total) total
	FROM nscc_payment_journal AS nti 
    $where nti.customer_id = :customer_id AND nti.tgl < :from)
UNION
(SELECT SUM(-nti1.total) FROM nscc_payment_journal AS nti
				INNER JOIN nscc_payment_journal AS nti1 ON nti.doc_ref = nti1.doc_ref_other #AND nti1.type_ = 1
        $where nti.customer_id = :customer_id AND nti1.tgl < :from)
UNION
(SELECT -SUM(npud.kas_dibayar) total
FROM nscc_payment_journal AS nti
  INNER JOIN nscc_pembantu_pelunasan_piutang_detil AS npud
			ON nti.payment_journal_id = npud.payment_journal_id
  INNER JOIN nscc_pembantu_pelunasan_piutang_detil AS npu
			ON npud.pembantu_pelunasan_piutang_id = npu.pembantu_pelunasan_piutang_id 
INNER JOIN nscc_pembantu_pelunasan_piutang AS npp
			ON npud.pembantu_pelunasan_piutang_id = npp.pembantu_pelunasan_piutang_id
            $where nti.customer_id = :customer_id
			 AND npp.tgl < :from)) c");
		return $res->queryScalar($param);
	}
	public function get_pelunasan($from,$to,$store = null){
		$where = "WHERE ";
		$param = array(':from' => $from,':to'=>$to, 'customer_id' => $this->customer_id);
		if ($store != null) {
			$where = "WHERE nti.store_kode = :store AND ";
			$param[':store'] = $store;
		}
		$res = app()->db->createCommand("SELECT * FROM ((SELECT nti.tgl,nti.doc_ref,'DEBT' note,nti.doc_ref_other no_faktur,
        nti.total piutang,0 payment,nti.total,nti.tdate FROM nscc_payment_journal AS nti
        $where
        nti.customer_id = :customer_id #AND nti.type_ = 0 
				AND nti.tgl >= :from AND nti.tgl <= :to
        )
        UNION
        (SELECT nti1.tgl,nti1.doc_ref,'PAYEMNT' note,nti.doc_ref_other nscc_payment_journal,
        0 piutang,-nti1.total payment,nti1.total,nti1.tdate FROM nscc_payment_journal AS nti
				INNER JOIN nscc_payment_journal AS nti1 ON nti.doc_ref = nti1.doc_ref_other #AND nti1.type_ = 1
        $where
        nti.customer_id = :customer_id
				AND nti.tgl >= :from AND nti.tgl <= :to
        )
        UNION
        (SELECT npp.tgl,npp.doc_ref,'PAYEMNT' note,nti.doc_ref_other no_faktur,0 piutang,npud.kas_dibayar payment,
        -npud.kas_dibayar total,npp.tdate FROM nscc_payment_journal AS nti
        INNER JOIN nscc_pembantu_pelunasan_piutang_detil AS npud
			ON nti.payment_journal_id = npud.payment_journal_id
        INNER JOIN nscc_pembantu_pelunasan_piutang_detil AS npu
			ON npud.pembantu_pelunasan_piutang_id = npu.pembantu_pelunasan_piutang_id
				INNER JOIN nscc_pembantu_pelunasan_piutang AS npp
			ON npud.pembantu_pelunasan_piutang_id = npp.pembantu_pelunasan_piutang_id
        $where nti.customer_id = :customer_id
				AND nti.tgl >= :from AND nti.tgl <= :to
        )) AS a
        ORDER BY no_faktur,note,tgl");

//        $res = app()->db->createCommand("SELECT * FROM ((SELECT nti.tgl,nti.doc_ref,'DEBT' note,nti.doc_ref_other no_faktur,
//        nti.total hutang,0 payment,nti.total,nti.tdate FROM nscc_transfer_item AS nti $where
//        nti.supplier_id = :supplier_id AND nti.type_ = 0 AND nti.tgl >= :from AND nti.tgl <= :to)
//        UNION
//        (SELECT nti1.tgl,nti1.doc_ref,'PAYEMNT' note,nti.doc_ref_other no_faktur,
//        0 hutang,-nti1.total payment,nti1.total,nti1.tdate FROM nscc_transfer_item AS nti
//				INNER JOIN nscc_transfer_item AS nti1 ON nti.doc_ref = nti1.doc_ref_other AND nti1.type_ = 1
//        $where nti.supplier_id = :supplier_id AND nti1.tgl >= :from AND nti1.tgl <= :to)
//        UNION
//        (SELECT npu.tgl,npu.doc_ref,'PAYEMNT' note,nti.doc_ref_other no_faktur,0 hutang,npud.kas_dibayar payment,
//        -npud.kas_dibayar total,npu.tdate FROM nscc_transfer_item AS nti
//        INNER JOIN nscc_pelunasan_utang_detil AS npud
//			ON nti.transfer_item_id = npud.transfer_item_id
//        INNER JOIN nscc_pelunasan_utang AS npu
//			ON npud.pelunasan_utang_id = npu.pelunasan_utang_id
//        $where nti.supplier_id = :supplier_id AND npu.tgl >= :from AND npu.tgl <= :to)) AS a
//        ORDER BY tgl,tdate");
		return $res->queryAll(true,$param);
	}
}