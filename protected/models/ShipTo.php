<?php

Yii::import('application.models._base.BaseShipTo');

class ShipTo extends BaseShipTo {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeValidate() {
        if ($this->ship_to_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->ship_to_id = $uuid;
        }
        return parent::beforeValidate();
    }

}
