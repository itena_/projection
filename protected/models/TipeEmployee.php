<?php
Yii::import('application.models._base.BaseTipeEmployee');
class TipeEmployee extends BaseTipeEmployee
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->tipe_employee_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->tipe_employee_id = $uuid;
        }
        return parent::beforeValidate();
    }
}