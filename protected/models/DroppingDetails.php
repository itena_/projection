<?php
Yii::import('application.models._base.BaseDroppingDetails');

class DroppingDetails extends BaseDroppingDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->dropping_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->dropping_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
    static function get_details_to_print($dropping_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                b.kode_barang,
                b.nama_barang,
                tid.qty,
                b.sat
            FROM nscc_dropping_details tid
                LEFT JOIN nscc_barang b ON b.barang_id = tid.barang_id
            WHERE 
                tid.dropping_id = :dropping_id
                AND tid.visible = 1
                ORDER BY b.kode_barang ASC
        ");
        return $comm->queryAll(true, array(':dropping_id' => $dropping_id));
    }

}