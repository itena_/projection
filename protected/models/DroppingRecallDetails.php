<?php
Yii::import('application.models._base.BaseDroppingRecallDetails');

class DroppingRecallDetails extends BaseDroppingRecallDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->dropping_recall_details_id == null) {
            $this->dropping_recall_details_id = $this->dbConnection->createCommand("SELECT UUID();")->queryScalar();
        }
        return parent::beforeValidate();
    }
}