<?php

Yii::import('application.models._base.BasePayMethod');

class PayMethod extends BasePayMethod
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->pay_method_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pay_method_id = $uuid;
        }
        return parent::beforeValidate();
    }
}