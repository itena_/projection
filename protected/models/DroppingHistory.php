<?php
Yii::import('application.models._base.BaseDroppingHistory');

class DroppingHistory extends BaseDroppingHistory
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function beforeValidate(){
        if ($this->id_history == null) {
            $this->id_history = $this->dbConnection->createCommand("SELECT UUID();")->queryScalar();
            $this->user_id = Yii::app()->user->getId();
            $this->tgl = $this->dbConnection->createCommand("SELECT NOW();")->queryScalar();
        }


        return parent::beforeValidate();
    }

    public static function add($order_dropping_id, $doc_ref, $store, $store_pengirim, $status, $desc){
        $namauser = Yii::app()->user->getName();
        $iduser = Yii::app()->user->getId();

        $history = new self();
        $history->order_dropping_id = $order_dropping_id;
        $history->user = $namauser;
        $history->user_id = $iduser;
        $history->doc_ref = $doc_ref;
        $history->store = $store;
        $history->store_pengirim = $store_pengirim;
        $history->status = $status;
        $history->tgl = new CDbExpression('NOW()');
        $history->desc = $desc;

        if (!$history->save())
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'DroppingHistory')) . CHtml::errorSummary($history));
        else
            return $history;
    }
}