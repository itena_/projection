<?php
Yii::import('application.models._base.BaseBarangAsset');

class BarangAsset extends BaseBarangAsset
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->barang_asset_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->barang_asset_id = $uuid;
        }
        return parent::beforeValidate();
    }
}