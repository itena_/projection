<?php
Yii::import('application.models._base.BaseAssignEmployeesView');

class AssignEmployeesView extends BaseAssignEmployeesView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/*
	 * tipe_employee_id
	 */
	const APT = 'f7b9c38c-92a2-11e7-baa7-507b9d297990';
	const PC = 'd1ef09d5-92a2-11e7-baa7-507b9d297990';
	const ADM = 'c7b04ec8-92a2-11e7-baa7-507b9d297990';
	const STK = '00f20248-92a3-11e7-baa7-507b9d297990';

	public static function onDuty($tipe_employee_id){
        return self::model()->findAll( "tipe_employee_id = :tipe_employee_id", array(':tipe_employee_id' => $tipe_employee_id));
    }
	public static function onDateDuty($tipe_employee_id, $tgl){
        return AssignEmployees::model()->findAllByAttributes(array(
            'tipe_employee_id' => $tipe_employee_id,
            'tgl' => $tgl
        ));
    }

}