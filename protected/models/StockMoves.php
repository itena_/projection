<?php
Yii::import('application.models._base.BaseStockMoves');
class StockMoves extends BaseStockMoves
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->stock_moves_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->stock_moves_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        if ($this->visible == null) {
            $this->visible = 1;
        }
        return parent::beforeValidate();
    }
    public static function get_saldo_item_before($kode_barang, $tgl, $store = "")
    {
        $where = "";
        $param = array(':kode_barang' => $kode_barang, ':tgl' => $tgl);
        if ($store != null) {
            $where = "AND nsm.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nsm.qty),0) FROM nscc_stock_moves AS nsm
        INNER JOIN nscc_barang nb ON nsm.barang_id = nb.barang_id
        WHERE  nb.kode_barang = :kode_barang AND nsm.tran_date < :tgl AND nsm.visible = 1 $where");
        return $comm->queryScalar($param);
    }
    public static function get_saldo_item_grup($barang_id, $store)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nsm.qty),0), g.kategori_id
        FROM nscc_stock_moves AS nsm
        LEFT JOIN nscc_barang b on b.barang_id = nsm.barang_id
        LEFT JOIN nscc_grup g on g.grup_id = b.grup_id
        WHERE nsm.barang_id = :barang_id AND nsm.visible = 1 AND nsm.store = :store");
        return $comm->queryAll(true,array(':barang_id' => $barang_id,
            ':store' => $store));
    }
    public static function get_saldo_item($barang_id, $store)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nsm.qty),0) FROM nscc_stock_moves AS nsm
        WHERE nsm.barang_id = :barang_id AND nsm.visible = 1 AND nsm.store = :store");
        return $comm->queryScalar(array(':barang_id' => $barang_id,
            ':store' => $store));
    }
    public static function get_grup($barang_id)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        kategori_id
        FROM nscc_barang b
        LEFT JOIN nscc_grup g on g.grup_id = b.grup_id
        WHERE b.barang_id = :barang_id");
        return $comm->queryScalar(array(':barang_id' => $barang_id));
    }

    /**
     * Query select : Stock In Transit
     *
     * @return DbCmd
     */
    public static function queryStockInTransit($params = array())
    {
        $cmd = DbCmd::instance()
            ->addSelect(array(
                't.barang_id',
                't.dropping_id',
                't.price',
                'd.doc_ref',
                'DATE_FORMAT(d.tgl, "%d-%b-%Y") tgl',
                'd.tdate',
                'd.store store_pengirim',
                'd.store_penerima',
                'd.doc_ref dropping_doc_ref',
                't.qty qty_dropping',
                'ifnull(t2.qty_received,0) qty_received',
                'qty_recalled' => 'ifnull(t2.qty_recalled,0) qty_recalled',
                        /* qty_dropping - qty_received - qty_recalled*/
                'qty' => '(t.qty-ifnull(t2.qty_received,0)-ifnull(t2.qty_recalled,0)) qty'
            ))
            ->addFrom('{{dropping_details}} t')
            ->addLeftJoin('{{dropping}} d', 'd.dropping_id = t.dropping_id')
            ->addLeftJoin(
                DbCmd::instance()
                    ->addSelect('t1.barang_id, t1.dropping_id, sum(t1.qty_received) qty_received, sum(t1.qty_recalled) qty_recalled')
                    ->addFrom(
                        DbCmd::instance()->union(array(
                            DbCmd::instance()
                                ->addSelect('rdd.barang_id, rd.dropping_id, sum(rdd.qty) qty_received, 0 qty_recalled')
                                ->addFrom('{{receive_dropping_details}} rdd')
                                ->addLeftJoin('{{receive_dropping}} rd', 'rd.receive_dropping_id = rdd.receive_dropping_id')
                                ->addCondition('rdd.visible = 1')
                                ->addGroup('rdd.barang_id, rd.dropping_id')
                            ,
                            DbCmd::instance()
                                ->addSelect('drd.barang_id, drd.dropping_id, 0 qty_received, sum(drd.qty) qty_recalled')
                                ->addFrom('{{dropping_recall_details}} drd')
                                ->addCondition('drd.visible = 1')
                                ->addGroup('drd.barang_id, drd.dropping_id')
                        ))->setAs('t1')
                    )
                    ->addGroup('t1.barang_id, t1.dropping_id')
                    ->setAs('t2')
                , 't2.dropping_id = t.dropping_id AND t2.barang_id = t.barang_id')
            ->addCondition('d.lunas NOT IN (:status_draft, :status_need_shipment)')
            ->addParam(':status_draft', PR_DRAFT)
            ->addParam(':status_need_shipment', PR_NEED_SHIPMENT)
            ->addCondition('t.visible = 1')
            ->addHaving('qty > 0')
            ;

        if (array_key_exists('dropping_doc_ref', $params)) {
            $cmd->addCondition('d.doc_ref LIKE :dropping_doc_ref');
            $cmd->addParam(':dropping_doc_ref', '%'.$params['dropping_doc_ref'].'%');
        }
        if (array_key_exists('store_pengirim', $params)) {
            $cmd->addCondition('d.store LIKE :store_pengirim');
            $cmd->addParam(':store_pengirim', '%'.$params['store_pengirim'].'%');
        }
        if (array_key_exists('store_penerima', $params)) {
            $cmd->addCondition('d.store_penerima LIKE :store_penerima');
            $cmd->addParam(':store_penerima', '%'.$params['store_penerima'].'%');
        }
        if (array_key_exists('barang_id', $params)) {
            $cmd->addCondition('t.barang_id = :barang_id');
            $cmd->addParam(':barang_id', $params['barang_id']);
        }
        if (array_key_exists('dropping_id', $params)) {
            $cmd->addCondition('t.dropping_id = :dropping_id');
            $cmd->addParam(':dropping_id', $params['dropping_id']);
        }

        return $cmd;
    }
    
    public static function push($trans_no = "")
    {
        $arrAttr = array('up' => 0);
        if($trans_no){
            $arrAttr['trans_no'] = $trans_no;
        }
        $models = StockMoves::model()->findAllByAttributes($arrAttr);
        foreach ($models as $model) {
            $model->up = 1;
            if($model->save()){
                U::runCommand('soap','stockmoves', '--id=' . $model->stock_moves_id,  'stockmoves_'.$model->stock_moves_id.'.log');
            }
        }
    }

      /**
     * After save attributes
     */
    protected function afterSave() {
        parent::afterSave();
        //U::runCommand('stockmoves', '--id=' . $this->stock_moves_id,  'protected/runtime/stockmoves_'.$this->stock_moves_id.'.log');        
    }
}