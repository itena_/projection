<?php
Yii::import('application.models._base.BaseCostBarangPerlengkapan');

class CostBarangPerlengkapan extends BaseCostBarangPerlengkapan
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->cost_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->cost_id = $uuid;
        }
        return parent::beforeValidate();
    }
}