<?php

/**
 * Created by PhpStorm.
 * User: wisnu
 * Date: 30/01/2018
 * Time: 8:53
 */

ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);
Yii::import('application.components.SoapClientYii');

class SyncronCommand extends CConsoleCommand
{
    public function actionSyncHistory($cabang,$start,$end)
    {
        try
        {
            $sales = Yii::app()->db->createCommand("select nv.*, sd.*, nb.kode_barang, nb.sat, ng.nama_grup, sd.price as price_item from nscc_narstrans_view nv
                                                        INNER JOIN nscc_salestrans_details as sd on sd.salestrans_id = nv.salestrans_id
                                                        INNER JOIN nscc_barang AS nb ON sd.barang_id = nb.barang_id
                                                    INNER JOIN nscc_grup AS ng ON ng.grup_id = nb.grup_id
                                                    where nv.store = :store and nv.tgl >= :start and nv.tgl <= :end")
                ->queryAll(true, array(":store" => $cabang, ":start" => $start, ":end" => $end ));

            $client = new SoapClient(SOAP_SYNCRON, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            $result = $client->runSyncHistory($sales, $cabang, $start, $end);
            var_dump($result);

            $txt = "\nReturn : ".$result;
            file_put_contents("logsyncscan.log", $txt, FILE_APPEND);
        }
        catch (SoapFault $e)
        {
            var_dump($e->getMessage());
            file_put_contents("logsyncscan.log", $e->getMessage(), FILE_APPEND);
        }
    }

    public function actionSyncPasien($cabang,$start,$end)
    {
        try
        {
            $getPasien = Yii::app()->db->createCommand("select * from nscc_customers nc
                                                            join nscc_salestrans ns on ns.customer_id = nc.customer_id
                                                            where ns.tgl >= '$start' and ns.tgl <= '$end'")->queryAll();

            $client = new SoapClient(SOAP_SYNCRON, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            $result = $client->runSyncPasien($getPasien, $cabang, $start, $end);
            var_dump($result);

            $txt = "\nReturn : ".$result;
            file_put_contents("logsyncscan.log", $txt, FILE_APPEND);
        }
        catch (SoapFault $e)
        {
            var_dump($e->getMessage());
            file_put_contents("logsyncscan.log", $e->getMessage(), FILE_APPEND);
        }
    }

    public function actionSyncGlt($cabang,$start,$end)
    {
        try
        {
            $client = new SoapClient(SOAP_SYNCRON, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            if($cabang == 'All')
            {
                $dbpusat =  Yii::app()->db;
                $getAllStore = $dbpusat->createCommand("SELECT store_code from nscc_sync_status 
                                                    WHERE status_sync <> '3' 
                                                    ORDER BY store_code ASC")->queryAll();

                foreach($getAllStore as $sto)
                {
                    $cab = $sto['store_code'];
                    try
                    {
                        $result = $client->runSyncGlt($cab, $start, $end);
                        var_dump($result);

                        $txt = "\nReturn : ".$result;
                        file_put_contents("logsyncscan.log", $txt, FILE_APPEND);
                    }
                    catch (Exception $e)
                    {
                        var_dump($e->getMessage());
                        continue;
                    }
                }
            }
            else
            {
                $result = $client->runSyncGlt($cabang, $start, $end);
                var_dump($result);

                $txt = "\nReturn : ".$result;
                file_put_contents("logsyncscan.log", $txt, FILE_APPEND);
            }


        }
        catch (SoapFault $e)
        {
            var_dump($e->getMessage());
            file_put_contents("logsyncscan.log", $e->getMessage(), FILE_APPEND);
        }
    }

    public function actionSyncBtr($cabang,$start,$end)
    {
        try
        {
            $client = new SoapClient(SOAP_SYNCRON, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            if($cabang == 'All')
            {
                $dbpusat =  Yii::app()->db;
                $getAllStore = $dbpusat->createCommand("SELECT store_code from nscc_sync_status 
                                                    WHERE status_sync <> '3' 
                                                    ORDER BY store_code ASC")->queryAll();

                foreach($getAllStore as $sto)
                {
                    $cab = $sto['store_code'];
                    try
                    {
                        $result = $client->runSyncBtr($cab, $start, $end);
                        var_dump($result);

                        $txt = "\nReturn : " . $result;
                        file_put_contents("logsyncscan.log", $txt, FILE_APPEND);
                    }
                    catch (Exception $e)
                    {
                        var_dump($e->getMessage());
                        continue;
                    }
                }
            }
            else
            {
                $result = $client->runSyncBtr($cabang, $start, $end);
                var_dump($result);

                $txt = "\nReturn : " . $result;
                file_put_contents("logsyncscan.log", $txt, FILE_APPEND);
            }
        }
        catch (SoapFault $e)
        {
            var_dump($e->getMessage());
            file_put_contents("logsyncscan.log", $e->getMessage(), FILE_APPEND);
        }
    }

    public function actionScan($cabang,$start,$end)
    {
        try
        {
            $client = new SoapClient(SOAP_SYNCRON, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            if($cabang == 'All')
            {
                $dbpusat =  Yii::app()->db;
                $getAllStore = $dbpusat->createCommand("SELECT store_code from nscc_sync_status 
                                                    WHERE status_scan <> '3' 
                                                    ORDER BY store_code ASC")->queryAll();

                foreach($getAllStore as $sto)
                {
                    $cab = $sto['store_code'];

                    try
                    {
                        $result = $client->runScan($cab, $start, $end);
                        var_dump($result);

                        $txt = "\nReturn : ".$result;
                        file_put_contents("logsyncscan.log", $txt, FILE_APPEND);
                    }
                    catch (Exception $e)
                    {
                        var_dump($e->getMessage());
                        continue;
                    }
                }
            }
            else
            {
                $result = $client->runScan($cabang, $start, $end);
                var_dump($result);
                $txt = "\nReturn : ".$result;
                file_put_contents("logsyncscan.log", $txt, FILE_APPEND);
            }
        }
        catch (SoapFault $e)
        {
            var_dump($e->getMessage());
            file_put_contents("logsyncscan.log", $e->getMessage(), FILE_APPEND);
        }
    }


    public function actionSyncCustomer($cabang,$start,$end)
    {
        try
        {
            $getCustomer = Yii::app()->db->createCommand("SELECT * FROM nscc_customers
                                                    WHERE date(awal) >= :start AND date(awal) <= :end")->queryAll(true, array(":start" => $start, ":end" => $end ));

            $client = new SoapClient(SOAP_SYNCRON, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            $result = $client->runSyncCustomer($getCustomer, $cabang, $start, $end);
            var_dump($result);

            $txt = "\nReturn : ".$result;
            file_put_contents("logsyncscan.log", $txt, FILE_APPEND);
        }
        catch (SoapFault $e)
        {
            var_dump($e->getMessage());
            file_put_contents("logsyncscan.log", $e->getMessage(), FILE_APPEND);
        }
    }

    public function actionSyncSls($cabang,$start,$end)
    {
        try
        {
            $client = new SoapClient(SOAP_SYNCRON, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            $result = $client->runSyncScriptSls($cabang, $start, $end);
            var_dump($result);

            $txt = "\nReturn : ".$result;
            file_put_contents("logsyncscan.log", $txt, FILE_APPEND);
        }
        catch (SoapFault $e)
        {
            var_dump($e);
        }
    }



    //NAAVA

    public function actionSyncHistoryNaava($cabang,$start,$end)
    {
        try
        {
            $sales = Yii::app()->db->createCommand("select nv.*, sd.*, nb.kode_barang, nb.sat, ng.nama_grup, sd.price as price_item from nscc_narstrans_view nv
                                                        INNER JOIN nscc_salestrans_details as sd on sd.salestrans_id = nv.salestrans_id
                                                        INNER JOIN nscc_barang AS nb ON sd.barang_id = nb.barang_id
                                                    INNER JOIN nscc_grup AS ng ON ng.grup_id = nb.grup_id
                                                    where nv.store = :store and nv.tgl >= :start and nv.tgl <= :end")
                ->queryAll(true, array(":store" => $cabang, ":start" => $start, ":end" => $end ));

            $client = new SoapClient(SOAP_SYNCRON, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            $result = $client->runSyncHistory($sales, $cabang, $start, $end);
            var_dump($result);

            $txt = "\nReturn : ".$result;
            file_put_contents("logsyncscan.log", $txt, FILE_APPEND);
        }
        catch (SoapFault $e)
        {
            var_dump($e);
        }
    }
    public function actionSyncPasienNaava($cabang,$start,$end)
    {
        try
        {
            $getPasien = Yii::app()->db->createCommand("select * from nscc_customers nc
                                                            join nscc_salestrans ns on ns.customer_id = nc.customer_id
                                                            where ns.tgl >= '$start' and ns.tgl <= '$end'")->queryAll();

            $client = new SoapClient(SOAP_SYNCRON, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            $result = $client->runSyncPasien($getPasien, $cabang, $start, $end);
            var_dump($result);

            $txt = "\nReturn : ".$result;
            file_put_contents("logsyncscan.log", $txt, FILE_APPEND);
        }
        catch (SoapFault $e)
        {
            var_dump($e);
        }
    }
}