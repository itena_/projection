<?php
/**
* send data to headoffice
*
*/
ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);
Yii::import('application.components.SoapClientYii');
class SendhoCommand extends CConsoleCommand{
     
     public function getConnect(){
         $connection=new CDbConnection('mysql:host=localhost;dbname=sync_log;port=3366','root','enteraja');
         $connection->active=true;
         return $connection;
     }  
     public function actionIndex(){
       /* $connection = self::getConnect();
        $id= '52531540-6ad6-11e7-bef2-a81e843c3480';
        $now = date('Y-m-d H:i:s');
        $command=$connection->createCommand("INSERT INTO `nscc_gl_trans`(`counter`,`created`) VALUES ('$id','$now')");
        $command->execute();*/
     }

     public function actionBank($id){
        $bank = Bank::model()->findByPk($id);
        if($bank != null){
            $model = new HBank;
            $model->bank_id = $id;
            $model->nama_bank = $bank->nama_bank;
            $model->ket = $bank->ket;
            $model->account_code = $bank->account_code;
            $model->store = $bank->store;
            $model->up = $bank->up;
            $model->visible = $bank->visible;
            if($model->save()){
                file_put_contents('success.txt','ok');
            }else{
                file_put_contents('error.txt','error');
            }
        } 
     }
     
     
     public function actionPushnegara($id){
         try {
            /** @var Negara $model */
            $model = Negara::model()->findByPk($id);
            $attrib = $model->getAttributes();
            //$attrib = $model->getAttributes();
            $client = new SoapClient('http://localhost/web-service/bank/quote', array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_NONE, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveNegara(json_encode($attrib));
            //$result = $client->saveNegara($model);
            var_dump($result);
        } catch (SoapFault $e) {
            var_dump($e);
        }
     }


     public function actionBanktrans($id){
         $bankTrans = BankTrans::model()->findByPk($id);
         if($bankTrans != null){
             $model = new HBankTrans;
             $model->bank_trans_id = $id;
             $model->type_ = $bankTrans->type_;
             $model->trans_no = $bankTrans->trans_no;
             $model->ref = $bankTrans->ref;
             $model->tgl = $bankTrans->tgl;
             $model->amount = $bankTrans->amount;
             $model->id_user = $bankTrans->id_user;
             $model->tdate = $bankTrans->tdate;
             $model->bank_id = $bankTrans->bank_id;
             $model->store = $bankTrans->store;
             $model->up = $bankTrans->up;
             $model->visible = $bankTrans->visible;
             if($model->save()){
                 file_put_contents('success.txt','ok');
             }else{
                $command=$connection->createCommand("INSERT INTO `nscc_bank_trans`(`bank_trans_id`,`created`) VALUES ('$id','0000-00-00 00:00:00')");
                $command->execute();
                throw new Exception(t('save.model.fail', 'app',array('{model}' => 'bank trans')) . CHtml::errorSummary($model));
             }
        }

     }

     public function actionBanktransupdate($id){
         $bankTrans = BankTrans::model()->findByPk($id);
         if($bankTrans != null){
             $model = HBankTrans::model()->findByPk($id);
             $model->bank_trans_id = $id;
             $model->type_ = $bankTrans->type_;
             $model->trans_no = $bankTrans->trans_no;
             $model->ref = $bankTrans->ref;
             $model->tgl = $bankTrans->tgl;
             $model->amount = $bankTrans->amount;
             $model->id_user = $bankTrans->id_user;
             $model->tdate = $bankTrans->tdate;
             $model->bank_id = $bankTrans->bank_id;
             $model->store = $bankTrans->store;
             $model->up = $bankTrans->up;
             $model->visible = $bankTrans->visible;
             if($model->save()){
                 file_put_contents('success.txt','ok');
             }else{
                $command=$connection->createCommand("INSERT INTO `nscc_bank_trans`(`bank_trans_id`,`created`) VALUES ('$id','0000-00-00 00:00:00')");
                $command->execute();
                throw new Exception(t('save.model.fail', 'app',array('{model}' => 'bank trans')) . CHtml::errorSummary($model));
             }
        }

     }

      public function actionGltrans($id){
          $gl = GlTrans::model()->findByPk($id);
          if($gl != null){
            $model = new HGlTrans;
            $model->counter = $id;
            $model->type = $gl->type;
            $model->type_no = $gl->type_no;
            $model->tran_date = $gl->tran_date;
            $model->memo_ = $gl->memo_;
            $model->amount = $gl->amount;
            $model->id_user = $gl->id_user;
            $model->account_code = $gl->account_code;
            $model->store = $gl->store;
            $model->tdate = $gl->tdate;
            $model->cf = $gl->cf;
            $model->up = $gl->up;
            $model->visible = $gl->visible;
            if($model->save()){
                 file_put_contents('success.txt','oke');
             }else{
                $command=$connection->createCommand("INSERT INTO `nscc_gl_trans`(`counter`,`created`) VALUES ('$id','0000-00-00 00:00:00')");
                $command->execute();
                throw new Exception(t('save.model.fail', 'app',array('{model}' => 'GL')) . CHtml::errorSummary($model));
             }
          }
      }

       public function actionGltransupdate($id){
          $gl = GlTrans::model()->findByPk($id);
          if($gl != null){
            $model = HGlTrans::model()->findByPk($id);
            $model->counter = $id;
            $model->type = $gl->type;
            $model->type_no = $gl->type_no;
            $model->tran_date = $gl->tran_date;
            $model->memo_ = $gl->memo_;
            $model->amount = $gl->amount;
            $model->id_user = $gl->id_user;
            $model->account_code = $gl->account_code;
            $model->store = $gl->store;
            $model->tdate = $gl->tdate;
            $model->cf = $gl->cf;
            $model->up = $gl->up;
            $model->visible = $gl->visible;
            if($model->save()){
                 file_put_contents('success.txt','oke');
             }else{
                $command=$connection->createCommand("INSERT INTO `nscc_gl_trans`(`counter`,`created`) VALUES ('$id','0000-00-00 00:00:00')");
                $command->execute();
                throw new Exception(t('save.model.fail', 'app',array('{model}' => 'GL')) . CHtml::errorSummary($model));
             }
          }
      }

}
?>