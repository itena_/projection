<?php

class DroppingHistoryController extends GxController {

public function actionCreate() {
$model = new DroppingHistory;
if (!Yii::app()->request->isAjaxRequest)
return;
if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['DroppingHistory'][$k] = $v;
}
$model->attributes = $_POST['DroppingHistory'];
$msg = "Data gagal disimpan.";

    if ($model->save()) {
$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->id_history;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg));
Yii::app()->end();

}

}

public function actionUpdate($id) {
$model = $this->loadModel($id, 'DroppingHistory');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['DroppingHistory'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['DroppingHistory'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->id_history;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->id_history));
}
}
}

public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'DroppingHistory')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}

    public function actionIndex() {
        $param = array();
        $criteria = new CDbCriteria();
        $id = '';
        if(isset($_POST['order_dropping_id'])){
            $id = $_POST['order_dropping_id'];
        }if(isset($_POST['receive_dropping_id'])){
            $model = $this->loadModel($_POST['receive_dropping_id'], 'ReceiveDropping');
            $id = $model->dropping->order_dropping_id ? $model->dropping->order_dropping_id: $model->dropping->dropping_id;
        }

        $criteria->addCondition('order_dropping_id = :order_dropping_id');
        $param[':order_dropping_id'] = $id;

        $criteria->params = $param;
        $criteria->order = "tgl ASC";
        $model = DroppingHistory::model()->findAll($criteria);
        $total = DroppingHistory::model()->count($criteria);

        $this->renderJson($model, $total);
    }

public function actionIndex1() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}

$criteria = new CDbCriteria();
$param = array();
if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
(isset($_POST['limit']) && isset($_POST['start']))) {
$criteria->limit = $limit;
$criteria->offset = $start;
}

/*    //$criteria = new CDbCriteria;
    $criteria->addCondition('order_dropping_id LIKE :order_dropping_id');
    $criteria->params[':order_dropping_id'] = $_POST['id'];*/
$model = DroppingHistory::model()->findAll($criteria);
$total = DroppingHistory::model()->count($criteria);

$this->renderJson($model, $total);

}

}