<?php
class DroppingDetailsController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        if (isset($_POST['dropping_id'])) {
            $criteria->addCondition("visible = 1");
            $criteria->addCondition("dropping_id = :dropping_id");
            $criteria->params = array(':dropping_id' => $_POST['dropping_id']);
            $model = DroppingDetails::model()->findAll($criteria);
            $total = DroppingDetails::model()->count($criteria);
            $this->renderJson($model, $total);
        } elseif (isset($_POST['order_dropping_id'])) {
            $criteria->addCondition("order_dropping_id = :order_dropping_id");
            $criteria->params = array(':order_dropping_id' => $_POST['order_dropping_id']);
            $model = OrderDroppingSisa::model()->findAll($criteria);
            $total = OrderDroppingSisa::model()->count($criteria);
            $this->renderJson($model, $total);
        }


    }
}