<?php
class SupplierController extends GxController
{
    public function actionCreate()
    {
        $model = new Supplier;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['account_code'];
            $status = true;
//            if (U::account_in_gl_trans($id)) {
//                $status = false;
//                $msg = t('coa.fail.use.gl','app',array('{coa}'=>$id));
//            } elseif (U::account_used_supplier($id)) {
//                $status = false;
//                $msg = t('coa.fail.use.model','app',array('{coa}'=>$id,'{model}'=>'Supplier'));
//            }
//            if (!$status) {
//                echo CJSON::encode(array(
//                    'success' => $status,
//                    'msg' => $msg
//                ));
//                Yii::app()->end();
//            }
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Supplier'][$k] = $v;
            }
            $model->attributes = $_POST['Supplier'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->supplier_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Supplier');
        if (isset($_POST) && !empty($_POST)) {
//            $id = $_POST['account_code'];
            $status = true;
//            if (U::account_in_gl_trans($id)) {
//                $status = false;
//                $msg = t('coa.fail.use.gl','app',array('{coa}'=>$id));
//            } elseif (U::account_used_supplier($id)) {
//                $status = false;
//                $msg = t('coa.fail.use.model','app',array('{coa}'=>$id,'{model}'=>'Supplier'));
//            }
//            if (!$status) {
//                echo CJSON::encode(array(
//                    'success' => $status,
//                    'msg' => $msg
//                ));
//                Yii::app()->end();
//            }
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Supplier'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Supplier'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->supplier_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->supplier_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['supplier_name'])) {
            $criteria->addCondition("supplier_name like :supplier_name");
            $param[':supplier_name'] = "%" . $_POST['supplier_name'] . "%";
        }
        if (isset($_POST['account_code'])) {
            $criteria->addCondition("account_code like :account_code");
            $param[':account_code'] = "%" . $_POST['account_code'] . "%";
        }
        if (isset($_POST['supp_address'])) {
            $criteria->addCondition("supp_address like :supp_address");
            $param[':supp_address'] = "%" . $_POST['supp_address'] . "%";
        }
        if (isset($_POST['supp_city'])) {
            $criteria->addCondition("supp_city like :supp_city");
            $param[':supp_city'] = "%" . $_POST['supp_city'] . "%";
        }
        $criteria->params = $param;
        $model = Supplier::model()->findAll($criteria);
        $total = Supplier::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}