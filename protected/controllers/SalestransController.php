<?php

Yii::import('application.components.Reference');
Yii::import('application.components.U');
Yii::import('application.components.PrintReceipt');
Yii::import('application.components.GL');

class SalestransController extends GxController {

    public function actionCreate() {
        $x = @fopen(SALES_LOCK, "w");
        if ($x === false) {
            echo CJSON::encode(array(
                'success' => false,
                'id' => '',
                'msg' => "Transaksi belum bisa dilakukan, silahkan coba lagi beberapa saat."
            ));
            Yii::app()->end();
        }
        if (!flock($x, LOCK_EX | LOCK_NB)) {
            echo CJSON::encode(array(
                'success' => false,
                'id' => '',
                'msg' => "Transaksi belum bisa dilakukan, silahkan coba lagi beberapa saat."
            ));
            Yii::app()->end();
        }
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            if (Tender::is_exist($_POST['tgl'])) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Tender Declaration already created'
                ));
                fflush($x);            // flush output before releasing the lock
                flock($x, LOCK_UN);
                fclose($x);
                Yii::app()->end();
            }
            $gl = new GL();
            $stocker = '';
            $prtresep = '';
            $resep = false;
            /** @var Konsul $konsul */
            $konsul = null;
            $detils = CJSON::decode($_POST['detil']);
            $payments = CJSON::decode($_POST['payment']);
            $pakets = CJSON::decode($_POST['paket']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Salestrans;
                //$auw->save();
                $ref = new Reference();
                $docref = $ref->get_next_reference(PENJUALAN);
                if (isset($_POST['log']) && $_POST['log'] == 0) {
                    $_POST['store'] = STOREID;
                } else {
                    $_POST['store'] = STOREID;
                }
                foreach ($_POST as $k => $v) {
                    //if ($k == 'detil' || $k = 'payment') continue;
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Salestrans'][$k] = $v;
                }
                $_POST['Salestrans']['type_'] = 1;
                $_POST['Salestrans']['doc_ref'] = $docref;
//                $_POST['Salestrans']['salestrans_id'] = "1";
//                $_POST['Salestrans']['tgl'] = new CDbExpression('NOW()');

                /** KMR - Jasa Medis Dokter Resep
                 *  Parameter di sys_pref - kmr_persen
                 *  jasa dihitung sebelum pajak
                 */
                if ($model->dokter_id != null) {
                    $kmr_persen = SysPrefs::get_val("kmr_persen");
                    if ($kmr_persen == null) {
                        $kmr_persen = 0;
                    }
                    $_POST['Salestrans']['kmr_persen'] = $kmr_persen;
                    $nilai_sebelum_tax = $_POST['Salestrans']['bruto'] - $_POST['Salestrans']['total_discrp1'];
                    $_POST['Salestrans']['kmr_total'] = round(($nilai_sebelum_tax * $_POST['Salestrans']['kmr_persen']) / 100, 2);
                }
                $model->attributes = $_POST['Salestrans'];



                /*
                 * DEPRECATED!!
                 * karena :
                 * - employee_id untuk PC, Admin, Apoteker, Stocker diambil dari master 'Assign Employees'
                 * - terdapat kemungkinan suatu jabatan berlaku untuk lebih dari 1 employee pada waktu yang sama
                 *   sehingga menyimpan data pada header sales tidak dapat dilakukan
                 */
                $model->apoteker_id = SysPrefs::get_val('apoteker');
                $model->stocker_id = SysPrefs::get_val('stocker');
                $model->admin_id = SysPrefs::get_val('admin_cabang');
                $model->pc_id = SysPrefs::get_val('pengelola_cabang');


                ### Cek id_antrian ###
                $jml = BeautyServiceView::model()
                        ->count('id_antrian = :id_antrian', [':id_antrian' => $model->id_antrian]);
                if ($jml > 0) {
                    $model->id_antrian = '';
                }
                ### Cek id_antrian ###
                
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')) . CHtml::errorSummary($model));
                }
                if(defined('REFERRAL')&& REFERRAL){
                $referral = new ReferralTrans;               
                $_POST['referral']['sales_id'] = $model->salestrans_id;
                $_POST['referral']['referral_id'] = $_POST['Salestrans']['referral_id'];
                $_POST['referral']['persen_bonus'] = "10";
                $_POST['referral']['bonus'] = 10/100 * $_POST['Salestrans']['total'] ;
                $_POST['referral']['tdate'] = new CDbExpression('NOW()');
                $_POST['referral']['store'] = STOREID;
                $_POST['referral']['id_user'] = $_POST['Salestrans']['customer_id'];
                $_POST['referral']['visible'] = 1;
                $referral->attributes = $_POST['referral'];
                    if (!$referral->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')) . CHtml::errorSummary($referral));
                    }
                }
                # Tambahan untuk konsul dari dokter #
                if ($model->konsul_id != null) {
                    $konsul = Konsul::model()->findByPk($model->konsul_id);
                    if ($konsul != null) {
                        $konsul->saveAttributes(array('salestrans_id' => $model->salestrans_id));
                    }
                }
                # Tambahan untuk konsul dari dokter #
                if ($model->rounding != 0) {
                    $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_ROUNDING, "Sales $docref", "Sales $docref", -$model->rounding, 1, $model->store);
                }
//                $transaction->commit();
//                Yii::app()->end();
                $kembali_sudah_dikurangkan = 0;
                foreach ($payments as $pay) {
                    $amount = get_number($pay['amount']);
                    $kembali = 0;
                    if ($pay['bank_id'] == Bank::get_bank_cash_id() && $kembali_sudah_dikurangkan == 0) {
                        if ($model->kembali > 0) {
                            $kembali = $model->kembali;
                            $kembali_sudah_dikurangkan = 1;
                        }
                    }
                    Payment::add_payment($pay['bank_id'], $model->salestrans_id, $pay['card_number'], $amount, $pay['card_id'], $kembali, $pay['ulpt']);
                }
                foreach ($pakets as $paket) {
                    $paket_trans = new PaketTrans;
                    $paket_trans->qty = get_number($paket['qty']);
                    $paket_trans->paket_trans_id = $paket['paket_trans_id'];
                    $paket_trans->paket_id = $paket['paket_id'];
                    $paket_trans->salestrans_id = $model->salestrans_id;
                    if (!$paket_trans->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Paket Trans')) . CHtml::errorSummary($paket_trans));
                    }
                }
                //GL BANK (sesuai cara bayar)
                //        VAT
                //        Diskon Faktur
                foreach ($model->payments as $pay) {
                    if ($pay->amount != 0) {
                        $pay_amount = 0;
                        if ($pay->bank->is_bank_cash()) {
                            $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $pay->bank->account_code, "Sales $docref", "Sales $docref", $pay->amount - $pay->kembali, 0, $model->store);
                            $pay_amount = $pay->amount - $pay->kembali;
                        } else {
                            if ($pay->card == null) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')) . 'Card type must select!');
                            }
                            $fee = ($pay->card->persen_fee / 100) * $pay->amount;
                            $fee = round($fee, 2);
                            $edc = $pay->amount - $fee;
                            $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $pay->bank->account_code, "Sales $docref", "Sales $docref", $edc, 0, $model->store);
                            $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_FEE_CARD, "Sales $docref", "Sales $docref", $fee, 0, $model->store);
                            $pay_amount = $edc;
                            $payment = Payment::model()->findByPk($pay->payment_id);
                            if ($payment == null) {
                                throw new Exception("Payment tidak ditemukan!");
                            }
                            $payment->feecard = $fee;
                            if (!$payment->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Payment')) . CHtml::errorSummary($payment));
                            }
                        }
                        if ($gl->is_bank_account($pay->bank->account_code)) {
                            $gl->add_bank_trans(PENJUALAN, $model->salestrans_id, $pay->bank_id, $docref, $model->tgl, $pay_amount, $model->user_id, $model->store);
                        }
                    }
                }
                if ($model->vat != 0) {
                    $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_VAT, "Sales $docref", "Sales $docref", -$model->vat, 1);
                }
                $total_durasi = 0;
                foreach ($detils as $detil) {
                    $salestrans_detail = new SalestransDetails;
                    $qty = get_number($detil['qty']);
                    if ($qty < 1) {
                        throw new Exception("Qty minimal 1");
                    }
                    $_POST['SalestransDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['SalestransDetails']['qty'] = $qty;
                    $_POST['SalestransDetails']['price'] = get_number($detil['price']);
                    $_POST['SalestransDetails']['disc'] = get_number($detil['disc']);
                    $_POST['SalestransDetails']['discrp'] = get_number($detil['discrp']);
                    $_POST['SalestransDetails']['ketpot'] = $detil['ketpot'];
                    $_POST['SalestransDetails']['vat'] = get_number($detil['vat']);
                    $_POST['SalestransDetails']['vatrp'] = get_number($detil['vatrp']);
                    $_POST['SalestransDetails']['total_pot'] = get_number($detil['total_pot']);
                    $_POST['SalestransDetails']['total'] = get_number($detil['total']);
                    $_POST['SalestransDetails']['bruto'] = get_number($detil['bruto']);
                    $_POST['SalestransDetails']['disc_name'] = $detil['disc_name'];
                    $_POST['SalestransDetails']['disc1'] = get_number($detil['disc1']);
                    $_POST['SalestransDetails']['discrp1'] = get_number($detil['discrp1']);
                    $_POST['SalestransDetails']['paket_trans_id'] = $detil['paket_trans_id'];
                    $_POST['SalestransDetails']['paket_details_id'] = $detil['paket_details_id'];
                    $_POST['SalestransDetails']['salestrans_id'] = $model->salestrans_id;
                    # Tambahan untuk konsul dari dokter #
                    if ($konsul != null) {
                        /** @var KonsulDetil $konsulDetil */
                        $konsulDetil = KonsulDetil::model()->findByAttributes(array(
                            'konsul_id' => $konsul->konsul_id,
                            'barang_id' => $_POST['SalestransDetails']['barang_id']
                        ));
                        if ($konsulDetil != null) {
                            $_POST['SalestransDetails']['konsul_detil_id'] = $konsulDetil->konsul_detil_id;
                            $konsulDetil->saveAttributes(array('used' => $qty));
                        }
                        ## Jika ada perawatan tambahan ##
                        if ($konsul->type_ != 0) {
                            /** @var Barang $b */
                            $b = Barang::model()->findByPk($konsulDetil->barang_id);
                            if ($b != null && $b->isPerawatan()) {
                                if ($konsul->beauty_id != null) {
                                    $detil['beauty_id'] = $konsul->beauty_id;
                                }
                            }
                        }
                        ## Jika ada perawatan tambahan ##
                    }
                    $durasi = 0;
                    /** @var Jual $jual */
                    $jual = Jual::model()->findByAttributes(array(
                        'barang_id' => $detil['barang_id'],
                        'store' => $model->store
                    ));
                    if ($jual != null) {
                        $durasi = $jual->duration;
                    }
                    $_POST['SalestransDetails']['durasi'] = $durasi;
                    $total_durasi += $durasi;
                    # Tambahan untuk konsul dari dokter #
                    $salestrans_detail->attributes = $_POST['SalestransDetails'];
                    if (!$salestrans_detail->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales Detail')) . CHtml::errorSummary($salestrans_detail));
                    }
                    //$salestrans_detail->save_beauty_tips($detil['beauty_id'], $detil['beauty2_id'], null, null, null);
                    //START CEK RESEP
                    $rsp = Resep::model()->findByAttributes(array('barang_id' => $salestrans_detail->barang_id));
                    if ($rsp != NULL) {
                        $resep = TRUE;
                    }
                    //END CEK RESEP
                    $tipe_jual = "Sales " . $salestrans_detail->barang->grup->nama_grup;
                    ######################################################################################
                    ###### UNTUK NATASHA
                    ###### DISCOUNT AMOUNT TIDAK DIMASUKKAN KE DISKON SAJA; TIDAK PERLU MASUK COA VOUCHER
                    ######################################################################################
                    if ($salestrans_detail->discrp1 != 0) {
//              DISKON VOUCHER
                        $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $salestrans_detail->barang->get_coa_sales_discrp1($model->store), "Disk Amount $docref", "Disk Amount $docref", $salestrans_detail->discrp1, 1);
                    }
                    //GL Diskon item
                    if ($salestrans_detail->discrp != 0) {
                        $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $salestrans_detail->barang->get_coa_sales_disc($model->store), $tipe_jual, $tipe_jual, $salestrans_detail->discrp, 1);
                    }
                    //GL Penjualan sesuai grup barang
                    if ($salestrans_detail->bruto != 0) {
                        $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $salestrans_detail->barang->get_coa_sales($model->store), $tipe_jual, $tipe_jual, -$salestrans_detail->bruto, 1, $model->store);
                    }
//                    if ($model->log == 0) {
                    if ($salestrans_detail->barang->grup->kategori->is_have_stock()) {
                        //------------------------------ khusus barang non jasa ------------------------------------
                        $saldo_stock = StockMoves::get_saldo_item($salestrans_detail->barang_id, $model->store);
                        if ($saldo_stock < $salestrans_detail->qty) {
                            throw new Exception(t('saldo.item.fail', 'app', array(
                                '{item}' => $salestrans_detail->barang->kode_barang,
                                '{h}' => $saldo_stock,
                                '{r}' => $salestrans_detail->qty
                            )));
                        }
                        U::add_stock_moves(PENJUALAN, $model->salestrans_id, $model->tgl, $salestrans_detail->barang_id, -$salestrans_detail->qty, $model->doc_ref, $salestrans_detail->barang->get_cost($model->store), $model->store);
                        // Hitung HPP
                        $cost = $salestrans_detail->barang->get_cost($model->store);
                        $hpp = $salestrans_detail->qty * $cost;
                        $salestrans_detail->cost = $cost;
                        $salestrans_detail->hpp = $hpp;
                        if (!$salestrans_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales Hpp Detail')) . CHtml::errorSummary($salestrans_detail));
                        }

                        if (HITUNG_GL_HPP) {
                            //GL HPP
                            //  Persediaan
                            $coa_sales_hpp = $salestrans_detail->barang->get_coa_sales_hpp($model->store);
                            if ($hpp != 0 && $coa_sales_hpp != null) {
                                $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $coa_sales_hpp, $tipe_jual, $tipe_jual, $hpp, 0);
                                $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $salestrans_detail->barang->tipeBarang->coa, $tipe_jual, $tipe_jual, -$hpp, 0);
                            }
                        }
                    }
//                    }
                    /*
                     * ---------------------- Bonus ----------------------
                     */
                    if (METHODE_BONUS == 1) {
                        Bonus::sales(PENJUALAN, $model, $salestrans_detail);
                    }
                }
                $model->saveAttributes(array(
                    'total_durasi' => $total_durasi
                ));
                $gl->validate();
                $ref->save(PENJUALAN, $model->salestrans_id, $docref);
                $cust = Customers::model()->findByPk($model->customer_id);
                $cust->akhir = new CDbExpression('NOW()');
                $cust->log = 0;
                $cust->up = 0;
                if (!$cust->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Customers')) . CHtml::errorSummary($cust));
                }
                ###  BONUS   ###
//                if ($model->beauty_id != null) {
//                    $bns = new Bonus;
//                    $bns->tgl = $model->tgl;
//                    $bns->employee_id = $model->beauty_id;
//                    $bns->type_ = PENJUALAN;
//                    $bns->trans_no = $model->salestrans_id;
//                    $bns->bruto = round($model->bruto / $jml, 2);
//                    $bns->dpp = round(($model->total - $model->vat) / $jml, 2);;
//                    $bns->total = round($model->total / $jml, 2);
//                    if (!$bns->save()) {
//                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Customers')) . CHtml::errorSummary($bns));
//                    }
//                } else {
                /** @var UserEmployee[] $emps */
//                    $emps = UserEmployee::model()->findAllByAttributes(array('id' => $model->user_id));
//                    if ($emps != null) {
//                        $jml = count($emps);
//                        foreach ($emps as $emp) {
//                            $bns = new Bonus;
//                            $bns->tgl = $model->tgl;
//                            $bns->employee_id = $emp->employee_id;
//                            $bns->type_ = PENJUALAN;
//                            $bns->trans_no = $model->salestrans_id;
//                            $bns->bruto = round($model->bruto / $jml, 2);
//                            $bns->dpp = round(($model->total - $model->vat) / $jml, 2);
//                            $bns->total = round($model->total / $jml, 2);
//                            if (!$bns->save()) {
//                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Customers')) . CHtml::errorSummary($bns));
//                            }
//                        }
//                    }
//                }
                ###  BONUS   ###
                /*
                 *  save note perawatan
                 */
                If (isset($_POST['note'])){
                    if ($_POST['note'] != '' ){
                        $aa = AishaAntrian::model()->findByPk($_POST['id_antrian']);
                        $aa->note = $_POST['note'];
                        $aa->hold = 1;
                        $aa->update();
                    }
                }                
                $prt = new PrintReceipt($model);
                $msg = $prt->buildTxt();
                $prtstk = new PrintStoker($model);
                $stocker = $prtstk->buildTxt();
                if ($resep == TRUE) {
                    $prtrsp = new PrintResep($model);
                    $prtresep = $prtrsp->buildTxt();
                }
//                $auw->save();
                //$msg = "";
                $transaction->commit();
                $status = true;
                if (RECEIPT_EMAIL) {
                    if (filter_var($model->customer->email, FILTER_VALIDATE_EMAIL)) {
                        $message = YiiBase::getPathOfAlias('webroot.protected.runtime') .
                                DIRECTORY_SEPARATOR . $docref;
                        $msgHtml = "<html>";
                        $msgHtml .= "<pre>";
                        $msgHtml .= $msg;
                        $msgHtml .= "</pre>";
                        $msgHtml .= "</html>";
                        file_put_contents($message, $msgHtml);
                        $id = '--to=' . $model->customer->email;
                        $id .= ' --from=nwis@gmail.com';
                        $id .= ' --fromAlias=nwis@gmail.com';
                        $id .= ' --subject="Receipt Sales"';
                        $id .= ' --message="' . base64_encode($message) . '"';
                        $id .= ' --html=true';
                        U::runConsole('utils', 'SentEmail', $id);
                    }
                }
                if (UPLOAD_NARS) {
                    U::runCommandNars('History', '--id=' . $model->salestrans_id);
                }
                if (PUSH_PUSAT) {

                    U::runCommand('checkdata', 'gltrans', '--tno=' . $model->salestrans_id, 'sr_gl_tno' . $model->salestrans_id . '.log');

                    U::runCommand('checkdata', 'banktrans', '--tno=' . $model->salestrans_id, 'sr_bt_tno' . $model->salestrans_id . '.log');

                    U::runCommand('checkdata', 'ref', '--tno=' . $model->salestrans_id, 'sr_ref_tno' . $model->salestrans_id . '.log');

                    U::runCommand('checkdata', 'stockmoves', '--tno=' . $model->salestrans_id, 'sr_sm_tno' . $model->salestrans_id . '.log');

                    U::runCommand('soap', 'salestrans', '--id=' . $model->salestrans_id, 'salestrns_' . $model->salestrans_id . '.log');
                }
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg,
                'stok' => $stocker,
                'resep' => $prtresep
            ));
            fflush($x);            // flush output before releasing the lock
            flock($x, LOCK_UN);
            fclose($x);
            Yii::app()->end();
        }
        fflush($x);            // flush output before releasing the lock
        flock($x, LOCK_UN);
        fclose($x);
    }

    public function actionUpdateComment() {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $sales = Salestrans::model()->findByPk($_POST['salestrans_id']);
                $sales->ketdisc = $_POST['ketdisc'];
                if (!$sales->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')) . CHtml::errorSummary($sales));
                }
                $transaction->commit();
                $msg = "";
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionReturAll() {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            if (METHODE_BONUS == 1) {
                $id_antrian = '';
                $perawatan = FALSE;
                $cek_end = BeautyServiceView::model()->findByAttributes(['salestrans_id' => $_POST['salestrans_id']]);
                if ($cek_end != NULL) {
                    if ($cek_end->start_at && !$cek_end->end_at) {
                        throw new Exception("Retur tidak bisa dilakukan karena pasien sedang dalam perawatan.");
                    }
                    $id_antrian = $cek_end->id_antrian;
                }
                $perawatan = TRUE;
            }
            $count = Users::get_access($_POST["username"], $_POST["password"], 244);
            if ($count) {
                /* @var $Sales Salestrans */
                $Sales = Salestrans::model()->findByPk($_POST['salestrans_id']);
                if ($Sales == null) {
                    throw new Exception("Sales transaction  not found.");
                }
                if ($Sales->store != STOREID) {
                    throw new Exception("Transaksi hanya bisa di retur dari cabang asal.");
                }
                $tgl = $Sales->tgl;
                if (Tender::is_exist($tgl)) {
                    echo CJSON::encode(array(
                        'success' => false,
                        'msg' => 'Tender Declaration already created'
                    ));
                    Yii::app()->end();
                }
                $prodExists = Salestrans::model()->count('doc_ref_sales = :doc_ref_sales', array(':doc_ref_sales' => $Sales->doc_ref));
                if ($prodExists > 0) {
                    throw new Exception("Sales Return already created.");
                }
                ### Suspend bisa digunakan lagi ###
                Konsul::model()->updateAll(['salestrans_id' => null], 'salestrans_id = :salestrans_id', [':salestrans_id' => $Sales->salestrans_id]);
                $Sales->saveAttributes(['id_antrian' => '']);
                ### Suspend bisa digunakan lagi ###
                $gl = new GL();
                $detils = $Sales->salestransDetails;
                $payments = $Sales->payments;
                $pakets = $Sales->paketTrans;
                app()->db->autoCommit = false;
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $model = new Salestrans;
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(RETURJUAL);
                    $model->user_id = $count;
                    $model->tgl = $tgl;
                    $model->doc_ref = $docref;
                    $model->type_ = -1;
                    $model->bruto = -$Sales->bruto;
                    $model->disc = $Sales->disc;
                    $model->discrp = -$Sales->discrp;
                    $model->totalpot = -$Sales->totalpot;
                    $model->total = -$Sales->total;
                    $model->ketdisc = $Sales->ketdisc;
                    $model->vat = -$Sales->vat;
                    $model->customer_id = $Sales->customer_id;
                    $model->doc_ref_sales = $Sales->doc_ref;
                    $model->parent_id = $Sales->salestrans_id;
                    $model->audit = $Sales->audit;
                    $model->store = $Sales->store;
                    $model->printed = $Sales->printed;
                    $model->override = $Sales->override;
                    $model->bayar = -$Sales->bayar;
                    $model->kembali = -$Sales->kembali;
                    $model->rounding = -$Sales->rounding;
                    $model->total_discrp1 = -$Sales->total_discrp1;
                    $model->dokter_id = $Sales->dokter_id;
                    $model->kmr_total = -$Sales->kmr_total;
                    $model->kmr_persen = $Sales->kmr_persen;
                    $model->beauty_id = $Sales->beauty_id;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Return Sales')) . CHtml::errorSummary($model));
                    }
                    if ($model->rounding != 0) {
                        $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_ROUNDING, "Sales $docref", "Sales $docref", -$model->rounding, 1, $model->store);
                    }
                    foreach ($payments as $pay) {
                        $amount = -$pay->amount;
                        $kembali = -$pay->kembali;
                        Payment::add_payment($pay->bank_id, $model->salestrans_id, $pay->card_number, $amount, $pay->card_id, $kembali);
                    }
                    
                    foreach ($pakets as $paket) {
                        $paket_trans_id = $this->generate_uuid();
                        $paket_trans = new PaketTrans;
                        $paket_trans->paket_trans_id = $paket_trans_id;
                        $paket_trans->qty = -$paket->qty;
                        $paket_trans->paket_id = $paket->paket_id;
                        $paket_trans->salestrans_id = $model->salestrans_id;
                        if (!$paket_trans->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Paket Trans')) . CHtml::errorSummary($paket_trans));
                        }
                    }
//                Payment::add_payment($_POST['Salestrans']['bank_id'], $model->salestrans_id, '', $model->total);
                    //GL BANK (sesuai cara bayar)
                    //        VAT
                    //        Diskon Faktur
                    foreach ($model->payments as $pay) {
                        if ($pay->amount != 0) {
                            $pay_amount = 0;
                            if ($pay->bank->is_bank_cash()) {
                                $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, $pay->bank->account_code, "Retur Sales $docref", "Retur Sales $docref", $pay->amount - $pay->kembali, 0, $model->store);
                                $pay_amount = $pay->amount - $pay->kembali;
                            } else {
                                if ($pay->card == null) {
                                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Return Sales')) . 'Card type must select!');
                                }
                                $fee = ($pay->card->persen_fee / 100) * $pay->amount;
                                $fee = round($fee, 2);
                                $edc = $pay->amount - $fee;
                                $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, $pay->bank->account_code, "Retur Sales $docref", "Retur Sales $docref", $edc, 0, $model->store);
                                $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_FEE_CARD, "Retur Sales $docref", "Retur Sales $docref", $fee, 0, $model->store);
                                $pay_amount = $edc;
                                $payment = Payment::model()->findByPk($pay->payment_id);
                                if ($payment == null) {
                                    throw new Exception("Payment tidak ditemukan!");
                                }
                                $payment->feecard = $fee;
                                if (!$payment->save()) {
                                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Payment')) . CHtml::errorSummary($payment));
                                }
                            }
                            if ($gl->is_bank_account($pay->bank->account_code)) {
                                $gl->add_bank_trans(RETURJUAL, $model->salestrans_id, $pay->bank_id, $docref, $model->tgl, $pay_amount, $model->user_id, $model->store);
                            }
                        }
                    }
                    ######################################################################################
                    ###### UNTUK NATASHA
                    ###### DISCOUNT AMOUNT TIDAK DIMASUKKAN KE DISKON SAJA; TIDAK PERLU MASUK COA VOUCHER
                    ######################################################################################
//	                if ($model->discrp != 0) {
////              DISKON VOUCHER
//                        $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_VOUCHER, "Retur Sales $docref", "Retur Sales $docref", $model->discrp, 1);
//                    }
                    ######################################################################################
                    if ($model->vat != 0) {
                        $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_VAT, "Retur Sales $docref", "Retur Sales $docref", -$model->vat, 1);
                    }
                    foreach ($detils as $detil) {
                        $salestrans_detail = new SalestransDetails;
                        $salestrans_detail->barang_id = $detil->barang_id;
                        $salestrans_detail->qty = -$detil->qty;
                        $salestrans_detail->price = $detil->price;
                        $salestrans_detail->disc = -$detil->disc;
                        $salestrans_detail->discrp = -$detil->discrp;
                        $salestrans_detail->ketpot = $detil->ketpot;
                        $salestrans_detail->vat = -$detil->vat;
                        $salestrans_detail->vatrp = -$detil->vatrp;
                        $salestrans_detail->total_pot = -$detil->total_pot;
                        $salestrans_detail->total = -$detil->total;
                        $salestrans_detail->bruto = -$detil->bruto;
                        $salestrans_detail->disc_name = $detil->disc_name;
                        $salestrans_detail->disc1 = $detil->disc1;
                        $salestrans_detail->discrp1 = -$detil->discrp1;
                        //$salestrans_detail->paket_trans_id = ($detil->paket_details_id == null) ? null : $paket_trans_id;
                        
                        /*
                         * cek multiple paket
                         */
                        $find_paket_trans_id = PaketTrans::model()->find("paket_trans_id=:paket_trans_id", [":paket_trans_id" => $detil->paket_trans_id]);
                        if ($find_paket_trans_id) {
                            $new_paket_trans_id = PaketTrans::model()->find("paket_id=:paket_id and salestrans_id=:salestrans_id", [":paket_id" => $find_paket_trans_id->paket_id,
                                ":salestrans_id" => $model->salestrans_id]);

                            $salestrans_detail->paket_trans_id = ($new_paket_trans_id == null) ? null : $new_paket_trans_id->paket_trans_id;
                        }
                        $salestrans_detail->paket_details_id = $detil->paket_details_id;
                        $salestrans_detail->salestrans_id = $model->salestrans_id;
                        if (!$salestrans_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Return Sales Detail')) . CHtml::errorSummary($salestrans_detail));
                        }
//                    $salestrans_detail->save_beauty_tips();
                        $tipe_jual = "Retur Sales " . $salestrans_detail->barang->grup->nama_grup;
                        //GL Diskon item
//                        if ($salestrans_detail->total_pot != 0) {
//                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref,
//                                $salestrans_detail->barang->get_coa_sales_disc($model->store), $tipe_jual, $tipe_jual,
//                                $salestrans_detail->total_pot, 1, $model->store);
//                        }
                        ######################################################################################
                        ###### UNTUK NATASHA
                        ###### DISCOUNT AMOUNT TIDAK DIMASUKKAN KE DISKON SAJA; TIDAK PERLU MASUK COA VOUCHER
                        ######################################################################################
                        if ($salestrans_detail->discrp1 != 0) {
//              DISKON VOUCHER
                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref,
	                            $salestrans_detail->barang->get_coa_sales_discrp1($model->store),
                                "Disk Amount $docref", "Disk Amount $docref",
                                $salestrans_detail->discrp1, 1);
                        }
                        //GL Diskon item
                        if ($salestrans_detail->discrp != 0) {
                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref,
                                $salestrans_detail->barang->get_coa_sales_disc($model->store), $tipe_jual, $tipe_jual,
                                $salestrans_detail->discrp, 1);
                        }
                        //GL Penjualan sesuai grup barang
                        if ($salestrans_detail->bruto != 0) {
//                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_SALES_RETURN, $tipe_jual, $tipe_jual, -$salestrans_detail->bruto, 1, $model->store);
                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, $salestrans_detail->barang->get_coa_sales_return($model->store), $tipe_jual, $tipe_jual, -$salestrans_detail->bruto, 1, $model->store);
                        }
                        if ($salestrans_detail->barang->grup->kategori->is_have_stock()) {
                            U::add_stock_moves(RETURJUAL, $model->salestrans_id, $model->tgl, $salestrans_detail->barang_id, -$salestrans_detail->qty, $model->doc_ref, $salestrans_detail->barang->get_cost($model->store), $model->store);
                            // Hitung HPP
                            $cost = $salestrans_detail->barang->get_cost($model->store);
                            $hpp = $salestrans_detail->qty * $cost;
                            $salestrans_detail->cost = $cost;
                            $salestrans_detail->hpp = $hpp;
                            if (!$salestrans_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales Hpp Detail')) . CHtml::errorSummary($salestrans_detail));
                            }

                            if (HITUNG_GL_HPP) {
                                //GL HPP
                                //  Persediaan
                                if ($hpp != 0) {
                                    $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, $salestrans_detail->barang->get_coa_sales_hpp($model->store), $tipe_jual, $tipe_jual, $hpp, 0, $model->store);
                                    $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, $salestrans_detail->barang->tipeBarang->coa, $tipe_jual, $tipe_jual, -$hpp, 0, $model->store);
                                }
                            }
                        }
                        /*
                         * ---------------------- Bonus ----------------------
                         */
                        if (METHODE_BONUS == 1) {
                            Bonus::salesRetur(RETURJUAL, $detil, $model, $salestrans_detail);
                        }
                    }
                    $gl->validate();
                    $ref->save(RETURJUAL, $model->salestrans_id, $docref);
                    ###  BONUS   ###
                    /** @var Bonus[] $bnsSales */
//                    $bnsSales = Bonus::model()->findAllByAttributes(array('trans_no' => $Sales->salestrans_id));
//                    if ($bnsSales != null) {
//                        foreach ($bnsSales as $b) {
//                            $bns = new Bonus;
//                            $bns->tgl = $b->tgl;
//                            $bns->employee_id = $b->employee_id;
//                            $bns->type_ = RETURJUAL;
//                            $bns->trans_no = $b->trans_no;
//                            $bns->bruto = -($b->bruto);
//                            $bns->dpp = -($b->dpp);
//                            $bns->total = -($b->total);
//                            if (!$bns->save()) {
//                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Customers')) . CHtml::errorSummary($bns));
//                            }
//                        }
//                    }
                    ###  BONUS   ###
                    /*
                     * ---------------------- Aisha Antrian set Sales = -1 ----------------------
                     */
                    if (METHODE_BONUS == 1) {
                        if ($perawatan == TRUE) {
                            $aa = AishaAntrian::model()->findAllByAttributes(array(
                                'id_antrian' => $id_antrian
                            ));
                            foreach ($aa as $a) {
                                $a->sales = -1;
                                $a->save();
                            }
                        }
                    }
                    ## Void Referral Nox ##
                    if (REFERRAL) {
                        $id = $_POST['salestrans_id'];
                        Yii::app()->db->createCommand(
                                        "UPDATE nscc_referral_trans SET visible ='-1' WHERE sales_id='$id'")
                                ->execute();
                    }
                    ###  Aisha Antrian   ###
                    $prt = new PrintReceipt($model);
                    $msg = $prt->buildTxt();
                    $transaction->commit();
                    $status = true;
                    if (UPLOAD_NARS) {
                        U::runCommandNars('History', '--id=' . $model->salestrans_id);
                    }
                } catch (Exception $ex) {
                    $transaction->rollback();
                    $status = false;
                    $msg = $ex->getMessage();
                }
                app()->db->autoCommit = true;
                echo CJSON::encode(array(
                    'success' => $status,
                    'id' => $docref,
                    'msg' => $msg
                ));
            } else {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => "You don't have permission."
                ));
            }
            Yii::app()->end();
        }
    }

    public function actionUpdate($id) {
        /** @var Salestrans $model */
        $model = $this->loadModel($id, 'Salestrans');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['Salestrans'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Salestrans'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->salestrans_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->salestrans_id));
            }
        }
    }

    public function actionIndex() {
//        $this->renderJsonArr(Salestrans::get_trans($_POST['tgl']));
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $param = array();
        $criteria = new CDbCriteria();
        if (isset($_POST['no_customer'])) {
            $criteria->addCondition('no_customer like :no_customer');
            $param[':no_customer'] = '%' . $_POST['no_customer'] . '%';
        }
        if (isset($_POST['doc_ref'])) {
            $criteria->addCondition('doc_ref like :doc_ref');
            $param[':doc_ref'] = '%' . $_POST['doc_ref'] . '%';
        }
        if (isset($_POST['store'])) {
            $criteria->addCondition('store like :store');
            $param[':store'] = '%' . $_POST['store'] . '%';
        }
        if (isset($_POST['nama_customer'])) {
            $criteria->addCondition('nama_customer like :nama_customer');
            $param[':nama_customer'] = '%' . $_POST['nama_customer'] . '%';
        }
        if (isset($_POST['total'])) {
            $criteria->addCondition('total = :total');
            $param[':total'] = $_POST['total'];
        }
        if (isset($_POST['customer_id'])) {
            $criteria->addCondition('customer_id = :customer_id');
            $param[':customer_id'] = $_POST['customer_id'];
        }
        if (isset($_POST['salestrans_id'])) {
            $criteria->addCondition('salestrans_id = :salestrans_id');
            $param[':salestrans_id'] = $_POST['salestrans_id'];
        }
        if (isset($_POST['query'])) {
            $criteria->addCondition('doc_ref like :doc_ref');
            $param[':doc_ref'] = "%" . $_POST['query'] . "%";
        }
        if (isset($_POST['counter'])) {
            $criteria->addCondition('counter = :counter');
            $param[':counter'] = $_POST['counter'];
            $_POST['tgl'] = Now();
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = date(:tgl)');
            $param[':tgl'] = $_POST['tgl'];
        }
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->order = 'doc_ref';
        $criteria->params = $param;
        $model = SalestransView::model()->findAll($criteria);
        $total = SalestransView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    
    public function actionSalesList() {
        $param = [];
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $cmd = DbCmd::instance()
                ->addFrom("{{salestrans_view}} sv")
                ->addSelect("sv.*, s.parent_id")
                ->addLeftJoin("{{salestrans}} s", "s.parent_id = sv.salestrans_id")
                ->addCondition("sv.tgl = :date")
                ->addOrder("sv.tgl");
        if (isset($_POST['no_customer'])) {
            $cmd->addCondition('no_customer like :no_customer');
            $param[':no_customer'] = '%' . $_POST['no_customer'] . '%';
        }
        if (isset($_POST['doc_ref'])) {
            $cmd->addCondition('sv.doc_ref like :doc_ref');
            $param[':doc_ref'] = '%' . $_POST['doc_ref'] . '%';
        }
        if (isset($_POST['store'])) {
            $cmd->addCondition('store like :store');
            $param[':store'] = '%' . $_POST['store'] . '%';
        }
        if (isset($_POST['nama_customer'])) {
            $cmd->addCondition('nama_customer like :nama_customer');
            $param[':nama_customer'] = '%' . $_POST['nama_customer'] . '%';
        }
        if (isset($_POST['total'])) {
            $cmd->addCondition('total = :total');
            $param[':total'] = $_POST['total'];
        }
        if (isset($_POST['counter'])) {
            $cmd->addCondition('sv.counter = :counter');
            $param[':counter'] = $_POST['counter'];
        }
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $cmd->setLimit($limit,$start);
        }
        $tgl = date("Y-m-d");
        $param[':date'] = "$tgl";
        $count = $cmd->queryCount($param);        
        $model = $cmd->queryAll(true, $param);        
        $this->renderJsonArrWithTotal($model, $count);
    }

    public function actionBeautyService() {
        $criteria = new CDbCriteria();
        $params = [];
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $params[':tgl'] = $_POST['tgl'];
        }
//            $tgl = $_POST['tgl'];
//            $arr = Salestrans::list_beuty_service($tgl, $limit, $start);
//            $arr2 = Salestrans::list_beuty_service($tgl, 0, 0);
//            $this->renderJsonArrWithTotal($arr, count($arr2));
        $criteria->order = 'doc_ref';
        $criteria->params = $params;
        $model = BeautyServiceView::model()->findAll($criteria);
        $total = BeautyServiceView::model()->count($criteria);
        $this->renderJson($model, $total);
        Yii::app()->end();
    }

    public function actionBeautyServiceCombination() {
        $criteria = new CDbCriteria();
        $params = [];
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $params[':tgl'] = $_POST['tgl'];
        }
//            $tgl = $_POST['tgl'];
//            $arr = Salestrans::list_beuty_service($tgl, $limit, $start);
//            $arr2 = Salestrans::list_beuty_service($tgl, 0, 0);
//            $this->renderJsonArrWithTotal($arr, count($arr2));
        $criteria->order = 'doc_ref';
        $criteria->params = $params;
        $model = BeautyCombination::model()->findAll($criteria);
        $total = BeautyCombination::model()->count($criteria);
        $this->renderJson($model, $total);
        Yii::app()->end();
    }

    public function actionHistory() {
        $this->renderJsonArr(Salestrans::get_trans_history($_POST['tgl']));
    }

    public function actionPrint() {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $sls = Salestrans::model()->findByPk($_POST['id']);
            $prt = new PrintReceipt($sls);
            echo CJSON::encode(array(
                'success' => $sls != null,
                'msg' => $prt->buildTxt()
            ));
            Yii::app()->end();
        }
    }

    public function actionPrintStoker() {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = 'Gagal print ke stoker';
            $sls = Salestrans::model()->findByPk($_POST['id']);
            if ($sls != null) {
                $prt = new PrintStoker($sls);
                $msg = $prt->buildTxt();
                if ($msg !== false) {
                    $status = true;
                } else {
                    $msg = 'Gagal print ke stoker';
                }
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionPrintResep() {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = 'Tidak ada resep.';
            $sls = Salestrans::model()->findByPk($_POST['id']);
            if ($sls != null) {
                $prt = new PrintResep($sls);
                $msg = $prt->buildTxt();
                if ($msg !== false) {
                    $status = true;
                } else {
                    $msg = 'Tidak ada resep.';
                }
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionPrintBeauty() {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = 'Tidak ada perawatan';
            $sls = BeautyServiceView::model()->findByAttributes(array('salestrans_details' => $_POST['id']));
            if ($sls != null) {
                $prt = new PrintBeauty($sls, $_POST['index']);
                $msg = $prt->beautyTxt();
                if ($msg !== false) {
                    $status = true;
                } else {
                    $msg = 'Tidak ada perawatan';
                }
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionPrintRekapBeauty() {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        $status = false;
        $prt = new PrintRekapBeauty(get_date_today());
        $msg = $prt->buildTxt();
        if ($msg !== false) {
            $status = true;
        } else {
            $msg = 'Tidak ada perawatan';
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }

    public function actionCekInputImport() {
        $detils = CJSON::decode($_POST['data']);
        $after_valided = array();
        foreach ($detils as $row) {
            try {
                $row['ERROR'] = '';
                $tgl = date2sql($row['TGL']);
                if (!check_in_range($_POST['tgl_from'], $_POST['tgl_to'], $tgl)) {
                    $row['ERROR'] = 'Tgl tidak masuk periode. ';
                } else {
                    $barang = Barang::model()->findByAttributes(array('kode_barang' => $row['ITEM']));
                    if ($barang == null) {
                        $row['ERROR'] = 'Item tidak ditemukan. ';
                    } else {
                        /** @var Jual $jual */
                        $jual = Jual::model()->findByAttributes(array('barang_id' => $barang->barang_id, 'store' => STOREID));
                        if ($jual == null) {
                            $row['ERROR'] = 'Harga tidak ditemukan. ';
                        } else {
                            /** @var GrupAttr $gattr */
                            $gattr = GrupAttr::model()->findByAttributes(array('grup_id' => $barang->grup_id, 'store' => STOREID));
                            if ($gattr == null) {
                                $row['ERROR'] = 'Grup Atribut tidak ditemukan. ';
                            }
                        }
                    }
                }
            } catch (Exception $e) {
                $row['ERROR'] = $e->getMessage();
            }
            $after_valided[] = $row;
        }
        Yii::app()->end(json_encode($after_valided));
    }

    public function actionImportSales() {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $msg = 'Import sales berhasil dilakukan.';
            $gl = new GL();
            $datah = CJSON::decode($_POST['data']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $no_receipt = null;
                foreach ($datah as $key1 => $row) {
                    $no_receipt = $row['SALES ID'];
                    $model = new Salestrans;
                    $model->salestrans_id = $this->generate_uuid();
                    $model->tgl = date2sql($row['TGL']);
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(PENJUALAN);
                    $model->doc_ref = $docref;
                    $model->store = STOREID;
                    $model->customer_id = $_POST['customer_id'];
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')) . CHtml::errorSummary($model));
                    }
                    $total_vatrp = $total_discrp = $total_subtotal = 0.00;
                    $data = $datah;
                    foreach ($data as $key => $detil) {
                        if ($detil['SALES ID'] == $no_receipt) {
                            $sd = new SalestransDetails;
                            $qty = get_number($detil['QTY']);
                            if ($qty < 1) {
                                throw new Exception("Qty minimal 1");
                            }
                            /** @var Barang $barang */
                            $barang = Barang::model()->findByAttributes(array('kode_barang' => $detil['ITEM']));
                            if ($barang == null) {
                                throw new Exception('Item tidak ditemukan.');
                            }
                            $sd->barang_id = $barang->barang_id;
                            $sd->qty = $qty;
                            /** @var Jual $jual */
                            $jual = Jual::model()->findByAttributes(array('barang_id' => $barang->barang_id, 'store' => STOREID));
                            if ($jual == null) {
                                throw new Exception('Harga tidak ditemukan.');
                            }
                            $sd->price = $jual->price;
                            $sd->bruto = round($sd->price * $sd->qty, 2);
                            $sd->disc = $jual->disc;
                            $sd->discrp = $jual->discrp;
                            if ($sd->disc != 0.00 && $sd->discrp == 0.00) {
                                $sd->discrp = round(($sd->disc / 100) * $sd->bruto, 2);
                            }
                            /** @var GrupAttr $gattr */
                            $gattr = GrupAttr::model()->findByAttributes(array('grup_id' => $barang->grup_id, 'store' => STOREID));
                            if ($gattr == null) {
                                throw new Exception('Grup Atribut tidak ditemukan.');
                            }
                            $sd->disc1 = 0;
                            $sd->discrp1 = 0;
                            $sd->total_pot = $sd->discrp;
                            $sd->vat = $gattr->vat;
                            $sd->vatrp = round(($sd->vat / 100) * ($sd->bruto - $sd->total_pot), 2);
                            $sd->total = (float) $sd->bruto - (float) $sd->total_pot;
                            $sd->salestrans_id = $model->salestrans_id;
                            if (!$sd->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales Detail')) . CHtml::errorSummary($sd));
                            }
                            $tipe_jual = "Sales " . $sd->barang->grup->nama_grup;
                            //GL Diskon item
                            if ($sd->total_pot != 0) {
                                $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $sd->barang->get_coa_sales_disc($model->store), $tipe_jual, $tipe_jual, $sd->total_pot, 1);
                            }
                            //GL Penjualan sesuai grup barang
                            if ($sd->bruto != 0) {
                                $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $sd->barang->get_coa_sales($model->store), $tipe_jual, $tipe_jual, -$sd->bruto, 1, $model->store);
                            }
//                    if ($model->log == 0) {
                            if ($sd->barang->grup->kategori->is_have_stock()) {
                                //------------------------------ khusus barang non jasa ------------------------------------
                                $saldo_stock = StockMoves::get_saldo_item($sd->barang_id, $model->store);
                                if ($saldo_stock < $sd->qty) {
                                    throw new Exception(t('saldo.item.fail', 'app', array(
                                        '{item}' => $sd->barang->kode_barang,
                                        '{h}' => $saldo_stock,
                                        '{r}' => $sd->qty
                                    )));
                                }
                                U::add_stock_moves(PENJUALAN, $model->salestrans_id, $model->tgl, $sd->barang_id, -$sd->qty, $model->doc_ref, $sd->barang->get_cost($model->store), $model->store);
                                // Hitung HPP
                                $cost = $sd->barang->get_cost($model->store);
                                $hpp = round($sd->qty * $cost, 2);
                                $sd->cost = $cost;
                                $sd->hpp = $hpp;
                                if (!$sd->save()) {
                                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales Hpp Detail')) . CHtml::errorSummary($sd));
                                }
                                //GL HPP
                                //  Persediaan
                                $coa_sales_hpp = $sd->barang->get_coa_sales_hpp($model->store);
                                if ($hpp != 0 && $coa_sales_hpp != null) {
                                    $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $coa_sales_hpp, $tipe_jual, $tipe_jual, $hpp, 0);
                                    $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $sd->barang->tipeBarang->coa, $tipe_jual, $tipe_jual, -$hpp, 0);
                                }
                            }
                            $total_subtotal += (float) $sd->total;
                            $total_discrp += (float) $sd->total_pot;
                            $total_vatrp += (float) $sd->vatrp;
                            unset($data[$key]);
                        }
                    }
                    $datah = $data;
                    $model->bruto = $total_subtotal;
                    $model->totalpot = $total_discrp;
                    $model->vat = $total_vatrp;
                    $total_ori = round($total_subtotal - $model->total_discrp1 + $model->vat, 2);
                    $total = round_up_to_nearest_n($total_ori, 50);
                    $model->total = $total;
                    $model->rounding = $total - $total_ori;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')) . CHtml::errorSummary($model));
                    }
                    if ($model->rounding != 0) {
                        $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_ROUNDING, "Sales $docref", "Sales $docref", -$model->rounding, 1, $model->store);
                    }
                    if (!isset($_POST['card_number'])) {
                        $_POST['card_number'] = null;
                    }
                    if (!isset($_POST['card_id'])) {
                        $_POST['card_id'] = null;
                    }
                    Payment::add_payment($_POST['bank_id'], $model->salestrans_id, $_POST['card_number'], $model->total, $_POST['card_id']);
                    //GL BANK (sesuai cara bayar)
                    //        VAT
                    //        Diskon Faktur
                    foreach ($model->payments as $pay) {
                        if ($pay->amount != 0) {
                            $pay_amount = 0;
                            if ($pay->bank->is_bank_cash()) {
                                $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $pay->bank->account_code, "Sales $docref", "Sales $docref", $pay->amount - $pay->kembali, 0, $model->store);
                                $pay_amount = $pay->amount - $pay->kembali;
                            } else {
                                if ($pay->card == null) {
                                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')) . 'Card type must select!');
                                }
                                $fee = ($pay->card->persen_fee / 100) * $pay->amount;
                                $fee = round($fee, 2);
                                $edc = $pay->amount - $fee;
                                $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $pay->bank->account_code, "Sales $docref", "Sales $docref", $edc, 0, $model->store);
                                $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_FEE_CARD, "Sales $docref", "Sales $docref", $fee, 0, $model->store);
                                $pay_amount = $edc;
                                $payment = Payment::model()->findByPk($pay->payment_id);
                                if ($payment == null) {
                                    throw new Exception("Payment tidak ditemukan!");
                                }
                                $payment->feecard = $fee;
                                if (!$payment->save()) {
                                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Payment')) . CHtml::errorSummary($payment));
                                }
                            }
                            if ($gl->is_bank_account($pay->bank->account_code)) {
                                $gl->add_bank_trans(PENJUALAN, $model->salestrans_id, $pay->bank_id, $docref, $model->tgl, $pay_amount, $model->user_id, $model->store);
                            }
                        }
                    }
                    ######################################################################################
                    ###### UNTUK NATASHA
                    ###### DISCOUNT AMOUNT TIDAK DIMASUKKAN KE DISKON SAJA; TIDAK PERLU MASUK COA VOUCHER
                    ######################################################################################
//                    if ($model->discrp != 0) {
////                  DISKON VOUCHER
//                        $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_VOUCHER, "Sales $docref", "Sales $docref", $model->discrp, 1);
//                    }
                    if ($model->vat != 0) {
                        $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_VAT, "Sales $docref", "Sales $docref", -$model->vat, 1);
                    }
                    $gl->validate();
                    $ref->save(PENJUALAN, $model->salestrans_id, $docref);
                    if (count($datah) <= 0) {
                        break;
                    }
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
//                'id' => $docref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }



}
