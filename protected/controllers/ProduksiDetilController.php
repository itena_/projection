<?php
class ProduksiDetilController extends GxController
{
    public function actionCreate()
    {
        $model = new ProduksiDetil;
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['ProduksiDetil'][$k] = $v;
            }
            $model->attributes = $_POST['ProduksiDetil'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->produksi_detil_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'ProduksiDetil');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['ProduksiDetil'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['ProduksiDetil'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->produksi_detil_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->produksi_detil_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'ProduksiDetil')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if(isset($_POST['produksi_id'])){
            $criteria->addCondition('produksi_id = :produksi_id');
            $param[':produksi_id'] = $_POST['produksi_id'];
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if ((isset($_POST['racikan_id']))) {
            $racikan = $_POST['racikan_id'];
            $cmd = new DbCmd('{{racikan_detail}} rd');
        
            $cmd->addSelect('rd.*, b.kode_barang, b.nama_barang, rd.qty qty_bom');
//            $cmd->addLeftJoin('{{racikan}} r', ' r.barang_id = t.barang_id ');
//            $cmd->addLeftJoin('{{racikan_detail}} rd', ' rd.racikan_id = r.racikan_id ');
            $cmd->addLeftJoin('{{barang}} b', ' b.barang_id = rd.barang_id ');
            $cmd->addCondition("rd.racikan_id = '$racikan'");
            $cmd->addOrder = 'b.kode_barang ASC';

            $model = $cmd->queryAll();
            $this->renderJsonArr($model);
        }
        $criteria->params = $param;
        $model = ProduksiDetil::model()->findAll($criteria);
        $total = ProduksiDetil::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}