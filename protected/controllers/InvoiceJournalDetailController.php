<?php

class InvoiceJournalDetailController extends GxController {

    public function actionIndex() {
        $criteria = new CDbCriteria();
        if(isset($_POST['invoice_journal_id'])){
            $criteria->addCondition('invoice_journal_id = :invoice_journal_id');
            $param[':invoice_journal_id'] = $_POST['invoice_journal_id'];
        }
        $criteria->params = $param;
        $model = InvoiceJournalDetail::model()->findAll($criteria);
        $total = InvoiceJournalDetail::model()->count($criteria);

        $this->renderJson($model, $total);
    }

}
