<?php
class BeautyController extends GxController
{
    public function actionCreate()
    {
        $model = new Employees;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $tipe = TipeEmployee::model()->findByAttributes(array('nama_' => 'Beautician'));
                if ($tipe == null) {
                    throw new Exception('Tipe employee untuk beauty tidak ditemukan');
                }
                $_POST['tipe'] = $tipe->tipe_employee_id;
                $_POST['kode_employee'] = $_POST['kode_beauty'];
                $_POST['nama_employee'] = $_POST['nama_beauty'];
                $_POST['store'] = STOREID;
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Beauty'][$k] = $v;
                }
                $model->attributes = $_POST['Beauty'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Beauty')) . CHtml::errorSummary($model));
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Beauty');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Beauty'][$k] = $v;
            }
            $msg = t('save.fail', 'app');
            $model->attributes = $_POST['Beauty'];
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id', 'app', array('{id}' => $model->beauty_id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->beauty_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['f']) && $_POST['f'] == 'cmp') {
            $criteria->addCondition("store = :store");
            $param[':store'] = STOREID;
        }
        if (isset($_POST['store'])) {
            $criteria->addCondition("store like :store");
            $param[':store'] = "%" . $_POST['store'] . "%";
        }
        if (isset($_POST['kode_beauty'])) {
            $criteria->addCondition("kode_beauty like :kode_beauty");
            $param[':kode_beauty'] = "%" . $_POST['kode_beauty'] . "%";
        }
        if (isset($_POST['nama_beauty'])) {
            $criteria->addCondition("nama_beauty like :nama_beauty");
            $param[':nama_beauty'] = "%" . $_POST['nama_beauty'] . "%";
        }
        if (isset($_POST['gol_id'])) {
            $criteria->addCondition("gol_id = :gol_id");
            $param[':gol_id'] = $_POST['gol_id'];
        }
        $criteria->params = $param;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        } else {
            if (isset ($_POST['mode']) && $_POST['mode'] != 'lib') {
                $criteria->addCondition("active = 1");
            }
        }
        $model = Beauty::model()->findAll($criteria);
        $total = Beauty::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionOffduty()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = BeautyOffduty::model()->findAll($criteria);
        $total = BeautyOffduty::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}