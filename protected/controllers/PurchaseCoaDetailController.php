<?php

class PurchaseCoaDetailController extends GxController {

    public function actionIndex() {
        $criteria = new CDbCriteria();
        if(isset($_POST['purchase_coa_id'])){
            $criteria->addCondition('purchase_coa_id = :purchase_coa_id');
            $param[':purchase_coa_id'] = $_POST['purchase_coa_id'];
        }
        $criteria->params = $param;
        $model = PurchaseCoaDetail::model()->findAll($criteria);
        $total = PurchaseCoaDetail::model()->count($criteria);

        $this->renderJson($model, $total);
    }

}
