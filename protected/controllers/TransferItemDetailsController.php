<?php
class TransferItemDetailsController extends GxController
{
    public function actionIndexIn()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("visible = 1");
        $criteria->addCondition("transfer_item_id = :transfer_item_id");
        $criteria->params = array(':transfer_item_id'=>$_POST['transfer_item_id']);
        $model = TransferItemDetails::model()->findAll($criteria);
        $total = TransferItemDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndexOut()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("visible = 1");
        $criteria->select = "transfer_item_details_id, -qty qty, barang_id, transfer_item_id, price, sub_total, disc, disc_rp, total_disc, total_dpp, ppn, ppn_rp, pph_id, pph, pph_rp, total, charge";
        $criteria->addCondition("transfer_item_id = :transfer_item_id");
        $criteria->params = array(':transfer_item_id'=>$_POST['transfer_item_id']);
        $model = TransferItemDetails::model()->findAll($criteria);
        $total = TransferItemDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}