<?php
class InvoiceJournalController extends GxController
{
    public function actionCreate()
    {        
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $is_new = $_POST['mode'] == 0;
            $ij_id = $_POST['id'];
            $posting = false;
            if (isset($_POST['posting'])) {
                $posting = $_POST['posting'] == 'true';
            }
            $status = false;
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {                               
                $model = $is_new ? new InvoiceJournal : $this->loadModel($ij_id, "InvoiceJournal");
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(INVOICE_JOURNAL);
                } else {
                    $sudahposting = InvoiceJournal::model()->findByPk($_POST['id']);
                    if($sudahposting->p == 1){
                        echo CJSON::encode(array(
                            'success' => FALSE,
                            'msg' => 'Gagal! Invoice Journal sudah terposting!'));
                        Yii::app()->end();
                    }
                    InvoiceJournalDetail::model()->deleteAll("invoice_journal_id = :invoice_journal_id", array(':invoice_journal_id' => $ij_id));
                    $docref = $model->doc_ref;
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil')
                        continue;
                    if (is_angka($v))
                        $v = get_number($v);
                    $_POST['InvoiceJournal'][$k] = $v;
                }
                $_POST['InvoiceJournal']['doc_ref'] = $docref;
//                $_POST['InvoiceJournal']['store'] = $_POST['store'];
                $model->attributes = $_POST['InvoiceJournal'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournal')) . CHtml::errorSummary($model));
                }
//                $total = 0;
                $gl = new GL();
                foreach ($detils as $detil) {
                    $item_details = new InvoiceJournalDetail();
                    $_POST['InvoiceJournalDetail']['store_kode'] = $detil['store_kode'];
                    $_POST['InvoiceJournalDetail']['debit'] = get_number($detil['debit']);
                    $_POST['InvoiceJournalDetail']['kredit'] = get_number($detil['kredit']);
//                    $_POST['InvoiceJournalDetail']['grup'] = get_number($detil['grup']);
                    $_POST['InvoiceJournalDetail']['account_code'] = $detil['account_code'];
                    $_POST['InvoiceJournalDetail']['memo_'] = $detil['memo_'];
                    $_POST['InvoiceJournalDetail']['invoice_journal_id'] = $model->invoice_journal_id;
                    $item_details->attributes = $_POST['InvoiceJournalDetail'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Invoice Journa')) . CHtml::errorSummary($item_details));
                    }
//                    $cm = ChartMaster::model()->findByAttributes(array('account_code' => $item_details->account_code));
//                    $ct = ChartTypes::model()->findByAttributes(array('id' => $cm->kategori));
//                    if ($item_details->account_code == '211101' || $item_details->account_code == '211202') {
//                        $total += $item_details->kredit;
//                    }
                    if ($posting) {                        
                        if ($item_details->kredit == 0) {
//                      Debit
                            $gl->add_gl(INVOICE_JOURNAL, $model->invoice_journal_id, $model->tgl, $model->doc_ref, $item_details->account_code, $item_details->memo_, '', $item_details->debit, 0, $item_details->store_kode);
                        } else if ($item_details->debit == 0) {
//                      Kredit
                            $gl->add_gl(INVOICE_JOURNAL, $model->invoice_journal_id, $model->tgl, $model->doc_ref, $item_details->account_code, $item_details->memo_, '', -($item_details->kredit), 0, $item_details->store_kode);
                        } else {
                            echo CJSON::encode(array(
                                'success' => FALSE,
                                'msg' => 'Gagal! Debit dan Kredit tidak masuk dalam jurnal!'));
                            Yii::app()->end();
                        }
                    }
                }
//                $model->total = $total;
                if ($posting) {
                    $gl->validate();
                    $model->p = 1;
                }
                $model->save();
                if ($is_new) {
                    $ref->save(INVOICE_JOURNAL, $model->invoice_journal_id, $docref);
                }
//                $ayam->save();
                $transaction->commit();
                $msg = "Sukses disimpan";
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

//<<<<<<< HEAD
//    public function actionPosting() {
//        $id = $_POST['id'];
//        app()->db->autoCommit = false;
//        $transaction = Yii::app()->db->beginTransaction();
//        try {
//            $model = $this->loadModel($id, 'InvoiceJournal');
//            $model->p = 1;
//            $model->save();
//            $transaction->commit();
//            $status = true;
//            $msg = 'taraaaa';
//        } catch (Exception $ex) {
//            $transaction->rollback();
//            $msg = $ex->getMessage();
//            $status = false;
//        }
//        echo CJSON::encode(array(
//            'success' => $status,
//            'msg' => $msg));
//        Yii::app()->end();
//    }
//    public function actionIndex() {
//=======
    public function actionUnposting($id)
    {
        /** @var InvoiceJournal $model */
        $model = $this->loadModel($id, 'InvoiceJournal');
        $msg = 'Invoice jurnal berhasil diunposting';
//        if (isset($_POST) && !empty($_POST)) {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            GlTrans::model()->deleteAllByAttributes([
                'type' => INVOICE_JOURNAL,
                'type_no' => $id
            ]);
            BankTrans::model()->deleteAllByAttributes([
                'type_' => INVOICE_JOURNAL,
                'trans_no' => $id
            ]);
            $model->p = 0;
            $model->editable = 0;
            if (!$model->save()) {
                throw new Exception(t('save.model.fail', 'app',
                        array('{model}' => 'Invoice Journal')) . CHtml::errorSummary($model));
            }
            $transaction->commit();
            $status = true;
        } catch (Exception $e) {
            $transaction->rollback();
            $status = false;
            $msg = $e->getMessage();
        }
        app()->db->autoCommit = true;
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
//        }
    }
    public function actionPosting()
    {
        $id = $_POST['id'];
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            /** @var InvoiceJournal $model */
            $model = $this->loadModel($id, 'InvoiceJournal');
            $model->p = 1;
            $model->save();
            $transaction->commit();
            $status = true;
            $msg = 'Invoice Journal berhasil di posting.';
        } catch (Exception $ex) {
            $transaction->rollback();
            $msg = $ex->getMessage();
            $status = false;
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }
    public function actionIndex()
    {
//>>>>>>> 00f23e4e0a2d8a03774de01464fb862336b2baff
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $param = array();
        $criteria = new CDbCriteria();
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = date(:tgl)');
            $param[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['doc_ref'])) {
            $docref = $_POST['doc_ref'];
            $criteria->addCondition('doc_ref like :doc_ref');
            $param[':doc_ref'] = "%$docref%";
        }
        if (isset($_POST['doc_ref_other'])) {
            $docrefother = $_POST['doc_ref_other'];
            $criteria->addCondition('doc_ref_other like :doc_ref_other');
            $param[':doc_ref_other'] = "%$docrefother%";
        }
//        if (isset($_POST['tgl'])) {
//            $criteria->addCondition('tgl = date(:tgl)');
//            $param[':tgl'] = $_POST['tgl'];
//        }
        $criteria->order = 'doc_ref';
        $criteria->params = $param;
        $model = InvoiceJournal::model()->findAll($criteria);
        $total = InvoiceJournal::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionImport()
    {
//        $id = $_POST['id'];
        app()->db->autoCommit = false;
        try {
            $transaction = Yii::app()->db->beginTransaction();
            $data = Yii::app()->db->createCommand(
//                            "SELECT * FROM import_julian")
                "SELECT * FROM import_julian_copy")
                ->queryAll(true);
            $grup = 0;
            $total = 0;
            foreach ($data as $d1) {
//                $transaction = Yii::app()->db->beginTransaction();
                if ($d1['grup'] != '') {
                    $supname = Yii::app()->db->createCommand(
                        "SELECT nama_supplier FROM supplier WHERE kode_supplier = '" . $d1['Supplier name'] . "'")
                        ->queryScalar();
                    $supcode = Supplier::model()->findByAttributes(array('supplier_name' => $supname));
                    $tgl = gmdate('Y-m-d', (($d1['Tgl'] - 25569) * 86400));
                    $tgljt = gmdate('Y-m-d', (($d1['Term of Payment'] - 25569) * 86400));
                    if ($d1['grup'] != $grup) {
                        $total = 0;
                        $grup = $d1['grup'];
                        $model = new InvoiceJournal();
                        $ref = new Reference();
                        $docref = $ref->get_next_reference(INVOICE_JOURNAL);
                        $model->doc_ref = $docref;
                        $model->doc_ref_other = $d1['invoice'];
                        $model->supplier_id = $supcode->supplier_id;
                        $model->tgl = $tgl;
                        $model->tgl_jatuh_tempo = $tgljt;
                        $model->store_kode = STOREID;
                        if (!$model->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournal')) . CHtml::errorSummary($model));
                        }
                        /*
                         * DEBIT
                         */
                        if ($d1['DPP'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['DPP'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = $d1['coa'];
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['PPN masukan'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['PPN masukan'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115008;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pajak dibayar dimuka PPh 22'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Pajak dibayar dimuka PPh 22'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115003;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pajak dibayar dimuka PPh 25'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Pajak dibayar dimuka PPh 25'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115001;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pajak dibayar dimuka PPh 21'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Pajak dibayar dimuka PPh 21'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115002;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pajak dibayar dimuka PPh 23'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Pajak dibayar dimuka PPh 23'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115004;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pajak dibayar dimuka PPh 4(2)'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Pajak dibayar dimuka PPh 4(2)'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115005;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pajak dibayar dimuka PPh 26'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Pajak dibayar dimuka PPh 26'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115006;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Beban Pajak PPN'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Beban Pajak PPN'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 660002;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Beban Pajak PPh'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Beban Pajak PPh'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 660003;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Beban Pajak Lainnya'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Beban Pajak Lainnya'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 669998;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Ekspedisi'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Ekspedisi'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 630005;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Foreign exchange rate - Realized Loss'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Foreign exchange rate - Realized Loss'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 730004;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        /*
                         * KREDIT
                         */
                        if ($d1['Hutang pph 21'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Hutang pph 21'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 211313;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Hutang pph 23'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Hutang pph 21'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 211313;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Hutang pph 4(2)'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Hutang pph 4(2)'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 211312;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Hutang Pajak - PPh 2529'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Hutang Pajak - PPh 2529'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 211310;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Foreign exchange rate - Realized Gain'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Foreign exchange rate - Realized Gain'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 730002;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pembulatan'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Pembulatan'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 739997;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['HUTANG DAGANG'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['HUTANG DAGANG'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 211101;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                            $total += $item_detail->kredit;
                        }
                        if ($d1['Biaya Yang Masih Harus Dibayar'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Biaya Yang Masih Harus Dibayar'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 211202;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                            $total += $item_detail->kredit;
                        }
                        $model->total = $total;
                        if (!$model->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournal')) . CHtml::errorSummary($model));
                        }
                        $ref->save(INVOICE_JOURNAL, $model->invoice_journal_id, $docref);
                    } else {
                        if ($d1['DPP'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['DPP'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = $d1['coa'];
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['PPN masukan'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['PPN masukan'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115008;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pajak dibayar dimuka PPh 22'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Pajak dibayar dimuka PPh 22'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115003;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pajak dibayar dimuka PPh 25'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Pajak dibayar dimuka PPh 25'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115001;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pajak dibayar dimuka PPh 21'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Pajak dibayar dimuka PPh 21'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115002;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pajak dibayar dimuka PPh 23'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Pajak dibayar dimuka PPh 23'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115004;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pajak dibayar dimuka PPh 4(2)'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Pajak dibayar dimuka PPh 4(2)'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115005;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pajak dibayar dimuka PPh 26'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Pajak dibayar dimuka PPh 26'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 115006;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Beban Pajak PPN'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Beban Pajak PPN'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 660002;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Beban Pajak PPh'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Beban Pajak PPh'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 660003;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Beban Pajak Lainnya'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Beban Pajak Lainnya'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 669998;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Ekspedisi'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Ekspedisi'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 630005;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Foreign exchange rate - Realized Loss'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = $d1['Foreign exchange rate - Realized Loss'];
                            $item_detail->kredit = 0;
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 730004;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        /*
                         * KREDIT
                         */
                        if ($d1['Hutang pph 21'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Hutang pph 21'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 211313;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Hutang pph 23'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Hutang pph 23'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 211311;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Hutang pph 4(2)'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Hutang pph 4(2)'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 211312;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Hutang Pajak - PPh 2529'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Hutang Pajak - PPh 2529'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 211310;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Foreign exchange rate - Realized Gain'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Foreign exchange rate - Realized Gain'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 730002;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['Pembulatan'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Pembulatan'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 739997;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                        }
                        if ($d1['HUTANG DAGANG'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['HUTANG DAGANG'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 211101;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                            $total += $item_detail->kredit;
                        }
                        if ($d1['Biaya Yang Masih Harus Dibayar'] != '') {
                            $item_detail = new InvoiceJournalDetail();
                            $item_detail->store_kode = $d1['Branch'];
                            $item_detail->debit = 0;
                            $item_detail->kredit = $d1['Biaya Yang Masih Harus Dibayar'];
                            $item_detail->memo_ = $d1['Note'];
                            $item_detail->account_code = 211202;
                            $item_detail->invoice_journal_id = $model->invoice_journal_id;
                            if (!$item_detail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                            }
                            $total += $item_detail->kredit;
                        }
                        $model->total = $total;
                        if (!$model->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournal')) . CHtml::errorSummary($model));
                        }
//                        $total = 0;
                    }
                } else {
//                    $total = 0;
                    $supname = Yii::app()->db->createCommand(
                        "SELECT nama_supplier FROM supplier WHERE kode_supplier = '" . $d1['Supplier name'] . "'")
                        ->queryScalar();
                    $supcode = Supplier::model()->findByAttributes(array('supplier_name' => $supname));
                    $tgl = gmdate('Y-m-d', (($d1['Tgl'] - 25569) * 86400));
                    $tgljt = gmdate('Y-m-d', (($d1['Term of Payment'] - 25569) * 86400));
                    $model = new InvoiceJournal();
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(INVOICE_JOURNAL);
                    $model->doc_ref = $docref;
                    $model->doc_ref_other = $d1['invoice'];
                    $model->supplier_id = $supcode->supplier_id;
                    $model->tgl = $tgl;
                    $model->tgl_jatuh_tempo = $tgljt;
                    $model->store_kode = STOREID;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournal')) . CHtml::errorSummary($model));
                    }
                    /*
                     * DEBIT
                     */
                    if ($d1['DPP'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = $d1['DPP'];
                        $item_detail->kredit = 0;
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = $d1['coa'];
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['PPN masukan'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = $d1['PPN masukan'];
                        $item_detail->kredit = 0;
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 115008;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Pajak dibayar dimuka PPh 22'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = $d1['Pajak dibayar dimuka PPh 22'];
                        $item_detail->kredit = 0;
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 115003;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Pajak dibayar dimuka PPh 25'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = $d1['Pajak dibayar dimuka PPh 25'];
                        $item_detail->kredit = 0;
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 115001;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Pajak dibayar dimuka PPh 21'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = $d1['Pajak dibayar dimuka PPh 21'];
                        $item_detail->kredit = 0;
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 115002;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Pajak dibayar dimuka PPh 23'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = $d1['Pajak dibayar dimuka PPh 23'];
                        $item_detail->kredit = 0;
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 115004;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Pajak dibayar dimuka PPh 4(2)'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = $d1['Pajak dibayar dimuka PPh 4(2)'];
                        $item_detail->kredit = 0;
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 115005;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Pajak dibayar dimuka PPh 26'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = $d1['Pajak dibayar dimuka PPh 26'];
                        $item_detail->kredit = 0;
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 115006;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Beban Pajak PPN'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = $d1['Beban Pajak PPN'];
                        $item_detail->kredit = 0;
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 660002;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Beban Pajak PPh'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = $d1['Beban Pajak PPh'];
                        $item_detail->kredit = 0;
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 660003;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Beban Pajak Lainnya'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = $d1['Beban Pajak Lainnya'];
                        $item_detail->kredit = 0;
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 669998;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Ekspedisi'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = $d1['Ekspedisi'];
                        $item_detail->kredit = 0;
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 630005;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Foreign exchange rate - Realized Loss'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = $d1['Foreign exchange rate - Realized Loss'];
                        $item_detail->kredit = 0;
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 730004;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    /*
                     * KREDIT
                     */
                    if ($d1['Hutang pph 21'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = 0;
                        $item_detail->kredit = $d1['Hutang pph 21'];
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 211313;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Hutang pph 23'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = 0;
                        $item_detail->kredit = $d1['Hutang pph 21'];
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 211313;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Hutang pph 4(2)'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = 0;
                        $item_detail->kredit = $d1['Hutang pph 4(2)'];
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 211312;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Hutang Pajak - PPh 2529'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = 0;
                        $item_detail->kredit = $d1['Hutang Pajak - PPh 2529'];
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 211310;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Foreign exchange rate - Realized Gain'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = 0;
                        $item_detail->kredit = $d1['Foreign exchange rate - Realized Gain'];
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 730002;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['Pembulatan'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = 0;
                        $item_detail->kredit = $d1['Pembulatan'];
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 739997;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                    }
                    if ($d1['HUTANG DAGANG'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = 0;
                        $item_detail->kredit = $d1['HUTANG DAGANG'];
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 211101;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                        $total += $item_detail->kredit;
                    }
                    if ($d1['Biaya Yang Masih Harus Dibayar'] != '') {
                        $item_detail = new InvoiceJournalDetail();
                        $item_detail->store_kode = $d1['Branch'];
                        $item_detail->debit = 0;
                        $item_detail->kredit = $d1['Biaya Yang Masih Harus Dibayar'];
                        $item_detail->memo_ = $d1['Note'];
                        $item_detail->account_code = 211202;
                        $item_detail->invoice_journal_id = $model->invoice_journal_id;
                        if (!$item_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournalDetail')) . CHtml::errorSummary($item_detail));
                        }
                        $total += $item_detail->kredit;
                    }
                    $model->total = $total;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'InvoiceJournal')) . CHtml::errorSummary($model));
                    }
                    $ref->save(INVOICE_JOURNAL, $model->invoice_journal_id, $docref);
                    $total = 0;
                }
//                $transaction->commit();
            }
            $transaction->commit();
            $status = true;
            $msg = 'taraaaa';
        } catch (Exception $ex) {
            $transaction->rollback();
            $msg = $ex->getMessage();
            $status = false;
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }
}
