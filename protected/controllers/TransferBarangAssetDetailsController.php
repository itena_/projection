<?php

class TransferBarangAssetDetailsController extends GxController {

public function actionIndexIn()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("transfer_asset_id = :transfer_asset_id");
        $criteria->params = array(':transfer_asset_id'=>$_POST['transfer_asset_id']);
        $model = TransferBarangAssetDetails::model()->findAll($criteria);
        $total = TransferBarangAssetDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndexOut()
    {
        $criteria = new CDbCriteria();
        $criteria->select = "transfer_asset_details_id,-qty qty,barang_asset_id,transfer_asset_id,
        price,-total total,disc,-discrp discrp,-bruto bruto,vat,-vatrp vatrp,disc1,-discrp1 discrp,-total_pot total_pot";
        $criteria->addCondition("transfer_asset_id = :transfer_asset_id");
        $criteria->params = array(':transfer_asset_id'=>$_POST['transfer_asset_id']);
        $model = TransferBarangAssetDetails::model()->findAll($criteria);
        $total = TransferBarangAssetDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}