<?php
class OrderDroppingController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {

            $status = false;
            $msg = "Stored data failed.";
            $docref = '';
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {

                $history = new DroppingHistory;
                $namauser = Yii::app()->user->getName();
                $iduser = Yii::app()->user->getId();

                $is_new = $_POST['mode'] == 0;
                $model = $is_new ? new OrderDropping : $this->loadModel($_POST['id'], 'OrderDropping');
                if (!$is_new && ($model == null)) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'OrderDropping')) . "Fatal error, record not found.");
                }
                if (!$is_new) {
                    $user = Users::model()->findByPk(Yii::app()->user->getId());

                    $creator = $model->store == STOREID;
                    $approver = $user->is_available_role(257);

                    if (!$creator && !$approver) {
                        throw new Exception("Anda tidak diperbolehkan mengubah data ini.");
                    }
                    if (!($creator && $model->lunas == PR_DRAFT) && !($approver && $model->lunas == PR_OPEN) ) {
                        throw new Exception("Data Transfer Requests tidak bisa diubah.");
                    }


                    if($model->lunas == PR_DRAFT)
                    {
                        $history->status = "DRAFT EDITED";
                        $history->desc = "Draft di edit oleh ".$namauser;
                    }
                    elseif ($model->lunas == PR_OPEN)
                    {
                        $history->status = "REQUEST EDITED";
                        $history->desc = "Request di edit oleh ".$namauser;
                    }

                    $docref = $model->doc_ref;



                    OrderDroppingDetails::model()->updateAll(array('visible' => 0), 'order_dropping_id = :order_dropping_id',
                        array(':order_dropping_id' => $model->order_dropping_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['OrderDropping'][$k] = $v;
                }
                
                if ($is_new) {
                    /*
                     * Order Dropping baru status DRAFT dan belum memiliki No. Reference
                     */
                    $_POST['OrderDropping']['doc_ref'] = PR_DRAFT;
                    $history->status = "DRAFTED";
                    $history->desc = "Draft Transfer Request telah dibuat oleh ".$namauser;
                }

                $model->up = DR_SEND;

                $_POST['OrderDropping']['doc_ref'] = $docref;
                $model->attributes = $_POST['OrderDropping'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'OrderDropping')) . CHtml::errorSummary($model));
                foreach ($detils as $detil) {
                    $item_details = new OrderDroppingDetails;
                    $_POST['OrderDroppingDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['OrderDroppingDetails']['qty'] = get_number($detil['qty']);
                    $_POST['OrderDroppingDetails']['order_dropping_id'] = $model->order_dropping_id;
                    $item_details->attributes = $_POST['OrderDroppingDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'OrderDroppingDetails')) . CHtml::errorSummary($item_details));
                }




                $history->order_dropping_id = $model->order_dropping_id;
                $history->user_id = $iduser;
                $history->user = $namauser;
                $history->doc_ref = $docref;
                $history->store = $model->store;
                $history->store_pengirim = $model->store_pengirim;
                $history->tgl = new CDbExpression('NOW()');

                if (!$history->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'DroppingHistory')) . CHtml::errorSummary($history));

                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
                //app()->db->autoCommit = true;

//                if(PUSH_PUSAT){
//                    U::runCommand('soap','orderdropping', '--id=' . $model->order_dropping_id,  'order_dropping_'.$model->order_dropping_id.'.log');
//                }
//                if (PUSH_ALL_CABANG) {
                    U::runCommand('soap','orderdroppingall', '--id=' . $model->order_dropping_id,  'orderdroppingall_'.$model->order_dropping_id.'.log');

                    U::runCommand('soap','droppinghistory', '--id=' . $history->id_history,  'droppinghistory_'.$history->id_history.'.log');
                    //                }
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionOpen()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $id= $_POST['id'];
            $model = $this->loadModel($id, "OrderDropping");
            //$history = DroppingHistory::model()->findByAttributes(['order_dropping_id' => $_POST['id']]);
            //$history = $this->loadModel($id, "DroppingHistory");

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if ($model->lunas != PR_DRAFT) {
                    throw new Exception("Transfer Requests telah diposting sebelumnya.");
                }
                if ($model->store != STOREID){
                    throw new Exception("Anda tidak diperbolehkan untuk memposting Transfer Requests ini.");
                }
                $ref = new Reference();
                $docref = $ref->get_next_reference(ORDER_DROPPING);
                $model->doc_ref = $docref;
                $model->lunas = PR_OPEN;
                $model->up = DR_SEND;


                $model->tdate = new CDbExpression('NOW()');
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'OrderDropping')) . CHtml::errorSummary($model));
                }
                $ref->save(ORDER_DROPPING, $model->order_dropping_id, $docref);


                $namauser = Yii::app()->user->getName();
                $iduser = Yii::app()->user->getId();

                $history = new DroppingHistory();
                $history->order_dropping_id = $model->order_dropping_id;
                $history->user_id = $iduser;
                $history->user = $namauser;
                $history->doc_ref = $model->doc_ref;
                $history->store = $model->store;
                $history->store_pengirim = $model->store_pengirim;
                $history->tgl = new CDbExpression('NOW()');
                $history->status = "SENDED";
                $history->desc = "Request telah dikirim ke ".$model->store_pengirim;

                if (!$history->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'DroppingHistory')) . CHtml::errorSummary($history));

                $transaction->commit();
                $msg = "Order berhasil diposting dengan nomor <b>".$model->doc_ref."</b>.";
                $status = true;
                //app()->db->autoCommit = true;

//                if(PUSH_PUSAT){
//                    U::runCommand('soap','orderdropping', '--id=' . $model->order_dropping_id,  'order_dropping_'.$model->order_dropping_id.'.log');
//                }
//                if (PUSH_ALL_CABANG) {
                U::runCommand('soap','orderdroppingall', '--id=' . $model->order_dropping_id,  'orderdroppingall_'.$model->order_dropping_id.'.log');
                U::runCommand('soap','droppinghistory', '--id=' . $history->id_history,  'droppinghistory_'.$history->id_history.'.log');
                //                }

            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $model->doc_ref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionResend()
    {
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Proses kirim ulang gagal.";

            try
            {
                $model = $this->loadModel($_POST['id'], 'OrderDropping');
                $model->up = DR_SEND;

                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'OrderDropping')) . CHtml::errorSummary($model));
                }

                U::runCommand('soap', 'ResendOrderdroppingAll', '--id=' . $model->order_dropping_id, 'orderdroppingall_' . $model->order_dropping_id . '.log');

                U::runCommand('soap','droppinghistoryresend', '--id=' . $model->order_dropping_id,  'droppinghistory_'.$model->order_dropping_id.'.log');

                $msg = "Nomor Transfer Requests <b>".$model->doc_ref."</b> telah di kirim ulang..";
                $status = true;

            }
            catch (Exception $ex)
            {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $model->doc_ref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionReapprove()
    {
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Proses Re approval gagal.";

            try {
                if (!HEADOFFICE) {
                    throw new Exception("Anda tidak dapat melakukan approve.");
                }

                $model = $this->loadModel($_POST['id'], 'OrderDropping');
                $model->up = DR_APPROVE;

                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'OrderDropping')) . CHtml::errorSummary($model));
                }

                $status = true;
                $msg = "Nomor Transfer Approval <b>".$model->doc_ref."</b> telah dikirim ulang.";

                app()->db->autoCommit = true;

//                if (PUSH_ALL_CABANG) {
                U::runCommand('soap','orderdroppingall', '--id=' . $model->order_dropping_id,  'orderdroppingall_'.$model->order_dropping_id.'.log');
//                }
                U::runCommand('soap','droppinghistoryresend', '--id=' . $model->order_dropping_id,  'droppinghistory_'.$model->order_dropping_id.'.log');


            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->order_dropping_id));
            }
        }
    }

    public function actionApprove()
    {
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Proses approval gagal.";

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if (!HEADOFFICE) {
                    throw new Exception("Anda tidak dapat melakukan approve.");
                }

                $model = $this->loadModel($_POST['id'], 'OrderDropping');
                $model->approved = 1;
                $model->approved_user_id = Yii::app()->user->getId();
                $model->approved_date = new CDbExpression('NOW()');
                $model->up = DR_APPROVE;
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'OrderDropping')) . CHtml::errorSummary($model));
                }

                //$model = $this->loadModel($_POST['id'], 'Users');
                $namauser = Yii::app()->user->getName();
                $iduser = Yii::app()->user->getId();

                $history = new DroppingHistory();
                $history->order_dropping_id = $model->order_dropping_id;
                $history->user_id = $iduser;
                $history->user = $namauser;
                $history->doc_ref = $model->doc_ref;
                $history->store = $model->store;
                $history->store_pengirim = $model->store_pengirim;
                $history->tgl = new CDbExpression('NOW()');
                $history->status = "APPROVED";
                $history->desc = "Request telah di approve oleh ".$namauser;

                if (!$history->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'DroppingHistory')) . CHtml::errorSummary($history));

                $transaction->commit();
                $status = true;
                $msg = "Nomor Transfer Requests <b>".$model->doc_ref."</b> telah disetujui.";
                //app()->db->autoCommit = true;

//                if (PUSH_ALL_CABANG) {
                U::runCommand('soap','orderdroppingall', '--id=' . $model->order_dropping_id,  'orderdroppingall_'.$model->order_dropping_id.'.log');

                U::runCommand('soap','droppinghistory', '--id=' . $history->id_history,  'droppinghistory_'.$history->id_history.'.log');
                    //                }
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            }
            else {
                $this->redirect(array('view', 'id' => $model->order_dropping_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'OrderDropping')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        
        $criteria = new CDbCriteria();
        $param = array();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $param[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['doc_ref'])) {
            $criteria->addCondition('doc_ref LIKE :doc_ref');
            $param[':doc_ref'] = '%' . $_POST['doc_ref'] . '%';
        }
        if (isset($_POST['note'])) {
            $criteria->addCondition('note LIKE :note');
            $param[':note'] = '%' . $_POST['note'] . '%';
        }
        if (isset($_POST['store'])) {
            $criteria->addCondition('store LIKE :store');
            $param[':store'] = '%' . $_POST['store'] . '%';
        }
        if (isset($_POST['store_pengirim'])) {
            $criteria->addCondition('store_pengirim LIKE :store_pengirim');
            $param[':store_pengirim'] = '%' . $_POST['store_pengirim'] . '%';
        }
        if (isset($_POST['lunas']) && $_POST['lunas'] != 'all') {
            $criteria->addCondition('lunas = :lunas');
            $param[':lunas'] = $_POST['lunas'];
        }
        if (isset($_POST['up']) && $_POST['up'] != 'all') {
            $criteria->addCondition('up = :up');
            $param[':up'] = $_POST['up'];
        }
        if (isset($_POST['approved']) && $_POST['approved'] != 'all') {
            $criteria->addCondition('approved = :approved');
            $param[':approved'] = $_POST['approved'];
        }
        
        if (isset($_POST['order_dropping_id'])) {
            $criteria->addCondition('order_dropping_id = :order_dropping_id');
            $param[':order_dropping_id'] = $_POST['order_dropping_id'];
        }
        if (isset($_POST['query'])) {
            /*
             * Filter saat input No. Transfer Requests pada form Dropping
             * No. Transfer Requests yang ditampilkan adalah :
             * - lunas != 2, Qty yang diminta belum terpenuhi
             * - approved = 1, Transfer Requests telah diapprove pihak operasional
             */
            $criteria->addCondition('doc_ref like :query');
            $criteria->addCondition('store_pengirim = :branch');
            $criteria->addCondition('lunas != :close AND lunas != :draft');
            $criteria->addCondition('approved = 1');
            $param[':query'] = '%' . $_POST['query'] . '%';
            $param[':close'] = PR_CLOSED;
            $param[':draft'] = PR_DRAFT;
            $param[':branch'] = STOREID;
        } else {
            /*
             * Filter saat menampilkan Menu Transfer Requests
             * hanya menampilkan :
             * - Order yang dibuat oleh cabang bersangkutan
             * - Order yang ditujukan kepada cabang bersangkutan
             *   (status tidak sama dengan DRAFT)
             */
            $criteria->addCondition('store = :branch OR (store_pengirim = :branch AND lunas != :draft)');
            $param[':branch'] = STOREID;
            $param[':draft'] = PR_DRAFT;
        }
        
        $criteria->order = 'tgl DESC, tdate DESC';
        
        $criteria->params = $param;
        $model = OrderDropping::model()->findAll($criteria);
        $total = OrderDropping::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}