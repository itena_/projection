<?php
class AishaAntrianController extends GxController
{
    public function actionAntrianCounter()
    {
        $bagian = $counter = '';
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if (isset($_POST['bagian'])) {
            $bagian = $_POST['bagian'];
        }
        if (isset($_POST['counter'])) {
            $counter = $_POST['counter'];
        }
        if (isset($_POST['counter_asal'])) {
            $counter = $_POST['counter_asal'];
        }
        global $step_antrian;
        $criteria = new CDbCriteria();
        $param = array();
        $criteria->addCondition("bagian = :bagian");
        $param[':bagian'] = $step_antrian[$bagian]['before'];
        $criteria->addCondition("end_ = :end_");
        switch ($step_antrian[$bagian]['before']) {
            case 'daftar' :
                if ($counter == COUNTER_MARKET) {
                    $criteria->addCondition("counter = :counter");
                    $param[':counter'] = $counter;
                } else {
                    $criteria->addCondition("(counter is null OR counter = '')");
                }
                $param[':end_'] = 1;
                break;
            case 'counter' :
//                $criteria->addCondition("bagian = 'counter'");
                $criteria->addCondition("medis is null OR medis = :medis");
                $param[':end_'] = 1;
                $param[':medis'] = null;
                break;
            default :
                /*$criteria->addCondition("counter = :counter");
                $param[':counter'] = $counter;
                $param[':end_'] = 1;*/

                if ($counter != COUNTER_MARKET) {
                    //$criteria->addCondition("medis <> null OR medis = :medis");
                    //$criteria->addCondition("medis = :medis");
                    //$param[':medis'] = 1;
                    $param[':end_'] = 1;
                }
                else
                {
                    $criteria->addCondition("counter = :counter");
                    $param[':counter'] = $counter;
                    $param[':end_'] = 1;
                }
                break;
        }
        $criteria->addCondition("hold = 0");
        $criteria->addCondition("DATE(tanggal)=DATE(NOW())");
        $criteria->order = 'tanggal ASC';
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = AishaAntrian::model()->findAll($criteria);
        $total = AishaAntrian::model()->count($criteria);
        if (isset($counter) && $counter != COUNTER_MARKET) {
            if ($step_antrian[$bagian]['before'] == 'daftar' && $total == 0) {
                $q = Yii::app()->db->createCommand("SELECT count(*) FROM aisha_antrian aa
              WHERE aa.counter = :counter AND aa.end_ = 1 AND aa.bagian = 'daftar' AND date(aa.tanggal) = date(now()) and aa.hold=0")
                    ->queryScalar([':counter' => COUNTER_MARKET]);
                if ($q > ANTRIAN_MARKET_MAX) {
                    Yii::app()->db->createCommand("
                    SET @tmsp = null;
                    SELECT MIN(aa.timestamp) INTO @tmsp FROM aisha_antrian aa WHERE aa.counter = :counter AND 
                    hold = 0 AND aa.end_ = 1 AND aa.bagian = 'daftar' AND date(aa.tanggal) = date(now());
                    UPDATE aisha_antrian SET counter = NULL, counter_asal = :counter WHERE `timestamp` = @tmsp AND @tmsp IS NOT NULL;")->execute([':counter' => COUNTER_MARKET]);
                }
            }
        }
        $this->renderJson($model, $total);
    }
    public function actionAntrianPendingCounter()
    {
        $bagian = '';
        //$bagian = $counter = '';
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if (isset($_POST['bagian'])) {
            $bagian = $_POST['bagian'];
        }

        global $step_antrian;
        $criteria = new CDbCriteria();
        $param = array();
        $criteria->addCondition("bagian = :bagian");
        $param[':bagian'] = $step_antrian[$bagian]['before'];
        $criteria->addCondition("hold = 1");

        $criteria->addCondition("DATE(tanggal)=DATE(NOW())");
        //$criteria->addCondition("counter <> 'E'");
        //$criteria->addCondition("counter_asal <> 'E'");
        /*if ($counter == COUNTER_MARKET)
        {
            $criteria->addCondition("counter = 'E'");
        }
        else
        {
            $criteria->addCondition("counter is null");
        }*/

        $criteria->order = 'tanggal ASC';
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = AishaAntrian::model()->findAll($criteria);
        $total = AishaAntrian::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionAntrianPendingDokter()
    {
        $bagian = '';
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if (isset($_POST['bagian'])) {
            $bagian = $_POST['bagian'];
        }
        global $step_antrian;
        $criteria = new CDbCriteria();
        $param = array();        
        $criteria->addCondition("bagian = :bagian");
        $param[':bagian'] = $step_antrian[$bagian]['before'];
//        $param[':bagian'] = 'medis';
        $criteria->addCondition("hold = 1");
        
        $medis = $_POST['c'];
        $criteria->addCondition("(medis is null OR medis = '$medis')");
        $criteria->addCondition("DATE(tanggal)=DATE(NOW())");
        $criteria->order = 'tanggal ASC';
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = AishaAntrian::model()->findAll($criteria);
        $total = AishaAntrian::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionMonitorAntrian()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['counter'])) {
            $criteria->addCondition("counter_=:counter");
            $param[':counter'] = $_POST['counter'];
        }
        $criteria->addCondition("DATE(tanggal)=DATE(NOW())");
        $criteria->order = 'tanggal ASC';
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = MonitorAntrian::model()->findAll($criteria);
        $total = MonitorAntrian::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionUlangiCounter()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $nomor = $_POST['nomor'];
            $bagian = $_POST['bagian'];
            $id_antrian = $_POST['id_antrian'];
            $counter = '';
            if (isset($_POST['counter'])) {
                $counter = $_POST['counter'];
            }
            $nomor_pasien = $_POST['no_base'];
            $customer_id = $_POST['customer_id'];
            $nama_customer = $_POST['nama_customer'];
            global $step_antrian;
            $kode = $step_antrian[$bagian]['kode'];
            $datane = "$$nomor|$kode|$counter|$nomor_pasien|$id_antrian|$customer_id|$nama_customer!";
            $rsl = $this->send2serverCounter($datane);
            if ($rsl[0]) {
                /** @var AishaAntrian $antrian */
                $antrian = AishaAntrian::model()->findByPk($_POST['id_antrian']);
                $his = new AntrianHistory;
                $his->id_antrian = $_POST['id_antrian'];
                $his->tanggal = $antrian->tanggal;
                $his->timestamp = new CDbExpression('NOW()');
                if (isset($_POST['counter'])) {
                    $his->counter = $antrian->counter;
                }
                $his->nomor_antrian = $antrian->nomor_antrian;
                $his->nomor_pasien = $antrian->nomor_pasien;
                $his->bagian = $antrian->bagian;
                $his->action = AN_PANGGIL;
                $his->save();
                $t = str_replace('$', '', $datane);
                $t = str_replace('!', '', $t);
                $arr = explode('|', $t);
                echo CJSON::encode(array(
                    'success' => true,
                    'msg' => $arr
                ));
            } else {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => $rsl[1]
                ));
            }
        }
        Yii::app()->end();
    }
    public function actionPanggilCounter()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
	        $isi_data = false;

	        if (isset($_POST['isi_data'])) {
		        if ($_POST['isi_data'] == "true")
		            $isi_data = true;
	        }

	        $bagian = $counter = $msg = $status = '';
            if (isset($_POST['bagian'])) {
                $bagian = $_POST['bagian'];
            }
            if (isset($_POST['counter'])) {
                $counter = $_POST['counter'];
            }

//        if ($_POST['id_antrian'] != '') {
//            $antrian = AishaAntrian::model()->findByPk($_POST['id_antrian']);
//            $his = new AntrianHistory;
//            $his->id_antrian = $_POST['id_antrian'];
//            $his->tanggal = $antrian->tanggal;
//            $his->timestamp = new CDbExpression('NOW()');
//            $his->counter = $antrian->counter;
//            $his->nomor_antrian = $antrian->nomor_antrian;
//            $his->nomor_pasien = $antrian->nomor_pasien;
//            $his->bagian = $antrian->bagian;
//            $his->action = AN_SELESAI;
//            $his->save();
//            if ($bagian == 'kasir') {
//                $antrian->saveAttributes(array('bagian' => 'selesai', 'end_' => 1));
//            } else {
//                $antrian->saveAttributes(array('end_' => 1));
//            }
//        }
            global $step_antrian;
            $criteria = new CDbCriteria();
            $criteria->addCondition("DATE(tanggal)=DATE(now())");
            $criteria->addCondition("bagian = :bagian");
            $param[':bagian'] = $step_antrian[$bagian]['before'];
            $criteria->addCondition("end_ = :end_");
            switch ($step_antrian[$bagian]['before']) {
                case 'daftar' :
                    if ($counter == COUNTER_MARKET) {
                        $criteria->addCondition("counter = :counter");
                        $param[':counter'] = $counter;
                    } else {
                        $criteria->addCondition("counter is null");
                    }
                    $param[':end_'] = 1;
                    break;
                case 'counter' :
//                    $criteria->addCondition("bagian = 'counter'");
                    $criteria->addCondition("medis is null OR medis = :medis");
                    if (isset($_POST['id_antrian'])) {
                        $criteria->addCondition("id_antrian = :id_antrian");
                        $param[':id_antrian'] = $_POST['id_antrian'];
                    }
                    $param[':end_'] = 1;
                    $param[':medis'] = $counter;
                    break;
                case 'kasir' :
//                    $criteria->addCondition("medis is null");
                    if (isset($_POST['id_antrian'])) {
                        $criteria->addCondition("id_antrian = :id_antrian");
                        $param[':id_antrian'] = $_POST['id_antrian'];
                    }
                    $param[':end_'] = 1;
                    break;
                default :
                    //$criteria->addCondition("counter = :counter");
                    //$param[':end_'] = 1;
                    //$param[':counter'] = $counter;

                    if ($counter != COUNTER_MARKET)
                    {
                        $param[':end_'] = 1;
                    }
                    else
                    {
                        $criteria->addCondition("counter = :counter");
                        $param[':counter'] = $counter;
                        $param[':end_'] = 1;
                    }
                    break;
            }
            $criteria->addCondition("hold = 0");
            $criteria->order = 'tanggal ASC';
            $criteria->params = $param;
            /** @var AishaAntrian $antri */
            $antri = AishaAntrian::model()->find($criteria);
            if ($antri == null) {
                $datane = "kosong";
            } else {
                $kode = $step_antrian[$bagian]['kode'];
                $datane = "$$antri->nomor_antrian|$kode|$counter|$antri->nomor_pasien|$antri->id_antrian|$antri->customer_id|$antri->nama_customer!";
                if ($bagian == 'counter' AND $counter != COUNTER_MARKET) {
//                $antri->counter = $counter;
                    if (!$antri->saveAttributes(array('counter' => $counter))) {
                        $msg .= " " . CHtml::errorSummary($antri);
                        $status = false;
                        echo CJSON::encode(array(
                            'success' => $status,
                            'msg' => $msg
                        ));
                        Yii::app()->end();
                    }
                }
                else if ($bagian == 'medis') {
                    if ($antri->medis == null) {
                        if (!$antri->saveAttributes(array('medis' => $counter))) {
                            $msg .= " " . CHtml::errorSummary($antri);
                            $status = false;
                            echo CJSON::encode(array(
                                'success' => $status,
                                'msg' => $msg
                            ));
                            Yii::app()->end();
                        }
                    }
//                $antri->medis = $counter;
                }
                //menambahkan konter (A,B,C,D) yang akan melakukan Sales ke kolom counter_cashier di tabel aishaantrian
                else if ($bagian == 'kasir') {
                    if (!$antri->saveAttributes(array('counter_cashier' => $counter))) {
                            $msg .= " " . CHtml::errorSummary($antri);
                            $status = false;
                            echo CJSON::encode(array(
                                'success' => $status,
                                'msg' => $msg
                            ));
                            Yii::app()->end();
                    }
                }

            }
            if ($datane == "kosong") {
                echo CJSON::encode(array(
                    'success' => true,
                    'msg' => 'Tidak ada antrian.'
                ));
            } else {
            	if (!$isi_data) {
		            $rsl = $this->send2serverCounter($datane);
		            if ($rsl[0]) {
			            $t = str_replace('$', '', $datane);
			            $t = str_replace('!', '', $t);
			            $arr = explode('|', $t);
			            if ($antri != null) {
//                    $antri->bagian = $bagian;
				            if (!$antri->saveAttributes(array('bagian' => $bagian, 'end_' => 0))) {
					            echo CJSON::encode(array(
						            'success' => true,
						            'msg' => CHtml::errorSummary($antri)
					            ));
				            }
				            echo CJSON::encode(array(
					            'success' => true,
					            'msg' => $arr
				            ));
			            }

			            $his = new AntrianHistory;
			            $his->id_antrian = $antri->id_antrian;
			            $his->tanggal = $antri->tanggal;
			            $his->timestamp = new CDbExpression('NOW()');
			            $his->counter = $antri->counter;
			            $his->nomor_antrian = $antri->nomor_antrian;
			            $his->nomor_pasien = $antri->nomor_pasien;
			            $his->bagian = $antri->bagian;
			            $his->action = AN_PANGGIL.'-'.$antri->counter;
			            $his->save();
		            }
		            else {
			            throw new Exception($rsl[1]);
		            }
	            }
	            else
	            {
		            $t = str_replace('$', '', $datane);
		            $t = str_replace('!', '', $t);
		            $arr = explode('|', $t);
	            	if ($antri != null) {
		            echo CJSON::encode(array(
			            'success' => true,
			            'msg' => $arr
		            ));
	                }
	            }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
            echo CJSON::encode(array(
                'success' => false,
                'msg' => $e->getMessage()
            ));
        }
        Yii::app()->end();
    }

    public function actionUpdate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if (!isset($_POST['id_antrian'])) {
                    echo CJSON::encode(array(
                        'success' => false,
                        'msg' => 'Data id antrian tidak ada.'
                    ));
                    Yii::app()->end();
                }
                $id = $_POST['id_antrian'];
                $mode = $_POST['mode'];
                /** @var AishaAntrian $model */
                $model = AishaAntrian::model()->findByPk($id);
                $his = new AntrianHistory;
                $his->id_antrian = $_POST['id_antrian'];
                $his->tanggal = $model->tanggal;
                $his->timestamp = new CDbExpression('NOW()');
                $his->counter = $model->counter;
                $his->nomor_antrian = $model->nomor_antrian;
                $his->nomor_pasien = $model->nomor_pasien;
                $his->bagian = $model->bagian;
                $status = false;
                $msg = '';
                global $step_antrian;
                if (!$model) {
                    $msg = 'Data antrian tidak ditemukan.';
                } else {
                    switch ($mode) {
                        case 'pending':
                            if ($_POST['bagian'] == 'medis') {
                                $model->medis = new CDbExpression('NULL');
                            } else if ($_POST['bagian'] == 'counter' && $model->counter != COUNTER_MARKET) {
                                $model->counter = new CDbExpression('NULL');
                            }
                            $model->hold = 1;
//                            if ($_POST['bagian'] != 'medis') {
                            $model->end_ = 1;
                            $model->counter_cashier = null;
                            $model->bagian = $step_antrian[$_POST['bagian']]['before'];
//                            }
                            $his->action = AN_PENDING;
                            break;
                        case 'unpending':
                            $model->hold = 0;
                            $his->action = AN_UNPENDING;
                            break;
                        case 'back':
                            $model->bagian = $step_antrian[$step_antrian[$_POST['bagian']]['before']]['before'];
                            $model->end_ = 1;
                            $his->action = AN_BACK . ' ' . $model->bagian;
                            break;
                        case 'perawatan':
                            if (isset($_POST['bagian'])) {
                                if ($_POST['bagian'] == 'perawatan' && $model->bagian == 'counter') {
                                    $sales = Salestrans::model()
                                        ->find('customer_id = :customer_id AND tgl = date(:tgl)', [
                                            ':customer_id' => $model->customer_id,
                                            ':tgl' => $model->tanggal
                                        ]);
                                    if ($sales == null) {
                                        throw new Exception('Pasien belum melakukan transaksi');
                                    } else {
                                        if ($sales->haveBeautyService()) {
                                            $model->bagian = 'kasir';
                                        } else {
                                            $model->bagian = 'selesai';
                                        }
                                    }
                                }
                            }
                            $his->action = AN_SELESAI;
                            $model->end_ = 1;
                            break;
                        case 'selesai':
                            $his->action = AN_SELESAI;
                            $model->end_ = 1;
                            break;
                        case 'requestkonsul':
                            $his->action = AN_SELESAI;
                            $model->end_ = 1;
                            $model->hold = 1;
//                            $model->bagian = 'medis'; //medis blm ke B hmm
                            $model->medis = $_POST['req_konsul'];
                            break;                        
                        case 'alasan':
                            if (isset($_POST['bagian'])) {
                                $model->bagian = $_POST['bagian'];
                            }
                            $model->alasan = $_POST['alasan'];
                            $his->action = AN_SELESAI;
                            $model->end_ = 1;
                            break;
                        case 'batal':
                            $model->bagian = $_POST['bagian'];
                            $his->bagian = $step_antrian[$_POST['bagian']]['after'];
                            $his->action = AN_BATAL;
                            $model->end_ = 1;
                            break;
                        case 'Nobase':
                            if (!isset($_POST['no_base'])) {
                                $msg = 'Nobase gagal diupdate.';
                                break;
                            }
                            $model->nomor_pasien = $_POST['no_base'];
                            $model->customer_id = $_POST['customer_id'];
                            $model->nama_customer = $_POST['nama_customer'];
                            $his->action = AN_NOBASE;
                            break;
                        default :
                            break;
                    }
                }
                if (!$model->update()) {
                    throw new Exception(CHtml::errorSummary($model));
                }
                $his->save();
                $transaction->commit();
                $status = true;
                $msg = 'berhasil di update';
            } catch (Exception $e) {
                $transaction->rollback();
                $status = false;
                $msg = $e->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionRelease()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if (!isset($_POST['id_antrian'])) {
                    echo CJSON::encode(array(
                        'success' => false,
                        'msg' => 'Data id antrian tidak ada.'
                    ));
                    Yii::app()->end();
                }
                $id = $_POST['id_antrian'];
                /** @var AishaAntrian $model */
                $model = AishaAntrian::model()->findByPk($id);
                //$model->nomor_pasien = NULL;
                //$model->customer_id = NULL;
                //$model->nama_customer = NULL;
                $model->counter = NULL;
                $model->end_ = 1;
                $model->bagian = 'daftar';
                $model->counter_asal = COUNTER_MARKET;
                if (!$model->save()) {
                    throw new Exception(CHtml::errorSummary($model));
                }
                $transaction->commit();
                $status = true;
                $msg = 'berhasil direlease';
            } catch (Exception $e) {
                $transaction->rollback();
                $status = false;
                $msg = $e->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function send2serverCounter($message)
    {
        $sk = @fsockopen(ANTRIAN_SERVER_HOST, ANTRIAN_SERVER_PORT, $errnum, $errstr, ANTRIAN_TIMEOUT);
        if (!is_resource($sk)) {
            return array(false, "connection fail: " . $errnum . " " . $errstr);
        }
        return array(true, fwrite($sk, $message));
    }
}