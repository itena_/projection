<?php

class PembantuPelunasanPiutangDetilController extends GxController {

	public function actionFaktur() {
		$customer_id = $_POST['customer_id'];
		$faktur = PembantuPelunasanPiutangDetil::get_piutang($customer_id, STOREID);
		$this->renderJsonArr($faktur);
	}

	public function actionIndex() {
		$criteria = new CDbCriteria();
		if ((isset($_POST['pembantu_pelunasan_piutang_id']))) {
			$criteria->addCondition("pembantu_pelunasan_piutang_id = '" . $_POST['pembantu_pelunasan_piutang_id'] . "'");
		}
		$model = PembantuPelunasanPiutangDetil::model()->findAll($criteria);
		$total = PembantuPelunasanPiutangDetil::model()->count($criteria);

		$this->renderJson($model, $total);
	}

}
