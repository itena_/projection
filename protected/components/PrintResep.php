<?php
class PrintResep extends PrintReceipt
{
    public function buildTxt()
    {
        $newLine = "\r\n";
        $raw = parent::setCenter($this->resepPrint->header0);
        $raw .= $newLine;
        $raw .= parent::setCenter($this->resepPrint->header1);
        $raw .= $newLine;
        $raw .= parent::setCenter($this->resepPrint->header2);
        $raw .= $newLine;
        $raw .= parent::setCenter($this->resepPrint->header3);
        $raw .= $newLine;
        $raw .= parent::setCenter($this->resepPrint->header4);
        $raw .= $newLine;
        $raw .= parent::setCenter($this->resepPrint->header5);
        $raw .= $newLine;
//        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        if ($this->resepPrint->doc_ref_resep != null) {
            $raw .= $newLine;
            $raw .= parent::addHeaderResep("Dokter", $this->resepPrint->dokter);
            $raw .= $newLine;
            if ($this->resepPrint->dokter){
                $sip = Employee::model()->findByPk($this->resepPrint->salestrans->dokter_id);
                $raw .= parent::addHeaderResep("SIP", $sip->no_ijin);
                $raw .= $newLine;
            }            
            $raw .= parent::addHeaderResep("No. Resep", $this->resepPrint->doc_ref_resep);
            $raw .= $newLine;
            $raw .= parent::addHeaderResep("Tanggal", sql2date($this->resepPrint->date_, "dd-MMM-yyyy"));
            $raw .= $newLine;
        } else {
            return false;
        }
        $raw .= $newLine;
        $criteria = new CDbCriteria();
        $criteria->addCondition('r_ = 1');
        $criteria->addCondition('resep_print_id = :resep_print_id');
        $criteria->order = 'no_resep';
        $criteria->params = [':resep_print_id' => $this->resepPrint->resep_print_id];
        /** @var ResepPrintDetails[] $resepPrintDetils */
        $resepPrintDetils = ResepPrintDetails::model()->findAll($criteria);
        foreach ($resepPrintDetils as $rpd) {
            $raw .= parent::addItemCodeResepSat("R$rpd->no_resep /",
                number_format($rpd->qty, 1, ',', '.'), $rpd->sat);
            $raw .= $newLine;
            $cri = new CDbCriteria();
            $cri->addCondition('resep_print_details_id = :resep_print_details_id');
            $cri->order = 'no_urut';
            $cri->params = [':resep_print_details_id' => $rpd->resep_print_details_id];
            /** @var ResepPrintBahan[] $resepPrintBahan */
            $resepPrintBahan = ResepPrintBahan::model()->findAll($cri);
            foreach ($resepPrintBahan as $rpb) {
                $qty = number_format($rpd->qty, 0);
//                BELUM DITERAPIN
                if ($rpb->sat == "note" || $rpb->sat == "Note" || !$rpb->sat){
                    $raw .= parent::addResepBahan("", "     $rpb->nama_bahan", "", "");
                    $raw .= $newLine;
                } else {
                    $raw .= parent::addResepBahan("", "$rpb->nama_bahan", number_format($rpb->qty/$qty,2), $rpb->sat);
                    $raw .= $newLine;
                }
//                $raw .= parent::addResepBahan("$rpb->no_urut.", "$qty x $rpb->nama_bahan", number_format($rpb->qty/$qty,2), $rpb->sat);
//                $raw .= $newLine;
            }
            $raw .= $newLine;
        }
        $raw .= $newLine;
        $raw .= parent::addHeaderResep("Nama Pasien",$this->resepPrint->salestrans->customer->nama_customer);
        $raw .= $newLine;
        $raw .= parent::addHeaderResep("No. RM",$this->resepPrint->salestrans->customer->no_customer);
        $raw .= $newLine;
        $raw .= $newLine;
        $raw .= parent::addLeftRight("", get_date_today("hh:mm:ss/dd-MM-yyyy"));
        $raw .= $newLine;
        return $raw;
    }
//    public function buildTxt()
//    {
//        $newLine = "\r\n";
//        $raw = parent::setCenter($this->resepPrint->header0);
//        $raw .= $newLine;
//        $raw .= parent::setCenter($this->resepPrint->header1);
//        $raw .= $newLine;
//        $raw .= parent::setCenter($this->resepPrint->header2);
//        $raw .= $newLine;
//        $raw .= parent::setCenter($this->resepPrint->header3);
//        $raw .= $newLine;
//        $raw .= parent::setCenter($this->resepPrint->header4);
//        $raw .= $newLine;
//        $raw .= parent::setCenter($this->resepPrint->header5);
//        $raw .= $newLine;
////        $raw .= parent::fillWithChar("=");
//        $raw .= $newLine;
//        if ($this->resepPrint->doc_ref_resep != null) {
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("No. Resep", $this->resepPrint->doc_ref_resep);
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("Tanggal", sql2date($this->resepPrint->date_, "dd-MMM-yyyy"));
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("Apt./AA", $this->resepPrint->apoteker);
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("A/N. Resep", $this->resepPrint->pasien_name);
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("Alamat", $this->resepPrint->alamat);
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("Dokter", $this->resepPrint->dokter);
//            $raw .= $newLine;
//        } else {
//            return false;
//        }
//        $raw .= $newLine;
//        $criteria = new CDbCriteria();
//        $criteria->addCondition('r_ = 1');
//        $criteria->addCondition('resep_print_id = :resep_print_id');
//        $criteria->order = 'no_resep';
//        $criteria->params = [':resep_print_id' => $this->resepPrint->resep_print_id];
//        /** @var ResepPrintDetails[] $resepPrintDetils */
//        $resepPrintDetils = ResepPrintDetails::model()->findAll($criteria);
//        foreach ($resepPrintDetils as $rpd) {
//            $raw .= parent::addItemCodeResepSat("R$rpd->no_resep /",
//                number_format($rpd->qty, 1, ',', '.'), $rpd->sat);
//            $raw .= $newLine;
//            $cri = new CDbCriteria();
//            $cri->addCondition('resep_print_details_id = :resep_print_details_id');
//            $cri->order = 'no_urut';
//            $cri->params = [':resep_print_details_id' => $rpd->resep_print_details_id];
//            /** @var ResepPrintBahan[] $resepPrintBahan */
//            $resepPrintBahan = ResepPrintBahan::model()->findAll($cri);
//            foreach ($resepPrintBahan as $rpb) {
//                $qty = number_format($rpd->qty, 0);
////                BELUM DITERAPIN
////                if ($rpb->sat == "note" || $rpb->sat == "Note" || !$rpb->sat){
////                    $raw .= parent::addResepBahan("", "     $rpb->nama_bahan", "", "");
////                    $raw .= $newLine;
////                } else {
////                    $raw .= parent::addResepBahan("", "$rpb->nama_bahan", number_format($rpb->qty/$qty,2), $rpb->sat);
////                    $raw .= $newLine;
////                }
//                $raw .= parent::addResepBahan("$rpb->no_urut.", "$qty x $rpb->nama_bahan", number_format($rpb->qty/$qty,2), $rpb->sat);
//                $raw .= $newLine;
//            }
//            $raw .= $newLine;
//        }
//        $raw .= $newLine;
//        if ($this->resepPrint->footer0 != '') {
//            $raw .= parent::setCenter($this->resepPrint->footer0);
//            $raw .= $newLine;
//        }
//        if ($this->resepPrint->footer1 != '') {
//            $raw .= parent::setCenter($this->resepPrint->footer1);
//            $raw .= $newLine;
//        }
//        if ($this->resepPrint->footer2 != '') {
//            $raw .= parent::setCenter($this->resepPrint->footer2);
//            $raw .= $newLine;
//        }
//        if ($this->resepPrint->footer3 != '') {
//            $raw .= parent::setCenter($this->resepPrint->footer3);
//            $raw .= $newLine;
//        }
//        if ($this->resepPrint->footer4 != '') {
//            $raw .= parent::setCenter($this->resepPrint->footer4);
//            $raw .= $newLine;
//        }
//        if ($this->resepPrint->footer5 != '') {
//            $raw .= parent::setCenter($this->resepPrint->footer5);
//            $raw .= $newLine;
//        }
//        $raw .= $newLine;
//        $raw .= parent::addLeftRight("", get_date_today("hh:mm:ss/dd-MM-yyyy"));
//        $raw .= $newLine;
//        return $raw;
//    }
}