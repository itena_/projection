<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 2/13/2016
 * Time: 10:11 AM
 */
class MyActiveRecord extends CActiveRecord
{
    private static $dbadvert = null;

    protected static function getAdvertDbConnection()
    {
        if (self::$dbadvert !== null)
            return self::$dbadvert;
        else
        {
            self::$dbadvert = Yii::app()->dbadvert;
            if (self::$dbadvert instanceof CDbConnection)
            {
                self::$dbadvert->setActive(true);
                return self::$dbadvert;
            }
            else
                throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
        }
    }

}