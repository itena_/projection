<?php
class SyncData
{
    /*
     *  Table yang tidak di sync adalah :
     *
     *  nscc_employee
     *  nscc_absen_trans
     *  nscc_nscc_tipe_absen
     *
     * */
    private $username;
    private $password;
    private $status_error;
    private $body_message;
    private $offset;
    private $limit;
    private $current_subyek;
    /**
     * SyncData constructor.
     * @param $offset
     * @param $limit
     */
    public function __construct($offset, $limit)
    {
        self::echo_log('SYNC', 'INFO', 'START PID >> ' . getmypid() . " START ROW >> $offset LIMIT ROW >> $limit");
        $this->offset = $offset;
        $this->limit = $limit;
    }
    public function history()
    {
        $this->username = yiiparam('Username');
        $this->password = yiiparam('Password');
//        if (is_connected(NARSDOMAIN, NARSPORT)) {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            self::upload_direct_nars();
            self::upload_direct_trans();
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
//                self::save_error_log('NARS_ERROR', $ex->getMessage());
            self::echo_log('NARS_ERROR', 'NARS', $ex->getMessage());
        }
//        } else {
//            self::echo_log('NARS_ERROR', 'NARS', 'No connection');
//        }
    }
    public function upload()
    {
        if (TOSTOREHO_PASIEN) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                self::upload_pasien();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
                self::echo_log('PASIEN_UP_ERROR', 'GLOBAL', $ex->getMessage());
            }
        }
        if (TOSTOREHO_TRANS) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                self::upload_global();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
                self::echo_log('GLOBAL_UP_ERROR', 'GLOBAL', $ex->getMessage());
            }
        }
        if (TOSTOREHO_TRANS) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                self::upload_master_all();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
                self::echo_log('MASTERHO_UP_ERROR', 'MASTER', $ex->getMessage());
            }
        }
        if (TOSTOREHO_TRANS) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                self::upload_transaksi();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
                self::echo_log('TRANS_UP_ERROR', 'TRANSACTION', $ex->getMessage());
            }
        }
        if (BROADCAST) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                self::broadcast_master();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
                self::echo_log('BROADCAST_UP_ERROR', 'MASTER', $ex->getMessage());
            }
        }
    }
    public function sync()
    {
//        Yii::app()->db->createCommand("
//        ALTER TABLE nscc_tipe_barang AUTO_INCREMENT = 1;
//        ALTER TABLE nscc_trans_tipe AUTO_INCREMENT = 1;")
//            ->execute(array());
//        app()->db->createCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;")->query();
//        if (is_connected('imap.gmail.com', 993) && is_connected('smtp.gmail.com', 587)) {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            self::download_pasien();
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            self::save_error_log($this->current_subyek, $ex->getMessage());
            self::echo_log('PASIEN_DOWN_ERROR', 'GLOBAL', $ex->getMessage());
        }
        if (DOWNLOAD_GLOBAL) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                self::download_global();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
                self::save_error_log($this->current_subyek, $ex->getMessage());
                self::echo_log('GLOBAL_DOWN_ERROR', 'GLOBAL', $ex->getMessage());
            }
        }
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            self::download_master();
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            self::save_error_log($this->current_subyek, $ex->getMessage());
            self::echo_log('MASTER_DOWN_ERROR', 'MASTER', $ex->getMessage());
        }
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            self::download_transaksi();
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            self::save_error_log($this->current_subyek, $ex->getMessage());
            self::echo_log('TRANS_DOWN_ERROR', 'TRANSACTION', $ex->getMessage());
        }
//            if (!HEADOFFICE) {
//                $transaction = Yii::app()->db->beginTransaction();
//                try {
//                    self::remainder();
//                    $transaction->commit();
//                } catch (Exception $ex) {
//                    $transaction->rollback();
//                    self::echo_log('SEND_REMAINDER', 'REMAINDER', $ex->getMessage());
//                }
//            }
//        } else {
//            self::echo_log('EMAIL', 'IMAP', 'No connection');
//        }
//        app()->db->autoCommit = true;
//        Yii::app()->db->createCommand("DELETE FROM nscc_sync WHERE DATEDIFF(NOW(),tdate) > :days")
//            ->execute(array(":days" => DAYSBEFOREDELSYNC));
//        app()->db->createCommand("COMMIT;")->query();
        self::echo_log('SYNC', 'INFO', 'STOP PID >> ' . getmypid() . " START ROW >> $this->offset LIMIT ROW >> $this->limit");
    }
    private function upload_direct_nars()
    {
        $cabang = Cabang::model()->findByAttributes(array('kode' => STOREID));
        if ($cabang == null) {
            throw new Exception("Cabang tidak ditemukan!");
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition("log = 0");
        $criteria->addCondition("store = :store");
        $criteria->limit = LIMIT_ROW_SYNC;
        $criteria->params = array(":store" => STOREID);
        $model = Customers::model()->findAll($criteria);
        $ok = $err = 0;
        $inc = 0;
        foreach ($model as $row) {
            $pasien = Pasien::model()->findByAttributes(array("no_base" => $row->no_customer, "id_cabang" => $cabang->id));
            //echo "Proses data nomor : " . $inc . " dari " . count($model);
            $inc++;
//            var_dump($pasien);
            if ($pasien == null) {
                $pasien = new Pasien;
//                $pasien->id = new CDbExpression('SELECT MAX(id)+1 FROM pasien');
            }
            $pasien->id_cabang = $cabang->id;
            if ($pasien->no_kartu == null) {
                $pasien->no_kartu = $row->customer_id;
            }
            $pasien->no_base = $row->no_customer;
            $pasien->nama = $row->nama_customer;
            $pasien->alamat = $row->alamat;
            if ($row->kota_id != null) {
                $kota = Kota::model()->findByPk($row->kota_id);
                $pasien->kota = $kota->nama_kota;
            }
            $pasien->telp = $row->telp;
            $pasien->tgl_lhr = $row->tgl_lahir;
            $pasien->jkel = $row->sex == 'MALE' ? 'PRIA' : "WANITA";
            $pasien->pekerjaan = $row->kerja;
            $pasien->awal = $row->awal;
            $timestamp = CDateTimeParser::parse($row->awal, 'yyyy-MM-dd');
            $pasien->tahun = Yii::app()->dateFormatter->format('yyyy', $timestamp);
            $pasien->bulan = Yii::app()->dateFormatter->format('MM', $timestamp);
            $pasien->timestamp = new CDbExpression('NOW()');
            if (!$pasien->save()) {
                $err++;
                self::save_error_log('Upload Nars', CHtml::errorSummary($pasien));
                throw new Exception("Model PASIEN error : " . CHtml::errorSummary($pasien));
            } else {
                $cust = Customers::model()->findByPk($row->customer_id);
                $cust->log = 1;
                if (!$cust->save()) {
                    self::save_error_log('Upload Nars', CHtml::errorSummary($cust));
                    throw new Exception("Model CUSTOMERS error : " . CHtml::errorSummary($cust));
                }
                $ok++;
            }
        }
        self::echo_log('upload_cust_nars', 'NARS', "Berhasil $ok, Error $err, Total " . count($model));
    }
    private function upload_direct_trans()
    {
        $cabang = Cabang::model()->findByAttributes(array('kode' => STOREID));
        if ($cabang == null) {
            throw new Exception("Cabang tidak ditemukan!");
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition("log = 0");
        $criteria->limit = LIMIT_ROW_SYNC;
        /** @var NarstransView[] $model */
        $model = NarstransView::model()->findAll($criteria);
        $ok = $err = 0;
        $inc = 0;
        foreach ($model as $row) {
            Faktur::model()->deleteAll('no_faktur = :no_faktur',
                array(':no_faktur' => $row->doc_ref));
            if ($row->type_ == 1) {
                Item::model()->deleteAll('no_faktur = :no_faktur AND qty > 0',
                    array(':no_faktur' => $row->doc_ref));
            } else {
                Item::model()->deleteAll('no_faktur = :no_faktur AND qty < 0',
                    array(':no_faktur' => $row->doc_ref));
            }
            $inc++;
            $faktur = new Faktur;
            $faktur->id_cabang = $cabang->id;
            $faktur->no_base = $row->no_customer;
            $faktur->tgl = $row->tgl;
            $faktur->tgl_lunas = $row->tgl;
            $faktur->no_faktur = $row->doc_ref;
            $faktur->no_kartu = $row->customer_id;
            $faktur->id_jenis = 0;
            $faktur->jumlah = $row->total;
            $faktur->bayar = $row->bayar;
            $faktur->timestamp = new CDbExpression('NOW()');
            $faktur->ketdisc = $row->ketdisc;
            $tm = CDateTimeParser::parse($row->tgl, 'yyyy-MM-dd');
            $faktur->tahun = Yii::app()->dateFormatter->format('yyyy', $tm);
            $faktur->bulan = Yii::app()->dateFormatter->format('MM', $tm);
            if (!$faktur->save()) {
                $err++;
                self::save_error_log('Upload Nars', CHtml::errorSummary($faktur));
                throw new Exception("Model FAKTUR error : " . CHtml::errorSummary($faktur));
            }
            $sales_details = Yii::app()->db->createCommand("SELECT
            nsd.salestrans_details,
            nsd.barang_id,
            nsd.salestrans_id,
            nsd.qty,
            nsd.disc,
            nsd.discrp,
            nsd.ketpot,
            nsd.vat,
            nsd.vatrp,
            nsd.bruto,
            nsd.total,
            nsd.total_pot,
            nsd.price,
            nsd.jasa_dokter,
            nsd.dokter_id,
            nsd.disc_name,
            nsd.hpp,
            nsd.cost,
            nsd.disc1,
            nsd.discrp1,
            nsd.paket_trans_id,
            nsd.paket_details_id,
            nb.kode_barang,
            nb.sat
            FROM
            nscc_salestrans_details AS nsd
            INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
            WHERE salestrans_id = :salestrans_id")
                ->queryAll(true, array(":salestrans_id" => $row->salestrans_id));
            foreach ($sales_details as $rowd) {
                $details = new Item;
                $details->id_cabang = $cabang->id;
                $details->no_faktur = $faktur->no_faktur;
                $details->tahun = $faktur->tahun;
                $details->kd_brng = $rowd['kode_barang'];
                $details->qty = $rowd['qty'];
                $details->satuan = $rowd['sat'];
                $details->harga = $rowd['price'];
                $details->id_jenis = 0;
                $details->timestamp = new CDbExpression('NOW()');
                $details->ketpot = $rowd['ketpot'];
                if (!$details->save()) {
                    $err++;
                    self::save_error_log('Upload Nars', CHtml::errorSummary($details));
                    throw new Exception("Model ITEM error : " . CHtml::errorSummary($details));
                }
            }
            $sales = Salestrans::model()->findByPk($row->salestrans_id);
            $sales->log = 1;
            if (!$sales->save()) {
                $err++;
                self::save_error_log('Upload Nars', CHtml::errorSummary($sales));
                throw new Exception("Model SALESTRANS error : " . CHtml::errorSummary($sales));
            }
            $ok++;
        }
        self::echo_log('UPLOAD_TRANS_NARS', 'NARS', "Berhasil $ok, Error $err, Total " . count($model));
    }
    private function upload_cust_nars()
    {
        $cust = Customers::get_backup();
        if (count($cust) == 0) {
//            $this->result['upload_cust_nars'] = 'nothing to upload';
            self::echo_log('NARS', 'UPLOAD_CUST_NARS', 'No connection');
            return;
        }
        $url = NARSURL;
        $data = array(
            'username' => SysPrefs::get_val('username_nars'),
            'password' => SysPrefs::get_val('password_nars'),
            'cabang' => STOREID,
            'tanggal' => "",
            'jenis' => 'pasien',
            'data' => CJSON::encode($cust)
        );
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'ignore_errors' => true,
                'content' => http_build_query($data),
            ),
        );
//        self::echo_log('NARS', 'DEBUG_CUST_NARS', $data);
//        self::echo_log('NARS', 'DEBUG_CUST_NARS', $options);
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result == '1') {
            $r = Yii::app()->db->createCommand("
           UPDATE nscc_customers SET log = 1 WHERE log = 0
           ")->execute();
//            $this->result['upload_cust_nars'] = 'success upload ' . number_format($r) . ' records';
            self::echo_log('NARS', 'UPLOAD_CUST_NARS', 'success upload ' . number_format($r) . ' records');
            return;
        }
//        $this->result['upload_cust_nars'] = 'failed upload records';
        self::echo_log('upload_cust_nars', 'NARS', $result);
    }
    private function upload_trans_nars()
    {
        $sales = Salestrans::get_backup();
        if (count($sales) == 0) {
//            $this->result['upload_trans_nars'] = 'nothing to upload';
            self::echo_log('UPLOAD_TRANS_NARS', 'NARS', 'nothing to upload');
            return;
        }
        $url = NARSURL;
        $data = array(
            'username' => SysPrefs::get_val('username_nars'),
            'password' => SysPrefs::get_val('password_nars'),
            'cabang' => STOREID,
            'tanggal' => "",
            'jenis' => 'transaksi',
            'data' => CJSON::encode($sales)
        );
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $context = stream_context_create($options);
        $result = @file_get_contents($url, false, $context);
        if ($result == '1') {
            $r = Yii::app()->db->createCommand("
           UPDATE nscc_salestrans SET log = 1 WHERE log = 0
           ")->execute();
//            $this->result['upload_trans_nars'] = 'success upload ' . number_format($r) . ' records';
            self::echo_log('UPLOAD_TRANS_NARS', 'NARS', 'success upload ' . number_format($r) . ' records');
            return;
        }
//        $this->result['upload_trans_nars'] = 'failed upload records';
        self::echo_log('UPLOAD_TRANS_NARS', 'NARS', 'failed upload records');
    }
    private function upload_pasien()
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $criteria->limit = LIMIT_ROW_SYNC;
        $body = array(
            'from' => STOREID,
        );
        $upload = array();
        $content = array();
        $modelName = "Customers";
        $tables = CActiveRecord::model($modelName)->findAll($criteria);
        if ($tables != null) {
            $upload[$modelName] = $tables;
            $content[$modelName] = number_format(count($tables)) . ' record';
        }
        if (count($upload) == 0) {
            self::echo_log('UPLOAD_PASIEN', $modelName, 'nothing to upload');
            return;
        }
        $to = yiiparam('Username');
        $time = date('Y-m-d H:i:s');
        $subject = STOREPASIEN . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
        $body['date'] = $time;
        $body['paket'] = false;
        $body['content'] = array($content);
        $status = self::upload_database_email(STOREPASIEN, $subject,
            json_encode($body, JSON_PRETTY_PRINT), CJSON::encode($upload)
        );
        if ($status == "OK") {
            foreach ($tables as $model) {
                $model->up = 1;
                if (!$model->save()) {
                    self::save_error_log('Upload Pasien', CHtml::errorSummary($model));
                    throw new Exception("Model " . $modelName . " error : " . CHtml::errorSummary($model));
                }
            }
            self::echo_log('UPLOAD_PASIEN', $modelName,
                'success upload ' . number_format(count($tables)) . ' records');
            $sync = new SyncEmail;
            $sync->type_ = "P";
            $sync->subject = $subject;
            $sync->store = STOREID;
            if (!$sync->save()) {
                throw new Exception("Model SYNC error : " . CHtml::errorSummary($sync));
            }
        } else {
            self::echo_log('UPLOAD_PASIEN', $modelName, 'failed upload records');
        }
    }
    private function download_pasien()
    {
        $array_error = array();
        $this->body_message = "";
        $data_email = self::download_database_email(STOREPASIEN, 'P');
        $json = $data_email['JSON'];
        if ($json === false) {
            self::echo_log('DOWNLOAD_PASIEN', 'failed download', $this->status_error);
            return;
        }
        $body = json_decode($this->body_message, true);
//        app()->db->createCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;")->query();
        foreach ($json as $subject => $email) {
            self::echo_log('SYNC', 'INFO', "PROCESS >> $subject");
            $this->current_subyek = $subject;
            $data = json_decode($email, true);
            if (is_array($data) || is_object($data)) {
                foreach ($data as $k => $v) {
                    foreach ($v as $row) {
                        $primarykey = CActiveRecord::model($k)->tableSchema->primaryKey;
                        $pkval = $row[$primarykey];
                        $model = CActiveRecord::model($k)->findByPk($pkval);
                        if ($model == null) {
                            $model = new $k;
                        } else {
                            $model->setIsNewRecord(false);
                        }
                        $model->attributes = $row;
                        if (!$model->save()) {
                            /*
                             * attribute save dulu untuk flag menyusul
                             * */
                            throw new Exception(t('save.model.fail', 'app',
                                    array('{model}' => $k)) . CHtml::errorSummary($model));
                        }
                        $model->up = 1;
                        $model->log = 1;
                        if (!$model->save()) {
                            self::save_error_log($subject, CHtml::errorSummary($model));
                            $array_error[] = $subject;
                        }
                    }
                    self::echo_log('DOWNLOAD_PASIEN', $k, 'success download ' . number_format(count($v)) . ' records');
                }
            }
            self::echo_log('SYNC', 'INFO', "FINISH >> $subject");
        }
//        app()->db->createCommand("COMMIT;")->query();
        foreach ($data_email['SYNC'] as $sync_row) {
            $index = array_search($sync_row->subject, $array_error);
            if ($index === false) {
                if (!$sync_row->save()) {
                    self::save_error_log('Sync email', CHtml::errorSummary($sync_row));
                }
            }
        }
    }
    private function upload_global()
    {
        $global = array(
            'SecurityRoles',
            'Gol',
//            'Customers',
            'StatusCust',
            'ChartMaster',
            'Supplier',
            'Kategori',
            'Negara',
            'Provinsi',
            'Kota',
            'Kecamatan',
            'Store',
//            'Card',
            'Barang',
            'BarangAsset',
            'BarangPerlengkapan',
            'CostBarangPerlengkapan',
            'ChartClass',
            'ChartTypes',
            'Grup',
            'GroupAsset',
            'KategoriClinical',
            'BarangClinical',
            'TipeTransClinical',
            'Info',
            'Wilayah',
            'TipeBarang',
            'PointTrans',
            'Event',
            'Souvenir',
            'TransTipe',
            'PromoGeneric'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $criteria->limit = LIMIT_ROW_SYNC;
        foreach ($global as $modelName) {
            $body = array(
                'from' => STOREID,
            );
            $upload = array();
            $content = array();
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
                if ($modelName == "Paket") {
                }
            }
            if (count($upload) == 0) {
                self::echo_log('UPLOAD_GLOBAL', $modelName, 'nothing to upload');
                continue;
            }
            $to = yiiparam('Username');
            $time = date('Y-m-d H:i:s');
            $subject = STOREGLOBAL . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $body['date'] = $time;
            $body['paket'] = false;
            $body['content'] = array($content);
            $status = self::upload_database_email(STOREGLOBAL, $subject,
                json_encode($body, JSON_PRETTY_PRINT), CJSON::encode($upload)
            );
            if ($status == "OK") {
                foreach ($tables as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        self::save_error_log('Upload Global', CHtml::errorSummary($model));
                        throw new Exception("Model " . $modelName . " error : " . CHtml::errorSummary($model));
                    }
                }
                self::echo_log('UPLOAD_GLOBAL', $modelName,
                    'success upload ' . number_format(count($tables)) . ' records');
                $sync = new SyncEmail;
                $sync->type_ = "G";
                $sync->subject = $subject;
                $sync->store = STOREID;
                if (!$sync->save()) {
                    throw new Exception("Model SYNC error : " . CHtml::errorSummary($sync));
                }
            } else {
                self::echo_log('UPLOAD_GLOBAL', $modelName, 'failed upload records');
            }
        }
        $body = array(
            'from' => STOREID,
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $criteria->limit = LIMIT_ROW_SYNC;
        $paket = Paket::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($paket != null) {
            $upload['Paket'] = $paket;
            $modelName = 'Paket';
            $content['Paket'] = number_format(count($paket)) . ' record';
            $paketDetails = array();
            $paketBarang = array();
            /** @var $row Paket */
            foreach ($paket as $row) {
                if (!empty($row->paketDetails)) {
                    $paketDetails = array_merge($paketDetails, $row->paketDetails);
                    foreach ($row->paketDetails as $pktbrg) {
                        if (!empty($pktbrg->paketBarangs)) {
                            $paketBarang = array_merge($paketBarang, $pktbrg->paketBarangs);
                        }
                    }
                }
//                $body['id'] = $row->paket_id;
                $id[] = $row->paket_id;
                if (!empty($paketDetails)) {
                    $upload['PaketDetails'] = $paketDetails;
                    $content['PaketDetails'] = number_format(count($paketDetails)) . ' record';
                }
                if (!empty($paketBarang)) {
                    $upload['PaketBarang'] = $paketBarang;
                    $content['PaketBarang'] = number_format(count($paketBarang)) . ' record';
                }
                $to = yiiparam('Username');
                $time = date('Y-m-d H:i:s');
                $subject = STOREGLOBAL . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
                $body['model'] = $modelName;
                $body['paket'] = true;
                $body['date'] = $time;
                $body['id'] = $id;
                $body['content'] = array($content);
                $status = self::upload_database_email(STOREGLOBAL, $subject,
                    json_encode($body, JSON_PRETTY_PRINT),
                    CJSON::encode($upload)
                );
                if ($status == "OK") {
                    foreach ($paket as $model) {
                        $model->up = 1;
                        if (!$model->save()) {
                            throw new Exception("Model PAKET error : " . CHtml::errorSummary($model));
                        }
                    }
                    self::echo_log('UPLOAD_GLOBAL', $modelName,
                        'success upload ' . number_format(count($paket)) . ' records');
                    $sync = new SyncEmail;
                    $sync->type_ = "G";
                    $sync->subject = $subject;
                    $sync->store = STOREID;
                    if (!$sync->save()) {
                        throw new Exception("Model SYNC error : " . CHtml::errorSummary($sync));
                    }
                } else {
                    self::echo_log('UPLOAD_GLOBAL', $modelName, 'failed upload records');
                }
            }
        }
    }
    private function upload_master_cabang($from = STOREID, $target = STOREIDTARGET)
    {
        $master = array(
            'SysPrefs',
            'Users',
            'Bank',
            'Diskon',
            'Price',
            'Dokter',
            'Beauty',
            'Beli',
            'Jual',
            'GrupAttr'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $criteria->addCondition("store = :store");
        $criteria->limit = LIMIT_ROW_SYNC;
        $criteria->params = array(':store' => $from);
        foreach ($master as $modelName) {
            $upload = array();
            $content = array();
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
            if (count($upload) == 0) {
//                $this->result['upload_master_cabang'][$modelName][$target] = 'nothing to upload';
                self::echo_log('UPLOAD_MASTER_CABANG', $modelName . "-" . $target, 'nothing to upload');
                continue;
            }
            $to = yiiparam('Username');
            $time = date('Y-m-d H:i:s');
            $subject = 'M' . $target . '-' . sha1($from . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email('M' . $target, $subject,
                json_encode(array(
                    'from' => $from,
                    'date' => $time,
                    'paket' => false,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                CJSON::encode($upload)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0 AND store = :store")->execute(array(':store' => $from));
                foreach ($tables as $model) {
                    $model->up = 1;
//                    $model->store = $from;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_master'][$modelName][$target] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_MASTER', $modelName . "-" . $target,
                    'success upload ' . number_format(count($tables)) . ' records');
                $sync = new SyncEmail;
                $sync->type_ = "M";
                $sync->subject = $subject;
                $sync->store = STOREID;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
            } else {
//                $this->result['upload_master'][$modelName][$target] = 'failed upload records';
                self::echo_log('UPLOAD_MASTER', $modelName . "-" . $target, 'failed upload records');
            }
        }
    }
    private function upload_master_all($from = STOREID, $target = STOREIDTARGET)
    {
        $master = array(
            'SysPrefs',
            'Users',
            'Bank',
            'Barang',
            'Price',
            'Dokter',
            'Beauty',
            'Beli',
            'Jual',
            'GrupAttr'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $criteria->limit = LIMIT_ROW_SYNC;
        foreach ($master as $modelName) {
            $upload = array();
            $content = array();
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
            if (count($upload) == 0) {
//                $this->result['upload_master_all'][$modelName][$target] = 'nothing to upload';
                self::echo_log('UPLOAD_MASTER_ALL', $modelName . "-" . $target, 'nothing to upload');
                continue;
            }
            $to = yiiparam('Username');
            $time = date('Y-m-d H:i:s');
            $subject = 'M' . $target . '-' . sha1($from . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email('M' . $target, $subject,
                json_encode(array(
                    'from' => $from,
                    'date' => $time,
                    'paket' => false,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                CJSON::encode($upload)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($tables as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_master_all'][$modelName][$target] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_MASTER_ALL', $modelName . "-" . $target,
                    'success upload ' . number_format(count($tables)) . ' records');
                $sync = new SyncEmail;
                $sync->type_ = "M";
                $sync->subject = $subject;
                $sync->store = STOREID;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
            } else {
//                $this->result['upload_master'][$modelName][$target] = 'failed upload records';
                self::echo_log('UPLOAD_MASTER', $modelName . "-" . $target, 'failed upload records');
            }
        }
    }
    private function upload_transaksi()
    {
        $trans = array(
            'Refs',
            'Comments',
            'GlTrans',
            'StockMoves',
            'BankTrans',
            'StockMovesClinical',
            'StockMovesPerlengkapan',
            'Mgm',
            'Msd',
            'SouvenirTrans',
            'SouvenirOut'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $criteria->limit = LIMIT_ROW_SYNC;
        $to = yiiparam('Username');
        foreach ($trans as $modelName) {
            $upload = array();
            $content = array();
            if (CActiveRecord::model($modelName)->hasAttribute('tdate')) {
                $criteria->order = 'tdate ASC';
            } else {
                $criteria->order = '';
            }
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
            if (count($upload) == 0) {
//                $this->result['upload_transaksi'][$modelName] = 'nothing to upload';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'nothing to upload');
                continue;
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email('T' . STOREIDTARGET, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => false,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                CJSON::encode($upload)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($tables as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($tables)) . ' records');
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// Tender ==============================================================================================================
        /** @var $tender Tender[] */
        $criteria->order = 'tdate ASC';
        $tender = Tender::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($tender != null) {
            $upload['Tender'] = $tender;
            $modelName = 'Tender';
            $content['Tender'] = number_format(count($tender)) . ' record';
            $tenderDetails = array();
            $printz = array();
            $printzDetails = array();
            foreach ($tender as $row) {
//                $row = new Tender;
                $id[] = $row->tender_id;
                if (!empty($row->tenderDetails)) {
                    $tenderDetails = array_merge($tenderDetails, $row->tenderDetails);
                }
                if (!empty($row->printzs)) {
                    $printz = array_merge($printz, $row->printzs);
                }
                foreach ($row->printzs as $row_printz) {
                    if (!empty($row_printz->printzDetails)) {
                        $printzDetails = array_merge($printzDetails, $row_printz->printzDetails);
                    }
                }
            }
            if (count($tenderDetails) > 0) {
                $upload['TenderDetails'] = $tenderDetails;
                $content['TenderDetails'] = number_format(count($tenderDetails)) . ' record';
            }
            if (!empty($printz)) {
                $upload['Printz'] = $printz;
                $content['Printz'] = number_format(count($printz)) . ' record';
            }
            if (!empty($printzDetails)) {
                $upload['PrintzDetails'] = $printzDetails;
                $content['PrintzDetails'] = number_format(count($printzDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email('T' . STOREIDTARGET, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                CJSON::encode($upload)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($tender as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($tender)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($tender)) . ' records');
                self::echo_log('UPLOAD_TRANSAKSI', 'TenderDetails',
                    'success upload ' . number_format(count($tenderDetails)) . ' records');
                self::echo_log('UPLOAD_TRANSAKSI', 'Printz',
                    'success upload ' . number_format(count($printz)) . ' records');
                self::echo_log('UPLOAD_TRANSAKSI', 'PrintzDetails',
                    'success upload ' . number_format(count($printzDetails)) . ' records');
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// Tender ==============================================================================================================
// TransferItem ========================================================================================================
        /** @var $transferItem TransferItem[] */
        $criteria->order = 'tdate ASC';
        $transferItem = TransferItem::model()->findAll($criteria);
//        $tender = new TransferItem;
        $upload = array();
        $content = array();
        $id = array();
        if ($transferItem != null) {
            $modelName = 'TransferItem';
            $upload['TransferItem'] = $transferItem;
            $content['TransferItem'] = number_format(count($transferItem)) . ' record';
            $transferItemDetails = array();
            foreach ($transferItem as $row) {
                $id[] = $row->transfer_item_id;
                if ($row->transferItemDetails != null) {
                    $transferItemDetails = array_merge($transferItemDetails, $row->transferItemDetails);
                }
            }
            if (count($transferItemDetails) > 0) {
                $upload['TransferItemDetails'] = $transferItemDetails;
                $content['TransferItemDetails'] = number_format(count($transferItemDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email('T' . STOREIDTARGET, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                CJSON::encode($upload)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($transferItem as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($transferItem)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($transferItem)) . ' records');
                self::echo_log('UPLOAD_TRANSAKSI', 'TransferItemDetails',
                    'success upload ' . number_format(count($transferItemDetails)) . ' records');
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// TransferItem ========================================================================================================
// TransferBarang ========================================================================================================
        /** @var $transferBarang TransferBarang[] */
        $criteria->order = 'tdate ASC';
        $transferBarang = TransferBarang::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($transferBarang != null) {
            $modelName = 'TransferBarang';
            $upload['TransferBarang'] = $transferBarang;
            $content['TransferBarang'] = number_format(count($transferBarang)) . ' record';
            $transferItemDetails = array();
            foreach ($transferBarang as $row) {
                $id[] = $row->transfer_barang_id;
                if ($row->transferBarangDetails != null) {
                    $transferItemDetails = array_merge($transferItemDetails, $row->transferBarangDetails);
                }
            }
            if (count($transferItemDetails) > 0) {
                $upload['TransferBarangDetails'] = $transferItemDetails;
                $content['TransferBarangDetails'] = number_format(count($transferItemDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email('T' . STOREIDTARGET, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                CJSON::encode($upload)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($transferBarang as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($transferItem)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($transferBarang)) . ' records');
                self::echo_log('UPLOAD_TRANSAKSI', 'TransferItemDetails',
                    'success upload ' . number_format(count($transferItemDetails)) . ' records');
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// TransferBarang ========================================================================================================
// Kas =================================================================================================================
        /* @var $kas Kas[] */
        $criteria->order = 'tdate ASC';
        $kas = Kas::model()->findAll($criteria);
//        $kas = new Kas;
        $upload = array();
        $content = array();
        $id = array();
        if ($kas != null) {
            $modelName = 'Kas';
            $upload['Kas'] = $kas;
            $content['Kas'] = number_format(count($kas)) . ' record';
            $pelunasanUtangDetails = array();
            foreach ($kas as $row) {
                $id[] = $row->kas_id;
                if (count($row->kasDetails) > 0) {
                    $pelunasanUtangDetails = array_merge($pelunasanUtangDetails, $row->kasDetails);
                }
            }
            if (count($pelunasanUtangDetails) > 0) {
                $upload['KasDetail'] = $pelunasanUtangDetails;
                $content['KasDetail'] = number_format(count($pelunasanUtangDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email('T' . STOREIDTARGET, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                CJSON::encode($upload)
            );
//            self::echo_log('UPLOAD_TRANSAKSI', $modelName,
//                CJSON::encode($upload));
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($kas as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($kas)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($kas)) . ' records');
                self::echo_log('UPLOAD_TRANSAKSI', 'KasDetail',
                    'success upload ' . number_format(count($pelunasanUtangDetails)) . ' records');
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// Kas =================================================================================================================
// Salestrans ==========================================================================================================
        /** @var $salestrans Salestrans[] */
        $criteria->order = 'tdate ASC';
        $salestrans = Salestrans::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($salestrans != null) {
            $modelName = 'Salestrans';
            $upload['Salestrans'] = $salestrans;
            $content['Salestrans'] = number_format(count($salestrans)) . ' record';
            $salestransDetails = array();
            $payments = array();
            $paketTrans = array();
            $customers = array();
            foreach ($salestrans as $row) {
                $id[] = $row->salestrans_id;
                $customers[] = $row->customer;
                if (count($row->salestransDetails) > 0) {
                    $salestransDetails = array_merge($salestransDetails, $row->salestransDetails);
                }
                if (count($row->payments) > 0) {
                    $payments = array_merge($payments, $row->payments);
                }
                if (count($row->paketTrans) > 0) {
                    $paketTrans = array_merge($paketTrans, $row->paketTrans);
                }
            }
            $upload['Customers'] = $customers;
            $content['Customers'] = number_format(count($customers)) . ' record';
            if (count($salestransDetails) > 0) {
                $upload['SalestransDetails'] = $salestransDetails;
                $content['SalestransDetails'] = number_format(count($salestransDetails)) . ' record';
            }
            if (count($payments) > 0) {
                $upload['Payment'] = $payments;
                $content['Payment'] = number_format(count($payments)) . ' record';
            }
            if (count($paketTrans) > 0) {
                $upload['PaketTrans'] = $paketTrans;
                $content['PaketTrans'] = number_format(count($paketTrans)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email('T' . STOREIDTARGET, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                CJSON::encode($upload)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($salestrans as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', 'Customers',
                    'success upload ' . number_format(count($customers)) . ' records');
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($salestrans)) . ' records');
                self::echo_log('UPLOAD_TRANSAKSI', 'SalestransDetails',
                    'success upload ' . number_format(count($salestransDetails)) . ' records');
                self::echo_log('UPLOAD_TRANSAKSI', 'Payment',
                    'success upload ' . number_format(count($payments)) . ' records');
                self::echo_log('UPLOAD_TRANSAKSI', 'PaketTrans',
                    'success upload ' . number_format(count($paketTrans)) . ' records');
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// Salestrans ==========================================================================================================
// BeautyServices ======================================================================================================
        /** @var $pelunasanUtang BeautyServices[] */
        $criteria->order = '';
        $pelunasanUtang = BeautyServices::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($pelunasanUtang != null) {
            $modelName = 'BeautyServices';
            $upload['BeautyServices'] = $pelunasanUtang;
            foreach ($pelunasanUtang as $row) {
                $id[] = $row->nscc_beauty_services_id;
            }
            $content['BeautyServices'] = number_format(count($pelunasanUtang)) . ' record';
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email('T' . STOREIDTARGET, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT), CJSON::encode($upload)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($pelunasanUtang as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($pelunasanUtang)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($pelunasanUtang)) . ' records');
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// BeautyServices ======================================================================================================
// PelunasanUtang ======================================================================================================
        /** @var $pelunasanUtang PelunasanUtang[] */
        $criteria->order = 'tdate ASC';
        $pelunasanUtang = PelunasanUtang::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($pelunasanUtang != null) {
            $modelName = 'PelunasanUtang';
            $upload['PelunasanUtang'] = $pelunasanUtang;
            $content['PelunasanUtang'] = number_format(count($pelunasanUtang)) . ' record';
            $pelunasanUtangDetails = array();
            foreach ($pelunasanUtang as $row) {
                $id[] = $row->pelunasan_utang_id;
                if ($row->pelunasanUtangDetils != null) {
                    $pelunasanUtangDetails = array_merge($pelunasanUtangDetails, $row->pelunasanUtangDetils);
                }
            }
            if (count($pelunasanUtangDetails) > 0) {
                $upload['PelunasanUtangDetil'] = $pelunasanUtangDetails;
                $content['PelunasanUtangDetil'] = number_format(count($pelunasanUtangDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email('T' . STOREIDTARGET, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT), CJSON::encode($upload)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($pelunasanUtang as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($pelunasanUtang)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($pelunasanUtang)) . ' records');
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// PelunasanUtang ======================================================================================================
        /* @var $cTrans ClinicalTrans[] */
        $criteria->order = 'tdate ASC';
        $cTrans = ClinicalTrans::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($cTrans != null) {
            $modelName = 'ClinicalTrans';
            $upload['ClinicalTrans'] = $cTrans;
            $content['ClinicalTrans'] = number_format(count($cTrans)) . ' record';
            $cTransDetails = array();
            foreach ($cTrans as $row) {
                $id[] = $row->clinical_trans_id;
                if (count($row->clinicalTransDetails) > 0) {
                    $cTransDetails = array_merge($cTransDetails, $row->clinicalTransDetails);
                }
            }
            if (count($cTransDetails) > 0) {
                $upload['ClinicalTransDetail'] = $cTransDetails;
                $content['ClinicalTransDetail'] = number_format(count($cTransDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email('T' . STOREIDTARGET, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                CJSON::encode($upload)
            );
            if ($status == "OK") {
                foreach ($cTrans as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($kas)) . ' records');
            } else {
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
        /* @var $prod Produksi[] */
        $criteria->order = 'tdate ASC';
        $prod = Produksi::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($prod != null) {
            $modelName = 'Produksi';
            $upload['Produksi'] = $prod;
            $content['Produksi'] = number_format(count($prod)) . ' record';
            $prodDetails = array();
            foreach ($prod as $row) {
                $id[] = $row->produksi_id;
                if (count($row->produksiDetils) > 0) {
                    $prodDetails = array_merge($prodDetails, $row->produksiDetils);
                }
            }
            if (count($prodDetails) > 0) {
                $upload['ProduksiDetil'] = $prodDetails;
                $content['ProduksiDetil'] = number_format(count($prodDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email('T' . STOREIDTARGET, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT), CJSON::encode($upload)
            );
            if ($status == "OK") {
                foreach ($cTrans as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($kas)) . ' records');
            } else {
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// OrderDropping ========================================================================================================
        /** @var $orderDropping OrderDropping[] */
        $criteria->order = 'tdate ASC';
        $orderDropping = OrderDropping::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($orderDropping != null) {
            $modelName = 'OrderDropping';
            $upload['OrderDropping'] = $orderDropping;
            $content['OrderDropping'] = number_format(count($orderDropping)) . ' record';
            $orderDroppingDetails = array();
            foreach ($orderDropping as $row) {
                $id[] = $row->order_dropping_id;
                if ($row->orderDroppingDetails != null) {
                    $orderDroppingDetails = array_merge($orderDroppingDetails, $row->orderDroppingDetails);
                }
            }
            if (count($orderDroppingDetails) > 0) {
                $upload['OrderDroppingDetails'] = $orderDroppingDetails;
                $content['OrderDroppingDetails'] = number_format(count($orderDroppingDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = STOREGLOBAL. '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email(STOREGLOBAL, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                CJSON::encode($upload)
            );
            if ($status == "OK") {
                foreach ($orderDropping as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($orderDropping)) . ' records');
                self::echo_log('UPLOAD_TRANSAKSI', 'OrderDroppingDetails',
                    'success upload ' . number_format(count($orderDroppingDetails)) . ' records');
            } else {
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// OrderDropping ========================================================================================================
// Dropping ========================================================================================================
        /** @var $dropping Dropping[] */
        $criteria->order = 'tdate ASC';
        $dropping = Dropping::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($dropping != null) {
            $modelName = 'Dropping';
            $upload['Dropping'] = $dropping;
            $content['Dropping'] = number_format(count($dropping)) . ' record';
            $droppingDetails = array();
            foreach ($dropping as $row) {
                $id[] = $row->dropping_id;
                if ($row->droppingDetails != null) {
                    $droppingDetails = array_merge($droppingDetails, $row->droppingDetails);
                }
            }
            if (count($droppingDetails) > 0) {
                $upload['DroppingDetails'] = $droppingDetails;
                $content['DroppingDetails'] = number_format(count($droppingDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = STOREGLOBAL . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email(STOREGLOBAL, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                CJSON::encode($upload)
            );
            if ($status == "OK") {
                foreach ($dropping as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($dropping)) . ' records');
                self::echo_log('UPLOAD_TRANSAKSI', 'DroppingDetails',
                    'success upload ' . number_format(count($droppingDetails)) . ' records');
            } else {
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// Dropping ========================================================================================================
        // ReceiveDropping ========================================================================================================
        /** @var $receiveDropping ReceiveDropping[] */
        $criteria->order = 'tdate ASC';
        $receiveDropping = ReceiveDropping::model()->findAll($criteria);
        $upload = array();
        $content = array();
        $id = array();
        if ($receiveDropping != null) {
            $modelName = 'ReceiveDropping';
            $upload['ReceiveDropping'] = $receiveDropping;
            $content['ReceiveDropping'] = number_format(count($receiveDropping)) . ' record';
            $receiveDroppingDetails = array();
            foreach ($receiveDropping as $row) {
                $id[] = $row->receive_dropping_id;
                if ($row->receiveDroppingDetails != null) {
                    $receiveDroppingDetails = array_merge($receiveDroppingDetails, $row->receiveDroppingDetails);
                }
            }
            if (count($receiveDroppingDetails) > 0) {
                $upload['ReceiveDroppingDetails'] = $receiveDroppingDetails;
                $content['ReceiveDroppingDetails'] = number_format(count($receiveDroppingDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = STOREGLOBAL . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = self::upload_database_email(STOREGLOBAL, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'paket' => true,
                    'model' => $modelName,
                    'id' => $id,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                CJSON::encode($upload)
            );
            if ($status == "OK") {
                foreach ($receiveDropping as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($receiveDropping)) . ' records');
                self::echo_log('UPLOAD_TRANSAKSI', 'ReceiveDroppingDetails',
                    'success upload ' . number_format(count($receiveDroppingDetails)) . ' records');
            } else {
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// ReceiveDropping ========================================================================================================
    }
    private function broadcast_master()
    {
        $store = Store::model()->findAll('store_kode <> :store_kode',
            array(':store_kode' => STOREID));
        foreach ($store as $target) {
            self::upload_master_cabang(STOREID, $target->store_kode);
        }
    }
    private function broadcast_transaksi()
    {
        $store = Store::model()->findAll('store_kode <> :store_kode',
            array(':store_kode' => STOREID));
        foreach ($store as $target) {
            $criteria = new CDbCriteria;
            $criteria->addCondition("up = 0");
            $criteria->addCondition("store = :store");
            $criteria->limit = LIMIT_ROW_SYNC;
            // Dropping ========================================================================================================
            /** @var $dropping Dropping[] */
            $criteria->order = 'tdate ASC';
            $criteria->params = array(':store' => $target);
            $dropping = Dropping::model()->findAll($criteria);
            $upload = array();
            $content = array();
            $id = array();
            if ($dropping != null) {
                $modelName = 'Dropping';
                $upload['Dropping'] = $dropping;
                $content['Dropping'] = number_format(count($dropping)) . ' record';
                $droppingDetails = array();
                foreach ($dropping as $row) {
                    $id[] = $row->dropping_id;
                    if ($row->droppingDetails != null) {
                        $droppingDetails = array_merge($droppingDetails, $row->droppingDetails);
                    }
                }
                if (count($droppingDetails) > 0) {
                    $upload['DroppingDetails'] = $droppingDetails;
                    $content['DroppingDetails'] = number_format(count($droppingDetails)) . ' record';
                }
                $time = date('Y-m-d H:i:s');
                $subject = 'T' . $target . '-' . sha1($target . "^" . $modelName . "^" . $time);
                $status = self::upload_database_email('T' . $target, $subject,
                    json_encode(array(
                        'from' => $target,
                        'date' => $time,
                        'paket' => true,
                        'model' => $modelName,
                        'id' => $id,
                        'content' => array($content)
                    ), JSON_PRETTY_PRINT),
                    CJSON::encode($upload)
                );
                if ($status == "OK") {
                    foreach ($dropping as $model) {
                        $model->up = 1;
                        if (!$model->save()) {
                            throw new Exception(t('save.model.fail', 'app',
                                    array('{model}' => $modelName)) . CHtml::errorSummary($model));
                        }
                    }
                    self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                        'success upload ' . number_format(count($dropping)) . ' records');
                    self::echo_log('UPLOAD_TRANSAKSI', 'TransferItemDetails',
                        'success upload ' . number_format(count($droppingDetails)) . ' records');
                } else {
                    self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
                }
            }
// Dropping ========================================================================================================
        }
    }
    private function download_global()
    {
        $array_error = array();
        $this->body_message = "";
        $data_email = self::download_database_email(STOREGLOBAL, 'G');
        $json = $data_email['JSON'];
        if ($json === false) {
            self::echo_log('DOWNLOAD_GLOBAL', 'failed download', $this->status_error);
            return;
        }
        $body = json_decode($this->body_message, true);
        if ($body['paket']) {
            switch ($body['model']) {
                case "Paket" :
                    foreach ($body['id'] as $row) {
                        Yii::app()->db->createCommand("SET @DISABLE_TRIGGERS=1;
                            DELETE np,npd,npb FROM nscc_paket AS np
                            LEFT JOIN nscc_paket_details AS npd ON npd.paket_id = np.paket_id
                            LEFT JOIN nscc_paket_barang AS npb ON npb.paket_details_id = npd.paket_details_id
                            WHERE np.paket_id = :paket_id;
                            SET @DISABLE_TRIGGERS=NULL;")
                            ->execute(array(':paket_id' => $row));
                    }
                    break;
            }
        }
//        app()->db->createCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;")->query();
        foreach ($json as $subject => $email) {
            self::echo_log('SYNC', 'INFO', "PROCESS >> $subject");
            $this->current_subyek = $subject;
            $data = json_decode($email, true);
            if (is_array($data) || is_object($data)) {
                foreach ($data as $k => $v) {
                    foreach ($v as $row) {
                        $primarykey = CActiveRecord::model($k)->tableSchema->primaryKey;
                        $pkval = $row[$primarykey];
                        $model = CActiveRecord::model($k)->findByPk($pkval);
                        if ($model == null) {
                            $model = new $k;
                        } else {
                            $model->setIsNewRecord(false);
                        }
                        $model->attributes = $row;
                        if (!$model->save()) {
                            /*
                             * attribute save dulu untuk flag menyusul
                             * */
                            throw new Exception(t('save.model.fail', 'app',
                                    array('{model}' => $k)) . CHtml::errorSummary($model));
                        }
                        if (CActiveRecord::model($k)->tableSchema->getColumn('up') != null) {
                            $model->up = 1;
                        }
                        if (!$model->save()) {
                            /*
                             * save untuk flag up karena tidak bisa dijadikan satu
                             * kalo bareng flag up akan selalu 0
                             * */
//                            echo "M : $k, PK : $primarykey, ID : $pkval". PHP_EOL;
//                            var_dump($model);
//                            echo PHP_EOL;
                            self::save_error_log($subject, CHtml::errorSummary($model));
                            $array_error[] = $subject;
//                            throw new Exception(t('save.model.fail', 'app',
//                                    array('{model}' => $k)) . CHtml::errorSummary($model));
                        }
                    }
                    self::echo_log('DOWNLOAD_GLOBAL', $k, 'success download ' . number_format(count($v)) . ' records');
                }
            }
            self::echo_log('SYNC', 'INFO', "FINISH >> $subject");
        }
//        app()->db->createCommand("COMMIT;")->query();
        foreach ($data_email['SYNC'] as $sync_row) {
            $index = array_search($sync_row->subject, $array_error);
            if ($index === false) {
                if (!$sync_row->save()) {
                    self::save_error_log('Sync email', CHtml::errorSummary($sync_row));
//                throw new Exception(t('save.model.fail', 'app',
//                        array('{model}' => 'Sync')) . CHtml::errorSummary($sync_row));
                }
            }
        }
    }
    private function download_master()
    {
        $array_error = array();
        $data_email = self::download_database_email(STOREID, 'M');
        $json = $data_email['JSON'];
        if ($json === false) {
//            $this->result['download_master'] = $this->status_error;
            self::echo_log('DOWNLOAD_MASTER', 'failed download', $this->status_error);
            return;
        }
//        app()->db->createCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;")->query();
        foreach ($json as $subject => $email) {
            self::echo_log('SYNC', 'INFO', "PROCESS >> $subject");
            $this->current_subyek = $subject;
            $data = json_decode($email, true);
            $error = false;
            foreach ($data as $k => $v) {
                foreach ($v as $row) {
                    if ($k == 'Price') {
                        CActiveRecord::model($k)->deleteAllByAttributes(array(
                            'barang_id' => $row['barang_id'],
                            'gol_id' => $row['gol_id'],
                            'store' => $row['store']
                        ));
                    }
                    $primarykey = CActiveRecord::model($k)->tableSchema->primaryKey;
//                            if (in_array($k, $this->skip_primary)) {
//                                $model = null;
//                                $row[$primarykey] = null;
//                            } else {
//                                $pkval = $row[$primarykey];
//                                $model = CActiveRecord::model($k)->findByPk($pkval);
//                            }
                    $pkval = $row[$primarykey];
                    $model = CActiveRecord::model($k)->findByPk($pkval);
                    if ($model == null) {
                        $model = new $k;
                    }
                    $model->attributes = $row;
                    if (!$model->save()) {
                        self::save_error_log($subject, CHtml::errorSummary($model));
                        $array_error[] = $subject;
                        $error = true;
//                        throw new Exception(t('save.model.fail', 'app',
//                                array('{model}' => $k)) . CHtml::errorSummary($model));
                    }
                    if (CActiveRecord::model($k)->tableSchema->getColumn('up') != null) {
                        $model->up = MARKAFTERDOWNLOAD ? 1 : 0;
                        $model->attributes = $row;
                        if (!$model->save()) {
                            throw new Exception(t('save.model.fail', 'app',
                                    array('{model}' => $k)) . CHtml::errorSummary($model));
                        }
                    }
                }
//                        $this->result['download_master'][$k] = 'success download ' . number_format(count($v)) . ' records';
                self::echo_log('DOWNLOAD_MASTER', $k,
                    'success download ' . number_format(count($v)) . ' records');
            }
            self::echo_log('SYNC', 'INFO', "FINISH >> $subject");
            if (!$error) {
                Email::model()->deleteAll('`subject` = :subject', array(':subject' => $subject));
                self::echo_log('SYNC', 'INFO', "DELETE >> $subject");
            }
        }
//        app()->db->createCommand("COMMIT;")->query();
        /** @var $sync_row SyncEmail */
//        foreach ($data_email['SYNC'] as $sync_row) {
//            $index = array_search($sync_row->subject, $array_error);
//            if ($index === false) {
//                if (!$sync_row->save()) {
//                    self::save_error_log('Sync email', CHtml::errorSummary($sync_row));
//                throw new Exception(t('save.model.fail', 'app',
//                        array('{model}' => 'Sync')) . CHtml::errorSummary($sync_row));
//                }
//            }
//        }
    }
    private function download_transaksi()
    {
        $array_error = array();
        $this->body_message = "";
        $data_email = self::download_database_email(STOREID, 'T');
        $json = $data_email['JSON'];
        if ($json === false) {
//            $this->result['download_transaksi'] = $this->status_error;
            self::echo_log('DOWNLOAD_TRANSAKSI', 'failed download', $this->status_error);
            return;
        }
        $body = json_decode($this->body_message, true);
//        app()->db->createCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;")->query();
        Yii::app()->db->createCommand("SET FOREIGN_KEY_CHECKS = 0;")
            ->execute(array());
        foreach ($json as $subject => $email) {
            $data = json_decode($email, true);
            self::echo_log('SYNC', 'INFO', "PROCESS >> $subject");
            $this->current_subyek = $subject;
            $error = false;
            foreach ($data as $k => $v) {
                foreach ($v as $row) {
                    $primarykey = CActiveRecord::model($k)->tableSchema->primaryKey;
//                    if (in_array($k, $this->skip_primary)) {
//                        $model = null;
//                        $row[$primarykey] = null;
//                    } else {
//                        $pkval = $row[$primarykey];
//                        $model = CActiveRecord::model($k)->findByPk($pkval);
//                    }
                    $pkval = $row[$primarykey];
                    $model = CActiveRecord::model($k)->findByPk($pkval);
                    if ($model == null) {
                        $model = new $k;
                    }
                    $model->attributes = $row;
                    if (!$model->save(false)) {
                        self::save_error_log($subject, CHtml::errorSummary($model));
                        $array_error[] = $subject;
                        $error = true;
//                        throw new Exception(t('save.model.fail', 'app',
//                                array('{model}' => $k)) . CHtml::errorSummary($model));
                    }
                    if (CActiveRecord::model($k)->tableSchema->getColumn('up') != null) {
//                        $model->up = MARKAFTERDOWNLOAD ? 1 : 0;
//                        $model->attributes = $row;
                        if (!$model->saveAttributes(array('up' => MARKAFTERDOWNLOAD ? 1 : 0))) {
                            throw new Exception(t('save.model.fail', 'app',
                                    array('{model}' => $k)) . CHtml::errorSummary($model));
                        }
                    }
                }
//                $this->result['download_transaksi'][$k] = 'success download ' . number_format(count($v)) . ' records';
                self::echo_log('DOWNLOAD_TRANSAKSI', $k, 'success download ' . number_format(count($v)) . ' records');
            }
            self::echo_log('SYNC', 'INFO', "FINISH >> $subject");
            if (!$error) {
                Email::model()->deleteAll('`subject` = :subject', array(':subject' => $subject));
                self::echo_log('SYNC', 'INFO', "DELETE >> $subject");
            }
        }
        Yii::app()->db->createCommand("SET FOREIGN_KEY_CHECKS = 1;")
            ->execute(array());
//        app()->db->createCommand("COMMIT;")->query();
        /** @var $sync_row SyncEmail */
//        foreach ($data_email['SYNC'] as $sync_row) {
//            $index = array_search($sync_row->subject, $array_error);
//            if ($index === false) {
//                Email::model()->deleteAllByAttributes(array('subject' => $sync_row->subject));
//                Email::model()->deleteAll('`subject` = :subject', array(':subject' => $sync_row->subject));
//                self::echo_log('SYNC', 'INFO', "DELETE >> $sync_row->subject");
//            }
//            unset($sync_row);
//            if (!$sync_row->save()) {
//                throw new Exception(t('save.model.fail', 'app',
//                        array('{model}' => 'Sync')) . CHtml::errorSummary($sync_row));
//            }
//        }
    }
    private function download_email($imapmainbox, $type_ = null)
    {
        $json = array();
        $messagestatus = "ALL";
        $imapaddress = IMAPADDRESS;
        $hostname = $imapaddress . ($type_ == 'G' ? '' : $type_) . $imapmainbox;
        $connection = @imap_open($hostname, $this->username, $this->password);
        if ($connection === false) {
            $this->status_error = 'Cannot connect to Gmail: ' . imap_last_error();
            return false;
        }
        $check = imap_check($connection);
        if ($check->Nmsgs == 0) {
            @imap_close($connection);
            return false;
        }
        $emails = imap_search($connection, $messagestatus);
        if ($emails) {
            foreach ($emails as $email_number) {
                $header = imap_fetch_overview($connection, $email_number, 0);
                if (empty($header)) {
                    self::echo_log('DONWLOAD_EMAIL', 'HEADER_EMPTY', var_export($header, true));
                    continue;
                }
                $subject = $header[0]->subject;
                $date = strtotime($header[0]->date);
                $date2 = time();
                $subTime = $date2 - $date;
                $h = $subTime / (60 * 60 * 24);
                if ($h > DAYSBEFOREDELSYNC) {
                    imap_delete($connection, $email_number, 1);
                    continue;
                }
                $criteria = new CDbCriteria;
                $criteria->addCondition("type_ = :type_ AND subject = :subject");
                $criteria->params = array(':type_' => $type_, ':subject' => $subject);
                $count = Sync::model()->count($criteria);
                if ($count > 0) {
                    continue;
                }
                $message = imap_fetchbody($connection, $email_number, 1.1);
                if ($message == "") {
                    $message = imap_fetchbody($connection, $email_number, 1);
                }
                $structure = imap_fetchstructure($connection, $email_number);
                $attachments = array();
                if (isset($structure->parts) && count($structure->parts)) {
                    for ($i = 0; $i < count($structure->parts); $i++) {
                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename' => '',
                            'name' => '',
                            'attachment' => ''
                        );
                        if ($structure->parts[$i]->ifdparameters) {
                            foreach ($structure->parts[$i]->dparameters as $object) {
                                if (strtolower($object->attribute) == 'filename') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = $object->value;
                                }
                            }
                        }
                        if ($structure->parts[$i]->ifparameters) {
                            foreach ($structure->parts[$i]->parameters as $object) {
                                if (strtolower($object->attribute) == 'name') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = $object->value;
                                }
                            }
                        }
                        if ($attachments[$i]['is_attachment']) {
                            $attachments[$i]['attachment'] = imap_fetchbody($connection, $email_number, $i + 1);
                            if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    }
                }
                if (count($attachments) != 0) {
                    foreach ($attachments as $at) {
                        if ($at['is_attachment'] == 1) {
                            $json[] = Decrypt(bzdecompress($at['attachment']));
                            $sync = new SyncEmail;
                            $sync->type_ = $type_;
                            $this->body_message = $message;
                            $sync->subject = $subject;
                            $sync->message = $message;
                            $sync->store = STOREID;
                            if (!$sync->save()) {
                                throw new Exception(t('save.model.fail', 'app',
                                        array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                            }
                        }
                    }
                }
            }
        }
        imap_expunge($connection);
        imap_close($connection);
        return $json;
    }
    private function download_database_email($imapmainbox, $type_ = null)
    {
        $sync_email = array();
        $json = array();
        $subject = $body = $attachments = null;
        $target = ($type_ == 'G' || $type_ == 'P' ? '' : $type_) . $imapmainbox;
//        $rec = app()->dbadvert->createCommand("DELETE FROM email WHERE DATEDIFF(now(),tdate) > :day");
//        $result = $rec->execute(array(':day' => DAYSBEFOREDELSYNC));
//        $sql = "SELECT subject,body,attachments FROM email
//          WHERE target = :target AND (email.subject NOT IN (SELECT sync_email.subject FROM sync_email WHERE store = :store))
//          :limit_";
//        $dataReader = app()->dbadvert->createCommand($sql)
//            ->query(array(":target" => $target, ":store" => STOREID, ":limit_" => "LIMIT " . LIMIT_ROW_SYNC_DOWN));
        $dataReader = app()->dbadvert->createCommand()
            ->select('subject,body,attachments')
//            ->from('email')
//            ->where("target = :target AND (email.subject NOT IN (SELECT sync_email.subject FROM sync_email WHERE store = :store))",
//                array(":target" => $target, ":store" => STOREID))
            ->from('email_tmp')
            ->where("target = :target AND store = :store",
                array(":target" => $target, ":store" => STOREID))
            ->offset($this->offset)
            ->limit($this->limit)
            ->query(array());
        $dataReader->bindColumn(1, $subject);
        $dataReader->bindColumn(2, $body);
        $dataReader->bindColumn(3, $attachments, PDO::PARAM_LOB);
        while ($dataReader->read() !== false) {
//            $criteria = new CDbCriteria;
//            $criteria->addCondition("type_ = :type_ AND subject = :subject");
//            $criteria->params = array(':type_' => $type_, ':subject' => $subject);
//            $count = Sync::model()->count($criteria);
//            if ($count > 0) {
//                //$this->echo_log('DOWNLOAD_EMAIL', 'UNCOMPRESS', $subject);
//                continue;
//            }
            if ($attachments == null) {
                $this->echo_log('DOWNLOAD_EMAIL', 'UNCOMPRESS', $subject);
                $sync = new SyncEmail;
                $sync->type_ = "E";
//                $this->body_message = $body;
                $sync->subject = $subject;
                $sync->message = '[UNCOMPRESS]' . $body;
                $sync->store = STOREID;
                $sync_email[] = $sync;
//                if (!$sync->save()) {
//                    throw new Exception(t('save.model.fail', 'app',
//                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
//                }
                continue;
            }
            $uncompress = @gzuncompress($attachments);
            if (!$uncompress) {
                $this->echo_log('DOWNLOAD_EMAIL', 'UNCOMPRESS', $subject);
                $sync = new SyncEmail;
                $sync->type_ = "E";
//                $this->body_message = $body;
                $sync->subject = $subject;
                $sync->message = '[UNCOMPRESS]' . $body;
                $sync->store = STOREID;
                $sync_email[] = $sync;
//                if (!$sync->save()) {
//                    throw new Exception(t('save.model.fail', 'app',
//                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
//                }
                continue;
            }
            $json[$subject] = $uncompress;
            $sync = new SyncEmail;
            $sync->type_ = $type_;
            $this->body_message = $body;
            $sync->subject = $subject;
            $sync->message = $body;
            $sync->store = STOREID;
            $sync_email[] = $sync;
//            if (!$sync->save()) {
//                throw new Exception(t('save.model.fail', 'app',
//                        array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
//            }
        }
        /*
                $criteria = new CDbCriteria();
                $criteria->addCondition("target = :target");
                $criteria->params = array(":target" => $target);
                $model = Email::model()->findAll($criteria);
                foreach ($model as $mail) {
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("type_ = :type_ AND subject = :subject");
                    $criteria->params = array(':type_' => $type_, ':subject' => $mail->subject);
                    $count = Sync::model()->count($criteria);
                    if ($count > 0) {
                        continue;
                    }
                    $json[] = gzuncompress($mail->attachments);
                    $sync = new Sync;
                    $sync->type_ = $type_;
                    $this->body_message = $mail->body;
                    $sync->subject = $mail->subject;
                    $sync->message = $mail->body;
                    if (!$sync->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                    }
                }
        */
        return array('JSON' => $json, 'SYNC' => $sync_email);
    }
    function upload_database_email($target, $subject, $message, $attachdata)
    {
        $compress = gzcompress($attachdata, 9);
        $now = new CDbExpression('NOW()');
        $sql = "INSERT INTO email (target, subject,body,attachments,tdate) VALUES(:target,:subject,:body,:attachments,now())";
        $command = app()->dbadvert->createCommand($sql);
        $command->bindParam(":target", $target);
        $command->bindParam(":subject", $subject);
        $command->bindParam(":body", $message);
        $command->bindParam(":attachments", $compress, PDO::PARAM_LOB);
//        $command->bindParam(":tdate",$now);
        $row = $command->execute();
        if ($row > 0) {
            return 'OK';
        }
        return 'ERROR';
//        echo "Target : " . $target
//            . "\n Subject : " . $subject
//            . "\n Message : " . $message
//            . "\n Attach : " . $attachdata;
        //$model = new Email();
        //$model->target = $target;
        //$model->subject = $subject;
        //$model->body = $message;
        //$model->attachments = new CDbExpression('COMPRESS(:att)', array(':att' => gzcompress($attachdata, 9)));
        //$model->attachments = base64_encode(gzcompress($attachdata, 9));
        //$model->tdate = new CDbExpression('NOW()');
        //if (!$model->save()) {
        //    return CHtml::errorSummary($model);
        //}
        //return 'OK';
    }
    private function echo_log($cat, $model, $note)
    {
        echo json_encode(
                array(
                    'time' => date("Y-m-d H:i:s"),
                    'PID' => getmypid(),
                    'event' => $cat,
                    'model' => $model,
                    'note' => $note
                )) . PHP_EOL;
    }
    private function save_error_log($subject, $msg)
    {
        $error = new SyncError();
        $error->pid = getmypid();
        $error->message = $msg;
        $error->store = STOREID;
        $error->subject = $subject;
        $error->tdate = new CDbExpression('NOW()');
        if (!$error->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Sync')) . CHtml::errorSummary($error));
        }
    }
    private function remainder()
    {
        $rec = app()->db->createCommand("SELECT nti.store,ns.supplier_name,
        DATE_FORMAT(nti.tgl,'%d %b %Y') Date,DATE_FORMAT(nti.tgl_jatuh_tempo,'%d %b %Y') DueDate,
        DATEDIFF(nti.tgl_jatuh_tempo, NOW()) remainDays,nti.doc_ref,nti.doc_ref_other,nti.total,
        nti.total - (IF (SUM(npud.kas_dibayar) IS NULL,0,SUM(npud.kas_dibayar))) AS 'Remain'
        FROM nscc_transfer_item AS nti
        LEFT JOIN nscc_pelunasan_utang_detil AS npud ON nti.transfer_item_id = npud.transfer_item_id
        INNER JOIN nscc_supplier AS ns ON nti.supplier_id = ns.supplier_id
        WHERE nti.lunas = 0 AND DATEDIFF(nti.tgl_jatuh_tempo, NOW()) < :start
        GROUP BY nti.transfer_item_id
        HAVING Remain > 0");
        $result = $rec->queryAll(true, array(':start' => REMAINDER_START));
        if (count($result) == 0) {
            return;
        }
        $stat = SysPrefs::model()->find('name_ = :name AND store =:store',
            array(':name' => 'last_remainder', ':store' => STOREID));
        $last_remainder = $stat->value_;
        $dStart = new DateTime($last_remainder);
        $dEnd = new DateTime();
        $dDiff = $dStart->diff($dEnd);
        //echo $dDiff->format('%R'); // use for point out relation: smaller/greater
        $days = $dDiff->days;
        if ($days < REMAINDER_INTERVAL) {
            return;
        }
        $sum_Remain = array_sum(array_column($result, 'Remain'));
        $sum_total = array_sum(array_column($result, 'total'));
        $body = "<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
</head>
<body>
<p>Dear Bapak Ferro,</p>
<p>Berikut data tagihan yang mendekati jatuh tempo.</p>
<table style='font-family: verdana,arial,sans-serif;font-size:11px;color:#333333;border: 1px solid #666666;' ><tr>
<th style='border: 1px solid #666666;' bgcolor='#dedede'>Supplier</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede'>Date</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede'>Due Date</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede' align='right'>Remain Days</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede'>Invoice No.</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede' align='right'>Total</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede' align='right'>Remain</th></tr>";
        foreach ($result as $row) {
            $body .= "<tr>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>" . $row['supplier_name'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>" . $row['Date'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>" . $row['DueDate'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . $row['remainDays'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>" . $row['doc_ref_other'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . number_format($row['total'], 2) . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . number_format($row['Remain'],
                    2) . "</td></tr>";
        }
        $body .= "<tr>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>TOTAL</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' colspan='4'></td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . number_format($sum_total, 2) . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . number_format($sum_Remain, 2) . "</td></tr>";
        $body .= "</table><p>Terima Kasih</p>
</body>
</html>";
//        $body = $this->render('Remainder', array('dp' => $dataProvider, 'total_remain' => $sum_Remain), true);
        $send = mailsend_remainder(REMAINDER_EMAIL, yiiparam('Username'),
            'REMAINDER TAGIHAN HUTANG NWIS ' . STOREID . ' ' . date('d/m/Y'), $body);
        if ($send == "OK") {
            $stat->value_ = date('Y-m-d');
            $stat->save();
            self::echo_log('REMAINDER', 'SEND_REMAINDER',
                'success remainder ' . number_format(count($rec)) . ' records');
//            $this->result['send_remainder'] = 'success remainder ' . number_format(count($rec)) . ' records';
        } else {
//            $this->result['send_remainder'] = 'failed remainder';
            self::echo_log('REMAINDER', 'SEND_REMAINDER', 'failed remainder');
        }
    }
}