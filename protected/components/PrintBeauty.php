<?php
/**
 * @property BeautyServiceView $s
 * @property ResepPrint $resepPrint
 */
class PrintBeauty extends BasePrint
{
    private $s;
    private $col_width = 40;
    private $index;
    function __construct($s, $index)
    {
//        $s = new Salestrans();
        $this->s = $s;
        $this->index = $index;
//        $this->sds = $sds;
    }
//    public function beautyTxt()
//    {
//        $newLine = "\r\n";
//        $raw = parent::setCenter(SysPrefs::get_val('receipt_header0'), $this->col_width);
//        $raw .= $newLine;
//        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header1'), $this->col_width);
//        $raw .= $newLine;
//        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header2'), $this->col_width);
//        $raw .= $newLine;
//        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header3'), $this->col_width);
//        $raw .= $newLine;
//        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header4'), $this->col_width);
//        $raw .= $newLine;
//        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header5'), $this->col_width);
//        $raw .= $newLine;
//        $raw .= parent::fillWithChar("=", $this->col_width);
//        $raw .= $newLine;
//        $raw .= parent::addHeaderSales("No. Receipt", $this->s->doc_ref);
//        $raw .= $newLine;
//        $raw .= parent::addHeaderSales("Date", sql2date($this->s->tgl, "dd-MMM-yyyy"));
//        $raw .= $newLine;
//        $raw .= parent::addHeaderSales("No. Customer", $this->s->no_customer);
//        $raw .= $newLine;
//        $raw .= parent::addHeaderSales("Nama Customer", $this->s->nama_customer);
//        $raw .= $newLine;
//        /** @var Salestrans $sales */
//        $sales = Salestrans::model()->findByPk($this->s->salestrans_id);
//        /** @var SalestransDetails $beauty */
//        $beauty = SalestransDetails::model()->findByPk($this->s->salestrans_details);
////        $beauty->beautyServices[0]->beauty->nama_employee
////        $beauty = Yii::app()->db->createCommand(
////            "SELECT bsw.*, b.nama_beauty, sd.price, d.nama_dokter
////                        FROM nscc_beauty_service_view AS bsw
////                        LEFT JOIN nscc_beauty AS b ON b.beauty_id = bsw.beauty_id
////                        LEFT JOIN nscc_salestrans_details AS sd ON sd.barang_id = bsw.barang_id
////                        LEFT JOIN nscc_dokter AS d ON d.dokter_id = bsw.dokter_id
////                        WHERE bsw.salestrans_id = '" . $this->s->salestrans_id . "' GROUP BY salestrans_details")
////            ->queryAll(true);
////        $beauty = Yii::app()->db->createCommand(
////                        "SELECT bsw.*, b.nama_beauty, sd.price, d.nama_dokter
////                        FROM posngnew.nscc_beauty_service_view as bsw
////                        LEFT JOIN nscc_beauty as b on b.beauty_id = bsw.beauty_id
////                        LEFT JOIN nscc_salestrans_details as sd on sd.barang_id = bsw.barang_id
////                        LEFT JOIN nscc_dokter as d on d.dokter_id = bsw.dokter_id
////                        WHERE bsw.salestrans_id = '" . $this->s->salestrans_id . "' AND bsw.salestrans_details = '" . $this->sds . "'")
////                ->queryAll(true);
//        if (count($beauty->beautyServices) <= 0) {
//            return false;
//        }
//        $raw .= parent::addHeaderSales('Beauty', $beauty->beautyServices[0]->beauty->nama_employee);
//        $raw .= $newLine;
//        if ($sales->dokter_id != null) {
//            /** @var Dokter $dokter */
//            $dokter = Dokter::model()->findByPk($sales->dokter_id);
//            if ($dokter != null) {
//                $raw .= parent::addHeaderSales('KMR', $dokter->nama_dokter);
//                $raw .= $newLine;
//            }
//        }
//        $raw .= parent::addHeaderSales("Perawatan", $beauty->barang->kode_barang);
//        $raw .= $newLine;
//        $raw .= parent::addHeaderSales("Note", $beauty->ketpot);
//        $raw .= $newLine;
//        $raw .= parent::addHeaderSales("Qty", $beauty->qty);
//        $raw .= $newLine;
//        $raw .= parent::addHeaderSales("Nominal", $beauty->beautyServices[0]->total);
//        $raw .= $newLine;
//        $raw .= parent::addLeftRight("", get_date_today("hh:mm:ss/dd-MM-yyyy"), $this->col_width);
//        $raw .= $newLine;
//        return $raw;
////        return base64_encode(chr(27) . chr(64) . parent::fillWithChar("-") . chr(27) . chr(105));
//    }
    public function beautyTxt()
    {   
        /** @var SalestransDetails $barang */
        $sd = SalestransDetails::model()->findByPk($this->s->salestrans_details);
        $grup = Barang::model()->findByPk($sd->barang_id)->grup_id;        
        $newLine = "\r\n";
        $raw = parent::setCenter(SysPrefs::get_val('receipt_header0'), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header1'), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header2'), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header3'), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header4'), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header5'), $this->col_width);
        $raw .= $newLine;
        $raw .= parent::fillWithChar("=", $this->col_width);
        $raw .= $newLine;
        /*
         * Pisah No. RP dan No. RPM
         */
        IF ($grup == TREATMENT_DOCTOR){
            $raw .= parent::addHeaderSales("No. RPM", $this->s->doc_ref);
        } ELSE IF ($grup == TREATMENT_SKIN_CARE){
            $raw .= parent::addHeaderSales("No. RP", $this->s->doc_ref);
        } ELSE {
            $raw .= parent::addHeaderSales("No. Receipt", $this->s->doc_ref);
        }        
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Date", sql2date($this->s->tgl, "dd-MMM-yyyy"));
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("No. Pasien", $this->s->no_customer);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Nama Pasien", $this->s->nama_customer);
        $raw .= $newLine;
        /** @var Salestrans $sales */
        $sales = Salestrans::model()->findByPk($this->s->salestrans_id);
        /** @var BeautyServiceView $beauty */
        $beauty = BeautyServiceView::model()->findByAttributes(["salestrans_details" => $sd->salestrans_details]);
        
        if (count($beauty) <= 0) {
            return false;
        }        
        if ($sales->dokter_id != null) {
            /** @var Dokter $dokter */
            $dokter = Dokter::model()->findByPk($sales->dokter_id);
            if ($dokter != null) {
                $raw .= parent::addHeaderSales('KMR', $dokter->nama_dokter);
                $raw .= $newLine;
            }
        }
        IF ($grup == TREATMENT_DOCTOR){
            $raw .= parent::addHeaderSales('Dokter', Dokter::model()->findByAttributes(["dokter_id" => $beauty->dokter_id])->nama_dokter);
            $raw .= $newLine;
        }
        $raw .= parent::addHeaderSales('Perawat', Beauty::model()->findByAttributes(["beauty_id" => $beauty->beauty_id])->nama_beauty);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Perawatan", $sd->barang->kode_barang);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Catatan", $sd->ketpot);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Jumlah", $sd->qty);
        $raw .= $newLine;
//        $raw .= parent::addHeaderSales("Nominal", $beauty->beautyServices[0]->total);
//        $raw .= $newLine;
        $raw .= parent::addLeftRight("", get_date_today("hh:mm:ss/dd-MM-yyyy"), $this->col_width);
        $raw .= $newLine;
        return $raw;
//        return base64_encode(chr(27) . chr(64) . parent::fillWithChar("-") . chr(27) . chr(105));
    }
}