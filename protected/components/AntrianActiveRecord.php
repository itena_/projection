<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 2/16/2016
 * Time: 9:54 AM
 */
class AntrianActiveRecord extends CActiveRecord
{
    private static $antrian = null;
    protected static function getAntrianDbConnection()
    {
        if (self::$antrian !== null)
            return self::$antrian;
        else {
            self::$antrian = Yii::app()->antrian;
            if (self::$antrian instanceof CDbConnection) {
                self::$antrian->setActive(true);
                return self::$antrian;
            } else
                throw new CDbException(Yii::t('yii', 'Active Record requires a "db" CDbConnection application component.'));
        }
    }
}