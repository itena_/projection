<?php
class PusatActiveRecord extends CActiveRecord
{
    private static $dbpusat = null;

    protected static function getPusatDbConnection()
    {
        if (self::$dbpusat !== null)
            return self::$dbpusat;
        else
        {
            self::$dbpusat = Yii::app()->dbpusat;
            if (self::$dbpusat instanceof CDbConnection)
            {
                self::$dbpusat->setActive(true);
                return self::$dbpusat;
            }
            else
                throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
        }
    }

}