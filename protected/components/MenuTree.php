<?php
class MenuTree
{
    var $security_role;
    var $security_roles_id;

    var $menu_users = array(
        'text' => 'User Manajement',
        'id' => 'jun.UsersGrid',
        'leaf' => true
    );
    var $security = array(
        'text' => 'Security Roles',
        'id' => 'jun.SecurityRolesGrid',
        'leaf' => true
    );
    function __construct($id)
    {
        $role = SecurityRoles::model()->findByPk($id);
        $this->security_role = explode(",", $role->sections);
        $this->security_roles_id = $role->security_roles_id;


    }
    function getChildMaster()
    {
        /** @var TODO 112 kosong */
        $child = array();
        if (in_array(100, $this->security_role)) {
            $child[] = array(
                'text' => 'Country',
                'id' => 'jun.NegaraGrid',
                'leaf' => true
            );
        }
        if (in_array(101, $this->security_role)) {
            $child[] = array(
                'text' => 'State',
                'id' => 'jun.ProvinsiGrid',
                'leaf' => true
            );
        }
        if (in_array(102, $this->security_role)) {
            $child[] = array(
                'text' => 'City',
                'id' => 'jun.KotaGrid',
                'leaf' => true
            );
        }
        if (in_array(103, $this->security_role)) {
            $child[] = array(
                'text' => 'Sub District',
                'id' => 'jun.KecamatanGrid',
                'leaf' => true
            );
        }
        if (in_array(104, $this->security_role)) {
            $child[] = array(
                'text' => 'Doctor',
                'id' => 'jun.DokterGrid',
                'leaf' => true
            );
        }
        if (in_array(105, $this->security_role)) {
            $child[] = array(
                'text' => 'Bank',
                'id' => 'jun.BankGrid',
                'leaf' => true
            );
        }
        if (in_array(106, $this->security_role)) {
            $child[] = array(
                'text' => 'Group',
                'id' => 'jun.GrupGrid',
                'leaf' => true
            );
        }
        if (in_array(107, $this->security_role)) {
            $child[] = array(
                'text' => 'Product/Treatment',
                'id' => 'jun.BarangGrid',
                'leaf' => true
            );
        }
        if (in_array(133, $this->security_role)) {
            $child[] = array(
                'text' => 'Resep',
                'id' => 'jun.ResepGrid',
                'leaf' => true
            );
        }
        if (in_array(108, $this->security_role)) {
            $child[] = array(
                'text' => 'Customers',
                'id' => 'jun.CustomersGrid',
                'leaf' => true
            );
        }
        if (in_array(108, $this->security_role)) {
            $child[] = array(
                'text' => 'Member Card',
                'id' => 'jun.MemberCardGrid',
                'leaf' => true
            );
        }
        if (in_array(109, $this->security_role)) {
            $child[] = array(
                'text' => 'Beautician',
                'id' => 'jun.BeautyGrid',
                'leaf' => true
            );
        }
        if (in_array(110, $this->security_role)) {
            $child[] = array(
                'text' => 'Beautician Class',
                'id' => 'jun.GolGrid',
                'leaf' => true
            );
        }
        if (in_array(111, $this->security_role)) {
            $child[] = array(
                'text' => 'Beautician Tip Rate',
                'id' => 'jun.PriceGrid',
                'leaf' => true
            );
        }
        if (in_array(112, $this->security_role)) {
            $child[] = array(
                'text' => 'Beauty Onduty',
                'id' => 'jun.BeautyOndutyGrid',
                'leaf' => true
            );
        }
        if (in_array(134, $this->security_role)) {
            $child[] = array(
                'text' => 'Apoteker',
                'id' => 'jun.ApotekerGrid',
                'leaf' => true
            );
        }
        if (in_array(135, $this->security_role)) {
            $child[] = array(
                'text' => 'Front Office',
                'id' => 'jun.FoGrid',
                'leaf' => true
            );
        }
        if (in_array(113, $this->security_role)) {
            $child[] = array(
                'text' => 'Status Customers',
                'id' => 'jun.StatusCustGrid',
                'leaf' => true
            );
        }
        if (in_array(136, $this->security_role)) {
            $child[] = array(
                'text' => 'Promosi',
                'id' => 'jun.PromosiGrid',
                'leaf' => true
            );
        }
        if (in_array(114, $this->security_role)) {
            $child[] = array(
                'text' => 'Discount',
                'id' => 'jun.DiskonGrid',
                'leaf' => true
            );
        }
        if (in_array(115, $this->security_role)) {
            $child[] = array(
                'text' => 'Store',
                'id' => 'jun.StoreGrid',
                'leaf' => true
            );
        }
        if (in_array(116, $this->security_role)) {
            $child[] = array(
                'text' => 'Supplier',
                'id' => 'jun.SupplierGrid',
                'leaf' => true
            );
        }
        if (in_array(504, $this->security_role)) {
            $child[] = array(
                'text' => 'Asset Class',
                'id' => 'jun.AssetGroupGrid',
                'leaf' => true
            );
        }


        if (in_array(131, $this->security_role)) {
            $child[] = array(
                'text' => 'Barang Asset',
                'id' => 'jun.BarangAssetGrid',
                'leaf' => true
            );
        }
        if (in_array(133, $this->security_role)) {
            $child[] = array(
                'text' => 'Barang Asset Group',
                'id' => 'jun.GroupAssetGrid',
                'leaf' => true
            );
        }
//        if (in_array(132, $this->security_role)) {
//            $child[] = array(
//                'text' => 'Chart Of Account Class',
//                'id' => 'jun.ChartClassGrid',
//                'leaf' => true
//            );
//        }
//        if (in_array(131, $this->security_role)) {
//            $child[] = array(
//                'text' => 'Chart Of Account Type',
//                'id' => 'jun.ChartTypesGrid',
//                'leaf' => true
//            );
//        }
//        if (in_array(117, $this->security_role)) {
//            $child[] = array(
//                'text' => 'Chart Of Account',
//                'id' => 'jun.ChartMasterGrid',
//                'leaf' => true
//            );
//        }
        if (in_array(117, $this->security_role)) {
            $child[] = array(
                'text' => 'Chart Of Account',
                'id' => 'jun.DesignLabaRugiGrid',
                'leaf' => true
            );
        }
        if (in_array(118, $this->security_role)) {
            $child[] = array(
                'text' => 'Default Purchase Price',
                'id' => 'jun.BeliGrid',
                'leaf' => true
            );
        }
        if (in_array(119, $this->security_role)) {
            $child[] = array(
                'text' => 'Region',
                'id' => 'jun.WilayahGrid',
                'leaf' => true
            );
        }
        if (in_array(121, $this->security_role)) {
            $child[] = array(
                'text' => 'Card Type',
                'id' => 'jun.CardGrid',
                'leaf' => true
            );
        }
        if (in_array(120, $this->security_role)) {
            $child[] = array(
                'text' => 'Keterangan Transaksi',
                'id' => 'jun.KetTransGrid',
                'leaf' => true
            );
        }
        if (in_array(122, $this->security_role)) {
            $child[] = array(
                'text' => 'Group Attribute',
                'id' => 'jun.GrupAttrGrid',
                'leaf' => true
            );
        }
        if (in_array(123, $this->security_role)) {
            $child[] = array(
                'text' => 'Selling Price',
                'id' => 'jun.JualGrid',
                'leaf' => true
            );
        }

        if (in_array(125, $this->security_role)) {
            $child[] = array(
                'text' => 'Operational Item',
                'id' => 'jun.BarangClinicalGrid',
                'leaf' => true
            );
        }
        if (in_array(126, $this->security_role)) {
            $child[] = array(
                'text' => 'Operational Item Category',
                'id' => 'jun.KategoriClinicalGrid',
                'leaf' => true
            );
        }
        if (in_array(127, $this->security_role)) {
            $child[] = array(
                'text' => 'Info',
                'id' => 'jun.InfoGrid',
                'leaf' => true
            );
        }
        if (in_array(128, $this->security_role)) {
            $child[] = array(
                'text' => 'Souvenir',
                'id' => 'jun.SouvenirGrid',
                'leaf' => true
            );
        }
        if (in_array(129, $this->security_role)) {
            $child[] = array(
                'text' => 'Promotion',
                'id' => 'jun.EventGrid',
                'leaf' => true
            );
        }
        if (in_array(130, $this->security_role)) {
            $child[] = array(
                'text' => 'Packages',
                'id' => 'jun.PaketGrid',
                'leaf' => true
            );
        }

        if (in_array(132, $this->security_role)) {
            $child[] = array(
                'text' => 'Barang Perlengkapan',
                'id' => 'jun.BarangPerlengkapanGrid',
                'leaf' => true
            );
        }
        if (in_array(137, $this->security_role)) {
            $child[] = array(
                'text' => 'Store Area',
                'id' => 'jun.AreaGrid',
                'leaf' => true
            );
        }
        if (in_array(138, $this->security_role)) {
            $child[] = array(
                'text' => 'Bonus name',
                'id' => 'jun.BonusNameGrid',
                'leaf' => true
            );
        }
        if (in_array(139, $this->security_role)) {
            $child[] = array(
                'text' => 'Bonus Jual',
                'id' => 'jun.BonusJualGrid',
                'leaf' => true
            );
        }
        if (in_array(140, $this->security_role)) {
            $child[] = array(
                'text' => 'User Employee',
                'id' => 'jun.UserEmployeeGrid',
                'leaf' => true
            );
        }
        if (in_array(141, $this->security_role)) {
            $child[] = array(
                'text' => 'Tag',
                'id' => 'jun.TagGrid',
                'leaf' => true
            );
        }
        if (in_array(142, $this->security_role)) {
            $child[] = array(
                'text' => 'Tag Product/Treatment',
                'id' => 'jun.TagBarangGrid',
                'leaf' => true
            );
        }
        if (in_array(143, $this->security_role)) {
            $child[] = array(
                'text' => 'Assign Employees',
                'id' => 'jun.AssignEmployeesGrid',
                'leaf' => true
            );
        }
        if (in_array(144, $this->security_role)) {
            $child[] = array(
                'text' => 'Racikan',
                'id' => 'jun.RacikanGrid',
                'leaf' => true
            );
        }
        if (in_array(145, $this->security_role)) {
            $child[] = array(
                'text' => 'Bonus Template',
                'id' => 'jun.BonusTemplateGrid',
                'leaf' => true
            );
        }
        if (in_array(146, $this->security_role)) {
            $child[] = array(
                'text' => 'Referral',
                'id' => 'jun.ReferralGrid',
                'leaf' => true
            );
        }
        if (in_array(147, $this->security_role)) {
            $child[] = array(
                'text' => 'Fee FeeReferral',
                'id' => 'jun.FeeReferral',
                'leaf' => true
            );
        }
        if (in_array(148, $this->security_role)) {
            $child[] = array(
                'text' => 'Ship To',
                'id' => 'jun.ShipToGrid',
                'leaf' => true
            );
        }



        //projection
        if (in_array(804, $this->security_role)) {
            $child[] = array(
                'text' => 'Unit',
                'id' => 'jun.UnitGrid',
                'leaf' => true
            );
        }
        if (in_array(803, $this->security_role)) {
            $child[] = array(
                'text' => 'Outlets',
                'id' => 'jun.OutletGrid',
                'leaf' => true
            );
        }
        if (in_array(801, $this->security_role)) {
            $child[] = array(
                'text' => 'Product Groups',
                'id' => 'jun.GroupprodukGrid',
                'leaf' => true
            );
        }
        if (in_array(802, $this->security_role)) {
            $child[] = array(
                'text' => 'Products',
                'id' => 'jun.ProdukGrid',
                'leaf' => true
            );
        }
        if (in_array(807, $this->security_role)) {
            $child[] = array(
                'text' => 'Account Categories',
                'id' => 'jun.CategoryGrid',
                'leaf' => true
            );
        }
        if (in_array(806, $this->security_role)) {
            $child[] = array(
                'text' => 'Account Group',
                'id' => 'jun.GroupGrid',
                'leaf' => true
            );
        }
        if (in_array(805, $this->security_role)) {
            $child[] = array(
                'text' => 'Account',
                'id' => 'jun.AccountGrid',
                'leaf' => true
            );
        }
        /*if (in_array(805, $this->security_role)) {
            $child[] = array(
                'text' => 'Chart Of Accounts',
                'id' => 'jun.DesignLabaRugiGrid',
                'leaf' => true
            );
        }*/


        return $child;
    }
    function getMaster($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Master',
            'expanded' => false,
            'children' => $child
        );
    }
    function getTransaction($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Transaction',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildTransaction()
    {
        $child = array();

        if (in_array(900, $this->security_role)) {
            $child[] = array(
                'text' => 'Sales Plan',
                'id' => 'jun.TransaksiOverrideGrid',
                'leaf' => true
            );
        }

        if (in_array(900, $this->security_role)) {
            $child[] = array(
                'text' => 'Sales Realization',
                'id' => 'jun.TransaksiOverrideRealisasiGrid',
                'leaf' => true
            );
        }
        if (in_array(808, $this->security_role)) {
            $child[] = array(
                'text' => 'Sales',
                'id' => 'jun.TransaksiGrid',
                'leaf' => true
            );
        }
        if (in_array(818, $this->security_role)) {
            $child[] = array(
                'text' => 'Investment Plans',
                'id' => 'jun.InvestasiGrid',
                'leaf' => true
            );
        }
        if (in_array(819, $this->security_role)) {
            $child[] = array(
                'text' => 'Investment Realizations',
                'id' => 'jun.InvestasiRealisasiGrid',
                'leaf' => true
            );
        }
        if (in_array(820, $this->security_role)) {
            $child[] = array(
                'text' => 'Investment Analysis',
                'id' => 'jun.AnalysisInvestasiGrid',
                'leaf' => true
            );
        }
        if (in_array(809, $this->security_role)) {
            $child[] = array(
                'text' => 'Budget Plans',
                'id' => 'jun.BudgetGrid',
                'leaf' => true
            );
        }
        if (in_array(810, $this->security_role)) {
            $child[] = array(
                'text' => 'Budget Realizations',
                'id' => 'jun.RealizationGrid',
                'leaf' => true
            );
        }
        if (in_array(811, $this->security_role)) {
            $child[] = array(
                'text' => 'Budget Analysis',
                'id' => 'jun.AnalysisGrid',
                'leaf' => true
            );
        }

        return $child;
    }
    function getReport($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Report',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildReport()
    {
        $child = array();
        if (in_array(821, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Sales Plans',
                'id' => 'jun.ReportTransactionOverride',
                'leaf' => true
            );
        }
        if (in_array(831, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Sales Realizations',
                'id' => 'jun.ReportTransactionOverrideRealization',
                'leaf' => true
            );
        }

        if (in_array(812, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Sales',
                'id' => 'jun.ReportTransaction',
                'leaf' => true
            );
        }
        if (in_array(816, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Investment Plans',
                'id' => 'jun.ReportInvestment',
                'leaf' => true
            );
        }
        if (in_array(822, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Investment Realizations',
                'id' => 'jun.ReportInvestmentRealization',
                'leaf' => true
            );
        }
        if (in_array(823, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Investment Analysis',
                'id' => 'jun.ReportInvestmentAnalysis',
                'leaf' => true
            );
        }

        if (in_array(813, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Budget Plans',
                'id' => 'jun.ReportBudget',
                'leaf' => true
            );
        }
        if (in_array(814, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Budget Realizations',
                'id' => 'jun.ReportRealization',
                'leaf' => true
            );
        }
        if (in_array(815, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Budget Analysis',
                'id' => 'jun.ReportAnalysis',
                'leaf' => true
            );
        }
        if (in_array(830, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Profit and Loss (Plans)',
                'id' => 'jun.ReportBep',
                'leaf' => true
            );
        }
        if (in_array(832, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Profit and Loss (Realizations)',
                'id' => 'jun.ReportBepRealization',
                'leaf' => true
            );
        }


        //ALL BU
        if (in_array(827, $this->security_role)) {
            $child[] = array(
                'text' => 'Report All Investment Plans',
                'id' => 'jun.ReportInvestmentPlanAll',
                'leaf' => true
            );
        }
        if (in_array(828, $this->security_role)) {
            $child[] = array(
                'text' => 'Report All Investment Realizations',
                'id' => 'jun.ReportInvestmentRealizationAll',
                'leaf' => true
            );
        }
        if (in_array(829, $this->security_role)) {
            $child[] = array(
                'text' => 'Report All Investment Analysis',
                'id' => 'jun.ReportInvestmentAnalysisAll',
                'leaf' => true
            );
        }

        if (in_array(824, $this->security_role)) {
            $child[] = array(
                'text' => 'Report All Budget Plans',
                'id' => 'jun.ReportBudgetPlanAll',
                'leaf' => true
            );
        }
        if (in_array(825, $this->security_role)) {
            $child[] = array(
                'text' => 'Report All Budget Realizations',
                'id' => 'jun.ReportBudgetRealizationAll',
                'leaf' => true
            );
        }
        if (in_array(826, $this->security_role)) {
            $child[] = array(
                'text' => 'Report All Budget Analysis',
                'id' => 'jun.ReportBudgetAnalysisAll',
                'leaf' => true
            );
        }




        return $child;
    }
    function getAdministration($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Administration',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildAdministration()
    {
        $child = array();
        if (in_array(400, $this->security_role)) {
            $child[] = array(
                'text' => 'User Management',
                'id' => 'jun.UsersGrid',
                'leaf' => true
            );
        }
        if (in_array(800, $this->security_role)) {
            $child[] = array(
                'text' => 'Business Units',
                'id' => 'jun.BusinessunitGrid',
                'leaf' => true
            );
        }
        if (in_array(401, $this->security_role)) {
            $child[] = array(
                'text' => 'Security Roles',
                'id' => 'jun.SecurityRolesGrid',
                'leaf' => true
            );
        }
        if (in_array(402, $this->security_role)) {
            $child[] = array(
                'text' => 'Backup / Restore',
                'id' => 'jun.BackupRestoreWin',
                'leaf' => true
            );
        }
        if (in_array(403, $this->security_role)) {
            $child[] = array(
                'text' => 'Import',
                'id' => 'jun.ImportXlsx',
                'leaf' => true
            );
        }
        if (in_array(404, $this->security_role)) {
            $child[] = array(
                'text' => 'Setting Client',
                'id' => 'jun.SettingsClientsGrid',
                'leaf' => true
            );
        }
        if (in_array(405, $this->security_role)) {
            $child[] = array(
                'text' => 'Preferences',
                'id' => 'jun.SysPrefsWin',
                'leaf' => true
            );
        }
        if (in_array(124, $this->security_role)) {
            $child[] = array(
                'text' => 'Employee',
                'id' => 'jun.EmployeeGrid',
                'leaf' => true
            );
        }
        if (in_array(406, $this->security_role)) {
            $child[] = array(
                'text' => 'User Employee',
                'id' => 'jun.UserEmployeeGrid',
                'leaf' => true
            );
        }
        if (in_array(502, $this->security_role)) {
            $child[] = array(
                'text' => 'Restrict Date User',
                'id' => 'jun.RestrictDateGrid',
                'leaf' => true
            );
        }
        if (in_array(503, $this->security_role)) {
            $child[] = array(
                'text' => 'Upload History',
                'id' => 'jun.UploadHistoryManual',
                'leaf' => true
            );
        }
        if (in_array(508, $this->security_role)) {
            $child[] = array(
                'text' => 'Sync',
                'id' => 'jun.SyncStatusGrid',
                'leaf' => true
            );
        }

        if (in_array(817, $this->security_role)) {
            $child[] = array(
                'text' => 'Import Data',
                'id' => 'jun.ImportDataProjection',
                'leaf' => true
            );
        }
        /*if (in_array(508, $this->security_role)) {
            $child[] = array(
                'text' => 'Sync Status',
                'id' => 'jun.SyncStatusGrid',
                'leaf' => true
            );
        }*/
        return $child;
    }

    /*function getSync($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Sync',
            'expanded' => false,
            'children' => $child
        );
    }*/

    /*function getChildSync()
    {
        $child = array();
        if (in_array(400, $this->security_role)) {
            $child[] = array(
                'text' => 'History',
                'id' => 'jun.SyncHistoryWin',
                'leaf' => true
            );
        }
        if (in_array(400, $this->security_role)) {
            $child[] = array(
                'text' => 'Customer',
                'id' => 'jun.SyncCustomerWin',
                'leaf' => true
            );
        }

        return $child;
    }*/


    function getGeneral()
    {
        $username = Yii::app()->user->name;
        $child = array();
        if (in_array(000, $this->security_role)) {
            $child[] = array(
                'text' => 'Change Password',
                'id' => 'jun.PasswordWin',
                'leaf' => true
            );
        }
        if (in_array(001, $this->security_role)) {
            $child[] = array(
                'text' => "Logout ($username)",
                'id' => 'logout',
                'leaf' => true
            );
        }
        return $child;
    }
    public function get_menu()
    {
        $data = array();
        $master = self::getMaster(self::getChildMaster());
        if (!empty($master)) {
            $data[] = $master;
        }
        $trans = self::getTransaction(self::getChildTransaction());
        if (!empty($trans)) {
            $data[] = $trans;
        }
        $report = self::getReport(self::getChildReport());
        if (!empty($report)) {
            $data[] = $report;
        }
        $adm = self::getAdministration(self::getChildAdministration());
        if (!empty($adm)) {
            $data[] = $adm;
        }



        $username = Yii::app()->user->name;

        if (in_array(000, $this->security_role)) {
            $data[] = array(
                'text' => 'Change Password',
                'id' => 'jun.PasswordWin',
                'leaf' => true
            );
        }

        if($this->security_roles_id == '2' || $this->security_roles_id == '5')
        {
            $bc = $_COOKIE['businessunitcode'];
            if (in_array(001, $this->security_role)) {
                $data[] = array(
                    'text' => "Logout ($username) - ($bc)",
                    'id' => 'logout',
                    'leaf' => true
                );
            }
        }
        else{
            if (in_array(001, $this->security_role)) {
                $data[] = array(
                    'text' => "Logout ($username)",
                    'id' => 'logout',
                    'leaf' => true
                );
            }
        }

        //Yii::app()->title = 'ads';

        return CJSON::encode($data);
    }
    public function getState($section)
    {
//        $state = 0;
//        if (in_array($section, $this->security_role)) {
//            $state++;
//            if (count($this->security_role) > 1) $state++;
//        }
//        return $state;
        return in_array($section, $this->security_role);
    }
}
