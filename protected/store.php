<?php
define('STOREID', 'JOG02');
define('STOREPASIEN', 'PASIEN');
define('STOREGLOBAL', 'GLOBAL');
define('STOREPACKAGE', 'PACKAGE');
define('STOREIDTARGET', 'IDN01');
define('BROADCAST', FALSE);
define('TOSTOREHO_PASIEN', FALSE);
define('TOSTOREHO_GLOBAL', FALSE);
define('DOWNLOAD_GLOBAL', TRUE);
define('TOSTOREHO_TRANS', FALSE);
define('MARKAFTERDOWNLOAD', TRUE);
define('ENABLESYNC', false);
define('HEADOFFICE', TRUE);
define('INTERVALSYNC', 600000);
define('ReportPath', 'D:\\ReportNWIS\\');
define('DAYSBEFOREDELSYNC', 30);
define('SYNCDOMAIN', '192.169.222.1');
define('NARSDOMAIN', '192.168.222.100'); //ip history
define('NARSPORT', 80);
define('NARSURL', 'http://history.naava:80/');
define('IMAPADDRESS', '{imap.gmail.com:993/imap/ssl/novalidate-cert}');
define('LIMIT_ROW_SYNC', 100);
define('LIMIT_ROW_SYNC_DOWN', 10);
define('THREAD', 1);
define('CMD_LOG', '/home/kacab/log/posng.log 2>&1');
define('MAX_THREAD', 5);
define('MAX_THREAD_HS', 25);
define('LIMIT_ROW_SYNC_DOWN_HS', 250);
define('TIME_HS_START', '20:00:00');
define('TIME_HS_STOP', '08:00:00');


define('PRINTER_RECEIPT', 'KASIR');
define('PRINTER_CARD', 'CARD');
define('VALID_CARD', 3);

define('WRITE_TO_FILE', false);

define('REMAINDER_INTERVAL', 1);
define('REMAINDER_START', 50);
define('REMAINDER_EMAIL', 'novebeta@gmail.com');

define('MGM_EVENT', '335f3eaa-da9b-11e4-b62d-00ffe52bbadb');
define('MSD_EVENT', '3a9ebc99-da9b-11e4-b62d-00ffe52bbadb');
define('KATEGORI_JASA', '1');
define('KATEGORI_PRODUK', '2');
define('TREATMENT_DOCTOR', '3');
define('KATEGORI_OTHER_INCOME', '4');

define('COA_VOUCHER', '611010');
define('COA_GRUP_BIAYA', '611000');
define('COA_GRUP_HUTANG', '211000');
define('COA_GRUP_PENDAPATAN', '79667535-9b2c-11e5-a972-00fff414d6a3');
define('COA_GRUP_BANK', '79667e1d-9b2c-11e5-a972-00fff414d6a3');
//define('COA_PERSEDIAAN', '114103');
define('COA_VAT', '213100');
define('COA_GRUP_KAS', '79667842-9b2c-11e5-a972-00fff414d6a3');
define('COA_BIAYA_ADM_BANK', '710004');
define('COA_VAT_BELI', '115600');
define('COA_SALES_RETURN', '440000');
define('COA_SALES_GRUP', '79667535-9b2c-11e5-a972-00fff414d6a3');
define('COA_PURCHASE_GRUP', '7942423f-9b2c-11e5-a972-00fff414d6a3');
define('COA_HPP_GRUP', '7942423f-9b2c-11e5-a972-00fff414d6a3');
define('COA_TRANDE_RECEIVABLES', '113000');
define('COA_FEE_CARD', '720003');
define('COA_LABA_RUGI', '330000');
define('COA_ROUNDING', '739997');
define('COA_ULPT', '214999');
define('COA_PERSEDIAAN_PUSAT', '221040');
define('COA_PERSEDIAAN_CABANG', '221040');
## IMPORT LAHA ##
define('COA_PIUTANG_CARD', '113060');
define('COA_SALES_DISK', '340020');
define('COA_SALES_OBAT', '310010');
define('COA_SALES_OBAT_PPN', '211322');
define('COA_SALES_APOTIK', '310030');
define('COA_SALES_APOTIK_PPN', '211322');
define('COA_SALES_JASA', '310020');
define('COA_PENDAPATAN_LAIN', '710004');
define('COA_PENDAPATAN_DIMUKA', '211802');
define('COA_HUTANG_PPH21', '211313');
define('COA_HUTANG_PPH22', '211315');
define('COA_HUTANG_PPH23', '211311');
define('COA_HUTANG_PPH42', '211312');


define('DIR_BACKUP','d:\\xampp\\htdocs\\posng\\backup\\');
define('MYSQLDUMP','/usr/bin/mysqldump');
//define('PHP_EXE','/usr/bin/php');
define('PHP_EXE','c:\\xampp\\php\\php.exe');
define('UPLOAD_NARS',TRUE);

define('ANTRIAN_SERVER_HOST', '192.168.202.2'); //ip komputer antrian
define('ANTRIAN_SERVER_PORT', 8001);
define('ANTRIAN_TIMEOUT', 30);
define('ANTRIAN_MARKET_MAX', 3);

define('COUNTER_MARKET', 'E');

/*
 * METHODE_BONUS :
 * 0 : Golongan (Naavagreen)
 * 1 : Persentase (Aishaderm)
 */
define('METHODE_BONUS', 0);
define('SOAP_PUSH','http://localhost/nsc_web_service/push/quote');
define('SOAP_SYNCRON','http://localhost/nsc_web_service/sync/quote');
define('SOAP_CUSTOMER', 'http://localhost/nsc_web_service/Transaksi/quote');
define('SOAP_SYNC', 'http://pos.service/sync/quote');
define('PUSH_PUSAT',TRUE);
define('PUSH_ALL_CABANG', TRUE);
define('STOREID_HO', 'SIN01'); //aisha : ANI01

define('RECEIPT_EMAIL',true);
define('ROUNDING',50); //malaysia 0.05
define('PRINT_NAMA_BARANG',FALSE);

/*
 * HITUNG_GL_HPP
 * Hitung jurnal hpp, pada sales dan retur sales
 * true : hitung jurnal
 * false: tidak hitung jurnal
 */
define('HITUNG_GL_HPP', true);
/*
 * Untuk Metode Bonus 1
 * Supaya Finance Pusat bisa melakukan transaksi sales
 * 9 ada di nscc_tipe_employee
 */
define('EMPLOYEE_FINANCE',9);
date_default_timezone_set('Asia/Jakarta');
setlocale(LC_TIME, 'INDONESIA');
/*
 * Untuk mendefinisikan nama PT dan Negara nya
 * Biasanya ada di Header Laha dkk
 * PT dan negara dipisah supaya lebih flexible penggunaan nya
 */
define('PT_NEGARA','NGI');
define('NEGARA','INDONESIA');

//edit service KMR,KMT,Jasa beauty
define('KMT_PERAWATAN', FALSE);
define('ONEPAGE_EDITSERVICE', FALSE);
define('HISTORY_NSC', FALSE);
//UNTUK MODIFIKASI YANG PRINTIL2AN
define('NATASHA_CUSTOM', TRUE);
define('GST', FALSE);


//untuk projection
define('BUSINESSUNIT', 'PNG');
define('BUSINESSUNITID', 'd72f3fcf-1616-11e8-a32f-201a069f4b32');

//untuk card sesuai PT
define('PT_CARD','PNG');
define('PRINT_STOK',TRUE);

define('COA_SALESPLAN', '10000');
define('COA_SALESPLAN_JASA', '10000');
define('COA_SALESPLAN_APOTIK', '10000');
define('COA_SALESPLAN_OBAT', '10000');

define('COA_SALESREALIZATION', '10000');
define('COA_SALESREALIZATION_JASA', '10000');
define('COA_SALESREALIZATION_APOTIK', '10000');
define('COA_SALESREALIZATION_OBAT', '10000');