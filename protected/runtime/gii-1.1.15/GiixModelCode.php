<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => 'nscc_',
  'modelPath' => 'application.modules.projection.models',
  'baseClass' => 'GxActiveRecord',
  'buildRelations' => true,
  'commentsAsLabels' => false,
);
