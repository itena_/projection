<?php
class m180428_061304_create_tracker extends CDbMigration {
	public function safeUp() {
		Yii::app()->db->createCommand( "CREATE TABLE `nscc_browser` (
`browser_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`session`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`ip_local`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`browserdata`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`fingerprint`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`useragent`  tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL ,
`browser`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`browserversion`  varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`browsermajorversion`  varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`ie`  tinyint(4) NOT NULL DEFAULT 0 ,
`chrome`  tinyint(4) NOT NULL DEFAULT 0 , 
`firefox`  tinyint(4) NOT NULL ,
`safari`  tinyint(4) NULL DEFAULT NULL ,
`opera`  tinyint(4) NULL DEFAULT NULL ,
`engine_`  varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`engineversion`  varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`os`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`osversion`  varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`windows`  tinyint(4) NULL DEFAULT NULL ,
`mac_os`  tinyint(4) NULL DEFAULT NULL ,
`linux`  tinyint(4) NULL DEFAULT NULL ,
`ubuntu`  tinyint(4) NULL DEFAULT NULL ,
`solaris`  tinyint(4) NULL DEFAULT NULL ,
`device`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`devicetype`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`devicevendor`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`cpu`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`mobile`  tinyint(4) NULL DEFAULT NULL ,
`mobilemajor`  tinyint(4) NULL DEFAULT NULL ,
`mobileandroid`  tinyint(4) NULL DEFAULT NULL ,
`mobileopera`  tinyint(4) NULL DEFAULT NULL ,
`mobilewindows`  tinyint(4) NULL DEFAULT NULL ,
`mobileblackberry`  tinyint(4) NULL DEFAULT NULL ,
`mobileios`  tinyint(4) NULL DEFAULT NULL ,
`iphone`  tinyint(4) NULL DEFAULT NULL ,
`ipad`  tinyint(4) NULL DEFAULT NULL ,
`ipod`  tinyint(4) NULL DEFAULT NULL ,
`screenprint`  tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL ,
`colordepth`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`currentresolution`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`availableresolution`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`devicexdpi`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`deviceydpi`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`plugins`  tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL ,
`java`  tinyint(4) NULL DEFAULT NULL ,
`javaversion`  tinyint(4) NULL DEFAULT NULL ,
`flash`  tinyint(4) NULL DEFAULT NULL ,
`flashversion`  varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`silverlight`  tinyint(4) NULL DEFAULT NULL ,
`silverlightversion`  varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`mimetypes`  tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL ,
`ismimetypes`  tinyint(4) NULL DEFAULT NULL ,
`font`  tinyint(4) NULL DEFAULT NULL ,
`fonts`  tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL ,
`localstorage`  tinyint(4) NULL DEFAULT NULL ,
`sessionstorage`  tinyint(4) NULL DEFAULT NULL ,
`cookie`  tinyint(4) NULL DEFAULT NULL ,
`timezone`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`language`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`systemlanguage`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`canvas`  tinyint(4) NULL DEFAULT NULL ,
`tdate`  datetime NOT NULL ,
`user_id`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`ip_public`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`browser_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
ROW_FORMAT=Compact
;" )->execute();
		Yii::app()->db->createCommand( "CREATE TABLE `nscc_log_trans` (
`log_trans_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`session`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`user_id`  varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' ,
`name`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`username`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`tdate`  datetime NOT NULL ,
`type`  smallint(6) NULL DEFAULT 0 ,
`type_no`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'aslinya bigint16' ,
`action`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`note`  varchar(600) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`browser_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`log_trans_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
ROW_FORMAT=Compact
;" )->execute();
		Yii::app()->db->createCommand( "ALTER TABLE `nscc_users` ADD COLUMN `session`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `up`;

" )->execute();
	}
	public function safeDown() {
		Yii::app()->db->createCommand( "
		ALTER TABLE `nscc_users` DROP COLUMN `session`;
DROP TABLE `nscc_browser`;
DROP TABLE `nscc_log_trans`;
		" )->execute();
	}
	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}