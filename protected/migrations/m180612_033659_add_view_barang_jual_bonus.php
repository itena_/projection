<?php

class m180612_033659_add_view_barang_jual_bonus extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand('		
		CREATE ALGORITHM=UNDEFINED 
		DEFINER=`root`@`127.0.0.1` 
		SQL SECURITY DEFINER 
		VIEW `nscc_barang_jual_bonus`AS 
		SELECT bj.* 
		, b.kode_barang
		, bn.bonus_name
		from 
			nscc_bonus_jual bj
		LEFT JOIN nscc_barang_jual b on b.barang_id = bj.barang_id
		LEFT JOIN nscc_bonus_name bn on bn.bonus_name_id = bj.bonus_name_id ;')->execute();
	}

	public function down()
	{
		echo "m180612_033659_add_view_barang_jual_bonus does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}