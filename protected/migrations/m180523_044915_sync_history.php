<?php

class m180523_044915_sync_history extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand('CREATE TABLE IF NOT EXISTS `nscc_sync_history` (
                                      `history_id` varchar(50) NOT NULL,
                                      `sync_id` varchar(50) NOT NULL,
                                      `store` varchar(50) NOT NULL,
                                      `username` varchar(50) NOT NULL,
                                      `type` varchar(50) NOT NULL,
                                      `tdate` datetime NOT NULL,
                                      `startdate` date NOT NULL,
                                      `enddate` date NOT NULL,
                                      `action` varchar(50) NOT NULL,
                                      `note` text NOT NULL,
                                      PRIMARY KEY (`history_id`)
                                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;')->execute();
	}

	public function down()
	{
		echo "m180523_044915_sync_history does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}