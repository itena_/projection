<?php

/**
 * This is the model class for table "{{apoteker}}".
 *
 * The followings are the available columns in table '{{apoteker}}':
 * @property string $apoteker_id
 * @property string $nama_apoteker
 * @property string $gol_id
 * @property string $kode_apoteker
 * @property integer $active
 * @property string $store
 * @property integer $up
 * @property string $tipe
 */
class HApoteker extends PusatActiveRecord
{
	public function getDbConnection()
    {
        return self::getPusatDbConnection();
    }
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{apoteker}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('apoteker_id, nama_apoteker, gol_id, kode_apoteker, store', 'required'),
			array('active, up', 'numerical', 'integerOnly'=>true),
			array('apoteker_id', 'length', 'max'=>50),
			array('nama_apoteker', 'length', 'max'=>100),
			array('gol_id, tipe', 'length', 'max'=>36),
			array('kode_apoteker, store', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('apoteker_id, nama_apoteker, gol_id, kode_apoteker, active, store, up, tipe', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'apoteker_id' => 'Apoteker',
			'nama_apoteker' => 'Nama Apoteker',
			'gol_id' => 'Gol',
			'kode_apoteker' => 'Kode Apoteker',
			'active' => 'Active',
			'store' => 'Store',
			'up' => 'Up',
			'tipe' => 'Tipe',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('apoteker_id',$this->apoteker_id,true);
		$criteria->compare('nama_apoteker',$this->nama_apoteker,true);
		$criteria->compare('gol_id',$this->gol_id,true);
		$criteria->compare('kode_apoteker',$this->kode_apoteker,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('store',$this->store,true);
		$criteria->compare('up',$this->up);
		$criteria->compare('tipe',$this->tipe,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HApoteker the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
