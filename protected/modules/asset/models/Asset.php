<?php
Yii::import('application.modules.asset.models._base.BaseAsset');

class Asset extends BaseAsset
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->asset_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->asset_id = $uuid;
        }
        return parent::beforeValidate();
    }

   protected function afterSave()
    {
        parent::afterSave();
        $branch = $this->branch;
        $golongan = $this->assetGroup->golongan;
        $docref = 'NG'.$branch.'/'.$golongan.'/'.date('m').'/'.date('y');

        //file_put_contents('tes.txt',$docref);
        /*$golongan = $this->asset_group_id;
        $docref = $branch.$golongan.date(Y).'001';

        file_put_contents('docref.txt',$docref);*/

    }

    /*public function getDocrefAsset($store,$gol,$no)
    {
        $docref = "NGASSET".$store.$gol.$no;
        return $docref;
    }*/
}