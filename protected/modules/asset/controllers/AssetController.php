<?php

class AssetController extends GxController {

    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
        return;

        if (isset($_POST) && !empty($_POST)) {

            $is_new = $_POST['mode'] == 0;
            $as_id = $_POST['id'];

            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();

            try
            {
                $model = $is_new ? new Asset : $this->loadModel($as_id, "Asset");
                $store = Store::model()->findByAttributes(array('store_kode' => STOREID));

                /*if ($is_new) {
                    $ref = new Reference();
                } else {
                    AssetDetail::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $as_id));
                    AssetPeriode::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $as_id));
                }*/

                foreach($_POST as $k=>$v){
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Asset'][$k] = $v;
                }

                $_POST['Asset']['status'] = 1;
                $branch = $_POST['Asset']['branch'];
                $grupid = $_POST['Asset']['asset_group_id'];
                $qty = $_POST['Asset']['qty'];
                $group = $this->getGolongan($grupid);
                $last = $this->getLastRow($branch) + 1;


                if(PT_NEGARA=='NGI')
                {
                    $cabang = 'NG'.$store->store_kode;
                }
                else
                {
                    $cabang = $store->store_kode;
                }
                $area = str_pad($store->wilayah_id, 2, '0', STR_PAD_LEFT);
                $urutan = str_pad($store->id_cabang, 2, '0', STR_PAD_LEFT);

                $golongan = $group[0]; //$this->getGolongan($grupid);
                $tariff = $group[1];
                $period = $group[2];
                $desc = $group[3];

                //$docref = $branch.'/'.$golongan.'/'.$last.'/'.date('m').'/'.date('y');

                $model->created_at = new CDbExpression('NOW()');
                $model->updated_at = new CDbExpression('NOW()');

                $year = date('Y', strtotime($_POST['Asset']['date_acquisition']));


                if ($is_new) {

                    $docref = $cabang.'AST'.$year.$last;
                } else {

                    $docref = $model->doc_ref;
                    $ati = $model->ati;

                    AssetDetail::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $model->asset_id));
                    AssetPeriode::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $model->asset_id));

                    U::delete_stock_moves_all(ASSET, $model->asset_id);
                }

                $barang = Barang::model()->findByPk($_POST['Asset']['barang_id']);
                $_POST['Asset']['doc_ref'] = $docref;
                //$_POST['Asset']['ati'] = $ati.'/'.$last;
                $_POST['Asset']['asset_name'] = $barang->kode_barang;
                $_POST['Asset']['new_price_acquisition'] = $_POST['Asset']['new_price_acquisition'] ? $_POST['Asset']['new_price_acquisition'] : "0";

                $ati = $area.$cabang.$urutan.'/'.$golongan;
                $model->attributes = $_POST['Asset'];
                $msg = "Data gagal disimpan.";

                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Asset')) . CHtml::errorSummary($model));
                }
                else
                {
                    for($i=0;$i<$qty;$i++)
                    {
                        $modeldetail = new AssetDetail;

                        $lastDetail = $this->getLastRowDetail($branch) + 1;

                        $docrefdetail = $docref.'/'.$lastDetail;
                        $modeldetail->ati = $ati.'/'.$lastDetail;
                        $modeldetail->asset_id = $model->asset_id;
                        $modeldetail->asset_trans_branch = $branch;
                        $modeldetail->asset_group_id = $grupid;
                        $modeldetail->barang_id = $model->barang_id;
                        $modeldetail->docref_other = $model->doc_ref;
                        $modeldetail->docref = $docrefdetail;
                        $modeldetail->asset_trans_name = $model->asset_name;
                        $modeldetail->asset_trans_date = $model->date_acquisition;
                        $modeldetail->asset_trans_price = $model->price_acquisition;
                        $modeldetail->asset_trans_new_price = $model->new_price_acquisition ;
                        $modeldetail->description = $model->description;
                        $modeldetail->class = $golongan;
                        $modeldetail->tariff = $tariff;
                        $modeldetail->period = $period;
                        $modeldetail->status = 1;

                        $penyusutan = $modeldetail->asset_trans_price * ($tariff/100) / $period;
                        $penyusutantahun = $penyusutan * $period;

                        $modeldetail->penyusutanperbulan = $penyusutan;
                        $modeldetail->penyusutanpertahun = $penyusutantahun;

                        $modeldetail->created_at = $model->created_at;
                        $modeldetail->updated_at = $model->updated_at;

                        if (!$modeldetail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Asset Detail')) . CHtml::errorSummary($modeldetail));
                        }
                        else
                        {
                            //mengurangi stok barang perlengkapan
                            U::add_stock_moves_all(
                                null,
                                ASSET, $model->asset_id,
                                $model->date_acquisition,
                                $model->barang_id,
                                -1,
                                $model->doc_ref,
                                $model->price_acquisition,
                                $model->branch
                            );



                            $lastdeprisiasi = 0;
                            $lastbalance = 0;

                            for($j=0;$j<$period;$j++)
                            {
                                $modelperiode = new AssetPeriode;

                                $modelperiode->docref = $modeldetail->docref;
                                $modelperiode->docref_other = $modeldetail->docref_other;
                                $modelperiode->ati = $modeldetail->ati;
                                $modelperiode->asset_trans_id = $modeldetail->asset_trans_id;
                                $modelperiode->asset_id = $modeldetail->asset_id;
                                $modelperiode->asset_group_id = $modeldetail->asset_group_id;
                                $modelperiode->barang_id = $modeldetail->barang_id;
                                $modelperiode->asset_trans_date = $modeldetail->asset_trans_date;
                                $modelperiode->asset_trans_price = $modeldetail->asset_trans_price;
                                $modelperiode->asset_trans_new_price = $modeldetail->asset_trans_new_price;
                                $modelperiode->asset_trans_name = $modeldetail->asset_trans_name;
                                $modelperiode->asset_trans_branch = $modeldetail->asset_trans_branch;
                                $modelperiode->description = $modeldetail->description;

                                $modelperiode->period = $modeldetail->period;
                                $modelperiode->class = $modeldetail->class;
                                $modelperiode->tariff = $modeldetail->tariff;


                                $penyusutan = $modelperiode->asset_trans_price * ($modelperiode->tariff/100) / $modelperiode->period;
                                $modelperiode->penyusutanperbulan = $penyusutan;
                                $lastdeprisiasi = $modelperiode->penyusutanperbulan;

                                $penyusutantahun = $penyusutan * $modelperiode->period;
                                $modelperiode->penyusutanpertahun = $penyusutantahun;

                                if($lastbalance == 0  || $lastdeprisiasi ==0)
                                {
                                    $modelperiode->balance = $modelperiode->asset_trans_price - $penyusutan;
                                    $lastbalance = $modelperiode->balance;
                                }
                                else
                                {
                                    $modelperiode->balance = $lastbalance - $lastdeprisiasi;
                                    $lastbalance = $modelperiode->balance;
                                }

                                //counter bulan
                                $x = $j+1;
                                $d = strtotime("+$x months",strtotime($modeldetail->asset_trans_date));
                                $newdate = date('Y-m-t',$d);
                                /////////////////////////////////////////////

                                $modelperiode->tglpenyusutan = $newdate;

                                $modelperiode->startdate = $modelperiode->asset_trans_date;
                                $modelperiode->enddate = $newdate = date('Y-m-t',strtotime("+$modelperiode->period months",strtotime($modeldetail->asset_trans_date)));

                                $modelperiode->status = $modeldetail->status;

                                $modelperiode->created_at = $modeldetail->created_at;
                                $modelperiode->updated_at = $modeldetail->updated_at;

                                if (!$modelperiode->save()) {
                                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($modelperiode));
                                }
                            }
                        }
                    }
                    $status = true;
                    $msg = "Data berhasil di simpan dengan id " . $model->asset_id;
                }
                /*else
                {
                    $msg .= " ".implode(", ", $model->getErrors());
                    $status = false;
                }*/

                $transaction->commit();
                StockMovesPerlengkapan::push($model->asset_id);

                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg));
                Yii::app()->end();
            }
            catch (Exception $ex)
            {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
                echo CJSON::encode(array(
                    'success' => 'failed',
                    'msg' => $msg
                ));
            }
        }
    }

    public function actionDataPeriode($docref)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('doc_ref LIKE :doc_ref');
        $criteria->params[':docref'] = '%' . $docref . '%';
        $criteria->order = "tglpenyusutan ASC";
        $model = AssetPeriode::model()->findAll($criteria);
        $total = AssetPeriode::model()->count($criteria);
        $this->renderJson($model, $total);

        /*$criteria = new CDbCriteria;
        $criteria->compare('branch',$store);
        $model = Asset::model()->findAll($criteria);
        $count = count($model);

        return $count;*/
    }

    public function actionPrintAsset()
    {
        $tdate = $_POST['date'];
        $date = date('Y-m-d', strtotime($tdate));
        $data = array();

        $criteria = new CDbCriteria;
        $criteria->compare('asset_trans_date',$date);
        $criteria->compare('asset_trans_branch',STOREID);

        $criteria->order = "asset_trans_name, ati ASC ";
        $model = AssetDetail::model()->findAll($criteria);

        //$model = AssetDetail::model()->findByAttributes(array('asset_trans_date' => $date, 'asset_trans_branch' => STOREID));

        if ($model != null) {

            if($model != null){
                foreach($model as $val){
                    $attribDetil = $val->getAttributes();
                    //$data[] = json_encode($attribDetil);
                    $data[] = $attribDetil;
                }
            }

            echo CJSON::encode(array(
                'success' => 'success',
                'data' => $data,
                'msg' => 'success'
            ));
        } else {
            echo CJSON::encode(array(
                'success' => false,
                'msg' => 'data tidak ditemukan'
            ));
        }
    }



    public function getLastRow($store)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('branch',$store);
        $model = Asset::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

    public function getLastRowDetail($store)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('asset_trans_branch',$store);
        $model = AssetDetail::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

    public function getGolongan($id)
    {
        $query = "select golongan as gol, tariff, period, nscc_asset_group.`desc` from nscc_asset_group
                     where asset_group_id = '".$id."' limit 1";

        $list= Yii::app()->db->createCommand($query)->queryAll();

        $rs=array();
        foreach($list as $item)
        {
            $rs[0]=$item['gol'];
            $rs[1]=$item['tariff'];
            $rs[2]=$item['period'];
            $rs[3]=$item['desc'];
        }
        return $rs;
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Asset');

        if (isset($_POST) && !empty($_POST))
        {
            foreach($_POST as $k=>$v){
                if (is_angka($v)) $v = get_number($v);
                $_POST['Asset'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Asset'];

            if ($model->save())
            {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->asset_id;
            }
            else
            {
                $msg .= " ".implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest)
            {
                echo CJSON::encode(array(
                'success'=>$status,
                'msg'=>$msg
                ));
                Yii::app()->end();
            }
            else
            {
                $this->redirect(array('view', 'id' => $model->asset_id));
            }
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try
            {
                $this->loadModel($id, 'Asset')->delete();
                AssetDetail::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $id));
                AssetPeriode::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $id));
                //$this->loadModel($id, 'AssetDetail')->delete();
            }
            catch (Exception $ex)
            {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
            Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

public function actionIndex() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}

$criteria = new CDbCriteria();

if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
(isset($_POST['limit']) && isset($_POST['start']))) {
$criteria->limit = $limit;
$criteria->offset = $start;
}


$model = Asset::model()->findAll($criteria);
$total = Asset::model()->count($criteria);

$this->renderJson($model, $total);

}

}