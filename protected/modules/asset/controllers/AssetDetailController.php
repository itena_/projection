<?php

class AssetDetailController extends GxController {

    public function actionCreate() {
$model = new AssetDetail;
if (!Yii::app()->request->isAjaxRequest)
return;
if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['AssetDetail'][$k] = $v;
}
$model->attributes = $_POST['AssetDetail'];
$msg = "Data gagal disimpan.";

    if ($model->save()) {
$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->asset_trans_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg));
Yii::app()->end();

}

}

    public function actionUpdate($id) {
$model = $this->loadModel($id, 'AssetDetail');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['AssetDetail'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['AssetDetail'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->asset_trans_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->asset_trans_id));
}
}
}

    public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'AssetDetail')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}

    public function actionIndex() {
        if(isset($_POST['limit'])) {
        $limit = $_POST['limit'];
        } else {
        $limit = 20;
        }

        if(isset($_POST['start'])){
        $start = $_POST['start'];

        } else {
        $start = 0;
        }

        $param = array();
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
        (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['asset_trans_date'])) {
            $asset_trans_date = $_POST['asset_trans_date'];
            $criteria->addCondition('asset_trans_date = date(:asset_trans_date)');
            $param[':asset_trans_date'] = "$asset_trans_date";
        }
        if (isset($_POST['docref'])) {
            $docref = $_POST['docref'];
            $criteria->addCondition('docref like :docref');
            $param[':docref'] = "%$docref%";
        }
        if (isset($_POST['asset_trans_branch'])) {
            $asset_trans_branch = $_POST['asset_trans_branch'];
            $criteria->addCondition('asset_trans_branch like :asset_trans_branch');
            $param[':asset_trans_branch'] = "%$asset_trans_branch%";
        }
        if (isset($_POST['asset_trans_name'])) {
            $asset_trans_name = $_POST['asset_trans_name'];
            $criteria->addCondition('asset_trans_name like :asset_trans_name');
            $param[':asset_trans_name'] = "%$asset_trans_name%";
        }
        if (isset($_POST['description'])) {
            $description = $_POST['description'];
            $criteria->addCondition('description like :description');
            $param[':description'] = "%$description%";
        }
        if (isset($_POST['period'])) {
            $period = $_POST['period'];
            $criteria->addCondition('period like :period');
            $param[':period'] = "%$period%";
        }
        if (isset($_POST['tariff'])) {
            $tariff = $_POST['period'];
            $criteria->addCondition('tariff like :tariff');
            $param[':tariff'] = "%$tariff%";
        }
        if (isset($_POST['class'])) {
            $class = $_POST['class'];
            $criteria->addCondition('class like :class');
            $param[':class'] = "%$class%";
        }

        if(!HEADOFFICE)
        {
            $criteria->addCondition('asset_trans_branch = :asset_trans_branch');
            $param[':asset_trans_branch'] = STOREID;
        }


        $criteria->order = 'docref';
        $criteria->params = $param;
        $model = AssetDetail::model()->findAll($criteria);
        $total = AssetDetail::model()->count($criteria);

        $this->renderJson($model, $total);

    }



}