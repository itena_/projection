<?php

class PurchaseCoaController extends GxController {

    public function actionCreate() {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
//            $model = new PurchaseCoa;
            $status = false;
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new PurchaseCoa;
                $ref = new Reference();
                $docref = $ref->get_next_reference(PURCHASE_COA);

                foreach ($_POST as $k => $v) {
                    if ($k == 'detil')
                        continue;
                    if (is_angka($v))
                        $v = get_number($v);
                    $_POST['PurchaseCoa'][$k] = $v;
                }
                $_POST['PurchaseCoa']['doc_ref'] = $docref;
                $model->attributes = $_POST['PurchaseCoa'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Purchase COA')) . CHtml::errorSummary($model));
                }
                /*
                 * DETIL
                 */
                foreach ($detils as $detil) {
                    $item_details = new PurchaseCoaDetail();
                    /*
                     * Debit
                     */
                    $_POST['PurchaseCoaDetail']['vat'] = $detil['vat'];
                    $_POST['PurchaseCoaDetail']['dpp'] = $detil['dpp'];
                    $_POST['PurchaseCoaDetail']['dppph21'] = $detil['dppph21'];
                    $_POST['PurchaseCoaDetail']['dppph23'] = $detil['dppph23'];
                    $_POST['PurchaseCoaDetail']['dppph25'] = $detil['dppph25'];
                    $_POST['PurchaseCoaDetail']['dppph42'] = $detil['dppph42'];
                    $_POST['PurchaseCoaDetail']['dppph26'] = $detil['dppph26'];
                    $_POST['PurchaseCoaDetail']['dppph22'] = $detil['dppph22'];
                    $_POST['PurchaseCoaDetail']['beban_ppn'] = $detil['beban_ppn'];
                    $_POST['PurchaseCoaDetail']['beban_pph'] = $detil['beban_pph'];
                    $_POST['PurchaseCoaDetail']['other_tax'] = $detil['other_tax'];
                    $_POST['PurchaseCoaDetail']['biaya_materai_ekspedisi'] = $detil['biaya_materai_ekspedisi'];
                    $_POST['PurchaseCoaDetail']['forex_loss'] = $detil['forex_loss'];
                    /*
                     * Kredit
                     */
                    $_POST['PurchaseCoaDetail']['pph_21'] = $detil['pph_21'];
                    $_POST['PurchaseCoaDetail']['pph23'] = $detil['pph23'];
                    $_POST['PurchaseCoaDetail']['pph42'] = $detil['pph42'];
                    $_POST['PurchaseCoaDetail']['pph25_29'] = $detil['pph25_29'];
                    $_POST['PurchaseCoaDetail']['round'] = $detil['round'];
                    $_POST['PurchaseCoaDetail']['forex_gain'] = $detil['forex_gain'];
                    $_POST['PurchaseCoaDetail']['total'] = $detil['total'];
                    $_POST['PurchaseCoaDetail']['total'] = $detil['total'];
                    $_POST['PurchaseCoaDetail']['store_kode'] = $detil['store_kode'];
                    
                    $_POST['PurchaseCoaDetail']['purchase_coa_id'] = $model->purchase_coa_id;
                    
                    $item_details->attributes = $_POST['PurchaseCoaDetail'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Purchase COA Detail')) . CHtml::errorSummary($item_details));
                    }
                }
//                $auw->save();
                $ref->save(PURCHASE_COA, $model->purchase_coa_id, $docref);
//                    $ayam->save();
                $transaction->commit();
                $msg = "Sukses disimpan";
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionPosting() {
        $status = true;
        $msg = 'taraaaa';
        $id = $_POST['id'];
        $model = $this->loadModel($id, 'PurchaseCoa');
        $model->p = 1;
        $model->save();
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }

    public function actionIndex() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = PurchaseCoa::model()->findAll($criteria);
        $total = PurchaseCoa::model()->count($criteria);
        $this->renderJson($model, $total);
    }

}
