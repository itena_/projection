<?php
Yii::import('application.modules.projection.models._base.BaseBudgetView');

class BudgetView extends BaseBudgetView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->budget_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->budget_id = $uuid;
        }
        return parent::beforeValidate();
    }
}