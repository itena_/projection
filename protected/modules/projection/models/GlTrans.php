<?php
Yii::import('application.modules.projection.models._base.BaseGlTrans');

class GlTrans extends BaseGlTrans
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->gltrans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->gltrans_id = $uuid;
        }
        return parent::beforeValidate();
    }
}