<?php
Yii::import('application.modules.projection.models._base.BaseAccountView');

class AccountView extends BaseAccountView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->account_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->account_id = $uuid;
        }
        return parent::beforeValidate();
    }
}