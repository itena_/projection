/*
Navicat MariaDB Data Transfer

Source Server         : Maria
Source Server Version : 100206
Source Host           : localhost:3366
Source Database       : posng_cabang

Target Server Type    : MariaDB
Target Server Version : 100206
File Encoding         : 65001

Date: 2017-08-23 14:42:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for nscc_restrict_date
-- ----------------------------
DROP TABLE IF EXISTS `nscc_restrict_date`;
CREATE TABLE `nscc_restrict_date` (
  `res_date_id` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `author` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `updater` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `up` tinyint(1) NOT NULL,
  PRIMARY KEY (`res_date_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
