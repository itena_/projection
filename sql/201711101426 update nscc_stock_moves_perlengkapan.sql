﻿-- 
-- Script was generated by Devart dbForge Studio for MySQL, Version 7.2.78.0
-- Product Home Page: http://www.devart.com/dbforge/mysql/studio
-- Script date 10/11/2017 14:27:32
-- Source server version: 5.6.16
-- Source connection string: User Id=root;Host=localhost;Character Set=utf8
-- Target server version: 10.1.23
-- Target connection string: User Id=root;Host=10.0.0.113;Database=posng;Protocol=Ssh;Character Set=utf8;SSH Host=172.169.222.151;SSH Port=22;SSH User=it;SSH Authentication Type=Password
-- Run this script against posng to synchronize it with posng
-- Please backup your target database before running this script
-- 

--
-- Disable foreign keys
--
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

SET NAMES 'utf8';

#USE posng;


--
-- Alter table "nscc_stock_moves_perlengkapan"
--
ALTER TABLE nscc_stock_moves_perlengkapan
  DROP FOREIGN KEY nscc_stock_moves_perlengkapan_ibfk_1,
  CHANGE COLUMN barang_perlengkapan_id barang_id VARCHAR(36) NOT NULL,
  ADD COLUMN visible TINYINT(1) NOT NULL DEFAULT 1 AFTER up;

ALTER TABLE nscc_stock_moves_perlengkapan
  ADD INDEX nscc_stock_moves_perlengkapan_ibfk_1_idx (barang_id);

ALTER TABLE nscc_stock_moves_perlengkapan
  ADD CONSTRAINT nscc_stock_moves_perlengkapan_ibfk_1 FOREIGN KEY (barang_id)
    REFERENCES nscc_barang(barang_id) ON DELETE RESTRICT ON UPDATE CASCADE;

DELIMITER $$

--
-- Alter trigger "nscc_stock_moves_perlengkapan_before_update"
--
DROP TRIGGER IF EXISTS nscc_stock_moves_perlengkapan_before_update$$
CREATE TRIGGER nscc_stock_moves_perlengkapan_before_update
	BEFORE UPDATE
	ON nscc_stock_moves_perlengkapan
	FOR EACH ROW
BEGIN
    IF !(OLD.type_no <=> NEW.type_no AND OLD.trans_no <=> NEW.trans_no
         AND OLD.tran_date <=> NEW.tran_date AND OLD.price <=> NEW.price
         AND OLD.reference <=> NEW.reference AND OLD.qty <=> NEW.qty
         AND OLD.barang_id <=> NEW.barang_id AND OLD.store <=> NEW.store
         AND OLD.tdate <=> NEW.tdate AND OLD.id_user <=> NEW.id_user)
    THEN
      SET NEW.up = 0;
    END IF;
  END
$$

DELIMITER ;

--
-- Enable foreign keys
--
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;