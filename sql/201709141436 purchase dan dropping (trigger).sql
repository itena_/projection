
      /*
      * Script created by Quest Schema Compare at 14/09/2017 13:47:32.
      * Please back up your database before running this script.
      *
      * Synchronizing objects from posng to posng.
      */
    USE `posng`;

-- Drop foreign keys, referenced to nscc_transfer_item
ALTER TABLE `posng`.`nscc_pelunasan_utang_detil` DROP FOREIGN KEY `nscc_pelunasan_utang_detil_ibfk_2`;

ALTER TABLE `posng`.`nscc_dropping_details` DROP FOREIGN KEY `fk_nscc_dropping_details`;

ALTER TABLE `posng`.`nscc_order_dropping` DROP FOREIGN KEY `nscc_order_dropping_fk0`;

ALTER TABLE `posng`.`nscc_order_dropping` DROP FOREIGN KEY `nscc_order_dropping_fk1`;

ALTER TABLE `posng`.`nscc_order_dropping_details` DROP FOREIGN KEY `fk_nscc_order_dropping_details`;

ALTER TABLE `posng`.`nscc_po` DROP FOREIGN KEY `fk_nscc_po`;

ALTER TABLE `posng`.`nscc_po` DROP FOREIGN KEY `nscc_po_fk1`;

ALTER TABLE `posng`.`nscc_po_details` DROP FOREIGN KEY `fk_nscc_po_details`;

ALTER TABLE `posng`.`nscc_receive_dropping` DROP FOREIGN KEY `fk_nscc_receive_dropping`;

ALTER TABLE `posng`.`nscc_receive_dropping_details` DROP FOREIGN KEY `fk_nscc_dropping_details_0`;

ALTER TABLE `posng`.`nscc_terima_barang` DROP FOREIGN KEY `fk_nscc_terima_barang`;

ALTER TABLE `posng`.`nscc_terima_barang_details` DROP FOREIGN KEY `fk_nscc_terima_barang_details`;

ALTER TABLE `posng`.`nscc_transfer_item` DROP FOREIGN KEY `fk_nscc_transfer_item_0`;

ALTER TABLE `posng`.`nscc_transfer_item` DROP FOREIGN KEY `nscc_transfer_item_ibfk_1`;

ALTER TABLE `posng`.`nscc_transfer_item_details` DROP FOREIGN KEY `nscc_transfer_item_details_ibfk_1`;

ALTER TABLE `posng`.`nscc_transfer_item_details` DROP FOREIGN KEY `nscc_transfer_item_details_ibfk_2`;

/* Header line. Object: nscc_dropping_details. Script date: 14/09/2017 13:47:32. */
ALTER TABLE `posng`.`nscc_dropping_details` DROP FOREIGN KEY `fk_nscc_dropping_details`;

/* Header line. Object: nscc_order_dropping. Script date: 14/09/2017 13:47:32. */
ALTER TABLE `posng`.`nscc_order_dropping` DROP FOREIGN KEY `nscc_order_dropping_fk0`;

ALTER TABLE `posng`.`nscc_order_dropping` DROP FOREIGN KEY `nscc_order_dropping_fk1`;

/* Header line. Object: nscc_order_dropping_details. Script date: 14/09/2017 13:47:32. */
ALTER TABLE `posng`.`nscc_order_dropping_details` DROP FOREIGN KEY `fk_nscc_order_dropping_details`;

/* Header line. Object: nscc_po. Script date: 14/09/2017 13:47:32. */
ALTER TABLE `posng`.`nscc_po` DROP FOREIGN KEY `fk_nscc_po`;

ALTER TABLE `posng`.`nscc_po` DROP FOREIGN KEY `nscc_po_fk1`;

/* Header line. Object: nscc_po_details. Script date: 14/09/2017 13:47:32. */
ALTER TABLE `posng`.`nscc_po_details` DROP FOREIGN KEY `fk_nscc_po_details`;

/* Header line. Object: nscc_receive_dropping. Script date: 14/09/2017 13:47:32. */
ALTER TABLE `posng`.`nscc_receive_dropping` DROP FOREIGN KEY `fk_nscc_receive_dropping`;

/* Header line. Object: nscc_receive_dropping_details. Script date: 14/09/2017 13:47:32. */
ALTER TABLE `posng`.`nscc_receive_dropping_details` DROP FOREIGN KEY `fk_nscc_dropping_details_0`;

/* Header line. Object: nscc_terima_barang. Script date: 14/09/2017 13:47:32. */
ALTER TABLE `posng`.`nscc_terima_barang` DROP FOREIGN KEY `fk_nscc_terima_barang`;

/* Header line. Object: nscc_terima_barang_details. Script date: 14/09/2017 13:47:32. */
ALTER TABLE `posng`.`nscc_terima_barang_details` DROP FOREIGN KEY `fk_nscc_terima_barang_details`;

/* Header line. Object: nscc_transfer_item. Script date: 14/09/2017 13:47:32. */
ALTER TABLE `posng`.`nscc_transfer_item` DROP FOREIGN KEY `fk_nscc_transfer_item_0`;

ALTER TABLE `posng`.`nscc_transfer_item` DROP FOREIGN KEY `nscc_transfer_item_ibfk_1`;

/* Header line. Object: nscc_transfer_item_details. Script date: 14/09/2017 13:47:32. */
ALTER TABLE `posng`.`nscc_transfer_item_details` DROP FOREIGN KEY `nscc_transfer_item_details_ibfk_1`;

ALTER TABLE `posng`.`nscc_transfer_item_details` DROP FOREIGN KEY `nscc_transfer_item_details_ibfk_2`;

/* Header line. Object: nscc_dropping_BEFORE_INSERT. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_dropping_BEFORE_INSERT`
  BEFORE INSERT ON `nscc_dropping`
  FOR EACH ROWBEGIN
IF NEW.dropping_id IS NULL OR LENGTH(NEW.dropping_id) = 0 THEN
  SET NEW.dropping_id = UUID();
  END IF;
END;

/* Header line. Object: nscc_dropping_BEFORE_UPDATE. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_dropping_BEFORE_UPDATE`
  BEFORE UPDATE ON `nscc_dropping`
  FOR EACH ROWBEGIN
	IF !(OLD.tgl  <=> NEW.tgl 
		AND OLD.doc_ref <=> NEW.doc_ref
		AND OLD.note <=> NEW.note
		AND OLD.tdate <=> NEW.tdate
		AND OLD.user_id <=> NEW.user_id
		AND OLD.type_ <=> NEW.type_
		AND OLD.store <=> NEW.store
		AND OLD.order_dropping_id <=> NEW.order_dropping_id
		AND OLD.store_penerima <=> NEW.store_penerima
		AND OLD.approved <=> NEW.approved
		AND OLD.approved_user_id <=> NEW.approved_user_id
		AND OLD.approved_date <=> NEW.approved_date
		AND OLD.lunas <=> NEW.lunas
	) THEN
		SET NEW.up = 0;
	END IF;
END;

/* Header line. Object: nscc_dropping_details_BEFORE_INSERT. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_dropping_details_BEFORE_INSERT`
  BEFORE INSERT ON `nscc_dropping_details`
  FOR EACH ROWBEGIN
IF NEW.dropping_detail_id IS NULL OR LENGTH(NEW.dropping_detail_id) = 0 THEN
  SET NEW.dropping_detail_id = UUID();
  END IF;
END;

/* Header line. Object: nscc_dropping_details_BEFORE_UPDATE. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_dropping_details_BEFORE_UPDATE`
  BEFORE UPDATE ON `nscc_dropping_details`
  FOR EACH ROWBEGIN
IF !(OLD.qty  <=> NEW.qty AND OLD.barang_id <=> NEW.barang_id 
	AND OLD.visible <=> NEW.visible AND OLD.dropping_id <=> NEW.dropping_id
) THEN 
		UPDATE nscc_dropping t SET t.up = 0 WHERE t.dropping_id = NEW.dropping_id;
		UPDATE nscc_dropping t SET t.up = 0 WHERE t.dropping_id = OLD.dropping_id;
  END IF;
END;

/* Header line. Object: nscc_order_dropping_BEFORE_INSERT. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_order_dropping_BEFORE_INSERT`
  BEFORE INSERT ON `nscc_order_dropping`
  FOR EACH ROWBEGIN
  IF NEW.order_dropping_id IS NULL OR LENGTH(NEW.order_dropping_id) = 0 THEN
      SET NEW.order_dropping_id = UUID();
  END IF;
 END;

/* Header line. Object: nscc_order_dropping_BEFORE_UPDATE. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_order_dropping_BEFORE_UPDATE`
  BEFORE UPDATE ON `nscc_order_dropping`
  FOR EACH ROWBEGIN
  IF !(OLD.tgl  <=> NEW.tgl 
        AND OLD.doc_ref <=> NEW.doc_ref
        AND OLD.note <=> NEW.note
        AND OLD.tdate <=> NEW.tdate
        AND OLD.user_id <=> NEW.user_id
        AND OLD.type_ <=> NEW.type_
        AND OLD.store <=> NEW.store
        AND OLD.store_pengirim <=> NEW.store_pengirim
        AND OLD.approved <=> NEW.approved
        AND OLD.approved_user_id <=> NEW.approved_user_id
        AND OLD.approved_date <=> NEW.approved_date
        AND OLD.lunas <=> NEW.lunas
   ) THEN
        SET NEW.up = 0;
   END IF;
END;

/* Header line. Object: nscc_order_dropping_details_BEFORE_INSERT. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_order_dropping_details_BEFORE_INSERT`
  BEFORE INSERT ON `nscc_order_dropping_details`
  FOR EACH ROWBEGIN
IF NEW.order_dropping_detail_id IS NULL OR LENGTH(NEW.order_dropping_detail_id) = 0 THEN
  SET NEW.order_dropping_detail_id = UUID();
  END IF;
END;

/* Header line. Object: nscc_order_dropping_details_BEFORE_UPDATE. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_order_dropping_details_BEFORE_UPDATE`
  BEFORE UPDATE ON `nscc_order_dropping_details`
  FOR EACH ROWBEGIN
IF !(OLD.qty  <=> NEW.qty AND OLD.barang_id <=> NEW.barang_id 
	AND OLD.visible <=> NEW.visible AND OLD.order_dropping_id <=> NEW.order_dropping_id
) THEN 
		UPDATE nscc_order_dropping nod SET nod.up = 0 WHERE nod.order_dropping_id = NEW.order_dropping_id;
		UPDATE nscc_order_dropping nod SET nod.up = 0 WHERE nod.order_dropping_id = OLD.order_dropping_id;
  END IF;
END;

/* Header line. Object: nscc_po_BEFORE_INSERT. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_po_BEFORE_INSERT`
  BEFORE INSERT ON `nscc_po`
  FOR EACH ROWBEGIN
IF NEW.po_id IS NULL OR LENGTH(NEW.po_id) = 0 THEN
  SET NEW.po_id = UUID();
  END IF;
END;

/* Header line. Object: nscc_po_BEFORE_UPDATE. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_po_BEFORE_UPDATE`
  BEFORE UPDATE ON `nscc_po`
  FOR EACH ROWBEGIN
	IF !(OLD.doc_ref <=> NEW.doc_ref 
        AND OLD.tgl <=> NEW.tgl
        AND OLD.tgl_delivery <=> NEW.tgl_delivery
        AND OLD.note <=> NEW.note
        AND OLD.acc_charge <=> NEW.acc_charge
        AND OLD.pr_id <=> NEW.pr_id
        AND OLD.doc_ref_pr <=> NEW.doc_ref_pr
        AND OLD.divisi <=> NEW.divisi
        AND OLD.store <=> NEW.store
        AND OLD.status <=> NEW.status
        AND OLD.tgl_status_change <=> NEW.tgl_status_change
		AND OLD.id_user <=> NEW.id_user
        AND OLD.tdate <=> NEW.tdate
        
        AND OLD.nama <=> NEW.nama
        AND OLD.ship_to_address <=> NEW.ship_to_address
		AND OLD.ship_to_company <=> NEW.ship_to_company
		AND OLD.ship_to_city <=> NEW.ship_to_city
		AND OLD.ship_to_country <=> NEW.ship_to_country
		AND OLD.ship_to_phone <=> NEW.ship_to_phone
		AND OLD.ship_to_fax <=> NEW.ship_to_fax
        
		AND OLD.supplier_id <=> NEW.supplier_id
		AND OLD.supp_nama <=> NEW.supp_nama
		AND OLD.supp_address <=> NEW.supp_address
		AND OLD.supp_city <=> NEW.supp_city
		AND OLD.supp_country <=> NEW.supp_country
		AND OLD.supp_phone <=> NEW.supp_phone
		AND OLD.supp_fax <=> NEW.supp_fax
		AND OLD.supp_email <=> NEW.supp_email
		AND OLD.termofpayment <=> NEW.termofpayment
		AND OLD.nama_rek <=> NEW.nama_rek
		AND OLD.no_rek <=> NEW.no_rek
		AND OLD.bank_rek <=> NEW.bank_rek
        
		AND OLD.sub_total <=> NEW.sub_total
		AND OLD.disc <=> NEW.disc
		AND OLD.total_disc_rp <=> NEW.total_disc_rp
		AND OLD.total_dpp <=> NEW.total_dpp
		AND OLD.tax_rp <=> NEW.tax_rp
		AND OLD.total_pph_rp <=> NEW.total_pph_rp
		AND OLD.total <=> NEW.total
        
        AND OLD.closed_id_user <=> NEW.closed_id_user
        AND OLD.closed_tdate <=> NEW.closed_tdate
	) THEN
		set NEW.up = 0;
	END IF;
END;

/* Header line. Object: nscc_po_details_BEFORE_INSERT. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_po_details_BEFORE_INSERT`
  BEFORE INSERT ON `nscc_po_details`
  FOR EACH ROWBEGIN
IF NEW.po_details_id IS NULL OR LENGTH(NEW.po_details_id) = 0 THEN
  SET NEW.po_details_id = UUID();
  END IF;
END;

/* Header line. Object: nscc_po_details_BEFORE_UPDATE. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_po_details_BEFORE_UPDATE`
  BEFORE UPDATE ON `nscc_po_details`
  FOR EACH ROWBEGIN
    IF !(OLD.po_id  <=> NEW.po_id
		AND OLD.seq <=> NEW.seq 
		AND OLD.description <=> NEW.description
        AND OLD.barang_id <=> NEW.barang_id
        AND OLD.qty <=> NEW.qty
		AND OLD.price <=> NEW.price
		AND OLD.sub_total <=> NEW.sub_total
		AND OLD.disc <=> NEW.disc
		AND OLD.disc_rp <=> NEW.disc_rp
		AND OLD.total_disc <=> NEW.total_disc
		AND OLD.total_dpp <=> NEW.total_dpp
		AND OLD.ppn <=> NEW.ppn
		AND OLD.ppn_rp <=> NEW.ppn_rp
		AND OLD.pph <=> NEW.pph
		AND OLD.pph_rp <=> NEW.pph_rp
		AND OLD.total <=> NEW.total
		AND OLD.charge <=> NEW.charge
		AND OLD.visible <=> NEW.visible
) THEN 
		UPDATE nscc_po t SET t.up = 0 WHERE t.po_id = NEW.po_id;
		UPDATE nscc_po t SET t.up = 0 WHERE t.po_id = OLD.po_id;
  END IF;
END;

/* Header line. Object: nscc_receive_dropping_BEFORE_INSERT. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_receive_dropping_BEFORE_INSERT`
  BEFORE INSERT ON `nscc_receive_dropping`
  FOR EACH ROWBEGIN
IF NEW.receive_dropping_id IS NULL OR LENGTH(NEW.receive_dropping_id) = 0 THEN
  SET NEW.receive_dropping_id = UUID();
  END IF;
END;

/* Header line. Object: nscc_receive_dropping_BEFORE_UPDATE. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_receive_dropping_BEFORE_UPDATE`
  BEFORE UPDATE ON `nscc_receive_dropping`
  FOR EACH ROWBEGIN
	IF !(OLD.tgl  <=> NEW.tgl 
		AND OLD.doc_ref <=> NEW.doc_ref
		AND OLD.note <=> NEW.note
		AND OLD.tdate <=> NEW.tdate
		AND OLD.user_id <=> NEW.user_id
		AND OLD.type_ <=> NEW.type_
		AND OLD.store <=> NEW.store
		AND OLD.dropping_id <=> NEW.dropping_id
		AND OLD.store_pengirim <=> NEW.store_pengirim
		AND OLD.lunas <=> NEW.lunas
	) THEN
		SET NEW.up = 0;
	END IF;
END;

/* Header line. Object: nscc_receive_dropping_details_BEFORE_INSERT. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_receive_dropping_details_BEFORE_INSERT`
  BEFORE INSERT ON `nscc_receive_dropping_details`
  FOR EACH ROWBEGIN
IF NEW.receive_dropping_detail_id IS NULL OR LENGTH(NEW.receive_dropping_detail_id) = 0 THEN
  SET NEW.receive_dropping_detail_id = UUID();
  END IF;
END;

/* Header line. Object: nscc_receive_dropping_details_BEFORE_UPDATE. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_receive_dropping_details_BEFORE_UPDATE`
  BEFORE UPDATE ON `nscc_receive_dropping_details`
  FOR EACH ROWBEGIN
IF !(OLD.qty  <=> NEW.qty AND OLD.barang_id <=> NEW.barang_id 
	AND OLD.visible <=> NEW.visible AND OLD.receive_dropping_id <=> NEW.receive_dropping_id
) THEN 
		UPDATE nscc_receive_dropping t SET t.up = 0 WHERE t.receive_dropping_id = NEW.receive_dropping_id;
		UPDATE nscc_receive_dropping t SET t.up = 0 WHERE t.receive_dropping_id = OLD.receive_dropping_id;
  END IF;
END;

/* Header line. Object: nscc_terima_barang_before_insert. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_terima_barang_before_insert`
  BEFORE INSERT ON `nscc_terima_barang`
  FOR EACH ROWBEGIN
IF NEW.terima_barang_id IS NULL OR LENGTH(NEW.terima_barang_id) = 0 THEN
  SET NEW.terima_barang_id = UUID();
  END IF;
END;

/* Header line. Object: nscc_terima_barang_before_update. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_terima_barang_before_update`
  BEFORE UPDATE ON `nscc_terima_barang`
  FOR EACH ROWBEGIN
	IF !(OLD.po_id  <=> NEW.po_id
		AND OLD.doc_ref <=> NEW.doc_ref 
		AND OLD.id_user <=> NEW.id_user
        AND OLD.tdate <=> NEW.tdate
        AND OLD.tgl <=> NEW.tgl
		AND OLD.store <=> NEW.store
		AND OLD.no_sj <=> NEW.no_sj
		AND OLD.tgl_sj <=> NEW.tgl_sj
		AND OLD.lock_edit <=> NEW.lock_edit
		AND OLD.status <=> NEW.status
	) THEN
		set NEW.up = 0;
	END IF;
END;

/* Header line. Object: nscc_terima_barang_details_before_insert. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_terima_barang_details_before_insert`
  BEFORE INSERT ON `nscc_terima_barang_details`
  FOR EACH ROWBEGIN
IF NEW.terima_barang_details_id IS NULL OR LENGTH(NEW.terima_barang_details_id) = 0 THEN
  SET NEW.terima_barang_details_id = UUID();
  END IF;
END;

/* Header line. Object: nscc_terima_barang_details_before_update. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_terima_barang_details_before_update`
  BEFORE UPDATE ON `nscc_terima_barang_details`
  FOR EACH ROWBEGIN
	IF !(OLD.qty  <=> NEW.qty AND OLD.description <=> NEW.description 
		AND OLD.seq <=> NEW.seq AND OLD.visible <=> NEW.visible 
		AND OLD.terima_barang_id <=> NEW.terima_barang_id
		AND OLD.barang_id <=> NEW.barang_id) THEN
		UPDATE nscc_terima_barang nt SET nt.up = 0 WHERE nt.terima_barang_id = NEW.terima_barang_id;
		UPDATE nscc_terima_barang nt SET nt.up = 0 WHERE nt.terima_barang_id = OLD.terima_barang_id;
	END IF;
END;

/* Header line. Object: nscc_transfer_item_before_insert. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_transfer_item_before_insert`
  BEFORE INSERT ON `nscc_transfer_item`
  FOR EACH ROWBEGIN
IF NEW.transfer_item_id IS NULL OR LENGTH(NEW.transfer_item_id) = 0 THEN
  SET NEW.transfer_item_id = UUID();
  END IF;
END;

/* Header line. Object: nscc_transfer_item_before_update. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_transfer_item_before_update`
  BEFORE UPDATE ON `nscc_transfer_item`
  FOR EACH ROWBEGIN  
	IF !(OLD.tgl  <=> NEW.tgl
		AND OLD.doc_ref <=> NEW.doc_ref 
		AND OLD.note <=> NEW.note 
        AND OLD.tdate <=> NEW.tdate
		AND OLD.doc_ref_other <=> NEW.doc_ref_other 
        AND OLD.user_id <=> NEW.user_id
		AND OLD.type_ <=> NEW.type_ 
        AND OLD.store <=> NEW.store
		AND OLD.supplier_id <=> NEW.supplier_id 
		AND OLD.tgl_jatuh_tempo <=> NEW.tgl_jatuh_tempo
        AND OLD.terima_barang_id <=> NEW.terima_barang_id
        AND OLD.no_fakturpajak <=> NEW.no_fakturpajak
		AND OLD.sub_total <=> NEW.sub_total 
		AND OLD.disc <=> NEW.disc 
        AND OLD.total_disc_rp <=> NEW.total_disc_rp
        AND OLD.total_dpp <=> NEW.total_dpp
        AND OLD.tax_rp <=> NEW.tax_rp 
        AND OLD.total_pph_rp <=> NEW.total_pph_rp
        AND OLD.total <=> NEW.total
        AND OLD.status <=> NEW.status
        AND OLD.closed_id_user <=> NEW.closed_id_user
        AND OLD.closed_tdate <=> NEW.closed_tdate
	) THEN
	  SET NEW.up = 0;
	END IF;
END;

/* Header line. Object: nscc_transfer_item_details_before_insert. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_transfer_item_details_before_insert`
  BEFORE INSERT ON `nscc_transfer_item_details`
  FOR EACH ROWBEGIN
IF NEW.transfer_item_details_id IS NULL OR LENGTH(NEW.transfer_item_details_id) = 0 THEN
  SET NEW.transfer_item_details_id = UUID();
END IF;
UPDATE nscc_transfer_item nti SET nti.up = 0 WHERE nti.transfer_item_id = NEW.transfer_item_id;
END;

/* Header line. Object: nscc_transfer_item_details_before_update. Script date: 14/09/2017 13:47:32. */
CREATE TRIGGER `nscc_transfer_item_details_before_update`
  BEFORE UPDATE ON `nscc_transfer_item_details`
  FOR EACH ROWBEGIN
IF !(OLD.qty  <=> NEW.qty
		AND OLD.barang_id <=> NEW.barang_id 
		AND OLD.transfer_item_id <=> NEW.transfer_item_id 
        AND OLD.price <=> NEW.price
        AND OLD.sub_total <=> NEW.sub_total
        AND OLD.disc <=> NEW.disc
		AND OLD.disc_rp <=> NEW.disc_rp 
		AND OLD.total_disc <=> NEW.total_disc
		AND OLD.total_dpp <=> NEW.total_dpp
		AND OLD.ppn <=> NEW.ppn 
        AND OLD.ppn_rp <=> NEW.ppn_rp
		AND OLD.pph <=> NEW.pph 
        AND OLD.pph_rp <=> NEW.pph_rp
		AND OLD.total <=> NEW.total 
		AND OLD.charge <=> NEW.charge 
		AND OLD.visible <=> NEW.visible 
	) THEN	  
			UPDATE nscc_transfer_item nti SET nti.up = 0 WHERE nti.transfer_item_id = NEW.transfer_item_id;
			UPDATE nscc_transfer_item nti SET nti.up = 0 WHERE nti.transfer_item_id = OLD.transfer_item_id;
END IF;
END;

-- Restore foreign keys, referenced to nscc_transfer_item
ALTER TABLE `posng`.`nscc_pelunasan_utang_detil` ADD CONSTRAINT `nscc_pelunasan_utang_detil_ibfk_2`
	FOREIGN KEY ( `transfer_item_id` ) REFERENCES `nscc_transfer_item` ( `transfer_item_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_dropping_details
ALTER TABLE `posng`.`nscc_dropping_details` ADD CONSTRAINT `fk_nscc_dropping_details`
	FOREIGN KEY ( `dropping_id` ) REFERENCES `nscc_dropping` ( `dropping_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_order_dropping
ALTER TABLE `posng`.`nscc_order_dropping` ADD CONSTRAINT `nscc_order_dropping_fk0`
	FOREIGN KEY ( `store` ) REFERENCES `nscc_store` ( `store_kode` ) ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_order_dropping` ADD CONSTRAINT `nscc_order_dropping_fk1`
	FOREIGN KEY ( `store_pengirim` ) REFERENCES `nscc_store` ( `store_kode` ) ON UPDATE CASCADE;

-- Update foreign keys of nscc_order_dropping_details
ALTER TABLE `posng`.`nscc_order_dropping_details` ADD CONSTRAINT `fk_nscc_order_dropping_details`
	FOREIGN KEY ( `order_dropping_id` ) REFERENCES `nscc_order_dropping` ( `order_dropping_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_po
ALTER TABLE `posng`.`nscc_po` ADD CONSTRAINT `fk_nscc_po`
	FOREIGN KEY ( `supplier_id` ) REFERENCES `nscc_supplier` ( `supplier_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_po` ADD CONSTRAINT `nscc_po_fk1`
	FOREIGN KEY ( `pr_id` ) REFERENCES `nscc_pr` ( `pr_id` ) ON DELETE SET NULL ON UPDATE CASCADE;

-- Update foreign keys of nscc_po_details
ALTER TABLE `posng`.`nscc_po_details` ADD CONSTRAINT `fk_nscc_po_details`
	FOREIGN KEY ( `po_id` ) REFERENCES `nscc_po` ( `po_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_receive_dropping
ALTER TABLE `posng`.`nscc_receive_dropping` ADD CONSTRAINT `fk_nscc_receive_dropping`
	FOREIGN KEY ( `dropping_id` ) REFERENCES `nscc_dropping` ( `dropping_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_receive_dropping_details
ALTER TABLE `posng`.`nscc_receive_dropping_details` ADD CONSTRAINT `fk_nscc_dropping_details_0`
	FOREIGN KEY ( `receive_dropping_id` ) REFERENCES `nscc_receive_dropping` ( `receive_dropping_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_terima_barang
ALTER TABLE `posng`.`nscc_terima_barang` ADD CONSTRAINT `fk_nscc_terima_barang`
	FOREIGN KEY ( `po_id` ) REFERENCES `nscc_po` ( `po_id` ) ON UPDATE CASCADE;

-- Update foreign keys of nscc_terima_barang_details
ALTER TABLE `posng`.`nscc_terima_barang_details` ADD CONSTRAINT `fk_nscc_terima_barang_details`
	FOREIGN KEY ( `terima_barang_id` ) REFERENCES `nscc_terima_barang` ( `terima_barang_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_transfer_item
ALTER TABLE `posng`.`nscc_transfer_item` ADD CONSTRAINT `fk_nscc_transfer_item_0`
	FOREIGN KEY ( `terima_barang_id` ) REFERENCES `nscc_terima_barang` ( `terima_barang_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_transfer_item` ADD CONSTRAINT `nscc_transfer_item_ibfk_1`
	FOREIGN KEY ( `supplier_id` ) REFERENCES `nscc_supplier` ( `supplier_id` ) ON UPDATE CASCADE;

-- Update foreign keys of nscc_transfer_item_details
ALTER TABLE `posng`.`nscc_transfer_item_details` ADD CONSTRAINT `nscc_transfer_item_details_ibfk_1`
	FOREIGN KEY ( `barang_id` ) REFERENCES `nscc_barang` ( `barang_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_transfer_item_details` ADD CONSTRAINT `nscc_transfer_item_details_ibfk_2`
	FOREIGN KEY ( `transfer_item_id` ) REFERENCES `nscc_transfer_item` ( `transfer_item_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

