ALTER 
ALGORITHM=UNDEFINED 
DEFINER=`rCpp9uvJpztw`@`%` 
SQL SECURITY DEFINER 
VIEW `nscc_monitor_antrian` AS 
SELECT
	`aa`.`id_antrian` AS `id_antrian`,
	`aa`.`nomor_pasien` AS `nomor_pasien`,
	`aa`.`nomor_antrian` AS `nomor_antrian`,
	`aa`.`tanggal` AS `tanggal`,
	`aa`.`counter` AS `counter_`,
	max(
		(
			CASE `nahg`.`bagian`
			WHEN 'daftar' THEN
				concat(
					'[',
					cast(`nahg`.`timestamp` AS time),
					'] ',
					`nahg`.`action`
				)
			END
		)
	) AS `daftar`,
	max(
		(
			CASE `nahg`.`bagian`
			WHEN 'counter' THEN
				concat(
					'[',
					cast(`nahg`.`timestamp` AS time),
					'] ',
					`nahg`.`action`
				)
			END
		)
	) AS `counter`,
	max(
		(
			CASE `nahg`.`bagian`
			WHEN 'medis' THEN
				concat(
					'[',
					cast(`nahg`.`timestamp` AS time),
					'] ',
					`nahg`.`action`
				)
			END
		)
	) AS `konsul`,
	max(
		(
			CASE `nahg`.`bagian`
			WHEN 'kasir' THEN
				concat(
					'[',
					cast(`nahg`.`timestamp` AS time),
					'] ',
					`nahg`.`action`
				)
			END
		)
	) AS `kasir`,
	`aa`.`alasan` AS `alasan`,
aa.counter_asal AS counter_asal
FROM
	(
		`aisha_antrian` `aa`
		LEFT JOIN `nscc_antrian_history_group` `nahg` ON (
			(
				`nahg`.`id_antrian` = `aa`.`id_antrian`
			)
		)
	)
GROUP BY
	`aa`.`id_antrian` ;