-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.24 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for posng
CREATE DATABASE IF NOT EXISTS `posng` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `posng`;

-- Dumping structure for table posng.nscc_member_card
CREATE TABLE IF NOT EXISTS `nscc_member_card` (
  `membercard_id` varchar(50) NOT NULL,
  `customer_id` varchar(50) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `no_card` varchar(100) DEFAULT NULL,
  `store` varchar(50) NOT NULL,
  `since` date NOT NULL,
  `valid` date NOT NULL,
  `type` varchar(50) DEFAULT 'EC',
  `category` varchar(50) DEFAULT NULL,
  `business_unit` varchar(50) NOT NULL,
  `sub_business_unit` varchar(50) NOT NULL,
  `active` enum('Y','N') NOT NULL DEFAULT 'N',
  `counter` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  `up` enum('1','0') NOT NULL DEFAULT '0',
  PRIMARY KEY (`membercard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table posng.nscc_member_card: ~2 rows (approximately)
/*!40000 ALTER TABLE `nscc_member_card` DISABLE KEYS */;

/*!40000 ALTER TABLE `nscc_member_card` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
