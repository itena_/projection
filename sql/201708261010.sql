CREATE TABLE `posng`.`nscc_bonus_name`
(
  `bonus_name_id` VARCHAR(36) NOT NULL,
  `bonus_name`    VARCHAR(50) NOT NULL,
  `up`            TINYINT(1)  NOT NULL DEFAULT '0',
  PRIMARY KEY (`bonus_name_id`)
)
  ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  ROW_FORMAT = COMPACT
  COMMENT = 'Nama bonus';
CREATE TABLE `posng`.`nscc_bonus_jual`
(
  `bonus_jual_id` VARCHAR(36)    NOT NULL,
  `barang_id`     VARCHAR(36)    NOT NULL,
  `bonus_name_id` VARCHAR(36)    NOT NULL,
  `persen_bonus`  DECIMAL(10, 2) NOT NULL DEFAULT '0.00',
  `store`         VARCHAR(20)    NOT NULL,
  `up`            TINYINT(1)     NOT NULL DEFAULT '0',
  KEY `nscc_bonus_jual_fk0_idx` (`barang_id`),
  KEY `nscc_bonus_jual_fk1_idx` (`store`),
  KEY `nscc_bonus_jual_fk2_idx` (`bonus_name_id`),
  UNIQUE INDEX

    `nscc_bonus_jual_unique1`

    (`barang_id`, `bonus_name_id`, `store`),
  PRIMARY KEY (`bonus_jual_id`)
)

  ENGINE = InnoDB

  CHARACTER SET = utf8mb4

  ROW_FORMAT = COMPACT

  COMMENT = 'Persentase bonus penjualan (cream/perawatan)';
ALTER TABLE `posng`.`nscc_bonus_jual`

  ADD CONSTRAINT `nscc_bonus_jual_fk0` FOREIGN KEY (`barang_id`)

REFERENCES `nscc_barang` (`barang_id`)
  ON UPDATE CASCADE;
ALTER TABLE `posng`.`nscc_bonus_jual`

  ADD CONSTRAINT `nscc_bonus_jual_fk1` FOREIGN KEY (`store`)

REFERENCES `nscc_store` (`store_kode`)
  ON UPDATE CASCADE;
ALTER TABLE `posng`.`nscc_bonus_jual`

  ADD CONSTRAINT `nscc_bonus_jual_fk2` FOREIGN KEY (`bonus_name_id`)

REFERENCES `nscc_bonus_name` (`bonus_name_id`)
  ON UPDATE CASCADE;
DROP TABLE IF EXISTS `posng`.`_temp_nscc_bonus`;
CREATE TABLE `posng`.`_temp_nscc_bonus`
(
  `bonus_id`      VARCHAR(36)    NOT NULL,
  `tgl`           DATE           NOT NULL,
  `employee_id`   VARCHAR(50)    NOT NULL,
  `type_`         INT(11)        NOT NULL,
  `trans_no`      VARCHAR(50)    NOT NULL,
  `barang_id`     VARCHAR(36)    NOT NULL,
  `amount`        DECIMAL(30, 2) NOT NULL DEFAULT '0.00',
  `bonus_jual_id` VARCHAR(36)    NOT NULL,
  `persen_bonus`  DECIMAL(10, 2) NOT NULL DEFAULT '0.00',
  `amount_bonus`  DECIMAL(30, 2) NOT NULL DEFAULT '0.00',
  `store`         VARCHAR(20)    NOT NULL,
  `tdate`         DATETIME       NOT NULL,
  `id_user`       VARCHAR(50)    NOT NULL,
  `up`            TINYINT(1)     NOT NULL DEFAULT '0',
  `visible`       TINYINT(1)     NOT NULL DEFAULT '1',
  `tipe_trans`    VARCHAR(20)             DEFAULT 'sales',
  KEY `idx_nscc_bonus` (`employee_id`),
  KEY `nscc_bonus_fk1_idx` (`barang_id`),
  KEY `nscc_bonus_fk2_idx` (`bonus_jual_id`),
  KEY `nscc_bonus_fk3_idx` (`store`),
  PRIMARY KEY (`bonus_id`)
)

  ENGINE = InnoDB

  CHARACTER SET = utf8mb4

  ROW_FORMAT = COMPACT;
INSERT INTO `posng`.`_temp_nscc_bonus` (`bonus_id`,
                                        `employee_id`,
                                        `id_user`,
                                        `tdate`,
                                        `tgl`,
                                        `trans_no`,
                                        `type_`)

  SELECT
    `bonus_id`,
    `employee_id`,
    `id_user`,
    `tdate`,
    `tgl`,
    `trans_no`,
    `type_`
  FROM `posng`.`nscc_bonus`;
DROP TABLE `posng`.`nscc_bonus`;
ALTER TABLE `posng`.`_temp_nscc_bonus`

  RENAME `nscc_bonus`;
ALTER TABLE `posng`.`nscc_bonus`

  ADD CONSTRAINT `fk_nscc_bonus` FOREIGN KEY

  (`employee_id`)

REFERENCES `nscc_employees` (`employee_id`)

  ON DELETE CASCADE

  ON UPDATE CASCADE;
ALTER TABLE `posng`.`nscc_bonus`

  ADD CONSTRAINT `nscc_bonus_fk1` FOREIGN KEY (`barang_id`)

REFERENCES `nscc_barang` (`barang_id`)
  ON UPDATE CASCADE;
ALTER TABLE `posng`.`nscc_bonus`

  ADD CONSTRAINT `nscc_bonus_fk2` FOREIGN KEY (`bonus_jual_id`)

REFERENCES `nscc_bonus_jual` (`bonus_jual_id`)
  ON UPDATE CASCADE;
ALTER TABLE `posng`.`nscc_bonus`

  ADD CONSTRAINT `nscc_bonus_fk3` FOREIGN KEY (`store`)

REFERENCES `nscc_store` (`store_kode`)
  ON UPDATE CASCADE;
DROP TABLE IF EXISTS `posng`.`_temp_nscc_salestrans`;
CREATE TABLE `posng`.`_temp_nscc_salestrans`
(
  `salestrans_id` VARCHAR(50)         NOT NULL,
  `tgl`           DATE                NOT NULL,
  `doc_ref`       VARCHAR(50)         NOT NULL,
  `tdate`         DATETIME            NOT NULL,
  `user_id`       VARCHAR(50)         NOT NULL,
  `bruto`         DECIMAL(30, 2)      NOT NULL DEFAULT '0.00',
  `disc`          DECIMAL(30, 2)      NOT NULL DEFAULT '0.00',
  `discrp`        DECIMAL(30, 2)      NOT NULL DEFAULT '0.00',
  `totalpot`      DECIMAL(30, 2)      NOT NULL DEFAULT '0.00',
  `total`         DECIMAL(30, 2)      NOT NULL DEFAULT '0.00',
  `ketdisc`       VARCHAR(255)        NOT NULL DEFAULT '-',
  `vat`           DECIMAL(30, 2)      NOT NULL DEFAULT '0.00',
  `customer_id`   VARCHAR(36)         NOT NULL,
  `doc_ref_sales` VARCHAR(50)                  DEFAULT NULL,
  `audit`         TINYINT(4)          NOT NULL DEFAULT '0',
  `store`         VARCHAR(20)         NOT NULL,
  `printed`       TINYINT(4)          NOT NULL DEFAULT '0',
  `override`      VARCHAR(50)                  DEFAULT NULL,
  `bayar`         DECIMAL(30, 2)      NOT NULL DEFAULT '0.00',
  `kembali`       DECIMAL(30, 2)      NOT NULL DEFAULT '0.00',
  `dokter_id`     VARCHAR(50)                  DEFAULT NULL,
  `log`           TINYINT(4)          NOT NULL DEFAULT '0',
  `up`            TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `total_discrp1` DECIMAL(30, 2)      NOT NULL DEFAULT '0.00',
  `type_`         TINYINT(4)          NOT NULL DEFAULT '1',
  `rounding`      DECIMAL(30, 2)      NOT NULL DEFAULT '0.00',
  `retur_note`    VARCHAR(600)                 DEFAULT NULL,
  `parent_id`     VARCHAR(36)                  DEFAULT NULL,
  `kmr_persen`    DECIMAL(5, 2)       NOT NULL DEFAULT '0.00',
  `kmr_total`     DECIMAL(30, 2)      NOT NULL DEFAULT '0.00',
  `start_at`      DATETIME                     DEFAULT NULL,
  `end_at`        DATETIME                     DEFAULT NULL,
  `pending`       TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
  `end_estimate`  DATETIME                     DEFAULT NULL,
  `konsul_id`     VARCHAR(36)                  DEFAULT NULL,
  `total_durasi`  BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
  `add_durasi`    BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
  `add_jasa`      VARCHAR(255)                 DEFAULT NULL,
  `counter`       VARCHAR(5)                   DEFAULT NULL,
  `id_antrian`    VARCHAR(36)                  DEFAULT NULL,
  `beauty_id`     VARCHAR(50)                  DEFAULT NULL,
  `stocker_id`    VARCHAR(50)                  DEFAULT NULL,
  `apoteker_id`   VARCHAR(50)                  DEFAULT NULL,
  `admin_id`      VARCHAR(50)                  DEFAULT NULL,
  `pc_id`         VARCHAR(50)                  DEFAULT NULL,
  KEY `idx_nscc_salestrans` (`customer_id`),
  KEY `idx_nscc_salestrans_3` (`dokter_id`),
  KEY `idx_nscc_salestrans_4` (`store`),
  PRIMARY KEY (`salestrans_id`),
  UNIQUE INDEX `UK_nscc_salestrans_salestrans_id`(`salestrans_id`)
)

  ENGINE = InnoDB

  CHARACTER SET = utf8mb4

  ROW_FORMAT = COMPACT;
INSERT INTO `posng`.`_temp_nscc_salestrans` (`add_durasi`,
                                             `add_jasa`,
                                             `apoteker_id`,
                                             `audit`,
                                             `bayar`,
                                             `beauty_id`,
                                             `bruto`,
                                             `counter`,
                                             `customer_id`,
                                             `disc`,
                                             `discrp`,
                                             `doc_ref`,
                                             `doc_ref_sales`,
                                             `dokter_id`,
                                             `end_at`,
                                             `end_estimate`,
                                             `id_antrian`,
                                             `kembali`,
                                             `ketdisc`,
                                             `kmr_persen`,
                                             `kmr_total`,
                                             `konsul_id`,
                                             `log`,
                                             `override`,
                                             `parent_id`,
                                             `pending`,
                                             `printed`,
                                             `retur_note`,
                                             `rounding`,
                                             `salestrans_id`,
                                             `start_at`,
                                             `stocker_id`,
                                             `store`,
                                             `tdate`,
                                             `tgl`,
                                             `total`,
                                             `total_discrp1`,
                                             `total_durasi`,
                                             `totalpot`,
                                             `type_`,
                                             `up`,
                                             `user_id`,
                                             `vat`)

  SELECT
    `add_durasi`,
    `add_jasa`,
    `apoteker_id`,
    `audit`,
    `bayar`,
    `beauty_id`,
    `bruto`,
    `counter`,
    `customer_id`,
    `disc`,
    `discrp`,
    `doc_ref`,
    `doc_ref_sales`,
    `dokter_id`,
    `end_at`,
    `end_estimate`,
    `id_antrian`,
    `kembali`,
    `ketdisc`,
    `kmr_persen`,
    `kmr_total`,
    `konsul_id`,
    `log`,
    `override`,
    `parent_id`,
    `pending`,
    `printed`,
    `retur_note`,
    `rounding`,
    `salestrans_id`,
    `start_at`,
    `stocker_id`,
    `store`,
    `tdate`,
    `tgl`,
    `total`,
    `total_discrp1`,
    `total_durasi`,
    `totalpot`,
    `type_`,
    `up`,
    `user_id`,
    `vat`
  FROM `posng`.`nscc_salestrans`;
DROP TABLE `posng`.`nscc_salestrans`;
ALTER TABLE `posng`.`_temp_nscc_salestrans`

  RENAME `nscc_salestrans`;