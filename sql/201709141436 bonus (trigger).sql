
      /*
      * Script created by Quest Schema Compare at 14/09/2017 14:20:52.
      * Please back up your database before running this script.
      *
      * Synchronizing objects from posng to posng.
      */
    USE `posng`;

ALTER TABLE `posng`.`nscc_bonus` DROP FOREIGN KEY `fk_nscc_bonus`;

ALTER TABLE `posng`.`nscc_bonus` DROP FOREIGN KEY `nscc_bonus_fk2`;

ALTER TABLE `posng`.`nscc_bonus` DROP FOREIGN KEY `nscc_bonus_fk3`;

ALTER TABLE `posng`.`nscc_bonus_jual` DROP FOREIGN KEY `nscc_bonus_jual_fk1`;

ALTER TABLE `posng`.`nscc_bonus_jual` DROP FOREIGN KEY `nscc_bonus_jual_fk2`;

/* Header line. Object: nscc_bonus. Script date: 14/09/2017 14:20:52. */
ALTER TABLE `posng`.`nscc_bonus` DROP FOREIGN KEY `fk_nscc_bonus`;

ALTER TABLE `posng`.`nscc_bonus` DROP FOREIGN KEY `nscc_bonus_fk2`;

ALTER TABLE `posng`.`nscc_bonus` DROP FOREIGN KEY `nscc_bonus_fk3`;

/* Header line. Object: nscc_bonus_jual. Script date: 14/09/2017 14:20:52. */
ALTER TABLE `posng`.`nscc_bonus_jual` DROP FOREIGN KEY `nscc_bonus_jual_fk1`;

ALTER TABLE `posng`.`nscc_bonus_jual` DROP FOREIGN KEY `nscc_bonus_jual_fk2`;

/* Header line. Object: nscc_bonus_BEFORE_UPDATE. Script date: 14/09/2017 14:20:52. */
CREATE TRIGGER `nscc_bonus_BEFORE_UPDATE`
  BEFORE UPDATE ON `nscc_bonus`
  FOR EACH ROWBEGIN
  IF !(OLD.tgl  <=> NEW.tgl AND OLD.employee_id <=> NEW.employee_id 
	AND OLD.type_ <=> NEW.type_ AND OLD.trans_no <=> NEW.trans_no
	AND OLD.barang_id <=> NEW.barang_id AND OLD.amount <=> NEW.amount
	AND OLD.bonus_jual_id <=> NEW.bonus_jual_id AND OLD.persen_bonus <=> NEW.persen_bonus
	AND OLD.amount_bonus <=> NEW.amount_bonus AND OLD.store <=> NEW.store
	AND OLD.tdate <=> NEW.tdate AND OLD.id_user <=> NEW.id_user
	AND OLD.visible <=> NEW.visible AND OLD.tipe_trans <=> NEW.tipe_trans) THEN
  SET NEW.up = 0;
  END IF;
END;

/* Header line. Object: nscc_bonus_jual_BEFORE_UPDATE. Script date: 14/09/2017 14:20:52. */
CREATE TRIGGER `nscc_bonus_jual_BEFORE_UPDATE`
  BEFORE UPDATE ON `nscc_bonus_jual`
  FOR EACH ROWBEGIN
IF !(OLD.barang_id  <=> NEW.barang_id 
	AND OLD.bonus_name_id  <=> NEW.bonus_name_id
	AND OLD.persen_bonus  <=> NEW.persen_bonus
	AND OLD.store  <=> NEW.store) THEN
		SET NEW.up = 0;
	END IF;
END;

/* Header line. Object: nscc_bonus_name_BEFORE_UPDATE. Script date: 14/09/2017 14:20:52. */
CREATE TRIGGER `nscc_bonus_name_BEFORE_UPDATE`
  BEFORE UPDATE ON `nscc_bonus_name`
  FOR EACH ROWBEGIN
	IF !(OLD.bonus_name_id  <=> NEW.bonus_name_id AND OLD.bonus_name  <=> NEW.bonus_name) THEN
		SET NEW.up = 0;
	END IF;
END;

-- Update foreign keys of nscc_bonus
ALTER TABLE `posng`.`nscc_bonus` ADD CONSTRAINT `fk_nscc_bonus`
	FOREIGN KEY ( `employee_id` ) REFERENCES `nscc_employees` ( `employee_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_bonus` ADD CONSTRAINT `nscc_bonus_fk2`
	FOREIGN KEY ( `bonus_jual_id` ) REFERENCES `nscc_bonus_jual` ( `bonus_jual_id` ) ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_bonus` ADD CONSTRAINT `nscc_bonus_fk3`
	FOREIGN KEY ( `store` ) REFERENCES `nscc_store` ( `store_kode` ) ON UPDATE CASCADE;

-- Update foreign keys of nscc_bonus_jual
ALTER TABLE `posng`.`nscc_bonus_jual` ADD CONSTRAINT `nscc_bonus_jual_fk1`
	FOREIGN KEY ( `store` ) REFERENCES `nscc_store` ( `store_kode` ) ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_bonus_jual` ADD CONSTRAINT `nscc_bonus_jual_fk2`
	FOREIGN KEY ( `bonus_name_id` ) REFERENCES `nscc_bonus_name` ( `bonus_name_id` ) ON UPDATE CASCADE;

