 ALTER TABLE `nscc_customers` ADD COLUMN `npwp`  varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `photo_kanan`

ALTER TABLE `nscc_customers` ADD COLUMN `npwp_nama`  varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `npwp`

ALTER TABLE `nscc_customers` ADD COLUMN `ktp`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `npwp_nama`

ALTER TABLE `nscc_customers` ADD COLUMN `fp_no`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `ktp`