DROP TRIGGER IF EXISTS `nscc_antrian_before_insert`;
CREATE TRIGGER `nscc_antrian_before_insert`
BEFORE INSERT ON `nscc_antrian`
FOR EACH ROW
  BEGIN
    IF NEW.id_antrian IS NULL OR LENGTH(NEW.id_antrian) = 0
    THEN
      SET NEW.id_antrian = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_bank_before_insert`;
CREATE TRIGGER `nscc_bank_before_insert`
BEFORE INSERT ON `nscc_bank`
FOR EACH ROW
  BEGIN
    IF NEW.bank_id IS NULL OR LENGTH(NEW.bank_id) = 0
    THEN
      SET NEW.bank_id = UUID();
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_bank_before_update`;
CREATE TRIGGER `nscc_bank_before_update`
BEFORE UPDATE ON `nscc_bank`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_bank <=> NEW.nama_bank AND OLD.ket <=> NEW.ket
         AND OLD.account_code <=> NEW.account_code AND OLD.store <=> NEW.store
         AND OLD.visible <=> NEW.visible)
    THEN
      SET NEW.up = 0;
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_bank_trans_before_insert`;
CREATE TRIGGER `nscc_bank_trans_before_insert`
BEFORE INSERT ON `nscc_bank_trans`
FOR EACH ROW
  BEGIN
    IF NEW.bank_trans_id IS NULL OR LENGTH(NEW.bank_trans_id) = 0
    THEN
      SET NEW.bank_trans_id = UUID();
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_bank_trans_before_update`;
CREATE TRIGGER `nscc_bank_trans_before_update`
BEFORE UPDATE ON `nscc_bank_trans`
FOR EACH ROW
  BEGIN
    IF !(OLD.type_ <=> NEW.type_ AND OLD.trans_no <=> NEW.trans_no
         AND OLD.ref <=> NEW.ref AND OLD.tgl <=> NEW.tgl
         AND OLD.amount <=> NEW.amount AND OLD.id_user <=> NEW.id_user
         AND OLD.tdate <=> NEW.tdate AND OLD.bank_id <=> NEW.bank_id
         AND OLD.store <=> NEW.store AND OLD.visible <=> NEW.visible)
    THEN
      SET NEW.up = 0;
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_barang_before_insert`;
CREATE TRIGGER `nscc_barang_before_insert`
BEFORE INSERT ON `nscc_barang`
FOR EACH ROW
  BEGIN
    IF NEW.barang_id IS NULL OR LENGTH(NEW.barang_id) = 0
    THEN
      SET NEW.barang_id = UUID();
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_barang_before_update`;
CREATE TRIGGER `nscc_barang_before_update`
BEFORE UPDATE ON `nscc_barang`
FOR EACH ROW
  BEGIN
    IF !(OLD.kode_barang <=> NEW.kode_barang AND OLD.nama_barang <=> NEW.nama_barang
         AND OLD.ket <=> NEW.ket AND OLD.grup_id <=> NEW.grup_id
         AND OLD.active <=> NEW.active AND OLD.sat <=> NEW.sat
         AND OLD.tipe_barang_id <=> NEW.tipe_barang_id)
    THEN
      SET NEW.up = 0;
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_barang_aset_before_insert`;
CREATE TRIGGER `nscc_barang_aset_before_insert`
BEFORE INSERT ON `nscc_barang_asset`
FOR EACH ROW
  BEGIN
    IF NEW.barang_asset_id IS NULL OR LENGTH(NEW.barang_asset_id) = 0
    THEN
      SET NEW.barang_asset_id = UUID();
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_barang_asset_before_update`;
CREATE TRIGGER `nscc_barang_asset_before_update`
BEFORE UPDATE ON `nscc_barang_asset`
FOR EACH ROW
  BEGIN
    IF !(OLD.kode_barang_asset <=> NEW.kode_barang_asset
         AND OLD.nama_barang_asset <=> NEW.nama_barang_asset
         AND OLD.ket_asset <=> NEW.ket_asset
         AND OLD.satuan <=> NEW.satuan)
    THEN
      SET NEW.up = 0;
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_barang_clinical_before_insert`;
CREATE TRIGGER `nscc_barang_clinical_before_insert`
BEFORE INSERT ON `nscc_barang_clinical`
FOR EACH ROW
  BEGIN
    IF NEW.barang_clinical IS NULL OR LENGTH(NEW.barang_clinical) = 0
    THEN
      SET NEW.barang_clinical = UUID();
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_barang_perlengkapan_before_insert`;
CREATE TRIGGER `nscc_barang_perlengkapan_before_insert`
BEFORE INSERT ON `nscc_barang_perlengkapan`
FOR EACH ROW
  BEGIN
    IF NEW.barang_perlengkapan_id IS NULL OR LENGTH(NEW.barang_perlengkapan_id) = 0
    THEN
      SET NEW.barang_perlengkapan_id = UUID();
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_barang_perlengkapanid_before_update`;
CREATE TRIGGER `nscc_barang_perlengkapanid_before_update`
BEFORE UPDATE ON `nscc_barang_perlengkapan`
FOR EACH ROW
  BEGIN
    IF !(OLD.kode_brg_perlengkapan <=> NEW.kode_brg_perlengkapan
         AND OLD.nama_brg_perlengkapan <=> NEW.nama_brg_perlengkapan
         AND OLD.ket_brg_perlengkapan <=> NEW.ket_brg_perlengkapan
         AND OLD.satuan_perlengkapan <=> NEW.satuan_perlengkapan)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_beauty_services_before_insert`;
CREATE TRIGGER `nscc_beauty_services_before_insert`
BEFORE INSERT ON `nscc_beauty_services`
FOR EACH ROW
  BEGIN
    IF NEW.nscc_beauty_services_id IS NULL OR LENGTH(NEW.nscc_beauty_services_id) = 0
    THEN
      SET NEW.nscc_beauty_services_id = UUID();
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_beauty_services_before_update`;
CREATE TRIGGER `nscc_beauty_services_before_update`
BEFORE UPDATE ON `nscc_beauty_services`
FOR EACH ROW
  BEGIN
    IF !(OLD.total <=> NEW.total AND OLD.beauty_id <=> NEW.beauty_id
         AND OLD.salestrans_details <=> NEW.salestrans_details AND OLD.final <=> NEW.final)
    THEN
      SET NEW.up = 0;
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_beli_before_insert`;
CREATE TRIGGER `nscc_beli_before_insert`
BEFORE INSERT ON `nscc_beli`
FOR EACH ROW
  BEGIN
    IF NEW.beli_id IS NULL OR LENGTH(NEW.beli_id) = 0
    THEN
      SET NEW.beli_id = UUID();
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_beli_before_update`;
CREATE TRIGGER `nscc_beli_before_update`
BEFORE UPDATE ON `nscc_beli`
FOR EACH ROW
  BEGIN
    IF !(OLD.price <=> NEW.price AND OLD.tax <=> NEW.tax
         AND OLD.store <=> NEW.store AND OLD.barang_id <=> NEW.barang_id)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_card_before_insert`;
CREATE TRIGGER `nscc_card_before_insert`
BEFORE INSERT ON `nscc_card`
FOR EACH ROW
  BEGIN
    IF NEW.card_id IS NULL OR LENGTH(NEW.card_id) = 0
    THEN
      SET NEW.card_id = UUID();
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_card_before_update`;
CREATE TRIGGER `nscc_card_before_update`
BEFORE UPDATE ON `nscc_card`
FOR EACH ROW
  BEGIN
    IF !(OLD.card_name <=> NEW.card_name AND OLD.persen_fee <=> NEW.persen_fee)
    THEN
      SET NEW.up = 0;
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_chart_class_before_update`;
CREATE TRIGGER `nscc_chart_class_before_update`
BEFORE UPDATE ON `nscc_chart_class`
FOR EACH ROW
  BEGIN
    IF !(OLD.class_name <=> NEW.class_name AND OLD.class_name <=> NEW.class_name
         AND OLD.ctype <=> NEW.ctype)
    THEN
      SET NEW.up = 0;
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_chart_master_before_update`;
CREATE TRIGGER `nscc_chart_master_before_update`
BEFORE UPDATE ON `nscc_chart_master`
FOR EACH ROW
  BEGIN
    IF !(OLD.account_code <=> NEW.account_code AND OLD.account_name <=> NEW.account_name
         AND OLD.status <=> NEW.status AND OLD.description <=> NEW.description
         AND OLD.kategori <=> NEW.kategori AND OLD.header <=> NEW.header
         AND OLD.saldo_normal <=> NEW.saldo_normal AND OLD.tipe <=> NEW.tipe)
    THEN
      SET NEW.up = 0;
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_chart_types_before_insert`;
CREATE TRIGGER `nscc_chart_types_before_insert`
BEFORE INSERT ON `nscc_chart_types`
FOR EACH ROW
  BEGIN
    IF NEW.id IS NULL OR LENGTH(NEW.id) = 0
    THEN
      SET NEW.id = UUID();
    END IF;
  END;
DROP TRIGGER IF EXISTS `nscc_chart_types_before_update`;
CREATE TRIGGER `nscc_chart_types_before_update`
BEFORE UPDATE ON `nscc_chart_types`
FOR EACH ROW
  BEGIN
    IF !(OLD.name <=> NEW.name AND OLD.class_id <=> NEW.class_id
         AND OLD.parent <=> NEW.parent AND OLD.seq <=> NEW.seq)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_clinical_trans_before_insert`;
CREATE TRIGGER `nscc_clinical_trans_before_insert`
BEFORE INSERT ON `nscc_clinical_trans`
FOR EACH ROW
  BEGIN
    IF NEW.clinical_trans_id IS NULL OR LENGTH(NEW.clinical_trans_id) = 0
    THEN
      SET NEW.clinical_trans_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_clinical_trans_detail_before_insert`;
CREATE TRIGGER `nscc_clinical_trans_detail_before_insert`
BEFORE INSERT ON `nscc_clinical_trans_detail`
FOR EACH ROW
  BEGIN
    IF NEW.clinical_trans_detail_id IS NULL OR LENGTH(NEW.clinical_trans_detail_id) = 0
    THEN
      SET NEW.clinical_trans_detail_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_comments_before_insert`;
CREATE TRIGGER `nscc_comments_before_insert`
BEFORE INSERT ON `nscc_comments`
FOR EACH ROW
  BEGIN
    IF NEW.id IS NULL OR LENGTH(NEW.id) = 0
    THEN
      SET NEW.id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_comments_before_update`;
CREATE TRIGGER `nscc_comments_before_update`
BEFORE UPDATE ON `nscc_comments`
FOR EACH ROW
  BEGIN
    IF !(OLD.type <=> NEW.type AND OLD.type_no <=> NEW.type_no
         AND OLD.date_ <=> NEW.date_ AND OLD.memo_ <=> NEW.memo_)
    THEN
      SET NEW.up = 0;
    END IF;
  END;


DROP TRIGGER IF EXISTS `nscc_cost_barang_perlengkapan_before_insert`;
CREATE TRIGGER `nscc_cost_barang_perlengkapan_before_insert`
BEFORE INSERT ON `nscc_cost_barang_perlengkapan`
FOR EACH ROW
  BEGIN
    IF NEW.cost_id IS NULL OR LENGTH(NEW.cost_id) = 0
    THEN
      SET NEW.cost_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_customers_before_insert`;
CREATE TRIGGER `nscc_customers_before_insert`
BEFORE INSERT ON `nscc_customers`
FOR EACH ROW
  BEGIN
    IF NEW.customer_id IS NULL OR LENGTH(NEW.customer_id) = 0
    THEN
      SET NEW.customer_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_customers_before_update`;
CREATE TRIGGER `nscc_customers_before_update`
BEFORE UPDATE ON `nscc_customers`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_customer <=> NEW.nama_customer AND OLD.no_customer <=> NEW.no_customer
         AND OLD.tempat_lahir <=> NEW.tempat_lahir AND OLD.tgl_lahir <=> NEW.tgl_lahir
         AND OLD.email <=> NEW.email AND OLD.telp <=> NEW.telp
         AND OLD.alamat <=> NEW.alamat AND OLD.awal <=> NEW.awal
         AND OLD.akhir <=> NEW.akhir AND OLD.kecamatan_id <=> NEW.kecamatan_id
         AND OLD.status_cust_id <=> NEW.status_cust_id AND OLD.store <=> NEW.store
         AND OLD.sex <=> NEW.sex AND OLD.kerja <=> NEW.kerja
         AND OLD.cardtype <=> NEW.cardtype AND OLD.validcard <=> NEW.validcard
         AND OLD.printcarddate <=> NEW.printcarddate AND OLD.locprintcard <=> NEW.locprintcard
         AND OLD.kota_id <=> NEW.kota_id AND OLD.provinsi_id <=> NEW.provinsi_id
         AND OLD.negara_id <=> NEW.negara_id AND OLD.friend_id <=> NEW.friend_id
         AND OLD.info_id <=> NEW.info_id)
    THEN
      SET NEW.up = 0;
      SET NEW.`log` = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_diskon_before_insert`;
CREATE TRIGGER `nscc_diskon_before_insert`
BEFORE INSERT ON `nscc_diskon`
FOR EACH ROW
  BEGIN
    IF NEW.diskon_id IS NULL OR LENGTH(NEW.diskon_id) = 0
    THEN
      SET NEW.diskon_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_diskon_before_update`;
CREATE TRIGGER `nscc_diskon_before_update`
BEFORE UPDATE ON `nscc_diskon`
FOR EACH ROW
  BEGIN
    IF !(OLD.ref_id <=> NEW.ref_id AND OLD.barang_id <=> NEW.barang_id
         AND OLD.value <=> NEW.value AND OLD.store <=> NEW.store)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_tipe_employee_before_insert`;
CREATE TRIGGER `nscc_tipe_employee_before_insert`
BEFORE INSERT ON `nscc_tipe_employee`
FOR EACH ROW
  BEGIN
    IF NEW.tipe_employee_id IS NULL OR LENGTH(NEW.tipe_employee_id) = 0
    THEN
      SET NEW.tipe_employee_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_employees_before_insert`;
CREATE TRIGGER `nscc_employees_before_insert`
BEFORE INSERT ON `nscc_employees`
FOR EACH ROW
  BEGIN
    IF NEW.employee_id IS NULL OR LENGTH(NEW.employee_id) = 0
    THEN
      SET NEW.employee_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_employees_before_update`;
CREATE TRIGGER `nscc_employees_before_update`
BEFORE UPDATE ON `nscc_employees`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_employee <=> NEW.nama_employee AND OLD.gol_id <=> NEW.gol_id
         AND OLD.kode_employee <=> NEW.kode_employee AND OLD.active <=> NEW.active
         AND OLD.store <=> NEW.store AND OLD.tipe <=> NEW.tipe)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_event_before_insert`;
CREATE TRIGGER `nscc_event_before_insert`
BEFORE INSERT ON `nscc_event`
FOR EACH ROW
  BEGIN
    IF NEW.event_id IS NULL OR LENGTH(NEW.event_id) = 0
    THEN
      SET NEW.event_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_gl_trans_before_insert`;
CREATE TRIGGER `nscc_gl_trans_before_insert`
BEFORE INSERT ON `nscc_gl_trans`
FOR EACH ROW
  BEGIN
    IF NEW.counter IS NULL OR LENGTH(NEW.counter) = 0
    THEN
      SET NEW.counter = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_gl_trans_before_update`;
CREATE TRIGGER `nscc_gl_trans_before_update`
BEFORE UPDATE ON `nscc_gl_trans`
FOR EACH ROW
  BEGIN
    IF !(OLD.type <=> NEW.type AND OLD.type_no <=> NEW.type_no
         AND OLD.tran_date <=> NEW.tran_date AND OLD.memo_ <=> NEW.memo_
         AND OLD.amount <=> NEW.amount AND OLD.id_user <=> NEW.id_user
         AND OLD.account_code <=> NEW.account_code AND OLD.store <=> NEW.store
         AND OLD.tdate <=> NEW.tdate AND OLD.cf <=> NEW.cf
         AND OLD.visible <=> NEW.visible)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_gol_before_insert`;
CREATE TRIGGER `nscc_gol_before_insert`
BEFORE INSERT ON `nscc_gol`
FOR EACH ROW
  BEGIN
    IF NEW.gol_id IS NULL OR LENGTH(NEW.gol_id) = 0
    THEN
      SET NEW.gol_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_gol_before_update`;
CREATE TRIGGER `nscc_gol_before_update`
BEFORE UPDATE ON `nscc_gol`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_gol <=> NEW.nama_gol)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_group_asset_before_insert`;
CREATE TRIGGER `nscc_group_asset_before_insert`
BEFORE INSERT ON `nscc_group_asset`
FOR EACH ROW
  BEGIN
    IF NEW.group_asset_id IS NULL OR LENGTH(NEW.group_asset_id) = 0
    THEN
      SET NEW.group_asset_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_group_asset_before_update`;
CREATE TRIGGER `nscc_group_asset_before_update`
BEFORE UPDATE ON `nscc_group_asset`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_group_asset <=> NEW.nama_group_asset)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_grup_before_insert`;
CREATE TRIGGER `nscc_grup_before_insert`
BEFORE INSERT ON `nscc_grup`
FOR EACH ROW
  BEGIN
    IF NEW.grup_id IS NULL OR LENGTH(NEW.grup_id) = 0
    THEN
      SET NEW.grup_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_grup_before_update`;
CREATE TRIGGER `nscc_grup_before_update`
BEFORE UPDATE ON `nscc_grup`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_grup <=> NEW.nama_grup AND OLD.kategori_id <=> NEW.kategori_id)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_grup_attr_before_insert`;
CREATE TRIGGER `nscc_grup_attr_before_insert`
BEFORE INSERT ON `nscc_grup_attr`
FOR EACH ROW
  BEGIN
    IF NEW.grup_attr_id IS NULL OR LENGTH(NEW.grup_attr_id) = 0
    THEN
      SET NEW.grup_attr_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_grup_attr_before_update`;
CREATE TRIGGER `nscc_grup_attr_before_update`
BEFORE UPDATE ON `nscc_grup_attr`
FOR EACH ROW
  BEGIN
    IF !(OLD.vat <=> NEW.vat AND OLD.tax <=> NEW.tax
         AND OLD.coa_jual <=> NEW.coa_jual AND OLD.coa_sales_disc <=> NEW.coa_sales_disc
         AND OLD.coa_sales_hpp <=> NEW.coa_sales_hpp AND OLD.coa_purchase <=> NEW.coa_purchase
         AND OLD.coa_purchase_return <=> NEW.coa_purchase_return AND OLD.coa_purchase_disc <=> NEW.coa_purchase_disc
         AND OLD.store <=> NEW.store AND OLD.grup_id <=> NEW.grup_id)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_info_before_insert`;
CREATE TRIGGER `nscc_info_before_insert`
BEFORE INSERT ON `nscc_info`
FOR EACH ROW
  BEGIN
    IF NEW.info_id IS NULL OR LENGTH(NEW.info_id) = 0
    THEN
      SET NEW.info_id = UUID();
    END IF;
  END;


DROP TRIGGER IF EXISTS `nscc_jual_before_insert`;
CREATE TRIGGER `nscc_jual_before_insert`
BEFORE INSERT ON `nscc_jual`
FOR EACH ROW
  BEGIN
    IF NEW.jual_id IS NULL OR LENGTH(NEW.jual_id) = 0
    THEN
      SET NEW.jual_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_jual_before_update`;
CREATE TRIGGER `nscc_jual_before_update`
BEFORE UPDATE ON `nscc_jual`
FOR EACH ROW
  BEGIN
    IF !(OLD.price <=> NEW.price AND OLD.cost <=> NEW.cost
         AND OLD.store <=> NEW.store AND OLD.barang_id <=> NEW.barang_id
         AND OLD.disc <=> NEW.disc AND OLD.discrp <=> NEW.discrp
         AND OLD.duration <=> NEW.duration)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_kas_before_insert`;
CREATE TRIGGER `nscc_kas_before_insert`
BEFORE INSERT ON `nscc_kas`
FOR EACH ROW
  BEGIN
    IF NEW.kas_id IS NULL OR LENGTH(NEW.kas_id) = 0
    THEN
      SET NEW.kas_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_kas_before_update`;
CREATE TRIGGER `nscc_kas_before_update`
BEFORE UPDATE ON `nscc_kas`
FOR EACH ROW
  BEGIN
    IF !(OLD.doc_ref <=> NEW.doc_ref AND OLD.no_kwitansi <=> NEW.no_kwitansi
         AND OLD.keperluan <=> NEW.keperluan AND OLD.total <=> NEW.total
         AND OLD.bank_id <=> NEW.bank_id AND OLD.tgl <=> NEW.tgl
         AND OLD.user_id <=> NEW.user_id AND OLD.tdate <=> NEW.tdate
         AND OLD.type_ <=> NEW.type_ AND OLD.arus <=> NEW.arus
         AND OLD.visible <=> NEW.visible AND OLD.store <=> NEW.store)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_kas_detail_before_insert`;
CREATE TRIGGER `nscc_kas_detail_before_insert`
BEFORE INSERT ON `nscc_kas_detail`
FOR EACH ROW
  BEGIN
    IF NEW.kas_detail_id IS NULL OR LENGTH(NEW.kas_detail_id) = 0
    THEN
      SET NEW.kas_detail_id = UUID();
    END IF;
    UPDATE nscc_kas nk
    SET nk.up = 0
    WHERE nk.kas_id = NEW.kas_id;
  END;

DROP TRIGGER IF EXISTS `nscc_kas_detail_before_update`;
CREATE TRIGGER `nscc_kas_detail_before_update`
BEFORE UPDATE ON `nscc_kas_detail`
FOR EACH ROW
  BEGIN
    IF !(OLD.item_name <=> NEW.item_name AND OLD.total <=> NEW.total
         AND OLD.kas_id <=> NEW.kas_id AND OLD.account_code <=> NEW.account_code)
    THEN
      UPDATE nscc_kas nk
      SET nk.up = 0
      WHERE nk.kas_id = NEW.kas_id;
      UPDATE nscc_kas nk
      SET nk.up = 0
      WHERE nk.kas_id = OLD.kas_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_kategori_before_insert`;
CREATE TRIGGER `nscc_kategori_before_insert`
BEFORE INSERT ON `nscc_kategori`
FOR EACH ROW
  BEGIN
    IF NEW.kategori_id IS NULL OR LENGTH(NEW.kategori_id) = 0
    THEN
      SET NEW.kategori_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_kategori_before_update`;
CREATE TRIGGER `nscc_kategori_before_update`
BEFORE UPDATE ON `nscc_kategori`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_kategori <=> NEW.nama_kategori AND OLD.have_stock <=> NEW.have_stock)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_kategori_clinical_before_insert`;
CREATE TRIGGER `nscc_kategori_clinical_before_insert`
BEFORE INSERT ON `nscc_kategori_clinical`
FOR EACH ROW
  BEGIN
    IF NEW.kategori_clinical_id IS NULL OR LENGTH(NEW.kategori_clinical_id) = 0
    THEN
      SET NEW.kategori_clinical_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_kecamatan_before_insert`;
CREATE TRIGGER `nscc_kecamatan_before_insert`
BEFORE INSERT ON `nscc_kecamatan`
FOR EACH ROW
  BEGIN
    IF NEW.kecamatan_id IS NULL OR LENGTH(NEW.kecamatan_id) = 0
    THEN
      SET NEW.kecamatan_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_kecamatan_before_update`;
CREATE TRIGGER `nscc_kecamatan_before_update`
BEFORE UPDATE ON `nscc_kecamatan`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_kecamatan <=> NEW.nama_kecamatan AND OLD.kota_id <=> NEW.kota_id)
    THEN
      SET NEW.up = 0;
    END IF;
  END;


DROP TRIGGER IF EXISTS `nscc_kota_before_insert`;
CREATE TRIGGER `nscc_kota_before_insert`
BEFORE INSERT ON `nscc_kota`
FOR EACH ROW
  BEGIN
    IF NEW.kota_id IS NULL OR LENGTH(NEW.kota_id) = 0
    THEN
      SET NEW.kota_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_kota_before_update`;
CREATE TRIGGER `nscc_kota_before_update`
BEFORE UPDATE ON `nscc_kota`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_kota <=> NEW.nama_kota AND OLD.provinsi_id <=> NEW.provinsi_id)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_mgm_before_insert`;
CREATE TRIGGER `nscc_mgm_before_insert`
BEFORE INSERT ON `nscc_mgm`
FOR EACH ROW
  BEGIN
    IF NEW.mgm_id IS NULL OR LENGTH(NEW.mgm_id) = 0
    THEN
      SET NEW.mgm_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_msd_before_insert`;
CREATE TRIGGER `nscc_msd_before_insert`
BEFORE INSERT ON `nscc_msd`
FOR EACH ROW
  BEGIN
    IF NEW.msd_id IS NULL OR LENGTH(NEW.msd_id) = 0
    THEN
      SET NEW.msd_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_negara_before_insert`;
CREATE TRIGGER `nscc_negara_before_insert`
BEFORE INSERT ON `nscc_negara`
FOR EACH ROW
  BEGIN
    IF NEW.negara_id IS NULL OR LENGTH(NEW.negara_id) = 0
    THEN
      SET NEW.negara_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_negara_before_update`;
CREATE TRIGGER `nscc_negara_before_update`
BEFORE UPDATE ON `nscc_negara`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_negara <=> NEW.nama_negara)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_paket_before_insert`;
CREATE TRIGGER `nscc_paket_before_insert`
BEFORE INSERT ON `nscc_paket`
FOR EACH ROW
  BEGIN
    IF NEW.paket_id IS NULL OR LENGTH(NEW.paket_id) = 0
    THEN
      SET NEW.paket_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_paket_before_update`;
CREATE TRIGGER `nscc_paket_before_update`
BEFORE UPDATE ON `nscc_paket`
FOR EACH ROW
  BEGIN
    IF !(OLD.paket_name <=> NEW.paket_name AND OLD.regular_price <=> NEW.regular_price
         AND OLD.proposed_price <=> NEW.proposed_price AND OLD.total_disc <=> NEW.total_disc
         AND OLD.disc <=> NEW.disc AND OLD.store <=> NEW.store)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_paket_barang_before_insert`;
CREATE TRIGGER `nscc_paket_barang_before_insert`
BEFORE INSERT ON `nscc_paket_barang`
FOR EACH ROW
  BEGIN
    IF NEW.paket_barang_id IS NULL OR LENGTH(NEW.paket_barang_id) = 0
    THEN
      SET NEW.paket_barang_id = UUID();
    END IF;
    UPDATE nscc_paket np INNER JOIN nscc_paket_details npd ON npd.paket_id = np.paket_id
    SET np.up = 0
    WHERE npd.paket_details_id = NEW.paket_details_id;
  END;

DROP TRIGGER IF EXISTS `nscc_paket_barang_after_delete`;
CREATE TRIGGER `nscc_paket_barang_after_delete`
AFTER DELETE ON `nscc_paket_barang`
FOR EACH ROW
  BEGIN
    IF (@DISABLE_TRIGGERS IS NULL)
    THEN
      UPDATE nscc_paket np INNER JOIN nscc_paket_details npd ON npd.paket_id = np.paket_id
      SET np.up = 0
      WHERE npd.paket_details_id = OLD.paket_details_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_paket_details_before_insert`;
CREATE TRIGGER `nscc_paket_details_before_insert`
BEFORE INSERT ON `nscc_paket_details`
FOR EACH ROW
  BEGIN
    IF NEW.paket_details_id IS NULL OR LENGTH(NEW.paket_details_id) = 0
    THEN
      SET NEW.paket_details_id = UUID();
    END IF;
    UPDATE nscc_paket np
    SET np.up = 0
    WHERE np.paket_id = NEW.paket_id;
  END;

DROP TRIGGER IF EXISTS `nscc_paket_details_before_update`;
CREATE TRIGGER `nscc_paket_details_before_update`
BEFORE UPDATE ON `nscc_paket_details`
FOR EACH ROW
  BEGIN
    IF !(OLD.kategori_name <=> NEW.kategori_name AND OLD.paket_id <=> NEW.paket_id
         AND OLD.price <=> NEW.price)
    THEN
      UPDATE nscc_paket np
      SET np.up = 0
      WHERE np.paket_id = NEW.paket_id;
      UPDATE nscc_paket np
      SET np.up = 0
      WHERE np.paket_id = OLD.paket_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_paket_details_after_delete`;
CREATE TRIGGER `nscc_paket_details_after_delete`
AFTER DELETE ON `nscc_paket_details`
FOR EACH ROW
  BEGIN
    IF (@DISABLE_TRIGGERS IS NULL)
    THEN
      UPDATE nscc_paket np
      SET np.up = 0
      WHERE np.paket_id = OLD.paket_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_paket_trans_before_insert`;
CREATE TRIGGER `nscc_paket_trans_before_insert`
BEFORE INSERT ON `nscc_paket_trans`
FOR EACH ROW
  BEGIN
    IF NEW.paket_trans_id IS NULL OR LENGTH(NEW.paket_trans_id) = 0
    THEN
      SET NEW.paket_trans_id = UUID();
    END IF;
    UPDATE nscc_salestrans ns
    SET ns.up = 0
    WHERE ns.salestrans_id = NEW.salestrans_id;
  END;

DROP TRIGGER IF EXISTS `nscc_paket_trans_before_update`;
CREATE TRIGGER `nscc_paket_trans_before_update`
BEFORE UPDATE ON `nscc_paket_trans`
FOR EACH ROW
  BEGIN
    IF !(OLD.paket_id <=> NEW.paket_id AND OLD.salestrans_id <=> NEW.salestrans_id
         AND OLD.qty <=> NEW.qty)
    THEN
      UPDATE nscc_salestrans ns
      SET ns.up = 0
      WHERE ns.salestrans_id = NEW.salestrans_id;
      UPDATE nscc_salestrans ns
      SET ns.up = 0
      WHERE ns.salestrans_id = OLD.salestrans_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_payment_before_insert`;
CREATE TRIGGER `nscc_payment_before_insert`
BEFORE INSERT ON `nscc_payment`
FOR EACH ROW
  BEGIN
    IF NEW.payment_id IS NULL OR LENGTH(NEW.payment_id) = 0
    THEN
      SET NEW.payment_id = UUID();
    END IF;
    UPDATE nscc_salestrans ns
    SET ns.up = 0
    WHERE ns.salestrans_id = NEW.salestrans_id;
  END;

DROP TRIGGER IF EXISTS `nscc_payment_before_update`;
CREATE TRIGGER `nscc_payment_before_update`
BEFORE UPDATE ON `nscc_payment`
FOR EACH ROW
  BEGIN
    IF !(OLD.card_number <=> NEW.card_number AND OLD.bank_id <=> NEW.bank_id
         AND OLD.salestrans_id <=> NEW.salestrans_id AND OLD.amount <=> NEW.amount
         AND OLD.kembali <=> NEW.kembali AND OLD.card_id <=> NEW.card_id)
    THEN
      UPDATE nscc_salestrans ns
      SET ns.up = 0
      WHERE ns.salestrans_id = NEW.salestrans_id;
      UPDATE nscc_salestrans ns
      SET ns.up = 0
      WHERE ns.salestrans_id = OLD.salestrans_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_pelunasan_utang_before_insert`;
CREATE TRIGGER `nscc_pelunasan_utang_before_insert`
BEFORE INSERT ON `nscc_pelunasan_utang`
FOR EACH ROW
  BEGIN
    IF NEW.pelunasan_utang_id IS NULL OR LENGTH(NEW.pelunasan_utang_id) = 0
    THEN
      SET NEW.pelunasan_utang_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_pelunasan_utang_before_update`;
CREATE TRIGGER `nscc_pelunasan_utang_before_update`
BEFORE UPDATE ON `nscc_pelunasan_utang`
FOR EACH ROW
  BEGIN
    IF !(OLD.total <=> NEW.total AND OLD.no_bg_cek <=> NEW.no_bg_cek
         AND OLD.no_bukti <=> NEW.no_bukti AND OLD.no_bg_cek <=> NEW.no_bg_cek
         AND OLD.doc_ref <=> NEW.doc_ref AND OLD.tgl <=> NEW.tgl
         AND OLD.bank_id <=> NEW.bank_id AND OLD.supplier_id <=> NEW.supplier_id
         AND OLD.store <=> NEW.store AND OLD.tdate <=> NEW.tdate
         AND OLD.user_id <=> NEW.user_id)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_pelunasan_utang_detil_before_insert`;
CREATE TRIGGER `nscc_pelunasan_utang_detil_before_insert`
BEFORE INSERT ON `nscc_pelunasan_utang_detil`
FOR EACH ROW
  BEGIN
    IF NEW.pelunasan_utang_detil_id IS NULL OR LENGTH(NEW.pelunasan_utang_detil_id) = 0
    THEN
      SET NEW.pelunasan_utang_detil_id = UUID();
    END IF;
    UPDATE nscc_pelunasan_utang npu
    SET npu.up = 0
    WHERE npu.pelunasan_utang_id = NEW.pelunasan_utang_id;
  END;

DROP TRIGGER IF EXISTS `nscc_pelunasan_utang_detil_before_update`;
CREATE TRIGGER `nscc_pelunasan_utang_detil_before_update`
BEFORE UPDATE ON `nscc_pelunasan_utang_detil`
FOR EACH ROW
  BEGIN
    IF !(OLD.kas_dibayar <=> NEW.kas_dibayar AND OLD.no_faktur <=> NEW.no_faktur
         AND OLD.sisa <=> NEW.sisa AND OLD.pelunasan_utang_id <=> NEW.pelunasan_utang_id
         AND OLD.transfer_item_id <=> NEW.transfer_item_id)
    THEN
      UPDATE nscc_pelunasan_utang npu
      SET npu.up = 0
      WHERE npu.pelunasan_utang_id = NEW.pelunasan_utang_id;
      UPDATE nscc_pelunasan_utang npu
      SET npu.up = 0
      WHERE npu.pelunasan_utang_id = OLD.pelunasan_utang_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_point_trans_before_insert`;
CREATE TRIGGER `nscc_point_trans_before_insert`
BEFORE INSERT ON `nscc_point_trans`
FOR EACH ROW
  BEGIN
    IF NEW.point_trans_id IS NULL OR LENGTH(NEW.point_trans_id) = 0
    THEN
      SET NEW.point_trans_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_price_before_insert`;
CREATE TRIGGER `nscc_price_before_insert`
BEFORE INSERT ON `nscc_price`
FOR EACH ROW
  BEGIN
    IF NEW.price_id IS NULL OR LENGTH(NEW.price_id) = 0
    THEN
      SET NEW.price_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_price_before_update`;
CREATE TRIGGER `nscc_price_before_update`
BEFORE UPDATE ON `nscc_price`
FOR EACH ROW
  BEGIN
    IF !(OLD.value <=> NEW.value AND OLD.barang_id <=> NEW.barang_id
         AND OLD.gol_id <=> NEW.gol_id AND OLD.store <=> NEW.store)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_printz_before_insert`;
CREATE TRIGGER `nscc_printz_before_insert`
BEFORE INSERT ON `nscc_printz`
FOR EACH ROW
  BEGIN
    IF NEW.prntz_id IS NULL OR LENGTH(NEW.prntz_id) = 0
    THEN
      SET NEW.prntz_id = UUID();
    END IF;
    UPDATE nscc_tender nt
    SET nt.up = 0
    WHERE nt.tender_id = NEW.tender_id;
  END;

DROP TRIGGER IF EXISTS `nscc_printz_before_update`;
CREATE TRIGGER `nscc_printz_before_update`
BEFORE UPDATE ON `nscc_printz`
FOR EACH ROW
  BEGIN
    IF !(OLD.store <=> NEW.store AND OLD.date_ <=> NEW.date_
         AND OLD.employee <=> NEW.employee AND OLD.sales <=> NEW.sales
         AND OLD.tender_id <=> NEW.tender_id AND OLD.return_sales <=> NEW.return_sales
         AND OLD.tax_sales <=> NEW.tax_sales AND OLD.disc_sales <=> NEW.disc_sales
         AND OLD.tendered <=> NEW.tendered AND OLD.change_ <=> NEW.change_
         AND OLD.started <=> NEW.started AND OLD.added <=> NEW.added
         AND OLD.removed <=> NEW.removed AND OLD.counted <=> NEW.counted
         AND OLD.status_ <=> NEW.status_)
    THEN
      UPDATE nscc_tender nt INNER JOIN nscc_printz np ON np.tender_id = nt.tender_id
      SET nt.up = 0
      WHERE np.prntz_id = NEW.prntz_id;
      UPDATE nscc_tender nt INNER JOIN nscc_printz np ON np.tender_id = nt.tender_id
      SET nt.up = 0
      WHERE np.prntz_id = OLD.prntz_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_printz_details_before_insert`;
CREATE TRIGGER `nscc_printz_details_before_insert`
BEFORE INSERT ON `nscc_printz_details`
FOR EACH ROW
  BEGIN
    IF NEW.printz_details_id IS NULL OR LENGTH(NEW.printz_details_id) = 0
    THEN
      SET NEW.printz_details_id = UUID();
    END IF;
    UPDATE nscc_tender nt INNER JOIN nscc_printz np ON np.tender_id = nt.tender_id
    SET nt.up = 0
    WHERE np.prntz_id = NEW.prntz_id;
  END;

DROP TRIGGER IF EXISTS `nscc_printz_details_before_update`;
CREATE TRIGGER `nscc_printz_details_before_update`
BEFORE UPDATE ON `nscc_printz_details`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_bank <=> NEW.nama_bank AND OLD.cash <=> NEW.cash
         AND OLD.started <=> NEW.started AND OLD.added <=> NEW.added
         AND OLD.collected <=> NEW.collected AND OLD.removed <=> NEW.removed
         AND OLD.counted <=> NEW.counted AND OLD.status_ <=> NEW.status_
         AND OLD.prntz_id <=> NEW.prntz_id)
    THEN
      UPDATE nscc_tender nt INNER JOIN nscc_printz np ON np.tender_id = nt.tender_id
      SET nt.up = 0
      WHERE np.prntz_id = NEW.prntz_id;
      UPDATE nscc_tender nt INNER JOIN nscc_printz np ON np.tender_id = nt.tender_id
      SET nt.up = 0
      WHERE np.prntz_id = OLD.prntz_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_produksi_before_insert`;
CREATE TRIGGER `nscc_produksi_before_insert`
BEFORE INSERT ON `nscc_produksi`
FOR EACH ROW
  BEGIN
    IF NEW.produksi_id IS NULL OR LENGTH(NEW.produksi_id) = 0
    THEN
      SET NEW.produksi_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_produksi_detil_before_insert`;
CREATE TRIGGER `nscc_produksi_detil_before_insert`
BEFORE INSERT ON `nscc_produksi_detil`
FOR EACH ROW
  BEGIN
    IF NEW.produksi_detil_id IS NULL OR LENGTH(NEW.produksi_detil_id) = 0
    THEN
      SET NEW.produksi_detil_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_promo_generic_before_insert`;
CREATE TRIGGER `nscc_promo_generic_before_insert`
BEFORE INSERT ON `nscc_promo_generic`
FOR EACH ROW
  BEGIN
    IF NEW.promo_generic_id IS NULL OR LENGTH(NEW.promo_generic_id) = 0
    THEN
      SET NEW.promo_generic_id = UUID();
    END IF;
  END;


DROP TRIGGER IF EXISTS `nscc_provinsi_before_insert`;
CREATE TRIGGER `nscc_provinsi_before_insert`
BEFORE INSERT ON `nscc_provinsi`
FOR EACH ROW
  BEGIN
    IF NEW.provinsi_id IS NULL OR LENGTH(NEW.provinsi_id) = 0
    THEN
      SET NEW.provinsi_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_provinsi_before_update`;
CREATE TRIGGER `nscc_provinsi_before_update`
BEFORE UPDATE ON `nscc_provinsi`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_provinsi <=> NEW.nama_provinsi AND OLD.negara_id <=> NEW.negara_id)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_refs_before_insert`;
CREATE TRIGGER `nscc_refs_before_insert`
BEFORE INSERT ON `nscc_refs`
FOR EACH ROW
  BEGIN
    IF NEW.refs_id IS NULL OR LENGTH(NEW.refs_id) = 0
    THEN
      SET NEW.refs_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_refs_before_update`;
CREATE TRIGGER `nscc_refs_before_update`
BEFORE UPDATE ON `nscc_refs`
FOR EACH ROW
  BEGIN
    IF !(OLD.type_no <=> NEW.type_no AND OLD.type_ <=> NEW.type_
         AND OLD.reference <=> NEW.reference)
    THEN
      SET NEW.up = 0;
    END IF;
  END;


DROP TRIGGER IF EXISTS `nscc_salestrans_before_insert`;
CREATE TRIGGER `nscc_salestrans_before_insert`
BEFORE INSERT ON `nscc_salestrans`
FOR EACH ROW
  BEGIN
    IF NEW.salestrans_id IS NULL OR LENGTH(NEW.salestrans_id) = 0
    THEN
      SET NEW.salestrans_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_salestrans_before_update`;
CREATE TRIGGER `nscc_salestrans_before_update`
BEFORE UPDATE ON `nscc_salestrans`
FOR EACH ROW
  BEGIN
    IF !(OLD.tgl <=> NEW.tgl AND OLD.doc_ref <=> NEW.doc_ref
         AND OLD.tdate <=> NEW.tdate AND OLD.user_id <=> NEW.user_id
         AND OLD.bruto <=> NEW.bruto AND OLD.disc <=> NEW.disc
         AND OLD.discrp <=> NEW.discrp AND OLD.ketdisc <=> NEW.ketdisc
         AND OLD.vat <=> NEW.vat AND OLD.totalpot <=> NEW.totalpot
         AND OLD.total <=> NEW.total AND OLD.customer_id <=> NEW.customer_id
         AND OLD.doc_ref_sales <=> NEW.doc_ref_sales AND OLD.store <=> NEW.store
         AND OLD.printed <=> NEW.printed AND OLD.override <=> NEW.override
         AND OLD.bayar <=> NEW.bayar AND OLD.kembali <=> NEW.kembali
         AND OLD.dokter_id <=> NEW.dokter_id AND OLD.total_discrp1 <=> NEW.total_discrp1
         AND OLD.type_ <=> NEW.type_ AND OLD.rounding <=> NEW.rounding
         AND OLD.retur_note <=> NEW.retur_note AND OLD.parent_id <=> NEW.parent_id
         AND OLD.kmr_persen <=> NEW.kmr_persen AND OLD.kmr_total <=> NEW.kmr_total
         AND OLD.start_at <=> NEW.start_at AND OLD.end_at <=> NEW.end_at
         AND OLD.pending <=> NEW.pending AND OLD.end_estimate <=> NEW.end_estimate)
    THEN
      SET NEW.up = 0;
    END IF;
  END;


DROP TRIGGER IF EXISTS `nscc_salestrans_details_before_insert`;
CREATE TRIGGER `nscc_salestrans_details_before_insert`
BEFORE INSERT ON `nscc_salestrans_details`
FOR EACH ROW
  BEGIN
    IF NEW.salestrans_details IS NULL OR LENGTH(NEW.salestrans_details) = 0
    THEN
      SET NEW.salestrans_details = UUID();
    END IF;
    UPDATE nscc_salestrans ns
    SET ns.up = 0
    WHERE ns.salestrans_id = NEW.salestrans_id;
  END;

DROP TRIGGER IF EXISTS `nscc_salestrans_details_before_update`;
CREATE TRIGGER `nscc_salestrans_details_before_update`
BEFORE UPDATE ON `nscc_salestrans_details`
FOR EACH ROW
  BEGIN
    IF !(OLD.barang_id <=> NEW.barang_id AND OLD.salestrans_id <=> NEW.salestrans_id
         AND OLD.qty <=> NEW.qty AND OLD.disc <=> NEW.disc
         AND OLD.discrp <=> NEW.discrp AND OLD.ketpot <=> NEW.ketpot
         AND OLD.vat <=> NEW.vat AND OLD.vatrp <=> NEW.vatrp
         AND OLD.bruto <=> NEW.bruto AND OLD.total <=> NEW.total
         AND OLD.total_pot <=> NEW.total_pot AND OLD.price <=> NEW.price
         AND OLD.jasa_dokter <=> NEW.jasa_dokter AND OLD.dokter_id <=> NEW.dokter_id
         AND OLD.disc_name <=> NEW.disc_name AND OLD.hpp <=> NEW.hpp
         AND OLD.cost <=> NEW.cost AND OLD.disc1 <=> NEW.disc1
         AND OLD.discrp1 <=> NEW.discrp1 AND OLD.paket_trans_id <=> NEW.paket_trans_id
         AND OLD.paket_details_id <=> NEW.paket_details_id AND OLD.durasi <=> NEW.durasi)
    THEN
      UPDATE nscc_salestrans ns
      SET ns.up = 0
      WHERE ns.salestrans_id = NEW.salestrans_id;
      UPDATE nscc_salestrans ns
      SET ns.up = 0
      WHERE ns.salestrans_id = OLD.salestrans_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_security_roles_before_insert`;
CREATE TRIGGER `nscc_security_roles_before_insert`
BEFORE INSERT ON `nscc_security_roles`
FOR EACH ROW
  BEGIN
    IF NEW.security_roles_id IS NULL OR LENGTH(NEW.security_roles_id) = 0
    THEN
      SET NEW.security_roles_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_security_roles_before_update`;
CREATE TRIGGER `nscc_security_roles_before_update`
BEFORE UPDATE ON `nscc_security_roles`
FOR EACH ROW
  BEGIN
    IF !(OLD.role <=> NEW.role AND OLD.ket <=> NEW.ket
         AND OLD.sections <=> NEW.sections)
    THEN
      SET NEW.up = 0;
    END IF;
  END;


DROP TRIGGER IF EXISTS `nscc_souvenir_before_insert`;
CREATE TRIGGER `nscc_souvenir_before_insert`
BEFORE INSERT ON `nscc_souvenir`
FOR EACH ROW
  BEGIN
    IF NEW.souvenir_id IS NULL OR LENGTH(NEW.souvenir_id) = 0
    THEN
      SET NEW.souvenir_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_souvenir_out_before_insert`;
CREATE TRIGGER `nscc_souvenir_out_before_insert`
BEFORE INSERT ON `nscc_souvenir_out`
FOR EACH ROW
  BEGIN
    IF NEW.souvenir_out_id IS NULL OR LENGTH(NEW.souvenir_out_id) = 0
    THEN
      SET NEW.souvenir_out_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_souvenir_trans_before_insert`;
CREATE TRIGGER `nscc_souvenir_trans_before_insert`
BEFORE INSERT ON `nscc_souvenir_trans`
FOR EACH ROW
  BEGIN
    IF NEW.souvenir_trans_id IS NULL OR LENGTH(NEW.souvenir_trans_id) = 0
    THEN
      SET NEW.souvenir_trans_id = UUID();
    END IF;
  END;


DROP TRIGGER IF EXISTS `nscc_status_cust_before_insert`;
CREATE TRIGGER `nscc_status_cust_before_insert`
BEFORE INSERT ON `nscc_status_cust`
FOR EACH ROW
  BEGIN
    IF NEW.status_cust_id IS NULL OR LENGTH(NEW.status_cust_id) = 0
    THEN
      SET NEW.status_cust_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_status_cust_before_update`;
CREATE TRIGGER `nscc_status_cust_before_update`
BEFORE UPDATE ON `nscc_status_cust`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_status <=> NEW.nama_status)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_stock_moves_before_insert`;
CREATE TRIGGER `nscc_stock_moves_before_insert`
BEFORE INSERT ON `nscc_stock_moves`
FOR EACH ROW
  BEGIN
    IF NEW.stock_moves_id IS NULL OR LENGTH(NEW.stock_moves_id) = 0
    THEN
      SET NEW.stock_moves_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_stock_moves_before_update`;
CREATE TRIGGER `nscc_stock_moves_before_update`
BEFORE UPDATE ON `nscc_stock_moves`
FOR EACH ROW
  BEGIN
    IF !(OLD.type_no <=> NEW.type_no AND OLD.trans_no <=> NEW.trans_no
         AND OLD.tran_date <=> NEW.tran_date AND OLD.price <=> NEW.price
         AND OLD.reference <=> NEW.reference AND OLD.qty <=> NEW.qty
         AND OLD.barang_id <=> NEW.barang_id AND OLD.store <=> NEW.store
         AND OLD.tdate <=> NEW.tdate AND OLD.id_user <=> NEW.id_user)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_stock_moves_clinical_before_insert`;
CREATE TRIGGER `nscc_stock_moves_clinical_before_insert`
BEFORE INSERT ON `nscc_stock_moves_clinical`
FOR EACH ROW
  BEGIN
    IF NEW.stock_moves_clinical_id IS NULL OR LENGTH(NEW.stock_moves_clinical_id) = 0
    THEN
      SET NEW.stock_moves_clinical_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_stock_moves_perlengkapan_before_insert`;
CREATE TRIGGER `nscc_stock_moves_perlengkapan_before_insert`
BEFORE INSERT ON `nscc_stock_moves_perlengkapan`
FOR EACH ROW
  BEGIN
    IF NEW.stock_moves_id IS NULL OR LENGTH(NEW.stock_moves_id) = 0
    THEN
      SET NEW.stock_moves_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_stock_moves_perlengkapan_before_update`;
CREATE TRIGGER `nscc_stock_moves_perlengkapan_before_update`
BEFORE UPDATE ON `nscc_stock_moves_perlengkapan`
FOR EACH ROW
  BEGIN
    IF !(OLD.type_no <=> NEW.type_no AND OLD.trans_no <=> NEW.trans_no
         AND OLD.tran_date <=> NEW.tran_date AND OLD.price <=> NEW.price
         AND OLD.reference <=> NEW.reference AND OLD.qty <=> NEW.qty
         AND OLD.barang_perlengkapan_id <=> NEW.barang_perlengkapan_id AND OLD.store <=> NEW.store
         AND OLD.tdate <=> NEW.tdate AND OLD.id_user <=> NEW.id_user)
    THEN
      SET NEW.up = 0;
    END IF;
  END;


DROP TRIGGER IF EXISTS `nscc_store_before_update`;
CREATE TRIGGER `nscc_store_before_update`
BEFORE UPDATE ON `nscc_store`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_store <=> NEW.nama_store AND OLD.tipe <=> NEW.tipe
         AND OLD.tanggal_backup <=> NEW.tanggal_backup AND OLD.transaksi_flag <=> NEW.transaksi_flag
         AND OLD.id_cabang <=> NEW.id_cabang AND OLD.wilayah_id <=> NEW.wilayah_id AND OLD.beban_acc <=> NEW.beban_acc)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_supplier_before_insert`;
CREATE TRIGGER `nscc_supplier_before_insert`
BEFORE INSERT ON `nscc_supplier`
FOR EACH ROW
  BEGIN
    IF NEW.supplier_id IS NULL OR LENGTH(NEW.supplier_id) = 0
    THEN
      SET NEW.supplier_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_supplier_before_update`;
CREATE TRIGGER `nscc_supplier_before_update`
BEFORE UPDATE ON `nscc_supplier`
FOR EACH ROW
  BEGIN
    IF !(OLD.supplier_name <=> NEW.supplier_name AND OLD.account_code <=> NEW.account_code)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_sys_prefs_before_insert`;
CREATE TRIGGER `nscc_sys_prefs_before_insert`
BEFORE INSERT ON `nscc_sys_prefs`
FOR EACH ROW
  BEGIN
    IF NEW.sys_prefs_id IS NULL OR LENGTH(NEW.sys_prefs_id) = 0
    THEN
      SET NEW.sys_prefs_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_sys_prefs_before_update`;
CREATE TRIGGER `nscc_sys_prefs_before_update`
BEFORE UPDATE ON `nscc_sys_prefs`
FOR EACH ROW
  BEGIN
    IF !(OLD.name_ <=> NEW.name_ AND OLD.value_ <=> NEW.value_
         AND OLD.store <=> NEW.store)
    THEN
      IF NEW.name_ = "sync_active"
      THEN
        SET NEW.up = 1;
      ELSE
        SET NEW.up = 0;
      END IF;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_tender_before_insert`;
CREATE TRIGGER `nscc_tender_before_insert`
BEFORE INSERT ON `nscc_tender`
FOR EACH ROW
  BEGIN
    IF NEW.tender_id IS NULL OR LENGTH(NEW.tender_id) = 0
    THEN
      SET NEW.tender_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_tender_before_update`;
CREATE TRIGGER `nscc_tender_before_update`
BEFORE UPDATE ON `nscc_tender`
FOR EACH ROW
  BEGIN
    IF !(OLD.tgl <=> NEW.tgl AND OLD.doc_ref <=> NEW.doc_ref
         AND OLD.user_id <=> NEW.user_id AND OLD.tdate <=> NEW.tdate
         AND OLD.total <=> NEW.total AND OLD.store <=> NEW.store)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_tender_details_before_insert`;
CREATE TRIGGER `nscc_tender_details_before_insert`
BEFORE INSERT ON `nscc_tender_details`
FOR EACH ROW
  BEGIN
    IF NEW.tender_details_id IS NULL OR LENGTH(NEW.tender_details_id) = 0
    THEN
      SET NEW.tender_details_id = UUID();
    END IF;
    UPDATE nscc_tender nt
    SET nt.up = 0
    WHERE nt.tender_id = NEW.tender_id;
  END;

DROP TRIGGER IF EXISTS `nscc_tender_details_before_update`;
CREATE TRIGGER `nscc_tender_details_before_update`
BEFORE UPDATE ON `nscc_tender_details`
FOR EACH ROW
  BEGIN
    IF !(OLD.tender_id <=> NEW.tender_id AND OLD.bank_id <=> NEW.bank_id
         AND OLD.amount <=> NEW.amount)
    THEN
      UPDATE nscc_tender nt
      SET nt.up = 0
      WHERE nt.tender_id = NEW.tender_id;
      UPDATE nscc_tender nt
      SET nt.up = 0
      WHERE nt.tender_id = OLD.tender_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_terima_barang_before_insert`;
CREATE TRIGGER `nscc_terima_barang_before_insert`
BEFORE INSERT ON `nscc_terima_barang`
FOR EACH ROW
  BEGIN
    IF NEW.terima_barang_id IS NULL OR LENGTH(NEW.terima_barang_id) = 0
    THEN
      SET NEW.terima_barang_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_terima_barang_before_update`;
CREATE TRIGGER `nscc_terima_barang_before_update`
BEFORE UPDATE ON `nscc_terima_barang`
FOR EACH ROW
  BEGIN
    IF !(OLD.po_id <=> NEW.po_id AND OLD.doc_ref <=> NEW.doc_ref
         AND OLD.id_user <=> NEW.id_user AND OLD.tdate <=> NEW.tdate AND OLD.tgl <=> NEW.tgl
         AND OLD.store <=> NEW.store)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_terima_barang_details_before_insert`;
CREATE TRIGGER `nscc_terima_barang_details_before_insert`
BEFORE INSERT ON `nscc_terima_barang_details`
FOR EACH ROW
  BEGIN
    IF NEW.terima_barang_details_id IS NULL OR LENGTH(NEW.terima_barang_details_id) = 0
    THEN
      SET NEW.terima_barang_details_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_terima_barang_details_before_update`;
CREATE TRIGGER `nscc_terima_barang_details_before_update`
BEFORE UPDATE ON `nscc_terima_barang_details`
FOR EACH ROW
  BEGIN
    IF !(OLD.qty <=> NEW.qty AND OLD.description <=> NEW.description
         AND OLD.seq <=> NEW.seq AND OLD.visible <=> NEW.visible
         AND OLD.terima_barang_id <=> NEW.terima_barang_id
         AND OLD.seq <=> NEW.seq)
    THEN
      UPDATE nscc_terima_barang nt
      SET nt.up = 0
      WHERE nt.terima_barang_id = NEW.terima_barang_id;
      UPDATE nscc_terima_barang nt
      SET nt.up = 0
      WHERE nt.terima_barang_id = OLD.terima_barang_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_tipe_barang_before_update`;
CREATE TRIGGER `nscc_tipe_barang_before_update`
BEFORE UPDATE ON `nscc_tipe_barang`
FOR EACH ROW
  BEGIN
    IF !(OLD.nama_tipe <=> NEW.nama_tipe AND OLD.coa <=> NEW.coa)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_tipe_trans_clinical_before_insert`;
CREATE TRIGGER `nscc_tipe_trans_clinical_before_insert`
BEFORE INSERT ON `nscc_tipe_trans_clinical`
FOR EACH ROW
  BEGIN
    IF NEW.tipe_clinical_id IS NULL OR LENGTH(NEW.tipe_clinical_id) = 0
    THEN
      SET NEW.tipe_clinical_id = UUID();
    END IF;
  END;


DROP TRIGGER IF EXISTS `nscc_trans_tipe_before_update`;
CREATE TRIGGER `nscc_trans_tipe_before_update`
BEFORE UPDATE ON `nscc_trans_tipe`
FOR EACH ROW
  BEGIN
    IF !(OLD.tipe_name <=> NEW.tipe_name)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_transfer_barang_before_insert`;
CREATE TRIGGER `nscc_transfer_barang_before_insert`
BEFORE INSERT ON `nscc_transfer_barang`
FOR EACH ROW
  BEGIN
    IF NEW.transfer_barang_id IS NULL OR LENGTH(NEW.transfer_barang_id) = 0
    THEN
      SET NEW.transfer_barang_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_transfer_barang_before_update`;
CREATE TRIGGER `nscc_transfer_barang_before_update`
BEFORE UPDATE ON `nscc_transfer_barang`
FOR EACH ROW
  BEGIN
    IF !(OLD.tgl <=> NEW.tgl
         AND OLD.doc_ref <=> NEW.doc_ref
         AND OLD.note <=> NEW.note
         AND OLD.tdate <=> NEW.tdate
         AND OLD.doc_ref_other <=> NEW.doc_ref_other
         AND OLD.user_id <=> NEW.user_id
         AND OLD.type_ <=> NEW.type_
         AND OLD.store <=> NEW.store
         AND OLD.total <=> NEW.total
         AND OLD.disc <=> NEW.disc
         AND OLD.discrp <=> NEW.discrp
         AND OLD.bruto <=> NEW.bruto
         AND OLD.vat <=> NEW.vat
         AND OLD.tgl_jatuh_tempo <=> NEW.tgl_jatuh_tempo
         AND OLD.lunas <=> NEW.lunas
         AND OLD.total_pot <=> NEW.total_pot AND OLD.total_discrp1 <=> NEW.total_discrp1)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_transfer_barang_asset_before_update`;
CREATE TRIGGER `nscc_transfer_barang_asset_before_update`
BEFORE UPDATE ON `nscc_transfer_barang_asset`
FOR EACH ROW
  BEGIN
    IF !(OLD.tgl <=> NEW.tgl AND OLD.doc_ref <=> NEW.doc_ref
         AND OLD.note <=> NEW.note AND OLD.tdate <=> NEW.tdate
         AND OLD.doc_ref_other <=> NEW.doc_ref_other AND OLD.user_id <=> NEW.user_id
         AND OLD.type_ <=> NEW.type_ AND OLD.store <=> NEW.store
         AND OLD.supplier_id <=> NEW.supplier_id AND OLD.total <=> NEW.total
         AND OLD.disc <=> NEW.disc AND OLD.discrp <=> NEW.discrp
         AND OLD.bruto <=> NEW.bruto AND OLD.vat <=> NEW.vat
         AND OLD.tgl_jatuh_tempo <=> NEW.tgl_jatuh_tempo AND OLD.lunas <=> NEW.lunas
         AND OLD.total_pot <=> NEW.total_pot AND OLD.total_discrp1 <=> NEW.total_discrp1)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_transfer_barang_details_before_insert`;
CREATE TRIGGER `nscc_transfer_barang_details_before_insert`
BEFORE INSERT ON `nscc_transfer_barang_details`
FOR EACH ROW
  BEGIN
    IF NEW.transfer_barang_detail_id IS NULL OR LENGTH(NEW.transfer_barang_detail_id) = 0
    THEN
      SET NEW.transfer_barang_detail_id = UUID();
    END IF;
    UPDATE nscc_transfer_barang nti
    SET nti.up = 0
    WHERE nti.transfer_barang_id = NEW.transfer_barang_id;
  END;

DROP TRIGGER IF EXISTS `nscc_transfer_barang_detail_before_update`;
CREATE TRIGGER `nscc_transfer_barang_detail_before_update`
BEFORE UPDATE ON `nscc_transfer_barang_details`
FOR EACH ROW
  BEGIN
    IF !(OLD.qty <=> NEW.qty AND OLD.barang_id <=> NEW.barang_id
         AND OLD.transfer_barang_id <=> NEW.transfer_barang_id
         AND OLD.total <=> NEW.total
         AND OLD.disc <=> NEW.disc
         AND OLD.discrp <=> NEW.discrp
         AND OLD.bruto <=> NEW.bruto
         AND OLD.vat <=> NEW.vat
         AND OLD.vatrp <=> NEW.vatrp
         AND OLD.disc1 <=> NEW.disc1
         AND OLD.discrp1 <=> NEW.discrp1
         AND OLD.total_pot <=> NEW.total_pot)
    THEN
      UPDATE nscc_transfer_barang nti
      SET nti.up = 0
      WHERE nti.transfer_barang_id = NEW.transfer_barang_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_transfer_barang_perlengkapan_before_insert`;
CREATE TRIGGER `nscc_transfer_barang_perlengkapan_before_insert`
BEFORE INSERT ON `nscc_transfer_barang_perlengkapan`
FOR EACH ROW
  BEGIN
    IF NEW.transfer_perlengkapan_id IS NULL OR LENGTH(NEW.transfer_perlengkapan_id) = 0
    THEN
      SET NEW.transfer_perlengkapan_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_transfer_barang_perlengkapan_before_update`;
CREATE TRIGGER `nscc_transfer_barang_perlengkapan_before_update`
BEFORE UPDATE ON `nscc_transfer_barang_perlengkapan`
FOR EACH ROW
  BEGIN
    IF !(OLD.tgl <=> NEW.tgl AND OLD.doc_ref <=> NEW.doc_ref
         AND OLD.note <=> NEW.note AND OLD.tdate <=> NEW.tdate
         AND OLD.doc_ref_other <=> NEW.doc_ref_other AND OLD.user_id <=> NEW.user_id
         AND OLD.type_ <=> NEW.type_ AND OLD.store <=> NEW.store
         AND OLD.supplier_id <=> NEW.supplier_id AND OLD.total <=> NEW.total
         AND OLD.disc <=> NEW.disc AND OLD.discrp <=> NEW.discrp
         AND OLD.bruto <=> NEW.bruto AND OLD.vat <=> NEW.vat
         AND OLD.tgl_jatuh_tempo <=> NEW.tgl_jatuh_tempo AND OLD.lunas <=> NEW.lunas
         AND OLD.total_pot <=> NEW.total_pot AND OLD.total_discrp1 <=> NEW.total_discrp1)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_transfer_barang_perlengkapan_details_before_insert`;
CREATE TRIGGER `nscc_transfer_barang_perlengkapan_details_before_insert`
BEFORE INSERT ON `nscc_transfer_barang_perlengkapan_details`
FOR EACH ROW
  BEGIN
    IF NEW.transfer_perlengkapan_details_id IS NULL OR LENGTH(NEW.transfer_perlengkapan_details_id) = 0
    THEN
      SET NEW.transfer_perlengkapan_details_id = UUID();
    END IF;
    UPDATE nscc_transfer_barang_perlengkapan nti
    SET nti.up = 0
    WHERE nti.transfer_perlengkapan_id = NEW.transfer_perlengkapan_id;
  END;

DROP TRIGGER IF EXISTS `nscc_transfer_barang_perlengkapan_details_before_update`;
CREATE TRIGGER `nscc_transfer_barang_perlengkapan_details_before_update`
BEFORE UPDATE ON `nscc_transfer_barang_perlengkapan_details`
FOR EACH ROW
  BEGIN
    IF !(OLD.qty <=> NEW.qty AND OLD.transfer_perlengkapan_id <=> NEW.transfer_perlengkapan_id
         AND OLD.transfer_perlengkapan_id <=> NEW.transfer_perlengkapan_id AND OLD.price <=> NEW.price
         AND OLD.total <=> NEW.total AND OLD.disc <=> NEW.disc
         AND OLD.discrp <=> NEW.discrp AND OLD.bruto <=> NEW.bruto
         AND OLD.vat <=> NEW.vat AND OLD.vatrp <=> NEW.vatrp
         AND OLD.disc1 <=> NEW.disc1 AND OLD.discrp1 <=> NEW.discrp1
         AND OLD.total_pot <=> NEW.total_pot)
    THEN
      UPDATE nscc_transfer_barang_perlengkapan nti
      SET nti.up = 0
      WHERE nti.transfer_perlengkapan_id = NEW.transfer_perlengkapan_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_transfer_item_before_insert`;
CREATE TRIGGER `nscc_transfer_item_before_insert`
BEFORE INSERT ON `nscc_transfer_item`
FOR EACH ROW
  BEGIN
    IF NEW.transfer_item_id IS NULL OR LENGTH(NEW.transfer_item_id) = 0
    THEN
      SET NEW.transfer_item_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_transfer_item_before_update`;
CREATE TRIGGER `nscc_transfer_item_before_update`
BEFORE UPDATE ON `nscc_transfer_item`
FOR EACH ROW
  BEGIN
    IF !(OLD.tgl <=> NEW.tgl AND OLD.doc_ref <=> NEW.doc_ref
         AND OLD.note <=> NEW.note AND OLD.tdate <=> NEW.tdate
         AND OLD.doc_ref_other <=> NEW.doc_ref_other AND OLD.user_id <=> NEW.user_id
         AND OLD.type_ <=> NEW.type_ AND OLD.store <=> NEW.store
         AND OLD.supplier_id <=> NEW.supplier_id AND OLD.total <=> NEW.total
         AND OLD.disc <=> NEW.disc AND OLD.discrp <=> NEW.discrp
         AND OLD.bruto <=> NEW.bruto AND OLD.vat <=> NEW.vat
         AND OLD.tgl_jatuh_tempo <=> NEW.tgl_jatuh_tempo AND OLD.lunas <=> NEW.lunas
         AND OLD.total_pot <=> NEW.total_pot AND OLD.total_discrp1 <=> NEW.total_discrp1)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_transfer_item_details_before_insert`;
CREATE TRIGGER `nscc_transfer_item_details_before_insert`
BEFORE INSERT ON `nscc_transfer_item_details`
FOR EACH ROW
  BEGIN
    IF NEW.transfer_item_details_id IS NULL OR LENGTH(NEW.transfer_item_details_id) = 0
    THEN
      SET NEW.transfer_item_details_id = UUID();
    END IF;
    UPDATE nscc_transfer_item nti
    SET nti.up = 0
    WHERE nti.transfer_item_id = NEW.transfer_item_id;
  END;

DROP TRIGGER IF EXISTS `nscc_transfer_item_details_before_update`;
CREATE TRIGGER `nscc_transfer_item_details_before_update`
BEFORE UPDATE ON `nscc_transfer_item_details`
FOR EACH ROW
  BEGIN
    IF !(OLD.qty <=> NEW.qty AND OLD.barang_id <=> NEW.barang_id
         AND OLD.transfer_item_id <=> NEW.transfer_item_id AND OLD.price <=> NEW.price
         AND OLD.total <=> NEW.total AND OLD.disc <=> NEW.disc
         AND OLD.discrp <=> NEW.discrp AND OLD.bruto <=> NEW.bruto
         AND OLD.vat <=> NEW.vat AND OLD.vatrp <=> NEW.vatrp
         AND OLD.disc1 <=> NEW.disc1 AND OLD.discrp1 <=> NEW.discrp1
         AND OLD.total_pot <=> NEW.total_pot)
    THEN
      UPDATE nscc_transfer_item nti
      SET nti.up = 0
      WHERE nti.transfer_item_id = NEW.transfer_item_id;
      UPDATE nscc_transfer_item nti
      SET nti.up = 0
      WHERE nti.transfer_item_id = OLD.transfer_item_id;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_user_employee_before_insert`;
CREATE TRIGGER `nscc_user_employee_before_insert`
BEFORE INSERT ON `nscc_user_employee`
FOR EACH ROW
  BEGIN
    IF NEW.user_employee_id IS NULL OR LENGTH(NEW.user_employee_id) = 0
    THEN
      SET NEW.user_employee_id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_users_before_insert`;
CREATE TRIGGER `nscc_users_before_insert`
BEFORE INSERT ON `nscc_users`
FOR EACH ROW
  BEGIN
    IF NEW.id IS NULL OR LENGTH(NEW.id) = 0
    THEN
      SET NEW.id = UUID();
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_users_before_update`;
CREATE TRIGGER `nscc_users_before_update`
BEFORE UPDATE ON `nscc_users`
FOR EACH ROW
  BEGIN
    IF !(OLD.user_id <=> NEW.user_id AND OLD.password <=> NEW.password
         AND OLD.last_visit_date <=> NEW.last_visit_date AND OLD.active <=> NEW.active
         AND OLD.security_roles_id <=> NEW.security_roles_id AND OLD.name <=> NEW.name
         AND OLD.store <=> NEW.store)
    THEN
      SET NEW.up = 0;
    END IF;
  END;

DROP TRIGGER IF EXISTS `nscc_wilayah_before_insert`;
CREATE TRIGGER `nscc_wilayah_before_insert`
BEFORE INSERT ON `nscc_wilayah`
FOR EACH ROW
  BEGIN
    IF NEW.wilayah_id IS NULL OR LENGTH(NEW.wilayah_id) = 0
    THEN
      SET NEW.wilayah_id = UUID();
    END IF;
  END;
