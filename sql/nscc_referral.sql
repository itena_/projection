/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : nox_pusat_1902

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2018-03-12 12:23:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for nscc_referral
-- ----------------------------
DROP TABLE IF EXISTS `nscc_referral`;
CREATE TABLE `nscc_referral` (
  `referral_id` varchar(36) NOT NULL,
  `referral_nama` varchar(255) DEFAULT NULL,
  `referral_no_hp` varchar(255) DEFAULT NULL,
  `referral_alamat` varchar(255) DEFAULT NULL,
  `referral_sosmed` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`referral_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
