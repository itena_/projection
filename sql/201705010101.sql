SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE `aisha_antrian` (
  `id_antrian`    VARCHAR(36)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NOT NULL,
  `nomor_pasien`  VARCHAR(100)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `customer_id`   VARCHAR(36)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `nama_customer` VARCHAR(100)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `alasan`        VARCHAR(255)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `nomor_antrian` INT(11)                    NULL     DEFAULT NULL,
  `hold`          TINYINT(4)                 NOT NULL DEFAULT 0,
  `bagian`        VARCHAR(20)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `counter`       VARCHAR(5)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `tanggal`       DATETIME                   NOT NULL,
  `timestamp`     TIMESTAMP                  NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `medis`         VARCHAR(5)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `end_`          TINYINT(4)                 NULL     DEFAULT NULL,
  PRIMARY KEY (`id_antrian`)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

CREATE TABLE `nscc_antrian` (
  `id_antrian` VARCHAR(36)
               CHARACTER SET utf8mb4
               COLLATE utf8mb4_general_ci NOT NULL,
  `nomor`      INT(11)                    NOT NULL,
  `counter`    VARCHAR(2)
               CHARACTER SET utf8mb4
               COLLATE utf8mb4_general_ci NOT NULL,
  `no_member`  VARCHAR(30)
               CHARACTER SET utf8mb4
               COLLATE utf8mb4_general_ci NOT NULL,
  `kartu`      VARCHAR(10)
               CHARACTER SET utf8mb4
               COLLATE utf8mb4_general_ci NOT NULL,
  `bagian`     ENUM ('pendaftaran', 'counter', 'medis', 'kasir', 'selesai')
               CHARACTER SET utf8mb4
               COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'pendaftaran',
  `tujuan`     ENUM ('konsultasi', 'pembelian')
               CHARACTER SET utf8mb4
               COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'konsultasi',
  `pending`    TINYINT(1) UNSIGNED        NOT NULL DEFAULT 0,
  `picked`     ENUM ('picked', 'pick')
               CHARACTER SET utf8mb4
               COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'pick',
  `tanggal`    DATETIME                   NOT NULL,
  `timestamp`  TIMESTAMP                  NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `spesial`    ENUM ('1', '0')
               CHARACTER SET utf8mb4
               COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_antrian`)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;


CREATE TABLE `nscc_antrian_history` (
  `antrian_history_id` VARCHAR(36)
                       CHARACTER SET utf8mb4
                       COLLATE utf8mb4_general_ci NOT NULL,
  `id_antrian`         VARCHAR(36)
                       CHARACTER SET utf8mb4
                       COLLATE utf8mb4_general_ci NOT NULL,
  `tanggal`            DATETIME                   NOT NULL,
  `bagian`             VARCHAR(255)
                       CHARACTER SET utf8mb4
                       COLLATE utf8mb4_general_ci NOT NULL,
  `counter`            VARCHAR(2)
                       CHARACTER SET utf8mb4
                       COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `action`             VARCHAR(50)
                       CHARACTER SET utf8mb4
                       COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `timestamp`          TIMESTAMP                  NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nomor_pasien`       VARCHAR(100)
                       CHARACTER SET utf8mb4
                       COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `nomor_antrian`      INT(11)                    NULL     DEFAULT NULL,
  PRIMARY KEY (`antrian_history_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;


CREATE TABLE `nscc_beauty_onduty` (
  `beauty_onduty_id` VARCHAR(36)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NOT NULL,
  `beauty_id`        VARCHAR(36)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NOT NULL,
  `note_`            VARCHAR(600)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tgl`              DATE                       NOT NULL,
  `time_start`       DATETIME                   NULL DEFAULT NULL,
  `estimate_end`     DATETIME                   NULL DEFAULT NULL,
  PRIMARY KEY (`beauty_onduty_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

ALTER TABLE `nscc_beauty_services`
  DROP FOREIGN KEY `nscc_beauty_services_ibfk_1`;

ALTER TABLE `nscc_beauty_services`
  ADD CONSTRAINT `nscc_beauty_services_ibfk_1` FOREIGN KEY (`beauty_id`) REFERENCES `nscc_employees` (`employee_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;


CREATE TABLE `nscc_bonus` (
  `bonus_id`    VARCHAR(36)
                CHARACTER SET utf8mb4
                COLLATE utf8mb4_general_ci NOT NULL,
  `tgl`         DATE                       NOT NULL,
  `employee_id` VARCHAR(50)
                CHARACTER SET utf8mb4
                COLLATE utf8mb4_general_ci NOT NULL,
  `bruto`       DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `type_`       INT(11)                    NOT NULL,
  `trans_no`    VARCHAR(50)
                CHARACTER SET utf8mb4
                COLLATE utf8mb4_general_ci NOT NULL,
  `tdate`       DATETIME                   NOT NULL,
  `id_user`     VARCHAR(50)
                CHARACTER SET utf8mb4
                COLLATE utf8mb4_general_ci NOT NULL,
  `dpp`         DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `total`       DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`bonus_id`),
  CONSTRAINT `fk_nscc_bonus` FOREIGN KEY (`employee_id`) REFERENCES `nscc_employees` (`employee_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_nscc_bonus` (`employee_id`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

ALTER TABLE `nscc_cost_barang_perlengkapan`
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci;


ALTER TABLE `nscc_customers`
  ADD COLUMN `thumb` LONGBLOB NULL
  AFTER `info_id`;

ALTER TABLE `nscc_customers`
  ADD COLUMN `photo` LONGBLOB NULL
  AFTER `thumb`;


CREATE TABLE `nscc_diagnosa` (
  `diagnosa_id` VARCHAR(36)
                CHARACTER SET utf8mb4
                COLLATE utf8mb4_general_ci NOT NULL,
  `diag`        VARCHAR(255)
                CHARACTER SET utf8mb4
                COLLATE utf8mb4_general_ci NOT NULL,
  `note_`       VARCHAR(600)
                CHARACTER SET utf8mb4
                COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `konsul_id`   VARCHAR(36)
                CHARACTER SET utf8mb4
                COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`diagnosa_id`),
  CONSTRAINT `fk_nscc_diagnosa` FOREIGN KEY (`konsul_id`) REFERENCES `nscc_konsul` (`konsul_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_nscc_diagnosa` (`konsul_id`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

ALTER TABLE `nscc_diskon`
  DROP FOREIGN KEY `nscc_diskon_ibfk_1`;

ALTER TABLE `nscc_diskon`
  ADD COLUMN `ref_id` VARCHAR(36)
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci NOT NULL
  AFTER `diskon_id`;

ALTER TABLE `nscc_diskon`
  ADD COLUMN `type_` TINYINT(4) NOT NULL DEFAULT 0
COMMENT '0 = diskon status customer\n1 = diskon promo'
  AFTER `up`;

ALTER TABLE `nscc_diskon`
  DROP COLUMN `status_cust_id`;

DROP INDEX `idx_nscc_diskon_1`
ON `nscc_diskon`;

CREATE UNIQUE INDEX `idx_nscc_diskon_1`
  ON `nscc_diskon` (`ref_id`, `barang_id`, `store`, `type_`) USING BTREE;

#DROP INDEX `idx_nscc_diskon` ON `nscc_diskon`;

CREATE INDEX `idx_nscc_diskon`
  ON `nscc_diskon` (`ref_id`) USING BTREE;

CREATE TABLE `nscc_tipe_employee` (
  `tipe_employee_id` VARCHAR(36)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NOT NULL,
  `kode`             VARCHAR(20)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_`            VARCHAR(100)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`tipe_employee_id`),
  UNIQUE INDEX `kode` (`kode`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of nscc_tipe_employee
-- ----------------------------
INSERT INTO `nscc_tipe_employee` VALUES ('be3df0cd-19c9-11e7-a9fe-201a069ec688', 'BT', 'Beautician');
INSERT INTO `nscc_tipe_employee` VALUES ('c50d12cb-2347-11e7-843e-201a069ec688', 'FO', 'FrontOffice');
INSERT INTO `nscc_tipe_employee` VALUES ('c7a1441e-19c9-11e7-a9fe-201a069ec688', 'DK', 'Doctor');
INSERT INTO `nscc_tipe_employee` VALUES ('e936d183-3c3b-11e7-88a3-0e00452769c9', 'APT', 'Apoteker');
INSERT INTO `nscc_tipe_employee` VALUES ('f32e3bab-3c3b-11e7-88a3-0e00452769c9', 'PC', 'Pengelola');

CREATE TABLE `nscc_employees` (
  `employee_id`   VARCHAR(50)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NOT NULL,
  `nama_employee` VARCHAR(100)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NOT NULL,
  `gol_id`        VARCHAR(36)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NOT NULL,
  `kode_employee` VARCHAR(20)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NOT NULL,
  `active`        TINYINT(4)                 NOT NULL DEFAULT 1,
  `store`         VARCHAR(20)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NOT NULL,
  `up`            TINYINT(3) UNSIGNED        NOT NULL DEFAULT 0,
  `tipe`          VARCHAR(36)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `nickname`      VARCHAR(100)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  PRIMARY KEY (`employee_id`),
  CONSTRAINT `nscc_employees_ibfk_1` FOREIGN KEY (`gol_id`) REFERENCES `nscc_gol` (`gol_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  UNIQUE INDEX `idx_nscc_beauty` (`nama_employee`, `store`) USING BTREE,
  INDEX `idx_nsc_beauty` (`gol_id`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

INSERT INTO `nscc_employees` (`employee_id`, `nama_employee`, `gol_id`, `kode_employee`, `active`, `store`, `up`, `tipe`, `nickname`)
  SELECT
    nb.beauty_id,
    nb.nama_beauty,
    nb.gol_id,
    nb.kode_beauty,
    nb.active,
    nb.store,
    nb.up,
    'be3df0cd-19c9-11e7-a9fe-201a069ec688',
    ''
  FROM nscc_beauty AS nb;

INSERT INTO `nscc_employees` (`employee_id`, `nama_employee`, `gol_id`, `kode_employee`, `active`, `store`, `up`, `tipe`, `nickname`)
  SELECT
    nb.dokter_id,
    nb.nama_dokter,
    2,
    '',
    nb.active,
    nb.store,
    nb.up,
    'c7a1441e-19c9-11e7-a9fe-201a069ec688',
    ''
  FROM nscc_dokter AS nb;

ALTER TABLE `nscc_jual`
  ADD COLUMN `duration` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0
COMMENT 'second'
  AFTER `barang_id`;

CREATE TABLE `nscc_konsul` (
  `konsul_id`     VARCHAR(36)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NOT NULL,
  `no_antrian`    INT(11)                    NULL     DEFAULT NULL,
  `customer_id`   VARCHAR(36)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `id_antrian`    VARCHAR(36)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `tgl`           DATE                       NOT NULL,
  `doc_ref`       VARCHAR(50)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NOT NULL,
  `tdate`         DATETIME                   NOT NULL,
  `user_id`       VARCHAR(50)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NOT NULL,
  `ref_id`        VARCHAR(50)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `salestrans_id` VARCHAR(50)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `counter`       VARCHAR(5)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `medis`         VARCHAR(5)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `dokter_id`     VARCHAR(50)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `beauty_id`     VARCHAR(50)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `type_`         TINYINT(4)                 NOT NULL DEFAULT 0
  COMMENT '0 = dari modul dokter, 1= tambahan beauty, 2 = tambahan dokter',
  `store`         VARCHAR(20)
                  CHARACTER SET utf8mb4
                  COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  PRIMARY KEY (`konsul_id`),
  CONSTRAINT `fk_nscc_konsul` FOREIGN KEY (`customer_id`) REFERENCES `nscc_customers` (`customer_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_nscc_konsul` (`customer_id`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

CREATE TABLE `nscc_konsul_detil` (
  `konsul_detil_id` VARCHAR(36)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NOT NULL,
  `barang_id`       VARCHAR(36)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NOT NULL,
  `qty`             INT(11)                    NULL     DEFAULT 0,
  `disc`            DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `discrp`          DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `ketpot`          VARCHAR(255)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NOT NULL DEFAULT '-',
  `vat`             DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `vatrp`           DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `bruto`           DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `total`           DECIMAL(30, 2)             NOT NULL DEFAULT 0.00
  COMMENT 'netto',
  `total_pot`       DECIMAL(30, 2)             NULL     DEFAULT NULL,
  `price`           DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `disc_name`       VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `hpp`             DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `cost`            DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `disc1`           DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `discrp1`         DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `konsul_id`       VARCHAR(36)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NOT NULL,
  `used`            INT(11)                    NOT NULL DEFAULT 0,
  PRIMARY KEY (`konsul_detil_id`),
  CONSTRAINT `fk_nscc_konsul_detil` FOREIGN KEY (`konsul_id`) REFERENCES `nscc_konsul` (`konsul_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_nscc_salestrans_details_1` (`barang_id`) USING BTREE,
  INDEX `idx_nscc_konsul_detil` (`konsul_id`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

CREATE TABLE `nscc_promosi` (
  `promosi_id`   VARCHAR(36)
                 CHARACTER SET utf8mb4
                 COLLATE utf8mb4_general_ci NOT NULL,
  `nama_promosi` VARCHAR(100)
                 CHARACTER SET utf8mb4
                 COLLATE utf8mb4_general_ci NOT NULL,
  `awal`         DATE                       NULL DEFAULT NULL,
  `akhir`        DATE                       NULL DEFAULT NULL,
  `time_awal`    TIME                       NULL DEFAULT NULL,
  `time_akhir`   TIME                       NULL DEFAULT NULL,
  `note_`        VARCHAR(600)
                 CHARACTER SET utf8mb4
                 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kode_promosi` VARCHAR(5)
                 CHARACTER SET utf8mb4
                 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`promosi_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;


CREATE TABLE `nscc_resep` (
  `resep_id`   VARCHAR(36)
               CHARACTER SET utf8mb4
               COLLATE utf8mb4_general_ci NOT NULL,
  `nama_resep` VARCHAR(100)
               CHARACTER SET utf8mb4
               COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `barang_id`  VARCHAR(36)
               CHARACTER SET utf8mb4
               COLLATE utf8mb4_general_ci NOT NULL,
  `up`         TINYINT(4)                 NOT NULL DEFAULT 0,
  PRIMARY KEY (`resep_id`),
  CONSTRAINT `fk_nscc_resep` FOREIGN KEY (`barang_id`) REFERENCES `nscc_barang` (`barang_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  UNIQUE INDEX `barang_id` (`barang_id`) USING BTREE,
  INDEX `idx_nscc_resep` (`barang_id`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

CREATE TABLE `nscc_resep_details` (
  `resep_details_id` VARCHAR(36)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NOT NULL,
  `nama_bahan`       VARCHAR(100)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NOT NULL,
  `qty`              DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `sat`              VARCHAR(20)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NOT NULL,
  `resep_id`         VARCHAR(36)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NOT NULL,
  `kode_bahan`       VARCHAR(20)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  PRIMARY KEY (`resep_details_id`),
  CONSTRAINT `fk_nscc_resep_details` FOREIGN KEY (`resep_id`) REFERENCES `nscc_resep` (`resep_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_nscc_resep_details` (`resep_id`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

CREATE TABLE `nscc_resep_payment` (
  `resep_payment_id` VARCHAR(36)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NOT NULL,
  `resep_print_id`   VARCHAR(36)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NOT NULL,
  `nama_bank`        VARCHAR(100)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `amount`           DECIMAL(30, 2)             NULL DEFAULT NULL,
  PRIMARY KEY (`resep_payment_id`),
  CONSTRAINT `fk_nscc_resep_payment` FOREIGN KEY (`resep_print_id`) REFERENCES `nscc_resep_print` (`resep_print_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_nscc_resep_payment` (`resep_print_id`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

CREATE TABLE `nscc_resep_print` (
  `resep_print_id`  VARCHAR(36)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NOT NULL,
  `doc_ref_resep`   VARCHAR(50)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `apoteker`        VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `stocker`         VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pasien_name`     VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat`          VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `dokter`          VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `salestrans_id`   VARCHAR(50)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NOT NULL,
  `header0`         VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `header1`         VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `header2`         VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `header3`         VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `header4`         VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `header5`         VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `doc_ref_receipt` VARCHAR(50)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `date_`           DATE                       NULL DEFAULT NULL,
  `pasien_code`     VARCHAR(50)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kasir`           VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `subtotal`        DECIMAL(30, 2)             NULL DEFAULT NULL,
  `disc`            DECIMAL(30, 2)             NULL DEFAULT NULL,
  `vat`             DECIMAL(30, 2)             NULL DEFAULT NULL,
  `rounding`        DECIMAL(30, 2)             NULL DEFAULT NULL,
  `amount_due`      DECIMAL(30, 2)             NULL DEFAULT NULL,
  `kembalian`       DECIMAL(30, 2)             NULL DEFAULT NULL,
  `footer0`         VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `footer1`         VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `footer2`         VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `footer3`         VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `footer4`         VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `footer5`         VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `beauty`          VARCHAR(100)
                    CHARACTER SET utf8mb4
                    COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`resep_print_id`),
  CONSTRAINT `fk_nscc_resep_trans` FOREIGN KEY (`salestrans_id`) REFERENCES `nscc_salestrans` (`salestrans_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_nscc_resep_trans` (`salestrans_id`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

CREATE TABLE `nscc_resep_print_bahan` (
  `resep_print_bahan_id`   VARCHAR(36)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_general_ci NOT NULL,
  `resep_print_details_id` VARCHAR(36)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_general_ci NOT NULL,
  `no_urut`                INT(11)                    NULL     DEFAULT NULL,
  `kode_bahan`             VARCHAR(20)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `nama_bahan`             VARCHAR(100)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_general_ci NOT NULL,
  `qty`                    DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `sat`                    VARCHAR(20)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`resep_print_bahan_id`),
  CONSTRAINT `fk_nscc_resep_print_bahan` FOREIGN KEY (`resep_print_details_id`) REFERENCES `nscc_resep_print_details` (`resep_print_details_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_nscc_resep_print_bahan` (`resep_print_details_id`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

CREATE TABLE `nscc_resep_print_details` (
  `resep_print_details_id` VARCHAR(36)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_general_ci NOT NULL,
  `nama_item`              VARCHAR(100)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_general_ci NOT NULL,
  `qty`                    DECIMAL(30, 2)             NOT NULL DEFAULT 0.00,
  `sat`                    VARCHAR(20)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_general_ci NOT NULL,
  `resep_print_id`         VARCHAR(36)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_general_ci NOT NULL,
  `kode_item`              VARCHAR(20)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_general_ci NULL     DEFAULT NULL,
  `no_urut`                INT(11)                    NULL     DEFAULT NULL,
  `price`                  DECIMAL(30, 2)             NULL     DEFAULT NULL,
  `total`                  DECIMAL(30, 2)             NULL     DEFAULT NULL,
  `r_`                     TINYINT(4)                 NOT NULL DEFAULT 0,
  `salestrans_details`     VARCHAR(50)
                           CHARACTER SET utf8mb4
                           COLLATE utf8mb4_general_ci NOT NULL,
  `vatrp`                  DECIMAL(30, 2)             NULL     DEFAULT NULL,
  `discrp`                 DECIMAL(30, 2)             NULL     DEFAULT NULL,
  `no_resep`               INT(11)                    NULL     DEFAULT NULL,
  `stok`                   TINYINT(1)                 NOT NULL DEFAULT 0,
  PRIMARY KEY (`resep_print_details_id`),
  CONSTRAINT `fk_nscc_resep_print_details` FOREIGN KEY (`resep_print_id`) REFERENCES `nscc_resep_print` (`resep_print_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_nscc_resep_print_details` (`resep_print_id`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

ALTER TABLE `nscc_salestrans`
  ADD COLUMN `start_at` DATETIME NULL DEFAULT NULL
  AFTER `kmr_total`;

ALTER TABLE `nscc_salestrans`
  ADD COLUMN `end_at` DATETIME NULL DEFAULT NULL
  AFTER `start_at`;

ALTER TABLE `nscc_salestrans`
  ADD COLUMN `pending` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0
  AFTER `end_at`;

ALTER TABLE `nscc_salestrans`
  ADD COLUMN `end_estimate` DATETIME NULL DEFAULT NULL
  AFTER `pending`;

ALTER TABLE `nscc_salestrans`
  ADD COLUMN `konsul_id` VARCHAR(36)
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci NULL DEFAULT NULL
  AFTER `end_estimate`;

ALTER TABLE `nscc_salestrans`
  ADD COLUMN `total_durasi` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0
COMMENT 'durasi standart dalam second'
  AFTER `konsul_id`;

ALTER TABLE `nscc_salestrans`
  ADD COLUMN `add_durasi` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0
  AFTER `total_durasi`;

ALTER TABLE `nscc_salestrans`
  ADD COLUMN `add_jasa` VARCHAR(255)
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci NULL DEFAULT NULL
  AFTER `add_durasi`;

ALTER TABLE `nscc_salestrans`
  ADD COLUMN `counter` VARCHAR(5)
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci NULL DEFAULT NULL
  AFTER `add_jasa`;

ALTER TABLE `nscc_salestrans`
  ADD COLUMN `id_antrian` VARCHAR(36)
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci NULL DEFAULT NULL
  AFTER `counter`;

ALTER TABLE `nscc_salestrans`
  ADD COLUMN `beauty_id` VARCHAR(50)
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci NULL DEFAULT NULL
  AFTER `id_antrian`;

ALTER TABLE `nscc_salestrans`
  ADD COLUMN `stocker_id` VARCHAR(50)
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci NULL DEFAULT NULL
  AFTER `beauty_id`;

ALTER TABLE `nscc_salestrans`
  ADD COLUMN `apoteker_id` VARCHAR(50)
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci NULL DEFAULT NULL
  AFTER `stocker_id`;


ALTER TABLE `nscc_salestrans_details`
  DROP FOREIGN KEY `nscc_salestrans_details_ibfk_3`;

ALTER TABLE `nscc_salestrans_details`
  ADD COLUMN `durasi` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0
COMMENT 'durasi standart dalam second'
  AFTER `paket_details_id`;

ALTER TABLE `nscc_salestrans_details`
  ADD COLUMN `konsul_detil_id` VARCHAR(36)
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci NULL DEFAULT NULL
  AFTER `durasi`;

ALTER TABLE `nscc_status_cust`
  ADD COLUMN `masa_berlaku` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0
COMMENT 'dalam bulan'
  AFTER `nama_status`;

ALTER TABLE `nscc_status_cust`
  ADD COLUMN `kode_status` VARCHAR(5)
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci NOT NULL
  AFTER `up`;


ALTER TABLE `nscc_store`
  ADD COLUMN `persen_beban_acc` DECIMAL(10, 0) NULL DEFAULT NULL
  AFTER `conn`;


ALTER TABLE `nscc_trans_tipe`
  ENGINE = InnoDB,
  ROW_FORMAT = COMPACT;

ALTER TABLE `nscc_session`
  ENGINE = InnoDB,
  ROW_FORMAT = COMPACT;

CREATE TABLE `nscc_user_employee` (
  `user_employee_id` VARCHAR(36)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NOT NULL,
  `id`               VARCHAR(50)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NOT NULL,
  `employee_id`      VARCHAR(50)
                     CHARACTER SET utf8mb4
                     COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`user_employee_id`),
  CONSTRAINT `fk_nscc_user_employee` FOREIGN KEY (`id`) REFERENCES `nscc_users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_nscc_user_employee_0` FOREIGN KEY (`employee_id`) REFERENCES `nscc_employees` (`employee_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_nscc_user_employee` (`id`) USING BTREE,
  INDEX `idx_nscc_user_employee_0` (`employee_id`) USING BTREE
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = COMPACT;

DROP TABLE `nscc_absen_trans`;

DROP TABLE `nscc_audit`;

DROP TABLE `nscc_audit_details`;

DROP TABLE `nscc_beauty`;

DROP TABLE `nscc_dokter`;

DROP TABLE `nscc_employee`;

SET FOREIGN_KEY_CHECKS = 1;

