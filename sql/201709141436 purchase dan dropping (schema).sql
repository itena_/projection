
      /*
      * Script created by Quest Schema Compare at 14/09/2017 13:38:47.
      * Please back up your database before running this script.
      *
      * Synchronizing objects from posng to posng.
      */
    USE `posng`;

-- Drop foreign keys, referenced to nscc_supplier
ALTER TABLE `posng`.`nscc_pelunasan_utang` DROP FOREIGN KEY `nscc_pelunasan_utang_ibfk_2`;

-- Drop foreign keys, referenced to nscc_supplier
ALTER TABLE `posng`.`nscc_transfer_barang_asset` DROP FOREIGN KEY `nscc_transfer_asset_ibfk_1`;

-- Drop foreign keys, referenced to nscc_supplier
ALTER TABLE `posng`.`nscc_transfer_barang_perlengkapan` DROP FOREIGN KEY `FK_nscc_transfer_barang_perlengkapan_nscc_supplier`;

-- Drop foreign keys, referenced to nscc_transfer_item
ALTER TABLE `posng`.`nscc_pelunasan_utang_detil` DROP FOREIGN KEY `nscc_pelunasan_utang_detil_ibfk_2`;

ALTER TABLE `posng`.`nscc_dropping_details` DROP FOREIGN KEY `fk_nscc_dropping_details`;

ALTER TABLE `posng`.`nscc_order_dropping_details` DROP FOREIGN KEY `fk_nscc_order_dropping_details`;

ALTER TABLE `posng`.`nscc_po` DROP FOREIGN KEY `fk_nscc_po`;

ALTER TABLE `posng`.`nscc_po_details` DROP FOREIGN KEY `fk_nscc_po_details`;

ALTER TABLE `posng`.`nscc_receive_dropping` DROP FOREIGN KEY `fk_nscc_receive_dropping`;

ALTER TABLE `posng`.`nscc_receive_dropping_details` DROP FOREIGN KEY `fk_nscc_dropping_details_0`;

ALTER TABLE `posng`.`nscc_supplier` DROP FOREIGN KEY `nscc_supplier_ibfk_1`;

ALTER TABLE `posng`.`nscc_terima_barang` DROP FOREIGN KEY `fk_nscc_terima_barang`;

ALTER TABLE `posng`.`nscc_terima_barang_details` DROP FOREIGN KEY `fk_nscc_terima_barang_details`;

ALTER TABLE `posng`.`nscc_transfer_item` DROP FOREIGN KEY `fk_nscc_transfer_item_0`;

ALTER TABLE `posng`.`nscc_transfer_item` DROP FOREIGN KEY `nscc_transfer_item_ibfk_1`;

ALTER TABLE `posng`.`nscc_transfer_item_details` DROP FOREIGN KEY `nscc_transfer_item_details_ibfk_1`;

ALTER TABLE `posng`.`nscc_transfer_item_details` DROP FOREIGN KEY `nscc_transfer_item_details_ibfk_2`;

/* Header line. Object: nscc_dropping. Script date: 14/09/2017 13:38:47. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_dropping`;

CREATE TABLE `posng`.`_temp_nscc_dropping` (
	`dropping_id` varchar(50) NOT NULL,
	`tgl` date NOT NULL,
	`doc_ref` varchar(50) NOT NULL,
	`note` mediumtext default NULL,
	`tdate` datetime NOT NULL,
	`user_id` varchar(50) NOT NULL,
	`type_` tinyint(3) unsigned NOT NULL default '0',
	`store` varchar(20) NOT NULL,
	`order_dropping_id` varchar(50) default NULL,
	`store_penerima` varchar(20) NOT NULL,
	`approved` tinyint(3) NOT NULL default '0',
	`approved_user_id` varchar(50) default NULL,
	`approved_date` datetime default NULL,
	`lunas` tinyint(3) unsigned NOT NULL default '0',
	`up` tinyint(3) unsigned NOT NULL default '0',
	KEY `fk_nscc_transfer_barang_nscc_store_0` ( `store` ),
	KEY `idx_nscc_dropping` ( `order_dropping_id` ),
	PRIMARY KEY  ( `dropping_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
;

INSERT INTO `posng`.`_temp_nscc_dropping`
( `doc_ref`, `dropping_id`, `lunas`, `note`, `order_dropping_id`, `store`, `tdate`, `tgl`, `type_`, `up`, `user_id` )
SELECT
`doc_ref`, `dropping_id`, `lunas`, `note`, `order_dropping_id`, `store`, `tdate`, `tgl`, `type_`, `up`, `user_id`
FROM `posng`.`nscc_dropping`;

DROP TABLE `posng`.`nscc_dropping`;

ALTER TABLE `posng`.`_temp_nscc_dropping` RENAME `nscc_dropping`;

/* Header line. Object: nscc_dropping_details. Script date: 14/09/2017 13:38:47. */
ALTER TABLE `posng`.`nscc_dropping_details` DROP FOREIGN KEY `fk_nscc_dropping_details`;

/* Header line. Object: nscc_order_dropping. Script date: 14/09/2017 13:38:47. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_order_dropping`;

CREATE TABLE `posng`.`_temp_nscc_order_dropping` (
	`order_dropping_id` varchar(50) NOT NULL,
	`tgl` date NOT NULL,
	`doc_ref` varchar(50) default NULL,
	`note` mediumtext default NULL,
	`tdate` datetime NOT NULL,
	`user_id` varchar(50) NOT NULL,
	`type_` tinyint(3) unsigned NOT NULL default '0',
	`store` varchar(20) NOT NULL,
	`store_pengirim` varchar(20) default NULL,
	`approved` tinyint(3) NOT NULL default '0',
	`approved_user_id` varchar(50) default NULL,
	`approved_date` datetime default NULL,
	`lunas` tinyint(3) NOT NULL default '-1',
	`up` tinyint(3) unsigned NOT NULL default '0',
	KEY `fk_nscc_transfer_barang_nscc_store_1` ( `store` ),
	KEY `nscc_order_dropping_fk1_idx` ( `store_pengirim` ),
	PRIMARY KEY  ( `order_dropping_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
;

INSERT INTO `posng`.`_temp_nscc_order_dropping`
( `doc_ref`, `lunas`, `note`, `order_dropping_id`, `store`, `tdate`, `tgl`, `type_`, `up`, `user_id` )
SELECT
`doc_ref`, `lunas`, `note`, `order_dropping_id`, `store`, `tdate`, `tgl`, `type_`, `up`, `user_id`
FROM `posng`.`nscc_order_dropping`;

DROP TABLE `posng`.`nscc_order_dropping`;

ALTER TABLE `posng`.`_temp_nscc_order_dropping` RENAME `nscc_order_dropping`;

/* Header line. Object: nscc_order_dropping_details. Script date: 14/09/2017 13:38:47. */
ALTER TABLE `posng`.`nscc_order_dropping_details` DROP FOREIGN KEY `fk_nscc_order_dropping_details`;

/* Header line. Object: nscc_po. Script date: 14/09/2017 13:38:47. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_po`;

CREATE TABLE `posng`.`_temp_nscc_po` (
	`po_id` varchar(36) NOT NULL,
	`doc_ref` varchar(50) NOT NULL,
	`tgl` date NOT NULL,
	`tgl_delivery` date NOT NULL,
	`note` varchar(600) default NULL,
	`acc_charge` varchar(100) default NULL,
	`pr_id` varchar(36) default NULL,
	`doc_ref_pr` varchar(50) default NULL,
	`divisi` varchar(100) default NULL,
	`store` varchar(20) NOT NULL,
	`status` smallint(6) NOT NULL,
	`tgl_status_change` datetime default NULL,
	`id_user` varchar(36) NOT NULL,
	`tdate` datetime NOT NULL,
	`nama` varchar(100) NOT NULL,
	`ship_to_address` varchar(100) NOT NULL,
	`ship_to_company` varchar(100) NOT NULL,
	`ship_to_city` varchar(100) default NULL,
	`ship_to_country` varchar(100) default NULL,
	`ship_to_phone` varchar(100) NOT NULL,
	`ship_to_fax` varchar(100) NOT NULL,
	`supplier_id` varchar(36) NOT NULL,
	`supp_nama` varchar(100) default NULL,
	`supp_address` varchar(100) default NULL,
	`supp_company` varchar(100) default NULL,
	`supp_city` varchar(100) default NULL,
	`supp_country` varchar(100) default NULL,
	`supp_phone` varchar(100) default NULL,
	`supp_fax` varchar(100) default NULL,
	`supp_email` varchar(100) default NULL,
	`termofpayment` int(11) NOT NULL default '0',
	`nama_rek` varchar(100) NOT NULL,
	`no_rek` varchar(100) NOT NULL,
	`bank_rek` varchar(100) NOT NULL,
	`sub_total` decimal(30,2) NOT NULL,
	`disc` decimal(5,2) NOT NULL default '0.00',
	`total_disc_rp` decimal(30,2) NOT NULL,
	`total_dpp` decimal(30,2) NOT NULL default '0.00',
	`ppn` char(1) NOT NULL default 'N',
	`tax_rp` decimal(30,2) NOT NULL default '0.00',
	`total_pph_rp` decimal(30,2) NOT NULL default '0.00',
	`total` decimal(30,2) NOT NULL,
	`up` tinyint(4) NOT NULL default '0',
	`received` tinyint(4) NOT NULL default '0',
	`closed_id_user` varchar(36) default NULL,
	`closed_tdate` datetime default NULL,
	`type_` tinyint(4) NOT NULL default '1',
	KEY `idx_nscc_po` ( `supplier_id` ),
	KEY `nscc_po_fk1_idx` ( `pr_id` ),
	PRIMARY KEY  ( `po_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
;

INSERT INTO `posng`.`_temp_nscc_po`
( `acc_charge`, `bank_rek`, `disc`, `divisi`, `doc_ref`, `doc_ref_pr`, `id_user`, `nama`, `nama_rek`, `no_rek`, `note`, `po_id`, `ppn`, `received`, `ship_to_address`, `ship_to_city`, `ship_to_company`, `ship_to_country`, `ship_to_fax`, `ship_to_phone`, `status`, `store`, `sub_total`, `supp_address`, `supp_city`, `supp_company`, `supp_country`, `supp_email`, `supp_fax`, `supp_nama`, `supp_phone`, `supplier_id`, `tax_rp`, `tdate`, `termofpayment`, `tgl`, `tgl_delivery`, `tgl_status_change`, `total`, `total_disc_rp`, `total_dpp`, `total_pph_rp`, `up` )
SELECT
`acc_charge`, `bank_rek`, `disc`, `divisi`, `doc_ref`, `doc_ref_pr`, `id_user`, `nama`, `nama_rek`, `no_rek`, `note`, `po_id`, `ppn`, `received`, `ship_to_address`, `ship_to_city`, `ship_to_company`, `ship_to_country`, `ship_to_fax`, `ship_to_phone`, `status`, `store`, `sub_total`, `supp_address`, `supp_city`, `supp_company`, `supp_country`, `supp_email`, `supp_fax`, `supp_nama`, `supp_phone`, `supplier_id`, `tax_rp`, `tdate`, `termofpayment`, `tgl`, `tgl_delivery`, `tgl_status_change`, `total`, `total_disc_rp`, `total_dpp`, `total_pph_rp`, `up`
FROM `posng`.`nscc_po`;

DROP TABLE `posng`.`nscc_po`;

ALTER TABLE `posng`.`_temp_nscc_po` RENAME `nscc_po`;

/* Header line. Object: nscc_po_details. Script date: 14/09/2017 13:38:47. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_po_details`;

CREATE TABLE `posng`.`_temp_nscc_po_details` (
	`po_details_id` varchar(36) NOT NULL,
	`po_id` varchar(36) NOT NULL,
	`seq` int(11) NOT NULL default '0',
	`description` varchar(100) default NULL,
	`barang_id` varchar(36) NOT NULL,
	`qty` double NOT NULL,
	`price` decimal(30,2) NOT NULL,
	`sub_total` decimal(30,2) NOT NULL,
	`disc` decimal(5,2) NOT NULL default '0.00',
	`disc_rp` decimal(30,2) NOT NULL,
	`total_disc` decimal(30,2) NOT NULL default '0.00',
	`total_dpp` decimal(30,2) NOT NULL default '0.00',
	`ppn` decimal(5,2) NOT NULL default '0.00',
	`ppn_rp` decimal(30,2) NOT NULL default '0.00',
	`pph` decimal(5,2) NOT NULL default '0.00',
	`pph_rp` decimal(30,2) NOT NULL default '0.00',
	`total` decimal(30,2) NOT NULL,
	`charge` varchar(100) default NULL,
	`visible` tinyint(4) NOT NULL default '1',
	KEY `idx_nscc_po_details` ( `po_id` ),
	PRIMARY KEY  ( `po_details_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
;

INSERT INTO `posng`.`_temp_nscc_po_details`
( `barang_id`, `charge`, `description`, `disc`, `disc_rp`, `po_details_id`, `po_id`, `pph`, `pph_rp`, `ppn`, `ppn_rp`, `price`, `qty`, `seq`, `sub_total`, `total`, `total_disc`, `total_dpp`, `visible` )
SELECT
`barang_id`, `charge`, `description`, `disc`, `disc_rp`, `po_details_id`, `po_id`, `pph`, `pph_rp`, `ppn`, `ppn_rp`, `price`, `qty`, `seq`, `sub_total`, `total`, `total_disc`, `total_dpp`, `visible`
FROM `posng`.`nscc_po_details`;

DROP TABLE `posng`.`nscc_po_details`;

ALTER TABLE `posng`.`_temp_nscc_po_details` RENAME `nscc_po_details`;

/* Header line. Object: nscc_pr. Script date: 14/09/2017 13:38:47. */
CREATE TABLE `posng`.`nscc_pr` (
	`pr_id` varchar(36) NOT NULL,
	`doc_ref` varchar(45) default NULL,
	`pemohon` varchar(45) NOT NULL,
	`divisi` varchar(45) NOT NULL,
	`acc_charge` varchar(100) NOT NULL,
	`tgl` date NOT NULL,
	`tgl_kirim` date NOT NULL,
	`total` double default '0',
	`note` varchar(45) default NULL,
	`status` varchar(45) NOT NULL default '-1',
	`tdate` datetime NOT NULL,
	`store` varchar(20) NOT NULL,
	`user_id` varchar(36) NOT NULL,
	`up` tinyint(1) NOT NULL default '0',
	PRIMARY KEY  ( `pr_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
;

/* Header line. Object: nscc_receive_dropping. Script date: 14/09/2017 13:38:47. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_receive_dropping`;

CREATE TABLE `posng`.`_temp_nscc_receive_dropping` (
	`receive_dropping_id` varchar(50) NOT NULL,
	`tgl` date NOT NULL,
	`doc_ref` varchar(50) NOT NULL,
	`note` mediumtext default NULL,
	`tdate` date NOT NULL,
	`user_id` varchar(50) NOT NULL,
	`type_` tinyint(3) unsigned NOT NULL default '0',
	`store` varchar(20) NOT NULL,
	`dropping_id` varchar(50) NOT NULL,
	`store_pengirim` varchar(20) NOT NULL,
	`lunas` tinyint(3) unsigned NOT NULL default '0',
	`up` tinyint(3) unsigned NOT NULL default '0',
	KEY `fk_nscc_transfer_barang_nscc_store_2` ( `store` ),
	KEY `idx_nscc_receive_dropping` ( `dropping_id` ),
	PRIMARY KEY  ( `receive_dropping_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
;

INSERT INTO `posng`.`_temp_nscc_receive_dropping`
( `doc_ref`, `dropping_id`, `lunas`, `note`, `receive_dropping_id`, `store`, `tdate`, `tgl`, `type_`, `up`, `user_id` )
SELECT
`doc_ref`, `dropping_id`, `lunas`, `note`, `receive_dropping_id`, `store`, `tdate`, `tgl`, `type_`, `up`, `user_id`
FROM `posng`.`nscc_receive_dropping`;

DROP TABLE `posng`.`nscc_receive_dropping`;

ALTER TABLE `posng`.`_temp_nscc_receive_dropping` RENAME `nscc_receive_dropping`;

/* Header line. Object: nscc_receive_dropping_details. Script date: 14/09/2017 13:38:47. */
ALTER TABLE `posng`.`nscc_receive_dropping_details` DROP FOREIGN KEY `fk_nscc_dropping_details_0`;

/* Header line. Object: nscc_supplier. Script date: 14/09/2017 13:38:47. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_supplier`;

CREATE TABLE `posng`.`_temp_nscc_supplier` (
	`supplier_id` varchar(36) NOT NULL,
	`supplier_name` varchar(100) NOT NULL,
	`account_code` varchar(15) NOT NULL default '0',
	`up` tinyint(3) unsigned NOT NULL default '0',
	`supp_address` varchar(100) default NULL,
	`supp_company` varchar(100) default NULL,
	`supp_phone` varchar(100) default NULL,
	`supp_fax` varchar(100) default NULL,
	`supp_city` varchar(100) default NULL,
	`supp_country` varchar(100) default NULL,
	`supp_email` varchar(100) default NULL,
	`bank_rek` varchar(100) default NULL,
	`no_rek` varchar(100) default NULL,
	`nama_rek` varchar(100) default NULL,
	`termofpayment` int(11) default '0',
	`name_cp` varchar(100) default NULL,
	KEY `idx_nscc_supplier` ( `account_code` ),
	PRIMARY KEY  ( `supplier_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
;

INSERT INTO `posng`.`_temp_nscc_supplier`
( `account_code`, `bank_rek`, `nama_rek`, `name_cp`, `no_rek`, `supp_address`, `supp_city`, `supp_company`, `supp_country`, `supp_email`, `supp_fax`, `supp_phone`, `supplier_id`, `supplier_name`, `termofpayment`, `up` )
SELECT
`account_code`, `bank_rek`, `nama_rek`, `name_cp`, `no_rek`, `supp_address`, `supp_city`, `supp_company`, `supp_country`, `supp_email`, `supp_fax`, `supp_phone`, `supplier_id`, `supplier_name`, `termofpayment`, `up`
FROM `posng`.`nscc_supplier`;

DROP TABLE `posng`.`nscc_supplier`;

ALTER TABLE `posng`.`_temp_nscc_supplier` RENAME `nscc_supplier`;

/* Header line. Object: nscc_terima_barang. Script date: 14/09/2017 13:38:47. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_terima_barang`;

CREATE TABLE `posng`.`_temp_nscc_terima_barang` (
	`terima_barang_id` varchar(36) NOT NULL,
	`po_id` varchar(36) NOT NULL,
	`doc_ref` varchar(50) NOT NULL,
	`id_user` varchar(36) NOT NULL,
	`tdate` datetime NOT NULL,
	`tgl` date NOT NULL,
	`store` varchar(20) NOT NULL,
	`no_sj` varchar(45) default NULL,
	`tgl_sj` date default NULL,
	`lock_edit` tinyint(1) default '0',
	`status` tinyint(1) NOT NULL default '-1',
	`up` tinyint(4) NOT NULL default '0',
	KEY `idx_nscc_terima_barang` ( `po_id` ),
	PRIMARY KEY  ( `terima_barang_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
;

INSERT INTO `posng`.`_temp_nscc_terima_barang`
( `doc_ref`, `id_user`, `po_id`, `store`, `tdate`, `terima_barang_id`, `tgl`, `up` )
SELECT
`doc_ref`, `id_user`, `po_id`, `store`, `tdate`, `terima_barang_id`, `tgl`, `up`
FROM `posng`.`nscc_terima_barang`;

DROP TABLE `posng`.`nscc_terima_barang`;

ALTER TABLE `posng`.`_temp_nscc_terima_barang` RENAME `nscc_terima_barang`;

/* Header line. Object: nscc_terima_barang_details. Script date: 14/09/2017 13:38:47. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_terima_barang_details`;

CREATE TABLE `posng`.`_temp_nscc_terima_barang_details` (
	`terima_barang_details_id` varchar(36) NOT NULL,
	`terima_barang_id` varchar(36) NOT NULL,
	`seq` int(11) NOT NULL default '0',
	`description` varchar(100) default NULL,
	`barang_id` varchar(36) NOT NULL,
	`qty` double NOT NULL,
	`visible` tinyint(4) NOT NULL default '1',
	`stock_moves_id` varchar(36) NOT NULL,
	KEY `idx_nscc_terima_barang_details` ( `terima_barang_id` ),
	PRIMARY KEY  ( `terima_barang_details_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
;

INSERT INTO `posng`.`_temp_nscc_terima_barang_details`
( `barang_id`, `description`, `qty`, `seq`, `stock_moves_id`, `terima_barang_details_id`, `terima_barang_id`, `visible` )
SELECT
`barang_id`, `description`, `qty`, `seq`, `stock_moves_id`, `terima_barang_details_id`, `terima_barang_id`, `visible`
FROM `posng`.`nscc_terima_barang_details`;

DROP TABLE `posng`.`nscc_terima_barang_details`;

ALTER TABLE `posng`.`_temp_nscc_terima_barang_details` RENAME `nscc_terima_barang_details`;

/* Header line. Object: nscc_transfer_item. Script date: 14/09/2017 13:38:47. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_transfer_item`;

CREATE TABLE `posng`.`_temp_nscc_transfer_item` (
	`transfer_item_id` varchar(50) NOT NULL,
	`tgl` date NOT NULL,
	`doc_ref` varchar(50) NOT NULL,
	`note` mediumtext default NULL,
	`tdate` datetime NOT NULL,
	`doc_ref_other` varchar(50) default NULL,
	`user_id` varchar(50) NOT NULL,
	`type_` tinyint(4) NOT NULL default '0',
	`store` varchar(20) NOT NULL,
	`supplier_id` varchar(36) NOT NULL,
	`tgl_jatuh_tempo` date default NULL,
	`terima_barang_id` varchar(36) NOT NULL,
	`no_fakturpajak` varchar(50) default NULL,
	`sub_total` decimal(30,2) NOT NULL,
	`disc` decimal(5,2) NOT NULL default '0.00',
	`total_disc_rp` decimal(30,2) NOT NULL,
	`total_dpp` decimal(30,2) NOT NULL default '0.00',
	`tax_rp` decimal(30,2) NOT NULL default '0.00',
	`total_pph_rp` decimal(30,2) NOT NULL default '0.00',
	`total` decimal(30,2) NOT NULL,
	`closed_id_user` varchar(36) default NULL,
	`closed_tdate` datetime default NULL,
	`status` tinyint(3) unsigned NOT NULL default '0',
	`up` tinyint(3) unsigned NOT NULL default '0',
	KEY `idx_nscc_transfer_item` ( `supplier_id` ),
	KEY `idx_nscc_transfer_item_0` ( `terima_barang_id` ),
	PRIMARY KEY  ( `transfer_item_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
;

INSERT INTO `posng`.`_temp_nscc_transfer_item`
( `disc`, `doc_ref`, `doc_ref_other`, `note`, `store`, `supplier_id`, `tdate`, `terima_barang_id`, `tgl`, `tgl_jatuh_tempo`, `total`, `transfer_item_id`, `type_`, `up`, `user_id` )
SELECT
`disc`, `doc_ref`, `doc_ref_other`, `note`, `store`, `supplier_id`, `tdate`, `terima_barang_id`, `tgl`, `tgl_jatuh_tempo`, `total`, `transfer_item_id`, `type_`, `up`, `user_id`
FROM `posng`.`nscc_transfer_item`;

DROP TABLE `posng`.`nscc_transfer_item`;

ALTER TABLE `posng`.`_temp_nscc_transfer_item` RENAME `nscc_transfer_item`;

/* Header line. Object: nscc_transfer_item_details. Script date: 14/09/2017 13:38:47. */
DROP TABLE IF EXISTS `posng`.`_temp_nscc_transfer_item_details`;

CREATE TABLE `posng`.`_temp_nscc_transfer_item_details` (
	`transfer_item_details_id` varchar(36) NOT NULL,
	`qty` int(11) NOT NULL default '0',
	`barang_id` varchar(36) NOT NULL,
	`transfer_item_id` varchar(50) NOT NULL,
	`price` decimal(30,2) NOT NULL default '0.00',
	`sub_total` decimal(30,2) NOT NULL default '0.00',
	`disc` decimal(5,2) NOT NULL default '0.00',
	`disc_rp` decimal(30,2) NOT NULL default '0.00',
	`total_disc` decimal(30,2) default NULL,
	`total_dpp` decimal(30,2) default NULL,
	`ppn` decimal(5,2) NOT NULL default '0.00',
	`ppn_rp` decimal(30,2) NOT NULL default '0.00',
	`pph` decimal(5,2) default NULL,
	`pph_rp` decimal(30,2) default NULL,
	`total` decimal(30,2) NOT NULL default '0.00',
	`charge` varchar(100) default NULL,
	`visible` tinyint(1) default '1',
	KEY `fk_nscc_transfer_item_details` ( `barang_id` ),
	KEY `idx_nscc_transfer_item_details_0` ( `transfer_item_id` ),
	PRIMARY KEY  ( `transfer_item_details_id` )
)
ENGINE = InnoDB
CHARACTER SET = utf8mb4
ROW_FORMAT = Compact
;

INSERT INTO `posng`.`_temp_nscc_transfer_item_details`
( `barang_id`, `disc`, `price`, `qty`, `total`, `transfer_item_details_id`, `transfer_item_id` )
SELECT
`barang_id`, `disc`, `price`, `qty`, `total`, `transfer_item_details_id`, `transfer_item_id`
FROM `posng`.`nscc_transfer_item_details`;

DROP TABLE `posng`.`nscc_transfer_item_details`;

ALTER TABLE `posng`.`_temp_nscc_transfer_item_details` RENAME `nscc_transfer_item_details`;

/* Header line. Object: nscc_dropping_approval. Script date: 14/09/2017 13:38:47. */
CREATE VIEW posng.`nscc_dropping_approval` AS 
select 1 AS `jenis`,`d`.`dropping_id` AS `id_dropping_id`,NULL AS `id_order_dropping_id`,`d`.`tgl` AS `tgl`,`d`.`doc_ref` AS `doc_ref`,`d`.`note` AS `note`,`d`.`tdate` AS `tdate`,`d`.`user_id` AS `user_id`,`d`.`type_` AS `type_`,`d`.`store` AS `store`,`d`.`order_dropping_id` AS `order_dropping_id`,`d`.`store_penerima` AS `store_penerima`,NULL AS `store_pengirim`,`d`.`approved` AS `approved`,`d`.`approved_user_id` AS `approved_user_id`,`d`.`approved_date` AS `approved_date`,`d`.`lunas` AS `lunas`,`d`.`up` AS `up` from posng.`nscc_dropping` `d` union select 0 AS `jenis`,NULL AS `id_dropping_id`,`od`.`order_dropping_id` AS `id_order_dropping_id`,`od`.`tgl` AS `tgl`,`od`.`doc_ref` AS `doc_ref`,`od`.`note` AS `note`,`od`.`tdate` AS `tdate`,`od`.`user_id` AS `user_id`,`od`.`type_` AS `type_`,`od`.`store` AS `store`,NULL AS `order_dropping_id`,NULL AS `store_penerima`,`od`.`store_pengirim` AS `store_pengirim`,`od`.`approved` AS `approved`,`od`.`approved_user_id` AS `approved_user_id`,`od`.`approved_date` AS `approved_date`,`od`.`lunas` AS `lunas`,`od`.`up` AS `up` from posng.`nscc_order_dropping` `od` where (`od`.`lunas` <> -(1));

/* Header line. Object: nscc_dropping_detail_view. Script date: 14/09/2017 13:38:47. */
CREATE VIEW posng.`nscc_dropping_detail_view` AS 
select `dd`.`barang_id` AS `barang_id`,sum(`dd`.`qty`) AS `qty`,`d`.`order_dropping_id` AS `order_dropping_id` from (posng.`nscc_dropping_details` `dd` left join posng.`nscc_dropping` `d` on((`dd`.`dropping_id` = `d`.`dropping_id`))) where (`dd`.`visible` = 1) group by `dd`.`barang_id`,`d`.`order_dropping_id`;

/* Header line. Object: nscc_dropping_sisa. Script date: 14/09/2017 13:38:47. */
ALTER VIEW `posng`.`nscc_dropping_sisa` AS 
select (`dd`.`qty` - coalesce(sum(`rdd`.`qty`),0)) AS `qty`,`dd`.`barang_id` AS `barang_id`,`dd`.`dropping_id` AS `dropping_id` from ((`posng`.`nscc_dropping_details` `dd` left join `posng`.`nscc_receive_dropping` `rd` on(((`rd`.`dropping_id` = `dd`.`dropping_id`) and (`dd`.`visible` = 1)))) left join `posng`.`nscc_receive_dropping_details` `rdd` on(((`rdd`.`receive_dropping_id` = `rd`.`receive_dropping_id`) and (`rdd`.`barang_id` = `dd`.`barang_id`) and (`rdd`.`visible` = 1) and (`dd`.`visible` = 1)))) group by `dd`.`barang_id`,`dd`.`dropping_id` having (`qty` > 0);

/* Header line. Object: nscc_order_dropping_sisa. Script date: 14/09/2017 13:38:47. */
ALTER VIEW `posng`.`nscc_order_dropping_sisa` AS 
select (`odd`.`qty` - ifnull(`t`.`qty`,0)) AS `qty`,`odd`.`barang_id` AS `barang_id`,`odd`.`order_dropping_id` AS `order_dropping_id` from (`posng`.`nscc_order_dropping_details` `odd` left join `posng`.`nscc_dropping_detail_view` `t` on(((`t`.`order_dropping_id` = `odd`.`order_dropping_id`) and (`t`.`barang_id` = `odd`.`barang_id`)))) where (`odd`.`visible` = 1) group by `odd`.`barang_id`,`odd`.`order_dropping_id` having (`qty` > 0);

/* Header line. Object: nscc_po_sisa_terima. Script date: 14/09/2017 13:38:47. */
ALTER VIEW `posng`.`nscc_po_sisa_terima` AS 
select `npd`.`po_details_id` AS `po_details_id`,`npd`.`po_id` AS `po_id`,`npd`.`seq` AS `seq`,`npd`.`barang_id` AS `barang_id`,(`npd`.`qty` - coalesce(sum(`ntbd`.`qty`),0)) AS `qty`,`npd`.`price` AS `price`,`npd`.`total` AS `total`,`npd`.`disc_rp` AS `disc_rp`,`npd`.`sub_total` AS `sub_total`,`npd`.`ppn` AS `ppn`,`npd`.`ppn_rp` AS `ppn_rp`,`npd`.`pph` AS `pph`,`npd`.`pph_rp` AS `pph_rp`,`npd`.`disc` AS `disc`,`npd`.`total_disc` AS `total_disc`,`npd`.`total_dpp` AS `total_dpp`,`npd`.`charge` AS `charge`,`npd`.`visible` AS `visible` from ((`posng`.`nscc_po_details` `npd` left join `posng`.`nscc_terima_barang` `ntb` on(((`npd`.`po_id` = `ntb`.`po_id`) and (`npd`.`visible` = 1)))) left join `posng`.`nscc_terima_barang_details` `ntbd` on(((`ntbd`.`terima_barang_id` = `ntb`.`terima_barang_id`) and (`ntbd`.`barang_id` = `npd`.`barang_id`) and (`ntbd`.`visible` = 1) and (`npd`.`visible` = 1)))) where (`npd`.`visible` = 1) group by `npd`.`barang_id`,`npd`.`po_id` having (`qty` > 0);

-- Restore foreign keys, referenced to nscc_supplier
ALTER TABLE `posng`.`nscc_pelunasan_utang` ADD CONSTRAINT `nscc_pelunasan_utang_ibfk_2`
	FOREIGN KEY ( `supplier_id` ) REFERENCES `nscc_supplier` ( `supplier_id` ) ON UPDATE CASCADE;

-- Restore foreign keys, referenced to nscc_supplier
ALTER TABLE `posng`.`nscc_transfer_barang_asset` ADD CONSTRAINT `nscc_transfer_asset_ibfk_1`
	FOREIGN KEY ( `supplier_id` ) REFERENCES `nscc_supplier` ( `supplier_id` ) ON UPDATE CASCADE;

-- Restore foreign keys, referenced to nscc_supplier
ALTER TABLE `posng`.`nscc_transfer_barang_perlengkapan` ADD CONSTRAINT `FK_nscc_transfer_barang_perlengkapan_nscc_supplier`
	FOREIGN KEY ( `supplier_id` ) REFERENCES `nscc_supplier` ( `supplier_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Restore foreign keys, referenced to nscc_transfer_item
ALTER TABLE `posng`.`nscc_pelunasan_utang_detil` ADD CONSTRAINT `nscc_pelunasan_utang_detil_ibfk_2`
	FOREIGN KEY ( `transfer_item_id` ) REFERENCES `nscc_transfer_item` ( `transfer_item_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_dropping_details
ALTER TABLE `posng`.`nscc_dropping_details` ADD CONSTRAINT `fk_nscc_dropping_details`
	FOREIGN KEY ( `dropping_id` ) REFERENCES `nscc_dropping` ( `dropping_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_order_dropping
ALTER TABLE `posng`.`nscc_order_dropping` ADD CONSTRAINT `nscc_order_dropping_fk0`
	FOREIGN KEY ( `store` ) REFERENCES `nscc_store` ( `store_kode` ) ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_order_dropping` ADD CONSTRAINT `nscc_order_dropping_fk1`
	FOREIGN KEY ( `store_pengirim` ) REFERENCES `nscc_store` ( `store_kode` ) ON UPDATE CASCADE;

-- Update foreign keys of nscc_order_dropping_details
ALTER TABLE `posng`.`nscc_order_dropping_details` ADD CONSTRAINT `fk_nscc_order_dropping_details`
	FOREIGN KEY ( `order_dropping_id` ) REFERENCES `nscc_order_dropping` ( `order_dropping_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_po
ALTER TABLE `posng`.`nscc_po` ADD CONSTRAINT `fk_nscc_po`
	FOREIGN KEY ( `supplier_id` ) REFERENCES `nscc_supplier` ( `supplier_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_po` ADD CONSTRAINT `nscc_po_fk1`
	FOREIGN KEY ( `pr_id` ) REFERENCES `nscc_pr` ( `pr_id` ) ON DELETE SET NULL ON UPDATE CASCADE;

-- Update foreign keys of nscc_po_details
ALTER TABLE `posng`.`nscc_po_details` ADD CONSTRAINT `fk_nscc_po_details`
	FOREIGN KEY ( `po_id` ) REFERENCES `nscc_po` ( `po_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_receive_dropping
ALTER TABLE `posng`.`nscc_receive_dropping` ADD CONSTRAINT `fk_nscc_receive_dropping`
	FOREIGN KEY ( `dropping_id` ) REFERENCES `nscc_dropping` ( `dropping_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_receive_dropping_details
ALTER TABLE `posng`.`nscc_receive_dropping_details` ADD CONSTRAINT `fk_nscc_dropping_details_0`
	FOREIGN KEY ( `receive_dropping_id` ) REFERENCES `nscc_receive_dropping` ( `receive_dropping_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_supplier
ALTER TABLE `posng`.`nscc_supplier` ADD CONSTRAINT `nscc_supplier_ibfk_1`
	FOREIGN KEY ( `account_code` ) REFERENCES `nscc_chart_master` ( `account_code` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_terima_barang
ALTER TABLE `posng`.`nscc_terima_barang` ADD CONSTRAINT `fk_nscc_terima_barang`
	FOREIGN KEY ( `po_id` ) REFERENCES `nscc_po` ( `po_id` ) ON UPDATE CASCADE;

-- Update foreign keys of nscc_terima_barang_details
ALTER TABLE `posng`.`nscc_terima_barang_details` ADD CONSTRAINT `fk_nscc_terima_barang_details`
	FOREIGN KEY ( `terima_barang_id` ) REFERENCES `nscc_terima_barang` ( `terima_barang_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

-- Update foreign keys of nscc_transfer_item
ALTER TABLE `posng`.`nscc_transfer_item` ADD CONSTRAINT `fk_nscc_transfer_item_0`
	FOREIGN KEY ( `terima_barang_id` ) REFERENCES `nscc_terima_barang` ( `terima_barang_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_transfer_item` ADD CONSTRAINT `nscc_transfer_item_ibfk_1`
	FOREIGN KEY ( `supplier_id` ) REFERENCES `nscc_supplier` ( `supplier_id` ) ON UPDATE CASCADE;

-- Update foreign keys of nscc_transfer_item_details
ALTER TABLE `posng`.`nscc_transfer_item_details` ADD CONSTRAINT `nscc_transfer_item_details_ibfk_1`
	FOREIGN KEY ( `barang_id` ) REFERENCES `nscc_barang` ( `barang_id` ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `posng`.`nscc_transfer_item_details` ADD CONSTRAINT `nscc_transfer_item_details_ibfk_2`
	FOREIGN KEY ( `transfer_item_id` ) REFERENCES `nscc_transfer_item` ( `transfer_item_id` ) ON DELETE CASCADE ON UPDATE CASCADE;
