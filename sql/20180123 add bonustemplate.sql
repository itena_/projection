CREATE TABLE `nscc_bonus_template` (
`bonus_template_id`  varchar(50) NOT NULL ,
`grup_id`  varchar(36) NOT NULL ,
`nama_grup`  varchar(20) NOT NULL ,
`bonus_name_id`  varchar(36) NOT NULL ,
`persen_bonus`  decimal(10,2) NOT NULL DEFAULT 0.00 ,
PRIMARY KEY (`bonus_template_id`),
UNIQUE INDEX `grupbonusname` (`grup_id`, `bonus_name_id`) USING BTREE 
)
ENGINE=InnoDB
ROW_FORMAT=Compact
;

/*
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f311b46c-ff3e-11e7-bde8-507b9d297990', '1', 'MEDICAL CREAM', '1', '0.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f31f242c-ff3e-11e7-bde8-507b9d297990', '1', 'MEDICAL CREAM', '2', '0.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3280132-ff3e-11e7-bde8-507b9d297990', '1', 'MEDICAL CREAM', '3', '0.40');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3361918-ff3e-11e7-bde8-507b9d297990', '1', 'MEDICAL CREAM', '4', '0.20');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3458c04-ff3e-11e7-bde8-507b9d297990', '1', 'MEDICAL CREAM', '5', '0.30');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3535497-ff3e-11e7-bde8-507b9d297990', '1', 'MEDICAL CREAM', '6', '0.50');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f360c8ee-ff3e-11e7-bde8-507b9d297990', '1', 'MEDICAL CREAM', '7', '2.75');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f371727d-ff3e-11e7-bde8-507b9d297990', '1', 'MEDICAL CREAM', '8', '1.50');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f38100a7-ff3e-11e7-bde8-507b9d297990', '1', 'MEDICAL CREAM', '9', '0.75');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f38fa350-ff3e-11e7-bde8-507b9d297990', '3', 'TREATMENT DOCTOR', '1', '0.15');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3aa6c2e-ff3e-11e7-bde8-507b9d297990', '3', 'TREATMENT DOCTOR', '2', '0.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3b3ee96-ff3e-11e7-bde8-507b9d297990', '3', 'TREATMENT DOCTOR', '3', '2.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3bdb00e-ff3e-11e7-bde8-507b9d297990', '3', 'TREATMENT DOCTOR', '4', '0.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3c64925-ff3e-11e7-bde8-507b9d297990', '3', 'TREATMENT DOCTOR', '5', '0.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3d05ecf-ff3e-11e7-bde8-507b9d297990', '3', 'TREATMENT DOCTOR', '6', '0.15');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3da29ce-ff3e-11e7-bde8-507b9d297990', '3', 'TREATMENT DOCTOR', '7', '0.25');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3e37486-ff3e-11e7-bde8-507b9d297990', '3', 'TREATMENT DOCTOR', '8', '2.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3eb8a20-ff3e-11e7-bde8-507b9d297990', '3', 'TREATMENT DOCTOR', '9', '1.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3f3d5d0-ff3e-11e7-bde8-507b9d297990', '5', 'COSMETIC', '1', '0.75');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f3fa6e51-ff3e-11e7-bde8-507b9d297990', '5', 'COSMETIC', '2', '1.50');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f4018d69-ff3e-11e7-bde8-507b9d297990', '5', 'COSMETIC', '3', '2.75');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f410e07f-ff3e-11e7-bde8-507b9d297990', '5', 'COSMETIC', '4', '0.50');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f41af1d0-ff3e-11e7-bde8-507b9d297990', '5', 'COSMETIC', '5', '0.30');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f42666f9-ff3e-11e7-bde8-507b9d297990', '5', 'COSMETIC', '6', '0.20');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f42d3b98-ff3e-11e7-bde8-507b9d297990', '5', 'COSMETIC', '7', '0.40');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f43418ad-ff3e-11e7-bde8-507b9d297990', '5', 'COSMETIC', '8', '0.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f43e5c1b-ff3e-11e7-bde8-507b9d297990', '5', 'COSMETIC', '9', '0.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f446b8c1-ff3e-11e7-bde8-507b9d297990', '6', 'TREATMENT SKIN CARE', '1', '0.25');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f45cab1e-ff3e-11e7-bde8-507b9d297990', '6', 'TREATMENT SKIN CARE', '2', '0.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f4652e32-ff3e-11e7-bde8-507b9d297990', '6', 'TREATMENT SKIN CARE', '3', '2.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f46dd700-ff3e-11e7-bde8-507b9d297990', '6', 'TREATMENT SKIN CARE', '4', '0.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f4762df4-ff3e-11e7-bde8-507b9d297990', '6', 'TREATMENT SKIN CARE', '5', '0.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f47eac19-ff3e-11e7-bde8-507b9d297990', '6', 'TREATMENT SKIN CARE', '6', '0.15');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f486fcd6-ff3e-11e7-bde8-507b9d297990', '6', 'TREATMENT SKIN CARE', '7', '0.25');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f48def59-ff3e-11e7-bde8-507b9d297990', '6', 'TREATMENT SKIN CARE', '8', '0.00');
INSERT INTO `adwis`.`nscc_bonus_template` (`bonus_template_id`, `grup_id`, `nama_grup`, `bonus_name_id`, `persen_bonus`) VALUES ('f4964293-ff3e-11e7-bde8-507b9d297990', '6', 'TREATMENT SKIN CARE', '9', '4.00');
	
*/