﻿-- 
-- Script was generated by Devart dbForge Studio for MySQL, Version 7.2.78.0
-- Product Home Page: http://www.devart.com/dbforge/mysql/studio
-- Script date 28/10/2017 14:28:23
-- Source server version: 5.6.16
-- Source connection string: User Id=root;Host=localhost;Character Set=utf8
-- Target server version: 10.1.23
-- Target connection string: User Id=root;Host=10.0.0.113;Database=posng;Protocol=Ssh;Character Set=utf8;SSH Host=172.169.222.151;SSH Port=22;SSH User=it;SSH Authentication Type=Password
-- Run this script against posng to synchronize it with posng
-- Please backup your target database before running this script
-- 

--
-- Disable foreign keys
--
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

SET NAMES 'utf8';

#USE posng;


--
-- Alter table "nscc_terima_barang"
--
ALTER TABLE nscc_terima_barang
  ADD COLUMN note VARCHAR(100) DEFAULT NULL AFTER tgl_sj;

--
-- Create view "nscc_terima_barang_view"
--
CREATE
VIEW nscc_terima_barang_view
AS
SELECT
  `tb`.`terima_barang_id` AS `terima_barang_id`,
  `tb`.`po_id` AS `po_id`,
  `tb`.`doc_ref` AS `doc_ref`,
  `tb`.`id_user` AS `id_user`,
  `tb`.`tdate` AS `tdate`,
  `tb`.`tgl` AS `tgl`,
  `tb`.`store` AS `store`,
  `tb`.`no_sj` AS `no_sj`,
  `tb`.`tgl_sj` AS `tgl_sj`,
  `tb`.`note` AS `note`,
  `tb`.`lock_edit` AS `lock_edit`,
  `tb`.`status` AS `status`,
  `tb`.`up` AS `up`,
  `po`.`doc_ref` AS `doc_ref_po`
FROM (`nscc_terima_barang` `tb`
  LEFT JOIN `nscc_po` `po`
    ON ((`tb`.`po_id` = `po`.`po_id`)));

--
-- Create view "nscc_transfer_item_view"
--
CREATE
VIEW nscc_transfer_item_view
AS
SELECT
  `ti`.`transfer_item_id` AS `transfer_item_id`,
  `ti`.`tgl` AS `tgl`,
  `ti`.`doc_ref` AS `doc_ref`,
  `ti`.`note` AS `note`,
  `ti`.`tdate` AS `tdate`,
  `ti`.`doc_ref_other` AS `doc_ref_other`,
  `ti`.`user_id` AS `user_id`,
  `ti`.`type_` AS `type_`,
  `ti`.`store` AS `store`,
  `ti`.`supplier_id` AS `supplier_id`,
  `ti`.`tgl_jatuh_tempo` AS `tgl_jatuh_tempo`,
  `ti`.`terima_barang_id` AS `terima_barang_id`,
  `ti`.`no_fakturpajak` AS `no_fakturpajak`,
  `ti`.`sub_total` AS `sub_total`,
  `ti`.`disc` AS `disc`,
  `ti`.`total_disc_rp` AS `total_disc_rp`,
  `ti`.`total_dpp` AS `total_dpp`,
  `ti`.`tax_rp` AS `tax_rp`,
  `ti`.`total_pph_rp` AS `total_pph_rp`,
  `ti`.`total` AS `total`,
  `ti`.`closed_id_user` AS `closed_id_user`,
  `ti`.`closed_tdate` AS `closed_tdate`,
  `ti`.`status` AS `status`,
  `ti`.`up` AS `up`,
  `tb`.`no_sj` AS `no_sj`,
  `po`.`doc_ref` AS `doc_ref_po`
FROM ((`nscc_transfer_item` `ti`
  LEFT JOIN `nscc_terima_barang` `tb`
    ON ((`tb`.`terima_barang_id` = `ti`.`terima_barang_id`)))
  LEFT JOIN `nscc_po` `po`
    ON ((`tb`.`po_id` = `po`.`po_id`)));

--
-- Enable foreign keys
--
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;