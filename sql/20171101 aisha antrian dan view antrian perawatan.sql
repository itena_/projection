/*
*	Add field sales di aisha_antrian
*/
ALTER TABLE `aisha_antrian` ADD COLUMN `sales`  tinyint(4) NOT NULL DEFAULT 1 AFTER `end_`;

/*
*	Add where di view
*/
ALTER 
ALGORITHM=UNDEFINED 
DEFINER=`rCpp9uvJpztw`@`%` 
SQL SECURITY DEFINER 
VIEW `nscc_antrian_perawatan` AS 
SELECT
	`aa`.`id_antrian` AS `id_antrian`,
	`aa`.`nomor_pasien` AS `nomor_pasien`,
	`aa`.`customer_id` AS `customer_id`,
	`aa`.`alasan` AS `alasan`,
	`aa`.`nomor_antrian` AS `nomor_antrian`,
	`aa`.`hold` AS `hold`,
	`aa`.`bagian` AS `bagian`,
	`aa`.`counter` AS `counter`,
	`aa`.`tanggal` AS `tanggal`,
	`aa`.`timestamp` AS `timestamp`,
	`aa`.`medis` AS `medis`,
	`aa`.`end_` AS `end_`,
	`aa`.`sales` AS `sales`,
	`nbs`.`salestrans_id` AS `salestrans_id`,
	`nbs`.`doc_ref` AS `doc_ref`,
	`nbs`.`tgl` AS `tgl`,
	`nbs`.`barang_id` AS `barang_id`,
	`nbs`.`qty` AS `qty`,
	`nbs`.`jasa_dokter` AS `jasa_dokter`,
	`nbs`.`dokter_id` AS `dokter_id`,
	`nbs`.`kode_barang` AS `kode_barang`,
	`nbs`.`nama_barang` AS `nama_barang`,
	`nbs`.`nama_customer` AS `nama_customer`,
	`nbs`.`no_customer` AS `no_customer`,
	`nbs`.`tdate` AS `tdate`,
	`nbs`.`end_at` AS `end_at`,
	`nbs`.`kategori_id` AS `kategori_id`
FROM
	(
		`aisha_antrian` `aa`
		LEFT JOIN `nscc_beauty_service_view` `nbs` ON (
			(
				(
					`nbs`.`id_antrian` = `aa`.`id_antrian`
				)
				AND (
					isnull(`nbs`.`kategori_id`)
					OR (`nbs`.`kategori_id` = 1)
				)
			)
		)
	)
		WHERE aa.sales != -1 ;